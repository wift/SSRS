using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BenchmarkDotNet.Attributes;
using Fluid;
using Fluid.Values;
using Microsoft.AspNetCore.Html;
using SSRSCore.DisplayManagement;
using SSRSCore.DisplayManagement.Descriptors;
using SSRSCore.DisplayManagement.Implementation;
using SSRSCore.DisplayManagement.Liquid;
using SSRSCore.DisplayManagement.Liquid.Filters;
using SSRSCore.Environment.Extensions;
using SSRSCore.Environment.Extensions.Features;
using SSRSCore.Environment.Extensions.Manifests;
using SSRSCore.Modules.Manifest;

namespace SSRSCore.Benchmark
{
    [MemoryDiagnoser]
    public class ShapeFactoryBenchmark
    {
        private static readonly FilterArguments _filterArguments = new FilterArguments().Add("utc", DateTime.UtcNow).Add("format", "MMMM dd, yyyy");
        private static readonly FluidValue input = FluidValue.Create("DateTime");
        private static readonly TemplateContext _templateContext;

        static ShapeFactoryBenchmark()
        {
            _templateContext = new TemplateContext();
            var defaultShapeTable = new TestShapeTable
            {
                Bindings = new Dictionary<string, ShapeBinding>(),
                Descriptors = new Dictionary<string, ShapeDescriptor>()
            };
            var shapeFactory = new DefaultShapeFactory(
                events: Enumerable.Empty<IShapeFactoryEvents>(),
                shapeTableManager: new TestShapeTableManager(defaultShapeTable),
                themeManager: new MockThemeManager(new ExtensionInfo("path", new ManifestInfo(new ModuleAttribute()), (x, y) => Enumerable.Empty<IFeatureInfo>())));

            _templateContext.AmbientValues["DisplayHelper"] = new DisplayHelper(null, shapeFactory, null);
        }

        [Benchmark(Baseline = true)]
        public async Task OriginalShapeRender()
        {
            await ShapeRenderOriginal(input, _filterArguments, _templateContext);
        }

        [Benchmark]
        public async Task NewShapeRender()
        {
            await LiquidViewFilters.ShapeRender(input, _filterArguments, _templateContext);
        }

        private static async ValueTask<FluidValue> ShapeRenderOriginal(FluidValue input, FilterArguments arguments, TemplateContext context)
        {
            if (!context.AmbientValues.TryGetValue("DisplayHelper", out dynamic displayHelper))
            {
                throw new ArgumentException("DisplayHelper missing while invoking 'shape_render'");
            }

            if (input.ToObjectValue() is IShape shape)
            {
                return new HtmlContentValue(await (Task<IHtmlContent>)displayHelper(shape));
            }

            return NilValue.Instance;
        }
    }
}