using SSRSCore.DisplayManagement.Manifest;

[assembly: Theme(
    Name = "SSRSCore.Templates.Theme",
    Author = "The Orchard Team",
    Website = "https://orchardproject.net",
    Version = "0.0.1",
    Description = "SSRSCore.Templates.Theme"
)]
