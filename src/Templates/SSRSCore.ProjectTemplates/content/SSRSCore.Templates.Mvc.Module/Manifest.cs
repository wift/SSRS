using SSRSCore.Modules.Manifest;

[assembly: Module(
    Name = "SSRSCore.Templates.Mvc.Module",
    Author = "The Orchard Team",
    Website = "https://orchardproject.net",
    Version = "0.0.1",
    Description = "SSRSCore.Templates.Mvc.Module",
    Category = "SSRSCore.Templates.Mvc.Module"
)]
