﻿using SSRSCore.ContentManagement.Handlers;
using System.Threading.Tasks;
using SSRSCore.Templates.Cms.Module.Models;

namespace SSRSCore.Templates.Cms.Module.Handlers
{
    public class MyTestPartHandler : ContentPartHandler<MyTestPart>
    {
        public override Task InitializingAsync(InitializingContentContext context, MyTestPart part)
        {
            part.Show = true;

            return Task.CompletedTask;
        }
    }
}