using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
#if (AddPart)
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.Display.ContentDisplay;
using SSRSCore.ContentManagement.Handlers;
using SSRSCore.ContentTypes.Editors;
using SSRSCore.Data.Migration;
using SSRSCore.Templates.Cms.Module.Drivers;
using SSRSCore.Templates.Cms.Module.Handlers;
using SSRSCore.Templates.Cms.Module.Models;
using SSRSCore.Templates.Cms.Module.Settings;
#endif
using SSRSCore.Modules;

namespace SSRSCore.Templates.Cms.Module
{
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
#if (AddPart)
            services.AddContentPart<MyTestPart>();
            services.AddScoped<IContentPartDisplayDriver, MyTestPartDisplayDriver>();
            services.AddScoped<IContentPartDefinitionDisplayDriver, MyTestPartSettingsDisplayDriver>();
            services.AddScoped<IDataMigration, Migrations>();
            services.AddScoped<IContentPartHandler, MyTestPartHandler>();
#endif
        }

        public override void Configure(IApplicationBuilder builder, IEndpointRouteBuilder routes, IServiceProvider serviceProvider)
        {
            routes.MapAreaControllerRoute(
                name: "Home",
                areaName: "SSRSCore.Templates.Cms.Module",
                pattern: "Home/Index",
                defaults: new { controller = "Home", action = "Index" }
            );
        }
    }
}