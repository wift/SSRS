﻿using SSRSCore.ContentManagement;

namespace SSRSCore.Templates.Cms.Module.Models
{
    public class MyTestPart : ContentPart
    {
        public bool Show { get; set; }
    }
}
