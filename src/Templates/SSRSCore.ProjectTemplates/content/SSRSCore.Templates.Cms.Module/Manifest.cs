using SSRSCore.Modules.Manifest;

[assembly: Module(
    Name = "SSRSCore.Templates.Cms.Module",
    Author = "The Orchard Team",
    Website = "https://orchardproject.net",
    Version = "0.0.1",
    Description = "SSRSCore.Templates.Cms.Module",
#if (AddPart)
    Dependencies = new[] { "SSRSCore.Contents" },
#endif
    Category = "Content Management"
)]
