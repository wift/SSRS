using Microsoft.Extensions.DependencyInjection;
using SSRSCore.BackgroundTasks.Services;
using SSRSCore.Modules;
using SSRSCore.Navigation;
using SSRSCore.Security.Permissions;

namespace SSRSCore.BackgroundTasks
{
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services
                .AddScoped<BackgroundTaskManager>()
                .AddScoped<IPermissionProvider, Permissions>()
                .AddScoped<INavigationProvider, AdminMenu>()
                .AddScoped<IBackgroundTaskSettingsProvider, BackgroundTaskSettingsProvider>();
        }
    }
}
