using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using SSRSCore.Microsoft.Authentication.Settings;

namespace SSRSCore.Microsoft.Authentication.Services
{
    public interface IMicrosoftAccountService
    {
        Task<MicrosoftAccountSettings> GetSettingsAsync();
        Task UpdateSettingsAsync(MicrosoftAccountSettings settings);
        IEnumerable<ValidationResult> ValidateSettings(MicrosoftAccountSettings settings);
    }
}
