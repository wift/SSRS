using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using SSRSCore.Microsoft.Authentication.Settings;

namespace SSRSCore.Microsoft.Authentication.Services
{
    public interface IAzureADService
    {
        Task<AzureADSettings> GetSettingsAsync();
        Task UpdateSettingsAsync(AzureADSettings settings);
        IEnumerable<ValidationResult> ValidateSettings(AzureADSettings settings);
    }
}
