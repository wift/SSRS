using System;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using SSRSCore.Microsoft.Authentication.Services;
using SSRSCore.Microsoft.Authentication.Settings;
using SSRSCore.Recipes.Models;
using SSRSCore.Recipes.Services;

namespace SSRSCore.Microsoft.Authentication.Recipes
{
    /// <summary>
    /// This recipe step sets general OpenID Connect Client settings.
    /// </summary>
    public class AzureADSettingsStep : IRecipeStepHandler
    {
        private readonly IAzureADService _azureADService;

        public AzureADSettingsStep(IAzureADService azureADService)
        {
            _azureADService = azureADService;
        }

        public async Task ExecuteAsync(RecipeExecutionContext context)
        {
            if (!string.Equals(context.Name, nameof(AzureADSettings), StringComparison.OrdinalIgnoreCase))
            {
                return;
            }

            var model = context.Step.ToObject<AzureADSettingsStepModel>();

            var settings = await _azureADService.GetSettingsAsync();
            settings.AppId = model.AppId;
            settings.TenantId = model.TenantId;
            settings.DisplayName = model.DisplayName;
            settings.CallbackPath = model.CallbackPath;
            await _azureADService.UpdateSettingsAsync(settings);
        }
    }

    public class AzureADSettingsStepModel
    {
        public string DisplayName { get; set; }
        public string AppId { get; set; }
        public string TenantId { get; set; }
        public string CallbackPath { get; set; }
    }
}