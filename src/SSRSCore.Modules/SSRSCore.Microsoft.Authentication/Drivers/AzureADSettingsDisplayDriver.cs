using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Http;
using SSRSCore.DisplayManagement.Entities;
using SSRSCore.DisplayManagement.Handlers;
using SSRSCore.DisplayManagement.Notify;
using SSRSCore.DisplayManagement.Views;
using SSRSCore.Environment.Shell;
using SSRSCore.Microsoft.Authentication.Services;
using SSRSCore.Microsoft.Authentication.Settings;
using SSRSCore.Facebook.ViewModels;
using SSRSCore.Settings;
using SSRSCore.Microsoft.Authentication.ViewModels;
using System;

namespace SSRSCore.Microsoft.Authentication.Drivers
{
    public class AzureADSettingsDisplayDriver : SectionDisplayDriver<ISite, AzureADSettings>
    {
        private readonly IAuthorizationService _authorizationService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly INotifier _notifier;
        private readonly IAzureADService _clientService;
        private readonly IShellHost _shellHost;
        private readonly ShellSettings _shellSettings;

        public AzureADSettingsDisplayDriver(
            IAuthorizationService authorizationService,
            IDataProtectionProvider dataProtectionProvider,
            IAzureADService clientService,
            IHttpContextAccessor httpContextAccessor,
            INotifier notifier,
            IShellHost shellHost,
            ShellSettings shellSettings)
        {
            _authorizationService = authorizationService;
            _clientService = clientService;
            _httpContextAccessor = httpContextAccessor;
            _notifier = notifier;
            _shellHost = shellHost;
            _shellSettings = shellSettings;
        }

        public override async Task<IDisplayResult> EditAsync(AzureADSettings settings, BuildEditorContext context)
        {
            var user = _httpContextAccessor.HttpContext?.User;
            if (user == null || !await _authorizationService.AuthorizeAsync(user, Permissions.ManageMicrosoftAuthentication))
            {
                return null;
            }
            return Initialize<AzureADSettingsViewModel>("AzureADSettings_Edit", model =>
            {
                model.DisplayName = settings.DisplayName;
                model.AppId = settings.AppId;
                model.TenantId = settings.TenantId;
                if (settings.CallbackPath.HasValue)
                {
                    model.CallbackPath = settings.CallbackPath;
                }
            }).Location("Content:0").OnGroup(MicrosoftAuthenticationConstants.Features.AAD);
        }

        public override async Task<IDisplayResult> UpdateAsync(AzureADSettings settings, BuildEditorContext context)
        {
            if (context.GroupId == MicrosoftAuthenticationConstants.Features.AAD)
            {
                var user = _httpContextAccessor.HttpContext?.User;
                if (user == null || !await _authorizationService.AuthorizeAsync(user, Permissions.ManageMicrosoftAuthentication))
                {
                    return null;
                }
                var model = new AzureADSettingsViewModel();
                await context.Updater.TryUpdateModelAsync(model, Prefix);
                if (context.Updater.ModelState.IsValid)
                {
                    settings.DisplayName = model.DisplayName;
                    settings.AppId = model.AppId;
                    settings.TenantId = model.TenantId;
                    settings.CallbackPath = model.CallbackPath;
                    await _shellHost.ReloadShellContextAsync(_shellSettings);
                }
            }
            return await EditAsync(settings, context);
        }
    }
}
