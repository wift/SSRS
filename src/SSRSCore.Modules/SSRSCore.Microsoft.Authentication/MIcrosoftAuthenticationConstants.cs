namespace SSRSCore.Microsoft.Authentication
{
    public static class MicrosoftAuthenticationConstants
    {
        public static class Features
        {
            public const string MicrosoftAccount = "SSRSCore.Microsoft.Authentication.MicrosoftAccount";
            public const string AAD = "SSRSCore.Microsoft.Authentication.AzureAD";
        }
    }
}
