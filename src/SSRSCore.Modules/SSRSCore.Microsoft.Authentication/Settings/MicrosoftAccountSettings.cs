using Microsoft.AspNetCore.Http;

namespace SSRSCore.Microsoft.Authentication.Settings
{
    public class MicrosoftAccountSettings
    {
        public string AppId { get; set; }
        public string AppSecret { get; set; }
        public PathString CallbackPath { get; set; }
    }
}
