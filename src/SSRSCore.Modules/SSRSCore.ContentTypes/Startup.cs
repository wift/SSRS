using System;
using Microsoft.AspNetCore.Builder;
using SSRSCore.Modules;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using SSRSCore.ContentTypes.Deployment;
using SSRSCore.ContentTypes.Editors;
using SSRSCore.ContentTypes.RecipeSteps;
using SSRSCore.ContentTypes.Services;
using SSRSCore.Deployment;
using SSRSCore.DisplayManagement.Handlers;
using SSRSCore.Navigation;
using SSRSCore.Recipes;
using SSRSCore.Security.Permissions;

namespace SSRSCore.ContentTypes
{
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IPermissionProvider, Permissions>();
            services.AddScoped<INavigationProvider, AdminMenu>();
            services.AddScoped<IContentDefinitionService, ContentDefinitionService>();
            services.AddScoped<IStereotypesProvider, DefaultStereotypesProvider>();
            services.AddScoped<IStereotypeService, StereotypeService>();
            services.AddScoped<IContentDefinitionDisplayHandler, ContentDefinitionDisplayCoordinator>();
            services.AddScoped<IContentDefinitionDisplayManager, DefaultContentDefinitionDisplayManager>();
            services.AddScoped<IContentPartDefinitionDisplayDriver, ContentPartSettingsDisplayDriver>();
            services.AddScoped<IContentTypeDefinitionDisplayDriver, ContentTypeSettingsDisplayDriver>();
            services.AddScoped<IContentTypeDefinitionDisplayDriver, DefaultContentTypeDisplayDriver>();
            services.AddScoped<IContentTypePartDefinitionDisplayDriver, ContentTypePartSettingsDisplayDriver>();

            // TODO: Put in its own feature to be able to execute this recipe without having to enable
            // Content Types management UI
            services.AddRecipeExecutionStep<ContentDefinitionStep>();

        }

        public override void Configure(IApplicationBuilder builder, IEndpointRouteBuilder routes, IServiceProvider serviceProvider)
        {
            routes.MapAreaControllerRoute(
                name: "EditField",
                areaName: "SSRSCore.ContentTypes",
                pattern: "Admin/ContentParts/{id}/Fields/{name}/Edit",
                defaults: new { controller = "Admin", action = "EditField" }
            );

            routes.MapAreaControllerRoute(
                name: "EditTypePart",
                areaName: "SSRSCore.ContentTypes",
                pattern: "Admin/ContentTypes/{id}/ContentParts/{name}/Edit",
                defaults: new { controller = "Admin", action = "EditTypePart" }
            );

            routes.MapAreaControllerRoute(
                name: "RemovePart",
                areaName: "SSRSCore.ContentTypes",
                pattern: "Admin/ContentTypes/{id}/ContentParts/{name}/Remove",
                defaults: new { controller = "Admin", action = "RemovePart" }
            );
        }
    }


    [RequireFeatures("SSRSCore.Deployment")]
    public class DeploymentStartup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<IDeploymentSource, ContentDefinitionDeploymentSource>();
            services.AddSingleton<IDeploymentStepFactory>(new DeploymentStepFactory<ContentDefinitionDeploymentStep>());
            services.AddScoped<IDisplayDriver<DeploymentStep>, ContentDefinitionDeploymentStepDriver>();
        }
    }
}
