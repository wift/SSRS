using SSRSCore.ContentManagement.GraphQL.Options;
using SSRSCore.ContentManagement.GraphQL.Settings;
using SSRSCore.ContentManagement.Metadata.Models;

namespace SSRSCore.ContentTypes.GraphQL.ViewModels
{
    public class GraphQLContentTypePartSettingsViewModel
    {
        public GraphQLContentOptions Options { get; set; }
        public GraphQLContentTypePartSettings Settings { get; set; }
        public ContentTypePartDefinition Definition { get; set; }
    }
}
