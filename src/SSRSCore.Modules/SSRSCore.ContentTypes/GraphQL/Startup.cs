using Microsoft.Extensions.DependencyInjection;
using SSRSCore.ContentTypes.Editors;
using SSRSCore.ContentTypes.GraphQL.Drivers;
using SSRSCore.Modules;

namespace SSRSCore.ContentTypes.GraphQL
{
    [RequireFeatures("SSRSCore.Apis.GraphQL")]
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IContentTypePartDefinitionDisplayDriver, GraphQLContentTypePartSettingsDriver>();
        }
    }
}
