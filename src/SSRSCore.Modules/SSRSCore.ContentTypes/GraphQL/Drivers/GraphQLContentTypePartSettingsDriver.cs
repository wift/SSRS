using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using SSRSCore.ContentManagement.GraphQL.Options;
using SSRSCore.ContentManagement.GraphQL.Settings;
using SSRSCore.ContentManagement.Metadata.Models;
using SSRSCore.ContentTypes.Editors;
using SSRSCore.ContentTypes.GraphQL.ViewModels;
using SSRSCore.DisplayManagement.Views;

namespace SSRSCore.ContentTypes.GraphQL.Drivers
{
    public class GraphQLContentTypePartSettingsDriver : ContentTypePartDefinitionDisplayDriver
    {
        private readonly GraphQLContentOptions _contentOptions;

        public GraphQLContentTypePartSettingsDriver(IOptions<GraphQLContentOptions> optionsAccessor)
        {
            _contentOptions = optionsAccessor.Value;
        }

        public override IDisplayResult Edit(ContentTypePartDefinition contentTypePartDefinition)
        {
            if (contentTypePartDefinition.ContentTypeDefinition.Name == contentTypePartDefinition.PartDefinition.Name)
            {
                return null;
            }

            return Initialize<GraphQLContentTypePartSettingsViewModel>("GraphQLContentTypePartSettings_Edit", model =>
            {
                model.Definition = contentTypePartDefinition;
                model.Options = _contentOptions;
                model.Settings = contentTypePartDefinition.GetSettings<GraphQLContentTypePartSettings>();
            }).Location("Content");
        }

        public override async Task<IDisplayResult> UpdateAsync(ContentTypePartDefinition contentTypePartDefinition, UpdateTypePartEditorContext context)
        {
            if (contentTypePartDefinition.ContentTypeDefinition.Name == contentTypePartDefinition.PartDefinition.Name)
            {
                return null;
            }

            var model = new GraphQLContentTypePartSettingsViewModel();

            await context.Updater.TryUpdateModelAsync(model, Prefix);

            context.Builder.WithSettings(model.Settings);

            return Edit(contentTypePartDefinition, context.Updater);
        }
    }
}