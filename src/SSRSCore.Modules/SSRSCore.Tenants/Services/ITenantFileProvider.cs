using SSRSCore.Modules.FileProviders;

namespace SSRSCore.Tenants.Services
{
    public interface ITenantFileProvider : IStaticFileProvider
    {
    }
}
