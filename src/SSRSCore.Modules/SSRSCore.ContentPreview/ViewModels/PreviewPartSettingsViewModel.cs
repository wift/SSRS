using SSRSCore.ContentPreview.Models;

namespace SSRSCore.ContentPreview.ViewModels
{
    public class PreviewPartSettingsViewModel
    {
        public string Pattern { get; set; }
        public PreviewPartSettings PreviewPartSettings { get; set; }
    }
}
