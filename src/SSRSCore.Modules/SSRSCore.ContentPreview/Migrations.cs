using SSRSCore.ContentManagement.Metadata;
using SSRSCore.ContentManagement.Metadata.Settings;
using SSRSCore.Data.Migration;

namespace SSRSCore.ContentPreview
{
    public class Migrations : DataMigration
    {
        IContentDefinitionManager _contentDefinitionManager;

        public Migrations(IContentDefinitionManager contentDefinitionManager)
        {
            _contentDefinitionManager = contentDefinitionManager;
        }

        public int Create()
        {
            _contentDefinitionManager.AlterPartDefinition("PreviewPart", builder => builder
                .Attachable()
                .WithDescription("Provides a way to define the url that is used to render your content item."));

            return 1;
        }
    }
}
