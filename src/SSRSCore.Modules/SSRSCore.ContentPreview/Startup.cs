using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.Display.ContentDisplay;
using SSRSCore.ContentManagement.Handlers;
using SSRSCore.ContentPreview.Drivers;
using SSRSCore.ContentPreview.Handlers;
using SSRSCore.ContentPreview.Models;
using SSRSCore.ContentPreview.Settings;
using SSRSCore.ContentTypes.Editors;
using SSRSCore.Data.Migration;
using SSRSCore.Security.Permissions;

namespace SSRSCore.ContentPreview
{
    public class Startup : Modules.StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IContentDisplayDriver, ContentPreviewDriver>();
            services.AddScoped<IPermissionProvider, Permissions>();

            // Preview Part
            services.AddContentPart<PreviewPart>();
            services.AddScoped<IDataMigration, Migrations>();
            services.AddScoped<IContentPartHandler, PreviewPartHandler>();
            services.AddScoped<IContentTypePartDefinitionDisplayDriver, PreviewPartSettingsDisplayDriver>();
            services.TryAddEnumerable(ServiceDescriptor.Singleton<IStartupFilter, PreviewStartupFilter>());
        }
    }
}
