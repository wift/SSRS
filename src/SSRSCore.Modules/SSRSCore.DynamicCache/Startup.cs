using Microsoft.Extensions.DependencyInjection;
using SSRSCore.DisplayManagement.Descriptors;
using SSRSCore.DisplayManagement.Implementation;
using SSRSCore.DynamicCache.EventHandlers;
using SSRSCore.DynamicCache.Services;
using SSRSCore.DynamicCache.TagHelpers;
using SSRSCore.Environment.Cache;
using SSRSCore.Modules;

namespace SSRSCore.DynamicCache
{
    /// <summary>
    /// These services are registered on the tenant service collection
    /// </summary>
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            // Register the type as it implements multiple interfaces
            services.AddScoped<DefaultDynamicCacheService>();
            services.AddScoped<IDynamicCacheService>(sp => sp.GetRequiredService<DefaultDynamicCacheService>());
            services.AddScoped<ITagRemovedEventHandler>(sp => sp.GetRequiredService<DefaultDynamicCacheService>());

            services.AddScoped<IShapeDisplayEvents, DynamicCacheShapeDisplayEvents>();

            services.AddShapeAttributes<CachedShapeWrapperShapes>();
            
            services.AddSingleton<IDynamicCache, DefaultDynamicCache>();
            services.AddSingleton<DynamicCacheTagHelperService>();
            services.AddTagHelpers<DynamicCacheTagHelper>();
        }
    }
}
