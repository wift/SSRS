﻿using System.Threading.Tasks;
using SSRSCore.Environment.Extensions;

namespace SSRSCore.Themes.Services
{
    public interface ISiteThemeService
    {
        Task<IExtensionInfo> GetSiteThemeAsync();
        Task SetSiteThemeAsync(string themeName);
        Task<string> GetCurrentThemeNameAsync();
    }
}
