using SSRSCore.Modules;
using Microsoft.Extensions.DependencyInjection;
using SSRSCore.MetaWeblog;

namespace SSRSCore.Html.RemotePublishing
{
    [RequireFeatures("SSRSCore.RemotePublishing")]
    public class RemotePublishingStartup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IMetaWeblogDriver, HtmlBodyMetaWeblogDriver>();
        }
    }
}
