using SSRSCore.ContentManagement;

namespace SSRSCore.Html.Models
{
    public class HtmlBodyPart : ContentPart
    {
        public string Html { get; set; }
    }
}
