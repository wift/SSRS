using Fluid;
using Microsoft.Extensions.DependencyInjection;
using SSRSCore.Html.Drivers;
using SSRSCore.Html.Handlers;
using SSRSCore.Html.Indexing;
using SSRSCore.Html.Models;
using SSRSCore.Html.Settings;
using SSRSCore.Html.ViewModels;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.Display.ContentDisplay;
using SSRSCore.ContentManagement.Handlers;
using SSRSCore.ContentTypes.Editors;
using SSRSCore.Data.Migration;
using SSRSCore.Indexing;
using SSRSCore.Modules;

namespace SSRSCore.Html
{
    public class Startup : StartupBase
    {
        static Startup()
        {
            TemplateContext.GlobalMemberAccessStrategy.Register<HtmlBodyPartViewModel>();
        }

        public override void ConfigureServices(IServiceCollection services)
        {
            // Body Part
            services.AddContentPart<HtmlBodyPart>();
            services.AddScoped<IContentPartDisplayDriver, HtmlBodyPartDisplay>();
            services.AddScoped<IContentTypePartDefinitionDisplayDriver, HtmlBodyPartSettingsDisplayDriver>();
            services.AddScoped<IDataMigration, Migrations>();
            services.AddScoped<IContentPartIndexHandler, HtmlBodyPartIndexHandler>();
            services.AddScoped<IContentPartHandler, HtmlBodyPartHandler>();
        }
    }
}
