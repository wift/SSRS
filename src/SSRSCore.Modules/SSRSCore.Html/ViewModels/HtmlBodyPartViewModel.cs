using Microsoft.AspNetCore.Mvc.ModelBinding;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.Metadata.Models;
using SSRSCore.Html.Models;

namespace SSRSCore.Html.ViewModels
{
    public class HtmlBodyPartViewModel
    {
        public string Source { get; set; }
        public string Html { get; set; }

        [BindNever]
        public ContentItem ContentItem { get; set; } 

        [BindNever]
        public HtmlBodyPart HtmlBodyPart { get; set; }

        [BindNever]
        public ContentTypePartDefinition TypePartDefinition { get; set; }
    }
}
