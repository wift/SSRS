using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Html;
using SSRSCore.ContentManagement.Handlers;
using SSRSCore.ContentManagement.Metadata;
using SSRSCore.ContentManagement.Models;
using SSRSCore.Html.Models;
using SSRSCore.Html.Settings;

namespace SSRSCore.Html.Handlers
{
    public class HtmlBodyPartHandler : ContentPartHandler<HtmlBodyPart>
    {
        private readonly IContentDefinitionManager _contentDefinitionManager;

        public HtmlBodyPartHandler(IContentDefinitionManager contentDefinitionManager)
        {
            _contentDefinitionManager = contentDefinitionManager;
        }

        public override Task GetContentItemAspectAsync(ContentItemAspectContext context, HtmlBodyPart part)
        {
            return context.ForAsync<BodyAspect>(bodyAspect =>
            {
                var contentTypeDefinition = _contentDefinitionManager.GetTypeDefinition(part.ContentItem.ContentType);
                var contentTypePartDefinition = contentTypeDefinition.Parts.FirstOrDefault(p => p.PartDefinition.Name == nameof(HtmlBodyPart));
                var settings = contentTypePartDefinition.GetSettings<HtmlBodyPartSettings>();
                var body = part.Html;

                bodyAspect.Body = new HtmlString(body);
                return Task.CompletedTask;
            });
        }
    }
}
