using Microsoft.Extensions.DependencyInjection;
using SSRSCore.Apis;
using SSRSCore.Html.Models;
using SSRSCore.Modules;

namespace SSRSCore.Html.GraphQL
{
    [RequireFeatures("SSRSCore.Apis.GraphQL")]
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddObjectGraphType<HtmlBodyPart, HtmlBodyQueryObjectType>();
        }
    }
}
