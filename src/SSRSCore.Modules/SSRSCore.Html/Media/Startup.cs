using SSRSCore.Modules;
using Microsoft.Extensions.DependencyInjection;
using SSRSCore.DisplayManagement.Descriptors;

namespace SSRSCore.Html.Media
{
    [RequireFeatures("SSRSCore.Media")]
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IShapeTableProvider, MediaShapes>();
        }
    }
}
