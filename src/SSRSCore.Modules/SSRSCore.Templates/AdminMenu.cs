using Microsoft.Extensions.Localization;
using SSRSCore.Navigation;
using System;
using System.Threading.Tasks;

namespace SSRSCore.Templates
{
    public class AdminMenu : INavigationProvider
    {
        public AdminMenu(IStringLocalizer<AdminMenu> localizer)
        {
            T = localizer;
        }

        public IStringLocalizer T { get; set; }

        public Task BuildNavigationAsync(string name, NavigationBuilder builder)
        {
            if (!String.Equals(name, "admin", StringComparison.OrdinalIgnoreCase))
            {
                return Task.CompletedTask;
            }

            builder
                .Add(T["Configuration"], content => content
                    .Add(T["Templates"], "10", import => import
                        .Action("Index", "Template", new { area = "SSRSCore.Templates" })
                        .Permission(Permissions.ManageTemplates)
                        .LocalNav()
                    )
                );

            return Task.CompletedTask;
        }
    }
}
