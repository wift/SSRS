using SSRSCore.Deployment;
using SSRSCore.DisplayManagement.Handlers;
using SSRSCore.DisplayManagement.Views;

namespace SSRSCore.Templates.Deployment
{
    public class AllAdminTemplatesDeploymentStepDriver : DisplayDriver<DeploymentStep, AllAdminTemplatesDeploymentStep>
    {
        public override IDisplayResult Display(AllAdminTemplatesDeploymentStep step)
        {
            return
                Combine(
                    View("AllAdminTemplatesDeploymentStep_Summary", step).Location("Summary", "Content"),
                    View("AllAdminTemplatesDeploymentStep_Thumbnail", step).Location("Thumbnail", "Content")
                );
        }

        public override IDisplayResult Edit(AllAdminTemplatesDeploymentStep step)
        {
            return View("AllAdminTemplatesDeploymentStep_Edit", step).Location("Content");
        }
    }
}
