using SSRSCore.Deployment;

namespace SSRSCore.Templates.Deployment
{
    /// <summary>
    /// Adds templates to a <see cref="DeploymentPlanResult"/>. 
    /// </summary>
    public class AllTemplatesDeploymentStep : DeploymentStep
    {
        public AllTemplatesDeploymentStep()
        {
            Name = "AllTemplates";
        }
    }
}
