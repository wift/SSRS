using Microsoft.Extensions.Localization;
using SSRSCore.ContentManagement.Metadata.Models;
using SSRSCore.ContentManagement.Metadata.Settings;
using SSRSCore.ContentTypes.Editors;
using SSRSCore.DisplayManagement.Views;
using SSRSCore.Templates.ViewModels;

namespace SSRSCore.Templates.Settings
{
    public class TemplateContentTypeDefinitionDriver : ContentTypeDefinitionDisplayDriver
    {
        private readonly IStringLocalizer<TemplateContentTypeDefinitionDriver> S;

        public TemplateContentTypeDefinitionDriver(IStringLocalizer<TemplateContentTypeDefinitionDriver> localizer)
        {
            S = localizer;
        }

        public override IDisplayResult Edit(ContentTypeDefinition contentTypeDefinition)
        {
            return Initialize<ContentSettingsViewModel>("TemplateSettings", model =>
            {
                var stereotype = contentTypeDefinition.GetSettings<ContentTypeSettings>().Stereotype;

                if (string.IsNullOrWhiteSpace(stereotype))
                {
                    stereotype = "Content";
                }

                model.ContentSettingsEntries.Add(
                    new ContentSettingsEntry
                    {
                        Key = $"{stereotype}__{contentTypeDefinition.Name}",
                        Description = S["Template for a {0} content item in detail views", contentTypeDefinition.DisplayName]
                    });

                model.ContentSettingsEntries.Add(
                    new ContentSettingsEntry
                    {
                        Key = $"{stereotype}_Summary__{contentTypeDefinition.Name}",
                        Description = S["Template for a {0} content item in summary views", contentTypeDefinition.DisplayName]
                    });

            }).Location("Content");
        }
    }
}