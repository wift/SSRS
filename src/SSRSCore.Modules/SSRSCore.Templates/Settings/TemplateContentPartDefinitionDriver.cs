using Microsoft.Extensions.Localization;
using SSRSCore.ContentManagement.Metadata.Models;
using SSRSCore.ContentManagement.Metadata.Settings;
using SSRSCore.ContentTypes.Editors;
using SSRSCore.DisplayManagement.Views;
using SSRSCore.Templates.ViewModels;

namespace SSRSCore.Templates.Settings
{
    public class TemplateContentPartDefinitionDriver : ContentPartDefinitionDisplayDriver
    {
        private readonly IStringLocalizer<TemplateContentPartDefinitionDriver> S;

        public TemplateContentPartDefinitionDriver(IStringLocalizer<TemplateContentPartDefinitionDriver> localizer)
        {
            S = localizer;
        }

        public override IDisplayResult Edit(ContentPartDefinition contentPartDefinition)
        {
            return Initialize<ContentSettingsViewModel>("TemplateSettings", model =>
            {
                model.ContentSettingsEntries.Add(
                    new ContentSettingsEntry
                    {
                        Key = contentPartDefinition.Name,
                        Description = S["Template for a {0} part in detail views", contentPartDefinition.DisplayName()]
                    });

                model.ContentSettingsEntries.Add(
                    new ContentSettingsEntry
                    {
                        Key = $"{contentPartDefinition.Name}_Summary",
                        Description = S["Template for a {0} part in summary views", contentPartDefinition.DisplayName()]
                    });
            }).Location("Content");
        }
    }
}