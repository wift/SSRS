using SSRSCore.Modules.Manifest;

[assembly: Module(
    Name = "Templates",
    Author = "The Orchard Team",
    Website = "https://orchardproject.net",
    Version = "2.0.0"
)]

[assembly: Feature(
    Id = "SSRSCore.Templates",
    Name = "Templates",
    Description = "The Templates module provides a way to write custom shape templates from the admin.",
    Dependencies = new[] { "SSRSCore.Liquid" },
    Category = "Development"
)]

[assembly: Feature(
    Id = "SSRSCore.AdminTemplates",
    Name = "Admin Templates",
    Description = "The Admin Templates module provides a way to write custom admin shape templates.",
    Dependencies = new[] { "SSRSCore.Liquid" },
    Category = "Development"
)]
