using Microsoft.Extensions.DependencyInjection;
using SSRSCore.ContentTypes.Editors;
using SSRSCore.Deployment;
using SSRSCore.DisplayManagement;
using SSRSCore.DisplayManagement.Handlers;
using SSRSCore.Navigation;
using SSRSCore.Modules;
using SSRSCore.Recipes;
using SSRSCore.Security.Permissions;
using SSRSCore.Templates.Deployment;
using SSRSCore.Templates.Recipes;
using SSRSCore.Templates.Services;
using SSRSCore.Templates.Settings;

namespace SSRSCore.Templates
{
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IShapeBindingResolver, TemplatesShapeBindingResolver>();
            services.AddScoped<PreviewTemplatesProvider>();
            services.AddScoped<TemplatesManager>();
            services.AddScoped<IPermissionProvider, Permissions>();
            services.AddScoped<INavigationProvider, AdminMenu>();
            services.AddRecipeExecutionStep<TemplateStep>();

            // Template shortcuts in settings
            services.AddScoped<IContentPartDefinitionDisplayDriver, TemplateContentPartDefinitionDriver>();
            services.AddScoped<IContentTypeDefinitionDisplayDriver, TemplateContentTypeDefinitionDriver>();
            services.AddScoped<IContentTypePartDefinitionDisplayDriver, TemplateContentTypePartDefinitionDriver>();

            services.AddTransient<IDeploymentSource, AllTemplatesDeploymentSource>();
            services.AddSingleton<IDeploymentStepFactory>(new DeploymentStepFactory<AllTemplatesDeploymentStep>());
            services.AddScoped<IDisplayDriver<DeploymentStep>, AllTemplatesDeploymentStepDriver>();

            services.AddScoped<AdminTemplatesManager>();
            services.AddScoped<IPermissionProvider, AdminTemplatesPermissions>();
        }
    }


    [Feature("SSRSCore.AdminTemplates")]
    public class AdminTemplatesStartup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IShapeBindingResolver, AdminTemplatesShapeBindingResolver>();
            services.AddScoped<AdminPreviewTemplatesProvider>();
            services.AddScoped<INavigationProvider, AdminTemplatesAdminMenu>();
            services.AddRecipeExecutionStep<AdminTemplateStep>();

            services.AddTransient<IDeploymentSource, AllAdminTemplatesDeploymentSource>();
            services.AddSingleton<IDeploymentStepFactory>(new DeploymentStepFactory<AllAdminTemplatesDeploymentStep>());
            services.AddScoped<IDisplayDriver<DeploymentStep>, AllAdminTemplatesDeploymentStepDriver>();
        }
    }
}
