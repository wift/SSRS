﻿using SSRSCore.ContentManagement;
using SSRSCore.DisplayManagement.Shapes;

namespace SSRSCore.Demo.Models
{
    public class TestContentPartA : ContentPart
    {
        public ShapeMetadata Metadata { get; set; }
        public string Line { get; set; }
    }
}