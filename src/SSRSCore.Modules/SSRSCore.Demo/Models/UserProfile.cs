namespace SSRSCore.Demo.Models
{
    public class UserProfile
    {
        public int Age { get; set; }
        public string Name { get; set; }
    }
}
