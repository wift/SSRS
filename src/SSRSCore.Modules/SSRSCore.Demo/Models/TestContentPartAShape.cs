﻿using SSRSCore.ContentManagement;
using SSRSCore.DisplayManagement;
using SSRSCore.DisplayManagement.Shapes;

namespace SSRSCore.Demo.Models
{
    public class TestContentPartAShape : Shape
    {
        public string Line { get; set; }
    }
}