using SSRSCore.DisplayManagement.Views;

namespace SSRSCore.Demo.Models
{
    public class CustomViewModel : ShapeViewModel
    {
        public string Value { get; set; }
    }
}
