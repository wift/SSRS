﻿using SSRSCore.ContentManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSRSCore.Demo.Models
{
    public class TestContentField : ContentField
    {
        public string Text;
    }
}
