using Microsoft.AspNetCore.Razor.TagHelpers;
using SSRSCore.DisplayManagement;
using SSRSCore.DisplayManagement.TagHelpers;

namespace SSRSCore.Demo.TagHelpers
{
    [HtmlTargetElement("baz")]
	public class BazTagHelper : BaseShapeTagHelper
    {
		public BazTagHelper(IShapeFactory shapeFactory, IDisplayHelper displayHelper):
			base(shapeFactory, displayHelper)
		{
			Type = "Baz";
		}
    }
}
