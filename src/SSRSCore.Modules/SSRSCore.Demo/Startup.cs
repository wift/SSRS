using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using SSRSCore.BackgroundTasks;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.Display.ContentDisplay;
using SSRSCore.Data.Migration;
using SSRSCore.Demo.Commands;
using SSRSCore.Demo.ContentElementDisplays;
using SSRSCore.Demo.Drivers;
using SSRSCore.Demo.Models;
using SSRSCore.Demo.Services;
using SSRSCore.Demo.TagHelpers;
using SSRSCore.DisplayManagement.Descriptors;
using SSRSCore.DisplayManagement.Handlers;
using SSRSCore.Environment.Commands;
using SSRSCore.Modules;
using SSRSCore.Navigation;
using SSRSCore.Security.Permissions;
using SSRSCore.Users.Models;

namespace SSRSCore.Demo
{
    public class Startup : StartupBase
    {
        public override void Configure(IApplicationBuilder builder, IEndpointRouteBuilder routes, IServiceProvider serviceProvider)
        {
            routes.MapAreaControllerRoute(
                name: "Home",
                areaName: "SSRSCore.Demo",
                pattern: "Home/Index",
                defaults: new { controller = "Home", action = "Index" }
            );

            routes.MapAreaControllerRoute(
                name: "Display",
                areaName: "SSRSCore.Demo",
                pattern: "Home/Display/{contentItemId}",
                defaults: new { controller = "Home", action = "Display" }
            );

            routes.MapAreaControllerRoute(
                name: "Error",
                areaName: "SSRSCore.Demo",
                pattern: "Home/IndexError",
                defaults: new { controller = "Home", action = "IndexError" }
            );

            routes.MapAreaControllerRoute(
                name: "AdminDemo",
                areaName: "SSRSCore.Demo",
                pattern: "Admin/Demo/Index",
                defaults: new { controller = "Admin", action = "Index" }
            );

            builder.UseMiddleware<NonBlockingMiddleware>();
            builder.UseMiddleware<BlockingMiddleware>();
        }

        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<ITestDependency, ClassFoo>();
            services.AddScoped<ICommandHandler, DemoCommands>();
            services.AddSingleton<IBackgroundTask, TestBackgroundTask>();
            services.AddScoped<IShapeTableProvider, DemoShapeProvider>();
            services.AddShapeAttributes<DemoShapeProvider>();
            services.AddScoped<INavigationProvider, AdminMenu>();
            services.AddScoped<IContentDisplayDriver, TestContentElementDisplay>();
            services.AddScoped<IDataMigration, Migrations>();
            services.AddScoped<IPermissionProvider, Permissions>();
            services.AddContentPart<TestContentPartA>();

            services.AddScoped<IDisplayDriver<User>, UserProfileDisplayDriver>();

            services.Configure<RazorPagesOptions>(options =>
            {
                // Add a custom folder route
                options.Conventions.AddAreaFolderRoute("SSRSCore.Demo", "/", "Demo");

                // Add a custom page route
                options.Conventions.AddAreaPageRoute("SSRSCore.Demo", "/Hello", "Hello");

                // This declaration would define an home page
                //options.Conventions.AddAreaPageRoute("SSRSCore.Demo", "/Hello", "");
            });

            services.AddTagHelpers(typeof(BazTagHelper).Assembly);
        }
    }
}
