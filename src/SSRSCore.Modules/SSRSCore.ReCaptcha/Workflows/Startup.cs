using Microsoft.Extensions.DependencyInjection;
using SSRSCore.Modules;
using SSRSCore.Workflows.Helpers;

namespace SSRSCore.ReCaptcha.Workflows
{
    [RequireFeatures("SSRSCore.Workflows", "SSRSCore.ReCaptcha")]
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddActivity<ValidateReCaptchaTask, ValidateReCaptchaTaskDisplay>();
        }
    }
}
