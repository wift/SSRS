using System;
using System.Collections.Generic;
using System.Text;
using Fluid;
using Microsoft.Extensions.DependencyInjection;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.Display.ContentDisplay;
using SSRSCore.Data.Migration;
using SSRSCore.Modules;

namespace SSRSCore.ReCaptcha.Forms
{
    [RequireFeatures("SSRSCore.Forms", "SSRSCore.ReCaptcha")]
    public class Startup : StartupBase
    {
        static Startup()
        {
            TemplateContext.GlobalMemberAccessStrategy.Register<ReCaptchaPart>();
        }

        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IContentPartDisplayDriver, ReCaptchaPartDisplay>();
            services.AddContentPart<ReCaptchaPart>();

            services.AddScoped<IDataMigration, Migrations>();
        }
    }
}
