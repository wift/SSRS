using System;
using System.Threading.Tasks;
using SSRSCore.ContentManagement.Display.ContentDisplay;
using SSRSCore.ContentManagement.Display.Models;
using SSRSCore.DisplayManagement.Views;
using SSRSCore.Entities;
using SSRSCore.ReCaptcha.Configuration;
using SSRSCore.Settings;

namespace SSRSCore.ReCaptcha.Forms
{
    public class ReCaptchaPartDisplay : ContentPartDisplayDriver<ReCaptchaPart>
    {
        private readonly ISiteService _siteService;

        public ReCaptchaPartDisplay(ISiteService siteService)
        {
            _siteService = siteService;
        }

        public override IDisplayResult Display(ReCaptchaPart part, BuildPartDisplayContext context)
        {
            return Initialize("ReCaptchaPart", (Func<ReCaptchaPartViewModel, ValueTask>)(async m =>
            {
                var siteSettings = await _siteService.GetSiteSettingsAsync();
                var settings = siteSettings.As<ReCaptchaSettings>();
                m.SettingsAreConfigured = settings.IsValid();
            })).Location("Detail", "Content");
        }

        public override IDisplayResult Edit(ReCaptchaPart part, BuildPartEditorContext context)
        {
            return Initialize<ReCaptchaPartViewModel>("ReCaptchaPart_Fields_Edit", async m =>
            {
                var siteSettings = await _siteService.GetSiteSettingsAsync();
                var settings = siteSettings.As<ReCaptchaSettings>();
                m.SettingsAreConfigured = settings.IsValid();
            });
        }
    }
}
