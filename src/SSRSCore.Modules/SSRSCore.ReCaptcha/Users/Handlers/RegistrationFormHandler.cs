using System;
using System.Threading.Tasks;
using SSRSCore.ReCaptcha.Services;
using SSRSCore.Users;
using SSRSCore.Users.Events;

namespace SSRSCore.ReCaptcha.Users.Handlers
{
    public class RegistrationFormEventHandler : IRegistrationFormEvents
    {
        private readonly ReCaptchaService _reCaptchaService;

        public RegistrationFormEventHandler(ReCaptchaService recaptchaService)
        {
            _reCaptchaService = recaptchaService;
        }

        public Task RegisteredAsync(IUser user)
        {
            return Task.CompletedTask;
        }

        public Task RegistrationValidationAsync(Action<string, string> reportError)
        {
            return _reCaptchaService.ValidateCaptchaAsync(reportError);
        }
    }
}
