using Fluid;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.Display.ContentDisplay;
using SSRSCore.Data.Migration;
using SSRSCore.DisplayManagement.Handlers;
using SSRSCore.Modules;
using SSRSCore.Navigation;
using SSRSCore.ReCaptcha.Core;
using SSRSCore.ReCaptcha.Drivers;
using SSRSCore.ReCaptcha.Forms;
using SSRSCore.ReCaptcha.Users.Handlers;
using SSRSCore.Settings;
using SSRSCore.Users.Events;

namespace SSRSCore.ReCaptcha
{
    [Feature("SSRSCore.ReCaptcha")]
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddReCaptcha();

            services.AddScoped<IDisplayDriver<ISite>, ReCaptchaSettingsDisplayDriver>();
            services.AddScoped<INavigationProvider, AdminMenu>();
        }
    }

    [Feature("SSRSCore.ReCaptcha.Users")]
    public class StartupUsers : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IRegistrationFormEvents, RegistrationFormEventHandler>();
            services.AddScoped<ILoginFormEvent, LoginFormEventEventHandler>();
            services.AddScoped<IPasswordRecoveryFormEvents, PasswordRecoveryFormEventEventHandler>();
            services.Configure<MvcOptions>((options) =>
            {
                options.Filters.Add(typeof(ReCaptchaLoginFilter));
            });
        }
    }
}
