namespace SSRSCore.GitHub
{
    public static class GitHubConstants
    {
        public static class Features
        {
            public const string GitHubAuthentication = "SSRSCore.GitHub.Authentication";
        }
    }
}
