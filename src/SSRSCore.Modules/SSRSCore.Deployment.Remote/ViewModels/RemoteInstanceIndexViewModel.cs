﻿using SSRSCore.Deployment.Remote.Models;

namespace SSRSCore.Deployment.Remote.ViewModels
{
    public class RemoteInstanceIndexViewModel
    {
        public RemoteInstanceList RemoteInstanceList { get; set; }
    }
}
