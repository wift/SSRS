﻿using SSRSCore.Deployment.Remote.Models;

namespace SSRSCore.Deployment.Remote.ViewModels
{
    public class RemoteClientIndexViewModel
    {
        public RemoteClientList RemoteClientList { get; set; }
    }
}
