﻿using System.Collections.Generic;

namespace SSRSCore.Deployment.Remote.Models
{
    public class RemoteClientList
    {
        public List<RemoteClient> RemoteClients { get; set; } = new List<RemoteClient>();
    }
}
