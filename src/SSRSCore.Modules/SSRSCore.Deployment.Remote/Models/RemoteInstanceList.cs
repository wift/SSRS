﻿using System.Collections.Generic;

namespace SSRSCore.Deployment.Remote.Models
{
    public class RemoteInstanceList
    {
        public List<RemoteInstance> RemoteInstances { get; set; } = new List<RemoteInstance>();
    }
}
