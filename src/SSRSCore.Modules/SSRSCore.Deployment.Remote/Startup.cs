using System;
using Microsoft.AspNetCore.Builder;
using SSRSCore.Modules;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using SSRSCore.Deployment.Remote;
using SSRSCore.Deployment.Remote.Services;
using SSRSCore.Navigation;

namespace SSRSCore.Deployment
{
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<INavigationProvider, AdminMenu>();
            services.AddScoped<RemoteInstanceService>();
            services.AddScoped<RemoteClientService>();
            services.AddScoped<IDeploymentTargetProvider, RemoteInstanceDeploymentTargetProvider>();
        }

        public override void Configure(IApplicationBuilder app, IEndpointRouteBuilder routes, IServiceProvider serviceProvider)
        {
            routes.MapAreaControllerRoute(
                name: "DeploymentImport",
                areaName: "SSRSCore.Deployment.Remote",
                pattern: "Deployment/Import",
                defaults: new { controller = "ImportRemoteInstance", action = "Import" }
            );
        }
    }
}
