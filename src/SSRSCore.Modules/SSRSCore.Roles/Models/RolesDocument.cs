﻿using System.Collections.Generic;
using SSRSCore.Security;

namespace SSRSCore.Roles.Models
{
    public class RolesDocument
    {
        public int Id { get; set; }
        public List<Role> Roles { get; } = new List<Role>();
        public int Serial { get; set; }
    }
}
