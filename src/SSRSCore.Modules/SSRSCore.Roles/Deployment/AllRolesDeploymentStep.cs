using SSRSCore.Deployment;

namespace SSRSCore.Roles.Deployment
{
    /// <summary>
    /// Adds roles to a <see cref="DeploymentPlanResult"/>. 
    /// </summary>
    public class AllRolesDeploymentStep : DeploymentStep
    {
        public AllRolesDeploymentStep()
        {
            Name = "AllRoles";
        }
    }
}
