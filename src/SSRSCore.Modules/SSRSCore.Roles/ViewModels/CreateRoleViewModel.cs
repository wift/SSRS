﻿using System.ComponentModel.DataAnnotations;

namespace SSRSCore.Roles.ViewModels
{
    public class CreateRoleViewModel
    {
        [Required]
        public string RoleName { get; set; }
    }
}
