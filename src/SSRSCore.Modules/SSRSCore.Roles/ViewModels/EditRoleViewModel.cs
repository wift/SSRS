﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using SSRSCore.Security;
using SSRSCore.Security.Permissions;

namespace SSRSCore.Roles.ViewModels
{
    public class EditRoleViewModel
    {
        public string Name { get; set; }
        public IDictionary<string, IEnumerable<Permission>> RoleCategoryPermissions { get; set; }
        public IEnumerable<string> EffectivePermissions { get; set; }

        [BindNever]
        public Role Role { get; set; }
    }
}
