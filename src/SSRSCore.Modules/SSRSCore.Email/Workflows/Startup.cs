using Microsoft.Extensions.DependencyInjection;
using SSRSCore.Email.Workflows.Activities;
using SSRSCore.Email.Workflows.Drivers;
using SSRSCore.Modules;
using SSRSCore.Workflows.Helpers;

namespace SSRSCore.Email.Workflows
{
    [RequireFeatures("SSRSCore.Workflows")]
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddActivity<EmailTask, EmailTaskDisplay>();
        }
    }
}
