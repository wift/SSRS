using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using SSRSCore.DisplayManagement.Handlers;
using SSRSCore.Email.Drivers;
using SSRSCore.Email.Services;
using SSRSCore.Navigation;
using SSRSCore.Modules;
using SSRSCore.Security.Permissions;
using SSRSCore.Settings;

namespace SSRSCore.Email
{
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IPermissionProvider, Permissions>();
            services.AddScoped<IDisplayDriver<ISite>, SmtpSettingsDisplayDriver>();
            services.AddScoped<INavigationProvider, AdminMenu>();

            services.AddTransient<IConfigureOptions<SmtpSettings>, SmtpSettingsConfiguration>();
            services.AddScoped<ISmtpService, SmtpService>();
        }
    }
}
