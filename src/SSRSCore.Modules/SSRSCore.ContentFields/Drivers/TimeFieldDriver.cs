using System;
using System.Threading.Tasks;
using SSRSCore.ContentFields.ViewModels;
using SSRSCore.ContentManagement.Display.ContentDisplay;
using SSRSCore.ContentManagement.Display.Models;
using SSRSCore.DisplayManagement.ModelBinding;
using SSRSCore.DisplayManagement.Views;
using SSRSCore.Modules;

namespace SSRSCore.ContentFields.Fields
{
    public class TimeFieldDisplayDriver : ContentFieldDisplayDriver<TimeField>
    {
        public override IDisplayResult Display(TimeField field, BuildFieldDisplayContext context)
        {
            return Initialize<DisplayTimeFieldViewModel>(GetDisplayShapeType(context), model =>
            {
                model.Field = field;
                model.Part = context.ContentPart;
                model.PartFieldDefinition = context.PartFieldDefinition;
            })
            .Location("Content")
            .Location("SummaryAdmin", "");
        }

        public override IDisplayResult Edit(TimeField field, BuildFieldEditorContext context)
        {
            return Initialize<EditTimeFieldViewModel>(GetEditorShapeType(context), model =>
            {
                model.Value = field.Value;
                model.Field = field;
                model.Part = context.ContentPart;
                model.PartFieldDefinition = context.PartFieldDefinition;
            });
        }

        public override async Task<IDisplayResult> UpdateAsync(TimeField field, IUpdateModel updater, UpdateFieldEditorContext context)
        {
            await updater.TryUpdateModelAsync(field, Prefix, f => f.Value);

            return Edit(field, context);
        }
    }
}
