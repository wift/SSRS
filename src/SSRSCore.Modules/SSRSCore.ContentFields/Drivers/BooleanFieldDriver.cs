using System.Threading.Tasks;
using SSRSCore.ContentFields.Settings;
using SSRSCore.ContentFields.ViewModels;
using SSRSCore.ContentManagement.Display.ContentDisplay;
using SSRSCore.ContentManagement.Display.Models;
using SSRSCore.DisplayManagement.ModelBinding;
using SSRSCore.DisplayManagement.Views;

namespace SSRSCore.ContentFields.Fields
{
    public class BooleanFieldDisplayDriver : ContentFieldDisplayDriver<BooleanField>
    {
        public override IDisplayResult Display(BooleanField field, BuildFieldDisplayContext context)
        {
            return Initialize<DisplayBooleanFieldViewModel>(GetDisplayShapeType(context), model =>
            {
                model.Field = field;
                model.Part = context.ContentPart;
                model.PartFieldDefinition = context.PartFieldDefinition;
            })
            .Location("Content")
            .Location("SummaryAdmin", "");
        }

        public override IDisplayResult Edit(BooleanField field, BuildFieldEditorContext context)
        {
            return Initialize<EditBooleanFieldViewModel>(GetEditorShapeType(context), model =>
            {
                model.Value = (context.IsNew == false) ?
                    field.Value : context.PartFieldDefinition.GetSettings<BooleanFieldSettings>().DefaultValue;

                model.Field = field;
                model.Part = context.ContentPart;
                model.PartFieldDefinition = context.PartFieldDefinition;
            });
        }

        public override async Task<IDisplayResult> UpdateAsync(BooleanField field, IUpdateModel updater, UpdateFieldEditorContext context)
        {
            await updater.TryUpdateModelAsync(field, Prefix, f => f.Value);

            return Edit(field, context);
        }
    }
}
