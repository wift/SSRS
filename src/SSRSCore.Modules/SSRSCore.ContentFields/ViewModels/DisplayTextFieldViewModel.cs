﻿using SSRSCore.ContentFields.Fields;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.Metadata.Models;

namespace SSRSCore.ContentFields.ViewModels
{
    public class DisplayTextFieldViewModel
    {
        public string Text => Field.Text;
        public TextField Field { get; set; }
        public ContentPart Part { get; set; }
        public ContentPartFieldDefinition PartFieldDefinition { get; set; }
    }
}
