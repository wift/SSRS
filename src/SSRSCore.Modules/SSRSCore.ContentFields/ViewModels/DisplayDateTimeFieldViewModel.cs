using System;
using SSRSCore.ContentFields.Fields;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.Metadata.Models;

namespace SSRSCore.ContentFields.ViewModels
{
    public class DisplayDateTimeFieldViewModel
    {
        public DateTime? Value => Field.Value;
        public DateTime? LocalDateTime { get; set; }
        public DateTimeField Field { get; set; }
        public ContentPart Part { get; set; }
        public ContentPartFieldDefinition PartFieldDefinition { get; set; }
    }
}
