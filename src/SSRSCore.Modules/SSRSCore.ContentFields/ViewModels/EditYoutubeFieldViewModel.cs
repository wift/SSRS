using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using SSRSCore.ContentFields.Fields;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.Metadata.Models;

namespace SSRSCore.ContentFields.ViewModels
{
    public class EditYoutubeFieldViewModel
    {
        [DataType(DataType.Url, ErrorMessage = "The field only accepts Urls")]
        public string RawAddress { get; set; }
        public string EmbeddedAddress { get; set; }
        public YoutubeField Field { get; set; }
        public ContentPart Part { get; set; }
        public ContentPartFieldDefinition PartFieldDefinition { get; set; }
    }
}
