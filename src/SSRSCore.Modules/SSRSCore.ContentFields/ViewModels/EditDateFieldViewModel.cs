using System;
using SSRSCore.ContentFields.Fields;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.Metadata.Models;

namespace SSRSCore.ContentFields.ViewModels
{
    public class EditDateFieldViewModel
    {
        public DateTime? Value { get; set; }
        public DateField Field { get; set; }
        public ContentPart Part { get; set; }
        public ContentPartFieldDefinition PartFieldDefinition { get; set; }
    }
}
