using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using SSRSCore.ContentFields.Fields;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.Metadata.Models;

namespace SSRSCore.ContentFields.ViewModels
{
    public class EditLocalizationSetContentPickerFieldViewModel
    {
        public string LocalizationSets { get; set; }
        public LocalizationSetContentPickerField Field { get; set; }
        public ContentPart Part { get; set; }
        public ContentPartFieldDefinition PartFieldDefinition { get; set; }

        [BindNever]
        public IList<VueMultiselectItemViewModel> SelectedItems { get; set; }
    }
}
