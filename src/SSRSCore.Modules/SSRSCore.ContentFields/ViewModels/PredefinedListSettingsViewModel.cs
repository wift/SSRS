using SSRSCore.ContentFields.Settings;

namespace SSRSCore.ContentFields.ViewModels
{
    public class PredefinedListSettingsViewModel
    {
        public EditorOption Editor { get; set; }
        public string Options { get; set; }
        public string DefaultValue { get; set; }
    }
}
