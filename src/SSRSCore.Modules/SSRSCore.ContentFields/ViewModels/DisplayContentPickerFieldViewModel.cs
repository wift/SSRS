using System.Collections.Generic;
using SSRSCore.ContentFields.Fields;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.Metadata.Models;
using SSRSCore.DisplayManagement.ModelBinding;

namespace SSRSCore.ContentFields.ViewModels
{
    public class DisplayContentPickerFieldViewModel
    {
        public string[] ContentItemIds => Field.ContentItemIds;
        public ContentPickerField Field { get; set; }
        public ContentPart Part { get; set; }
        public ContentPartFieldDefinition PartFieldDefinition { get; set; }
    }
}
