using System;
using System.Collections.Generic;
using System.Text;
using SSRSCore.ContentFields.Fields;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.Metadata.Models;

namespace SSRSCore.ContentFields.ViewModels
{
    public class YoutubeFieldDisplayViewModel
    {
        public string EmbeddedAddress => Field.EmbeddedAddress;
        public string RawAddress => Field.RawAddress;
        public YoutubeField Field { get; set; }
        public ContentPart Part { get; set; }
        public ContentPartFieldDefinition PartFieldDefinition { get; set; }
    }
}
