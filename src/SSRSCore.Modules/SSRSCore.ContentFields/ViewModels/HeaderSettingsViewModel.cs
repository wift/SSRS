using SSRSCore.ContentFields.Settings;

namespace SSRSCore.ContentFields.ViewModels
{
    public class HeaderSettingsViewModel
    {
        public string Level { get; set; }
    }
}
