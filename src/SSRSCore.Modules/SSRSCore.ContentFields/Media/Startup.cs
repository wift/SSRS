using SSRSCore.Modules;
using Microsoft.Extensions.DependencyInjection;
using SSRSCore.DisplayManagement.Descriptors;

namespace SSRSCore.ContentFields.Media
{
    [RequireFeatures("SSRSCore.Media")]
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IShapeTableProvider, MediaShapes>();
        }
    }
}
