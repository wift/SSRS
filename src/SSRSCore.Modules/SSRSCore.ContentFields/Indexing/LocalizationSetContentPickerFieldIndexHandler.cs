using System.Threading.Tasks;
using SSRSCore.ContentFields.Fields;
using SSRSCore.Indexing;
using SSRSCore.Modules;

namespace SSRSCore.ContentFields.Indexing
{
    [RequireFeatures("SSRSCore.ContentLocalization")]
    public class LocalizationSetContentPickerFieldIndexHandler : ContentFieldIndexHandler<LocalizationSetContentPickerField>
    {
        public override Task BuildIndexAsync(LocalizationSetContentPickerField field, BuildFieldIndexContext context)
        {
            var options = DocumentIndexOptions.Store;

            foreach (var localizationSet in field.LocalizationSets)
            {
                foreach (var key in context.Keys)
                {
                    context.DocumentIndex.Set(key, localizationSet, options);
                }
            }

            return Task.CompletedTask;
        }
    }
}
