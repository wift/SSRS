using System;
using SSRSCore.ContentManagement;

namespace SSRSCore.ContentFields.Fields
{
    public class ContentPickerField : ContentField
    {
        public string[] ContentItemIds { get; set; } = Array.Empty<string>();
    }
}
