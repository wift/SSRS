using System;
using SSRSCore.ContentManagement;

namespace SSRSCore.ContentFields.Fields
{
    public class DateField : ContentField
    {
        public DateTime? Value { get; set; }
    }
}
