using System;
using SSRSCore.ContentManagement;

namespace SSRSCore.ContentFields.Fields
{
    public class DateTimeField : ContentField
    {
        public DateTime? Value { get; set; }
    }
}
