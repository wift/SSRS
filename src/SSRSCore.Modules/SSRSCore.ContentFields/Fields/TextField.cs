﻿using SSRSCore.ContentManagement;

namespace SSRSCore.ContentFields.Fields
{
    public class TextField : ContentField
    {
        public string Text { get; set; }
    }
}
