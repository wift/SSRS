﻿using SSRSCore.ContentManagement;

namespace SSRSCore.ContentFields.Fields
{
    public class HtmlField : ContentField
    {
        public string Html { get; set; }
    }
}
