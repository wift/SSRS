﻿using SSRSCore.ContentManagement;

namespace SSRSCore.ContentFields.Fields
{
    public class BooleanField : ContentField
    {
        public bool Value { get; set; }
    }
}
