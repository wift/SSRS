using System;
using SSRSCore.ContentManagement;
using SSRSCore.Modules;

namespace SSRSCore.ContentFields.Fields
{
    [RequireFeatures("SSRSCore.ContentLocalization")]
    public class LocalizationSetContentPickerField : ContentField
    {
        public string[] LocalizationSets { get; set; } = Array.Empty<string>();
    }
}
