﻿using SSRSCore.ContentManagement;

namespace SSRSCore.ContentFields.Fields
{
    public class NumericField : ContentField
    {
        public decimal? Value { get; set; }
    }
}
