using System;
using SSRSCore.ContentManagement;

namespace SSRSCore.ContentFields.Fields
{
    public class TimeField : ContentField
    {
        public TimeSpan? Value { get; set; }
    }
}
