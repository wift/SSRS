using System.Threading.Tasks;
using SSRSCore.ContentFields.Fields;
using SSRSCore.ContentManagement.Metadata.Models;
using SSRSCore.ContentTypes.Editors;
using SSRSCore.DisplayManagement.Views;

namespace SSRSCore.ContentFields.Settings
{

    public class TextFieldSettingsDriver : ContentPartFieldDefinitionDisplayDriver<TextField>
    {
        public override IDisplayResult Edit(ContentPartFieldDefinition partFieldDefinition)
        {
            return Initialize<TextFieldSettings>("TextFieldSettings_Edit", model => partFieldDefinition.PopulateSettings(model))
                .Location("Content");
        }

        public override async Task<IDisplayResult> UpdateAsync(ContentPartFieldDefinition partFieldDefinition, UpdatePartFieldEditorContext context)
        {
            var model = new TextFieldSettings();

            await context.Updater.TryUpdateModelAsync(model, Prefix);

            context.Builder.WithSettings(model);

            return Edit(partFieldDefinition);
        }
    }
}
