using System.Threading.Tasks;
using SSRSCore.ContentFields.Fields;
using SSRSCore.ContentManagement.Metadata.Models;
using SSRSCore.ContentTypes.Editors;
using SSRSCore.DisplayManagement.Views;

namespace SSRSCore.ContentFields.Settings
{
    public class YoutubeFieldSettingsDriver : ContentPartFieldDefinitionDisplayDriver<YoutubeField>
    {
        public override IDisplayResult Edit(ContentPartFieldDefinition partFieldDefinition)
        {
            return Initialize<YoutubeFieldSettings>("YoutubeFieldSetting_Edit", model =>
             {
                 partFieldDefinition.PopulateSettings(model);
                 model.Height = model.Height != default(int) ? model.Height : 315;
                 model.Width = model.Width != default(int) ? model.Width : 560;
             }).Location("Content");
        }

        public async override Task<IDisplayResult> UpdateAsync(ContentPartFieldDefinition partFieldDefinition, UpdatePartFieldEditorContext context)
        {
            var model = new YoutubeFieldSettings();
            await context.Updater.TryUpdateModelAsync(model, Prefix);

            context.Builder.WithSettings(model);

            return Edit(partFieldDefinition);
        }
    }
}
