using Newtonsoft.Json;

namespace SSRSCore.ContentFields.Settings
{
    public class TextFieldHeaderDisplaySettings
    {
        public string Level { get; set; }
    }
}