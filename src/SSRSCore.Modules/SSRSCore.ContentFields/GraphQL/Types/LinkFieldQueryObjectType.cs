using GraphQL.Types;
using SSRSCore.ContentFields.Fields;

namespace SSRSCore.ContentFields.GraphQL.Types
{
    public class LinkFieldQueryObjectType : ObjectGraphType<LinkField>
    {
        public LinkFieldQueryObjectType()
        {
            Name = nameof(LinkField);

            Field(x => x.Url, nullable: true).Description("the url of the link");
            Field(x => x.Text, nullable: true).Description("the text of the link");
        }
    }
}