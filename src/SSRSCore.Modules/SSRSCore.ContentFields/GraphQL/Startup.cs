using Microsoft.Extensions.DependencyInjection;
using SSRSCore.Apis;
using SSRSCore.ContentFields.Fields;
using SSRSCore.ContentFields.GraphQL.Fields;
using SSRSCore.ContentFields.GraphQL.Types;
using SSRSCore.ContentManagement.GraphQL.Queries.Types;
using SSRSCore.Modules;

namespace SSRSCore.ContentFields.GraphQL
{
    [RequireFeatures("SSRSCore.Apis.GraphQL")]
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IContentFieldProvider, ObjectGraphTypeFieldProvider>();
            services.AddScoped<IContentFieldProvider, ContentFieldsProvider>();

            services.AddObjectGraphType<LinkField, LinkFieldQueryObjectType>();
            services.AddObjectGraphType<ContentPickerField, ContentPickerFieldQueryObjectType>();
        }
    }
}
