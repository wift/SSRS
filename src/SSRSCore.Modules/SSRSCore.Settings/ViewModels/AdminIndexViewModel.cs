﻿namespace SSRSCore.Settings.ViewModels
{
    public class AdminIndexViewModel
    {
        public dynamic Shape { get; set; }
        public string GroupId { get; set; }
    }
}
