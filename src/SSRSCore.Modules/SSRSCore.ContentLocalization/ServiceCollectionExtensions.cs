using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using SSRSCore.ContentLocalization.Handlers;
using SSRSCore.ContentLocalization.Models;
using SSRSCore.ContentLocalization.Records;
using SSRSCore.ContentManagement;
using SSRSCore.Data.Migration;
using YesSql.Indexes;

namespace SSRSCore.ContentLocalization
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddContentLocalization(this IServiceCollection services)
        {
            services.AddContentPart<LocalizationPart>();

            services.TryAddScoped<IContentLocalizationManager, DefaultContentLocalizationManager>();
            services.AddSingleton<IIndexProvider, LocalizedContentItemIndexProvider>();
            services.AddScoped<IDataMigration, Migrations>();
            services.AddScoped<IContentLocalizationHandler, ContentLocalizationPartHandlerCoordinator>();

            return services;
        }
    }
}
