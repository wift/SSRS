using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Localization;
using SSRSCore.ContentLocalization.Drivers;
using SSRSCore.Modules;
using SSRSCore.Navigation;

namespace SSRSCore.ContentLocalization
{
    [Feature("SSRSCore.ContentLocalization.ContentCulturePicker")]
    public class AdminMenu : INavigationProvider
    {
        public AdminMenu(IStringLocalizer<AdminMenu> localizer)
        {
            T = localizer;
        }

        public IStringLocalizer T { get; set; }

        public Task BuildNavigationAsync(string name, NavigationBuilder builder)
        {
            if (!String.Equals(name, "admin", StringComparison.OrdinalIgnoreCase))
            {
                return Task.CompletedTask;
            }

            builder
                .Add(T["Configuration"], configuration => configuration
                    .Add(T["Settings"], settings => settings
                        .Add(T["ContentCulturePicker"], T["ContentCulturePicker"], registration => registration
                            .Action("Index", "Admin", new { area = "SSRSCore.Settings", groupId = ContentCulturePickerSettingsDriver.GroupId })
                            .Permission(Permissions.ManageContentCulturePicker)
                            .LocalNav()
                        )));

            return Task.CompletedTask;
        }
    }
}
