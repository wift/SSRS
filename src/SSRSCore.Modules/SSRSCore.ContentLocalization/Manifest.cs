using SSRSCore.Modules.Manifest;

[assembly: Module(
    Name = "Content Localization",
    Author = "The Orchard Team",
    Website = "http://orchardproject.net",
    Version = "1.0.0",
    Description = "Provides a part that allows to localize content items.",
    Category = "Internationalization"
)]

[assembly: Feature(
    Id = "SSRSCore.ContentLocalization",
    Name = "Content Localization",
    Description = "Provides a part that allows to localize content items.",
    Dependencies = new[] { "SSRSCore.ContentTypes", "SSRSCore.Localization" },
    Category = "Internationalization"
)]


[assembly: Feature(
    Id = "SSRSCore.ContentLocalization.ContentCulturePicker",
    Name = "Content Culture Picker",
    Description = "Provides a culture picker shape for the frontend.",
    Dependencies = new[] { "SSRSCore.ContentLocalization" },
    Category = "Internationalization"
)]
