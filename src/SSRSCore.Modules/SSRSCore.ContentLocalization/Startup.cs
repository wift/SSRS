using System;
using System.Globalization;
using System.Linq;
using Fluid;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using SSRSCore.ContentLocalization.Drivers;
using SSRSCore.ContentLocalization.Handlers;
using SSRSCore.ContentLocalization.Indexing;
using SSRSCore.ContentLocalization.Liquid;
using SSRSCore.ContentLocalization.Models;
using SSRSCore.ContentLocalization.Records;
using SSRSCore.ContentLocalization.Security;
using SSRSCore.ContentLocalization.Services;
using SSRSCore.ContentLocalization.ViewModels;
using SSRSCore.ContentManagement.Display.ContentDisplay;
using SSRSCore.ContentManagement.Handlers;
using SSRSCore.DisplayManagement.Handlers;
using SSRSCore.Indexing;
using SSRSCore.Liquid;
using SSRSCore.Modules;
using SSRSCore.Navigation;
using SSRSCore.Security.Permissions;
using SSRSCore.Settings;
using YesSql;

namespace SSRSCore.ContentLocalization
{
    public class Startup : StartupBase
    {
        static Startup()
        {
            TemplateContext.GlobalMemberAccessStrategy.Register<LocalizationPartViewModel>();
        }

        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IContentPartDisplayDriver, LocalizationPartDisplayDriver>();
            services.AddScoped<IContentPartIndexHandler, LocalizationPartIndexHandler>();
            services.AddSingleton<ILocalizationEntries, LocalizationEntries>();
            services.AddScoped<IContentPartHandler, LocalizationPartHandler>();
            services.AddContentLocalization();

            services.AddScoped<IPermissionProvider, Permissions>();
            services.AddScoped<IAuthorizationHandler, LocalizeContentAuthorizationHandler>();
        }
    }

    [Feature("SSRSCore.ContentLocalization.ContentCulturePicker")]
    public class ContentPickerStartup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<INavigationProvider, AdminMenu>();
            services.AddScoped<IContentCulturePickerService, ContentCulturePickerService>();
            services.AddScoped<IDisplayDriver<ISite>, ContentCulturePickerSettingsDriver>();
            services.Configure<RequestLocalizationOptions>(options =>
            {
                options.AddInitialRequestCultureProvider(new ContentRequestCultureProvider());
            });
        }

        public override void Configure(IApplicationBuilder builder, IEndpointRouteBuilder routes, IServiceProvider serviceProvider)
        {
            routes.MapAreaControllerRoute(
               name: "RedirectToLocalizedContent",
               areaName: "SSRSCore.ContentLocalization",
               pattern: "RedirectToLocalizedContent",
               defaults: new { controller = "ContentCulturePicker", action = "RedirectToLocalizedContent" }
           );

            var session = serviceProvider.GetRequiredService<ISession>();
            var entries = serviceProvider.GetRequiredService<ILocalizationEntries>();

            var indexes = session.QueryIndex<LocalizedContentItemIndex>(i => i.Published)
                .ListAsync().GetAwaiter().GetResult();

            entries.AddEntries(indexes.Select(i => new LocalizationEntry
            {
                ContentItemId = i.ContentItemId,
                LocalizationSet = i.LocalizationSet,
                Culture = i.Culture.ToLowerInvariant()
            }));
        }
    }
    [RequireFeatures("SSRSCore.Liquid")]
    public class LiquidStartup : StartupBase
    {
        static LiquidStartup()
        {
            TemplateContext.GlobalMemberAccessStrategy.Register<CultureInfo>();
        }
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddLiquidFilter<ContentLocalizationFilter>("localization_set");
            services.AddLiquidFilter<SwitchCultureUrlFilter>("switch_culture_url");
        }
    }
}
