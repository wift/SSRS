using Microsoft.Extensions.DependencyInjection;
using SSRSCore.Apis;
using SSRSCore.ContentLocalization.Models;
using SSRSCore.ContentManagement.GraphQL.Queries;
using SSRSCore.Modules;

namespace SSRSCore.ContentLocalization.GraphQL
{
    [RequireFeatures("SSRSCore.Apis.GraphQL")]
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddInputObjectGraphType<LocalizationPart, LocalizationInputObjectType>();
            services.AddObjectGraphType<LocalizationPart, LocalizationQueryObjectType>();
            services.AddTransient<IIndexAliasProvider, LocalizationPartIndexAliasProvider>();
        }
    }
}
