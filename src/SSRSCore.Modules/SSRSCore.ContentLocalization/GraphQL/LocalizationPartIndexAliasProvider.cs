using System.Collections.Generic;
using SSRSCore.ContentLocalization.Records;
using SSRSCore.ContentManagement.GraphQL.Queries;

namespace SSRSCore.ContentLocalization.GraphQL
{
    public class LocalizationPartIndexAliasProvider : IIndexAliasProvider
    {
        private static readonly IndexAlias[] _aliases = new[]
        {
            new IndexAlias
            {
                Alias = "localizationPart",
                Index = nameof(LocalizedContentItemIndex),
                With = q => q.With<LocalizedContentItemIndex>()
            }
        };

        public IEnumerable<IndexAlias> GetAliases()
        {
            return _aliases;
        }
    }
}