using SSRSCore.Modules;
using Microsoft.Extensions.DependencyInjection;
using SSRSCore.ContentManagement.Handlers;
using SSRSCore.Data.Migration;
using SSRSCore.Indexing.Services;

namespace SSRSCore.Indexing
{
    /// <summary>
    /// These services are registered on the tenant service collection
    /// </summary>
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IIndexingTaskManager, IndexingTaskManager>();
            services.AddScoped<IContentHandler, CreateIndexingTaskContentHandler>();
            services.AddScoped<IDataMigration, Migrations>();
        }
    }
}
