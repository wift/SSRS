using System;
using System.Collections.Generic;
using SSRSCore.Navigation;

namespace SSRSCore.AdminMenu.Models
{
    /// <summary>
    /// The list of all the AdminMenu stored on the system.
    /// </summary>
    public class AdminMenuList
    {
        public int Id { get; set; }
        public List<AdminMenu> AdminMenu { get; set; } = new List<AdminMenu>();
    }
}
