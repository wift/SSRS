using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Localization;
using SSRSCore.AdminMenu.Services;
using SSRSCore.Navigation;

namespace SSRSCore.AdminMenu
{
    public class AdminMenu : INavigationProvider
    {
        private readonly AdminMenuNavigationProvidersCoordinator _adminMenuNavigationProvider;

        public AdminMenu(AdminMenuNavigationProvidersCoordinator adminMenuNavigationProvider,
            IStringLocalizer<AdminMenu> localizer)
        {
            _adminMenuNavigationProvider = adminMenuNavigationProvider;
            S = localizer;
        }

        public IStringLocalizer S { get; set; }

        public async Task BuildNavigationAsync(string name, NavigationBuilder builder)
        {
            if (!String.Equals(name, "admin", StringComparison.OrdinalIgnoreCase))
            {
                return;
            }

            // Configuration and settings menus for the AdminMenu module
            builder.Add(S["Configuration"], cfg => cfg
                    .Add(S["Admin Menus"], "1.5", admt => admt
                        .Permission(Permissions.ManageAdminMenu)
                        .Action("List", "Menu", new { area = "SSRSCore.AdminMenu" })
                        .LocalNav()
                    ));

            // This is the entry point for the adminMenu: dynamically generated custom admin menus
           await  _adminMenuNavigationProvider.BuildNavigationAsync("adminMenu", builder);            
        }
    }
}
