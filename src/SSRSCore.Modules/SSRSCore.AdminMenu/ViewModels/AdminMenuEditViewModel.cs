using System.ComponentModel.DataAnnotations;

namespace SSRSCore.AdminMenu.ViewModels
{
    public class AdminMenuEditViewModel
    {
        public string Id { get; set; }

        [Required]
        public string Name { get; set; }
    }
}
