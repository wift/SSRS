using System.ComponentModel.DataAnnotations;

namespace SSRSCore.AdminMenu.ViewModels
{
    public class AdminMenuCreateViewModel
    {
        [Required]
        public string Name { get; set; }
    }
}
