using SSRSCore.Deployment;

namespace SSRSCore.AdminMenu.Deployment
{
    /// <summary>
    /// Adds all admin menus to a <see cref="DeploymentPlanResult"/>. 
    /// </summary>
    public class AdminMenuDeploymentStep : DeploymentStep
    {
        public AdminMenuDeploymentStep()
        {
            Name = "AdminMenu";
        }
    }
}
