# Admin Menu 管理员菜单 (SSRSCore.AdminMenu)

管理菜单模块为用户提供了通过管理UI创建自定义管理菜单的方法

## 常规概念

有两个基本概念:

1. **Admin Menu**: 管理节点的树，其根位于管理菜单的第一层。可以有一个或多个。  

2. **Admin Node**: 构成管理菜单的每个节点。AdminNode可以包含其他管理节点。每个管理节点的结果是在管理菜单上呈现一个或多个菜单项。
这些菜单项树与SSRS Core提供的标准管理菜单合并。在本文档中，当我们提到SSRS Core提供的菜单时，我们使用术语**Admin Menu**。
你可以禁用一个管理菜单，它不会显示。

您可以禁用管理节点，但它及其派生节点都不会显示。


## 如何创建管理员菜单

1. 确保管理菜单模块已启用。

2. 去配置: Admin Menu.

3. 创建一个新的管理菜单，并开始向其中添加管理节点。Link Admin节点是最简单的类型，非常适合测试该特性。

4. 当你继续添加管理节点，你会看到他们自动呈现在管理菜单。.


##  提供的管理节点类型 

在撰写本文时，SSRS Core提供了三种管理节点类型:

1. **Link Admin Node**: 它提供了一个简单的菜单项。用户可以添加一个文本、一个url和一个字体图标类，这样当前的管理主题就可以在呈现菜单项时使用它们。目前的管理主题是使用图标类只对第一级菜单项。
此链接是SSRSCore提供的惟一链接。AdminMenu模块本身。

2. **Content Types Admin Node**: 它提供一个菜单项列表，其中包含每个内容类型的菜单项。这些链接指向内容控制器中的索引操作。
此节点类型由SSRSCore提供内容模块。

3. **Lists Admin Node**: 它提供指向包含部件列表的内容项的编辑页面的菜单项。
例如，如果您有一个博客内容类型和几个博客内容项，它将为每个现有博客提供一个链接。
此节点类型由SSRSCore提供。列表模块。
注意，每个节点上都可以嵌套其他节点。
嵌套是通过在UI上拖放完成的。


## 管理菜单（Admin Menu）如何呈现为管理菜单项。

###   Admin Menu Module 如何工作的.
SSRSCore提供的管理菜单是开箱即用的，大致是这样构建的:
1. NavigationManager检索实现INavigationProvider的所有类。有许多这样的模块，它们的文件名为“AdminMenu.cs”

2. 在每个AdminMenu上，NavigationManager调用BuildNavigationAsync方法，向其传递一个构建器。构建器是每个AdminMenu可以在其中添加自己的menuitem的对象。

3. 一旦所有的AdminMenu类完成向构建器添加它们自己的菜单项，NavigationManager就使用构建器上的信息来“呈现”整个菜单。

### 启用管理菜单时发生了什么变化.

1. AdminMenu模块声明它自己的INavigationProvider，因此NavigationManager也将调用它。INavigationProvider的名称是AdminMenuNavigationProvidersCoordinator。

2. 协调器检索存储在数据库中的所有AdminMenu，并为每个AdminMenu调用BuildTreeAsync方法，其中每个节点递归地向构建器添加自己的菜单项。


## 部署计划步骤和配方步骤 
模块提供了一个管理菜单部署步骤。因此，管理用户可以花一些时间配置自定义管理菜单，将其添加到部署计划中，导出json文件，并在设置菜谱上使用生成的json。这样，使用该菜谱构建的站点将拥有用户准备的管理菜单。

## 权限 
有两种与模块相关的权限: 

1. Manage Admin Menus. 它是关于能够创建编辑和删除管理菜单从管理。.

2. View Admin Menus. 它支持为每个角色显示或隐藏管理菜单。您可以在标准编辑角色页面中进行此操作


## 开发自定义管理节点类型

任何模块都可以添加自己的自定义管理节点类型，以便用户可以使用它们来构建自定义管理菜单。
通常情况下，你需要遵循以下步骤:

1. 添加一个继承自' AdminNode '的类。在这个类上添加您想要的节点类型的特定属性。这是将进入数据库的信息。.

2. 添加一个驱动程序来处理管理节点的显示和编辑。这将无法处理管理菜单的实际呈现。驱动程序只与创建和编辑管理菜单所需的视图有关。

3. 或者，您可以实现一个ViewModel来在编辑视图和驱动程序之间移动信息.

4.添加一个实现IAdminNodeNavigationBuilder的类。在呈现菜单时，AdminMenuNavigationProvidersCoordinator类将调用它的BuildNavigationAsync()方法。.

5. 根据您的节点类型创建和编辑管理节点所需的视图。.


按照惯例，您应该将所有这些非视图类存储在一个“AdminNodes”文件夹中。这是可选的。

按照惯例，您必须将视图存储在“视图”文件夹中的“Items”文件夹中。这是必需的。

不要忘记在Startup类中注册相应的类。


### 基于LinkAdminNode的代码片段

This is the LinkAdminNode.cs

```csharp

    public class LinkAdminNode : AdminNode
    {
        [Required]
        public string LinkText { get; set; }

        public string LinkUrl { get; set; }

        public string IconClass { get; set; } = "far fa-circle";
    }
```

这是LinkAdminNodeBuilder构建链接的方式

这个类的负责的功能:

* 将数据库中的管理节点信息转换为menuItems并将它们添加到全局生成器中。.

* 在每个AdminNode的子节点上调用相同的BuildNavigationAsync()方法	.

此模式确保在流程结束时处理完整的树.

``` csharp
        public Task BuildNavigationAsync(MenuItem menuItem, 
                NavigationBuilder builder, 
                IEnumerable<IAdminNodeNavigationBuilder> treeNodeBuilders)
        {
            // 将接收到的项转换为我们正在处理的具体管理节点类型。
            var ltn = menuItem as LinkAdminNode;

            if ((ltn == null) ||( !ltn.Enabled))
            {
                return Task.CompletedTask;
            }

            // 这是向构建器添加菜单项的标准SSRS核心方法
            builder.Add(new LocalizedString(ltn.LinkText, ltn.LinkText), async itemBuilder => {

                // 添加一个实际连接
                itemBuilder.Url(ltn.LinkUrl);
                AddIconPickerClassToLink(ltn.IconClass, itemBuilder);
                

                // 让子管理节点在这个MenuItem中构建自己
                foreach (var childTreeNode in menuItem.Items)
                {
                    try
                    {
                        var treeBuilder = treeNodeBuilders
                                .Where(x => x.Name == childTreeNode.GetType().Name)
                                .FirstOrDefault();
                        
                        await treeBuilder.BuildNavigationAsync(childTreeNode,itemBuilder,treeNodeBuilders);
                    }
                    catch (Exception e)
                    {
                        _logger.LogError(e, 
                            "An exception occurred while building the '{MenuItem}' child Menu Item.",
                            childTreeNode.GetType().Name);
                    }
                }
            });

            return Task.CompletedTask;
        }

```

