using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using SSRSCore.AdminMenu.Models;

namespace SSRSCore.AdminMenu.AdminNodes
{
    public class LinkAdminNode : AdminNode
    {
        [Required]
        public string LinkText { get; set; }

        [Required]
        public string LinkUrl { get; set; }

        public string IconClass { get; set; }
    }
}
