using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using SSRSCore.AdminMenu.Models;

namespace SSRSCore.AdminMenu.AdminNodes
{
    public class PlaceholderAdminNode : AdminNode
    {
        [Required]
        public string LinkText { get; set; }
        public string IconClass { get; set; }
    }
}
