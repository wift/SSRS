using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using SSRSCore.AdminMenu.Models;
using SSRSCore.AdminMenu.AdminNodes;
using SSRSCore.AdminMenu.ViewModels;
using SSRSCore.DisplayManagement.Handlers;
using SSRSCore.DisplayManagement.ModelBinding;
using SSRSCore.DisplayManagement.Views;
using SSRSCore.Navigation;

namespace SSRSCore.AdminMenu.AdminNodes
{
    public class PlaceholderAdminNodeDriver : DisplayDriver<MenuItem, PlaceholderAdminNode>
    {
        public override IDisplayResult Display(PlaceholderAdminNode treeNode)
        {
            return Combine(
                View("PlaceholderAdminNode_Fields_TreeSummary", treeNode).Location("TreeSummary", "Content"),
                View("PlaceholderAdminNode_Fields_TreeThumbnail", treeNode).Location("TreeThumbnail", "Content")
            );
        }

        public override IDisplayResult Edit(PlaceholderAdminNode treeNode)
        {
            return Initialize<PlaceholderAdminNodeViewModel>("PlaceholderAdminNode_Fields_TreeEdit", model =>
            {
                model.LinkText = treeNode.LinkText;
                model.IconClass = treeNode.IconClass;
            }).Location("Content");
        }

        public override async Task<IDisplayResult> UpdateAsync(PlaceholderAdminNode treeNode, IUpdateModel updater)
        {
            var model = new PlaceholderAdminNodeViewModel();
            if(await updater.TryUpdateModelAsync(model, Prefix, x => x.LinkText, x => x.IconClass))
            {
                treeNode.LinkText = model.LinkText;
                treeNode.IconClass = model.IconClass;
            };
            
            return Edit(treeNode);
        }
    }
}
