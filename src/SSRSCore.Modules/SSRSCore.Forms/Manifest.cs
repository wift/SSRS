using SSRSCore.Modules.Manifest;

[assembly: Module(
    Name = "Forms",
    Author = "The Orchard Team",
    Website = "https://orchardproject.net",
    Version = "2.0.0"
)]

[assembly: Feature(
    Id = "SSRSCore.Forms",
    Name = "Forms",
    Description = "Provides widgets and activities to implement forms.",
    Dependencies = new [] { "SSRSCore.Widgets", "SSRSCore.Flows" },
    Category = "Content"
)]