using GraphQL.Types;
using SSRSCore.Forms.Models;

namespace SSRSCore.Forms.GraphQL
{
    public class ButtonPartQueryObjectType : ObjectGraphType<ButtonPart>
    {
        public ButtonPartQueryObjectType()
        {
            Name = "ButtonPart";

            Field(x => x.Text, nullable: true);
            Field(x => x.Type, nullable: true);
        }
    }
}