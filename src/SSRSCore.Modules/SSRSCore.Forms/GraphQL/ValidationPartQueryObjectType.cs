using GraphQL.Types;
using SSRSCore.Forms.Models;

namespace SSRSCore.Forms.GraphQL
{
    public class ValidationPartQueryObjectType : ObjectGraphType<ValidationPart>
    {
        public ValidationPartQueryObjectType()
        {
            Name = "ValidationPart";

            Field(x => x.For, nullable: true);
        }
    }
}