using GraphQL.Types;
using SSRSCore.Forms.Models;

namespace SSRSCore.Forms.GraphQL
{
    public class FormInputElementPartQueryObjectType : ObjectGraphType<FormInputElementPart>
    {
        public FormInputElementPartQueryObjectType()
        {
            Name = "FormInputElementPart";

            Field(x => x.Name, nullable: true);
        }
    }
}