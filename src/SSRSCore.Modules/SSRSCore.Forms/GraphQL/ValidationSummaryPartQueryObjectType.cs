using GraphQL.Types;
using SSRSCore.Forms.Models;

namespace SSRSCore.Forms.GraphQL
{
    public class ValidationSummaryPartQueryObjectType : ObjectGraphType<ValidationSummaryPart>
    {
        public ValidationSummaryPartQueryObjectType()
        {
            Name = "ValidationSummaryPart";
        }
    }
}