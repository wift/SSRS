using GraphQL.Types;
using SSRSCore.Forms.Models;

namespace SSRSCore.Forms.GraphQL
{
    public class FormElementPartQueryObjectType : ObjectGraphType<FormElementPart>
    {
        public FormElementPartQueryObjectType()
        {
            Name = "FormElementPart";

            Field(x => x.Id, nullable: true);
        }
    }
}