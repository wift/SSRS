using Microsoft.Extensions.DependencyInjection;
using SSRSCore.Forms.Workflows.Activities;
using SSRSCore.Forms.Workflows.Drivers;
using SSRSCore.Modules;
using SSRSCore.Workflows.Helpers;

namespace SSRSCore.Forms.Workflows
{
    [RequireFeatures("SSRSCore.Workflows")]
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddActivity<ValidateAntiforgeryTokenTask, ValidateAntiforgeryTokenTaskDisplay>();
            services.AddActivity<AddModelValidationErrorTask, AddModelValidationErrorTaskDisplay>();
            services.AddActivity<ValidateFormTask, ValidateFormTaskDisplay>();
            services.AddActivity<ValidateFormFieldTask, ValidateFormFieldTaskDisplay>();
            services.AddActivity<BindModelStateTask, BindModelStateTaskDisplay>();
            services.AddAntiforgery();
        }
    }
}
