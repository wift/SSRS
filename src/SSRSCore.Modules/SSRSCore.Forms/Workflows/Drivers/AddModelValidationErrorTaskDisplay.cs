using SSRSCore.Forms.Workflows.Activities;
using SSRSCore.Forms.Workflows.ViewModels;
using SSRSCore.Workflows.Display;

namespace SSRSCore.Forms.Workflows.Drivers
{
    public class AddModelValidationErrorTaskDisplay : ActivityDisplayDriver<AddModelValidationErrorTask, AddModelValidationErrorTaskViewModel>
    {
        protected override void EditActivity(AddModelValidationErrorTask activity, AddModelValidationErrorTaskViewModel model)
        {
            model.Key = activity.Key;
            model.ErrorMessage = activity.ErrorMessage;
        }

        protected override void UpdateActivity(AddModelValidationErrorTaskViewModel model, AddModelValidationErrorTask activity)
        {
            activity.Key = model.Key?.Trim();
            activity.ErrorMessage = model.ErrorMessage?.Trim();
        }
    }
}
