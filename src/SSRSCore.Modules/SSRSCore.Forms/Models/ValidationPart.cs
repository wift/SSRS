using SSRSCore.ContentManagement;

namespace SSRSCore.Forms.Models
{
    public class ValidationPart : ContentPart
    {
        public string For { get; set; }
    }
}
