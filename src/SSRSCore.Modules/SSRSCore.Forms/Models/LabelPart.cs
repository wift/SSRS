using SSRSCore.ContentManagement;

namespace SSRSCore.Forms.Models
{
    public class LabelPart : ContentPart
    {
        public string For { get; set; }
    }
}
