using SSRSCore.ContentManagement;

namespace SSRSCore.Forms.Models
{
    /// <summary>
    /// Turns a content item into a form element.
    /// </summary>
    public class FormElementPart : ContentPart
    {
        public string Id { get; set; }
    }
}
