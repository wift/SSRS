using SSRSCore.ContentManagement;

namespace SSRSCore.Forms.Models
{
    public class ButtonPart : ContentPart
    {
        public string Text { get; set; }
        public string Type { get; set; }
    }
}
