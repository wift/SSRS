using SSRSCore.ContentManagement;

namespace SSRSCore.Forms.Models
{
    public class TextAreaPart : ContentPart
    {
        public string DefaultValue { get; set; }
        public string Placeholder { get; set; }
    }
}
