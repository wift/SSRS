using System.Threading.Tasks;
using SSRSCore.ContentManagement.Display.ContentDisplay;
using SSRSCore.ContentManagement.Display.Models;
using SSRSCore.DisplayManagement.ModelBinding;
using SSRSCore.DisplayManagement.Views;
using SSRSCore.Forms.Models;
using SSRSCore.Forms.ViewModels;

namespace SSRSCore.Forms.Drivers
{
    public class LabelPartDisplay : ContentPartDisplayDriver<LabelPart>
    {
        public override IDisplayResult Display(LabelPart part)
        {
            return View("LabelPart", part).Location("Detail", "Content");
        }

        public override IDisplayResult Edit(LabelPart part, BuildPartEditorContext context)
        {
            return Initialize<LabelPartEditViewModel>("LabelPart_Fields_Edit", m =>
            {
                m.For = part.For;
            });
        }

        public override async Task<IDisplayResult> UpdateAsync(LabelPart part, IUpdateModel updater, UpdatePartEditorContext context)
        {
            var viewModel = new LabelPartEditViewModel();

            if (await updater.TryUpdateModelAsync(viewModel, Prefix))
            {
                part.For = viewModel.For?.Trim();
            }

            return Edit(part, context);
        }
    }
}
