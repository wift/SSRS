using System.Threading.Tasks;
using SSRSCore.ContentManagement.Display.ContentDisplay;
using SSRSCore.ContentManagement.Display.Models;
using SSRSCore.DisplayManagement.ModelBinding;
using SSRSCore.DisplayManagement.Views;
using SSRSCore.Forms.Models;
using SSRSCore.Forms.ViewModels;

namespace SSRSCore.Forms.Drivers
{
    public class FormPartDisplay : ContentPartDisplayDriver<FormPart>
    {
        public override IDisplayResult Edit(FormPart part, BuildPartEditorContext context)
        {
            return Initialize<FormPartEditViewModel>("FormPart_Fields_Edit", m =>
            {
                m.Action = part.Action;
                m.Method = part.Method;
                m.WorkflowTypeId = part.WorkflowTypeId;
            });
        }

        public async override Task<IDisplayResult> UpdateAsync(FormPart part, IUpdateModel updater)
        {
            var viewModel = new FormPartEditViewModel();

            if (await updater.TryUpdateModelAsync(viewModel, Prefix))
            {
                part.Action = viewModel.Action?.Trim();
                part.Method = viewModel.Method;
                part.WorkflowTypeId = viewModel.WorkflowTypeId;
            }

            return Edit(part);
        }
    }
}
