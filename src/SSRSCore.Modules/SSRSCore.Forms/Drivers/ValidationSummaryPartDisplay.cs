using SSRSCore.ContentManagement.Display.ContentDisplay;
using SSRSCore.DisplayManagement.Views;
using SSRSCore.Forms.Models;

namespace SSRSCore.Forms.Drivers
{
    public class ValidationSummaryPartDisplay : ContentPartDisplayDriver<ValidationSummaryPart>
    {
        public override IDisplayResult Display(ValidationSummaryPart part)
        {
            return View("ValidationSummaryPart", part).Location("Detail", "Content");
        }

        public override IDisplayResult Edit(ValidationSummaryPart part)
        {
            return View("ValidationSummaryPart_Fields_Edit", part);
        }
    }
}
