using System.Threading.Tasks;
using SSRSCore.ContentManagement.Display.ContentDisplay;
using SSRSCore.ContentManagement.Display.Models;
using SSRSCore.DisplayManagement.ModelBinding;
using SSRSCore.DisplayManagement.Views;
using SSRSCore.Forms.Models;
using SSRSCore.Forms.ViewModels;

namespace SSRSCore.Forms.Drivers
{
    public class ButtonPartDisplay : ContentPartDisplayDriver<ButtonPart>
    {
        public override IDisplayResult Display(ButtonPart part)
        {
            return View("ButtonPart", part).Location("Detail", "Content");
        }

        public override IDisplayResult Edit(ButtonPart part, BuildPartEditorContext context)
        {
            return Initialize<ButtonPartEditViewModel>("ButtonPart_Fields_Edit", m =>
            {
                m.Text = part.Text;
                m.Type = part.Type;
            });
        }

        public async override Task<IDisplayResult> UpdateAsync(ButtonPart part, IUpdateModel updater)
        {
            var viewModel = new ButtonPartEditViewModel();

            if (await updater.TryUpdateModelAsync(viewModel, Prefix))
            {
                part.Text = viewModel.Text?.Trim();
                part.Type = viewModel.Type?.Trim();
            }

            return Edit(part);
        }
    }
}
