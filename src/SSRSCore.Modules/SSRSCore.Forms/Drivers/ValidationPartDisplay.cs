using System.Threading.Tasks;
using SSRSCore.ContentManagement.Display.ContentDisplay;
using SSRSCore.ContentManagement.Display.Models;
using SSRSCore.DisplayManagement.ModelBinding;
using SSRSCore.DisplayManagement.Views;
using SSRSCore.Forms.Models;
using SSRSCore.Forms.ViewModels;

namespace SSRSCore.Forms.Drivers
{
    public class ValidationPartDisplay : ContentPartDisplayDriver<ValidationPart>
    {
        public override IDisplayResult Display(ValidationPart part)
        {
            return View("ValidationPart", part).Location("Detail", "Content");
        }

        public override IDisplayResult Edit(ValidationPart part, BuildPartEditorContext context)
        {
            return Initialize<ValidationPartEditViewModel>("ValidationPart_Fields_Edit", m =>
            {
                m.For = part.For;
            });
        }

        public async override Task<IDisplayResult> UpdateAsync(ValidationPart part, IUpdateModel updater)
        {
            var viewModel = new ValidationPartEditViewModel();

            if (await updater.TryUpdateModelAsync(viewModel, Prefix))
            {
                part.For = viewModel.For?.Trim();
            }

            return Edit(part);
        }
    }
}
