using System.Threading.Tasks;
using SSRSCore.ContentManagement.Display.ContentDisplay;
using SSRSCore.ContentManagement.Display.Models;
using SSRSCore.DisplayManagement.ModelBinding;
using SSRSCore.DisplayManagement.Views;
using SSRSCore.Forms.Models;
using SSRSCore.Forms.ViewModels;

namespace SSRSCore.Forms.Drivers
{
    public class FormInputElementPartDisplay : ContentPartDisplayDriver<FormInputElementPart>
    {
        public override IDisplayResult Edit(FormInputElementPart part, BuildPartEditorContext context)
        {
            return Initialize<FormInputElementPartEditViewModel>("FormInputElementPart_Fields_Edit", m =>
            {
                m.Name = part.Name;
            });
        }

        public async override Task<IDisplayResult> UpdateAsync(FormInputElementPart part, IUpdateModel updater)
        {
            var viewModel = new FormInputElementPartEditViewModel();

            if (await updater.TryUpdateModelAsync(viewModel, Prefix))
            {
                part.Name = viewModel.Name?.Trim();
                part.ContentItem.DisplayText = part.Name;
            }

            return Edit(part);
        }
    }
}
