using System.Threading.Tasks;
using SSRSCore.ContentManagement.Display.ContentDisplay;
using SSRSCore.ContentManagement.Display.Models;
using SSRSCore.DisplayManagement.ModelBinding;
using SSRSCore.DisplayManagement.Views;
using SSRSCore.Forms.Models;
using SSRSCore.Forms.ViewModels;

namespace SSRSCore.Forms.Drivers
{
    public class FormElementPartDisplay : ContentPartDisplayDriver<FormElementPart>
    {
        public override IDisplayResult Edit(FormElementPart part, BuildPartEditorContext context)
        {
            return Initialize<FormElementPartEditViewModel>("FormElementPart_Fields_Edit", m =>
            {
                m.Id = part.Id;
            });
        }

        public async override Task<IDisplayResult> UpdateAsync(FormElementPart part, IUpdateModel updater)
        {
            var viewModel = new FormElementPartEditViewModel();

            if (await updater.TryUpdateModelAsync(viewModel, Prefix))
            {
                part.Id = viewModel.Id?.Trim();
            }

            return Edit(part);
        }
    }
}
