using System.Threading.Tasks;
using SSRSCore.ContentManagement.Handlers;
using SSRSCore.Media.Services;

namespace SSRSCore.Media.Handlers
{
    /// <summary>
    /// Content Handler to delete files used on attached media fields once the content item is deleted.
    /// </summary>
    public class AttachedMediaFieldContentHandler : ContentHandlerBase
    {
        private readonly AttachedMediaFieldFileService _attachedMediaFieldFileService;

        public AttachedMediaFieldContentHandler(AttachedMediaFieldFileService attachedMediaFieldFileService)
        {
            _attachedMediaFieldFileService = attachedMediaFieldFileService;
        }

        public override async Task RemovedAsync(RemoveContentContext context)
        {
            if (context.NoActiveVersionLeft)
            {
                await _attachedMediaFieldFileService.DeleteContentItemFolderAsync(context.ContentItem);
            }
        }
    }
}
