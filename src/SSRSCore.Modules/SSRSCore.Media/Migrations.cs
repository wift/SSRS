using SSRSCore.ContentManagement.Metadata;
using SSRSCore.Data.Migration;
using SSRSCore.Media.Fields;
using SSRSCore.Media.Settings;

namespace SSRSCore.Media
{
    public class Migrations : DataMigration
    {
        private readonly IContentDefinitionManager _contentDefinitionManager;

        public Migrations(IContentDefinitionManager contentDefinitionManager)
        {
            _contentDefinitionManager = contentDefinitionManager;
        }

        // This migration does not need to run on new installations, but because there is no
        // initial migration record, there is no way to shortcut the Create migration.
        public int Create()
        {
            _contentDefinitionManager.MigrateFieldSettings<MediaField, MediaFieldSettings>();

            return 1;
        }
    }
}
