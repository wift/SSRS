using Microsoft.Extensions.DependencyInjection;
using SSRSCore.Apis;
using SSRSCore.Apis.GraphQL;
using SSRSCore.Media.Fields;
using SSRSCore.Modules;

namespace SSRSCore.Media.GraphQL
{
    [RequireFeatures("SSRSCore.Apis.GraphQL")]
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<ISchemaBuilder, MediaAssetQuery>();
            services.AddObjectGraphType<MediaField, MediaFieldQueryObjectType>();
            services.AddTransient<MediaAssetObjectType>();
        }
    }
}
