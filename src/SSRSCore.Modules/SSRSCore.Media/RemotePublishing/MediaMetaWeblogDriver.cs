using SSRSCore.ContentManagement;
using SSRSCore.XmlRpc;
using SSRSCore.XmlRpc.Models;
using SSRSCore.MetaWeblog;
using System;

namespace SSRSCore.Media.RemotePublishing
{
    public class MediaMetaWeblogDriver : MetaWeblogDriver
    {
        public override void SetCapabilities(Action<string, string> setCapability)
        {
            setCapability("supportsFileUpload", "Yes");
        }
    }
}
