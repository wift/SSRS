using SSRSCore.Media.Fields;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.Metadata.Models;

namespace SSRSCore.Media.ViewModels
{
    public class DisplayMediaFieldViewModel
    {
        public string[] Paths => Field.Paths;
        public MediaField Field { get; set; }
        public ContentPart Part { get; set; }
        public ContentPartFieldDefinition PartFieldDefinition { get; set; }
    }
}
