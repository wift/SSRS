using SSRSCore.ContentManagement;

namespace SSRSCore.Media.Fields
{
    public class MediaField : ContentField
    {
        public string[] Paths { get; set; }
    }
}
