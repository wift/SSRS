using Microsoft.AspNetCore.Mvc.ModelBinding;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.Metadata.Models;
using SSRSCore.Markdown.Models;

namespace SSRSCore.Markdown.ViewModels
{
    public class MarkdownBodyPartViewModel
    {
        public string Source { get; set; }
        public string Html { get; set; }

        [BindNever]
        public ContentItem ContentItem { get; set; }

        [BindNever]
        public MarkdownBodyPart MarkdownBodyPart { get; set; }

        [BindNever]
        public ContentTypePartDefinition TypePartDefinition { get; set; }
    }
}
