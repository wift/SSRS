﻿using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.Metadata.Models;
using SSRSCore.Markdown.Fields;

namespace SSRSCore.Markdown.ViewModels
{
    public class EditMarkdownFieldViewModel
    {
        public string Markdown { get; set; }
        public MarkdownField Field { get; set; }
        public ContentPart Part { get; set; }
        public ContentPartFieldDefinition PartFieldDefinition { get; set; }
    }
}
