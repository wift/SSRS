using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.Metadata.Models;
using SSRSCore.Markdown.Fields;

namespace SSRSCore.Markdown.ViewModels
{
    public class MarkdownFieldViewModel
    {
        public string Markdown { get; set; }
        public string Html { get; set; }
        public MarkdownField Field { get; set; }
        public ContentPart Part { get; set; }
        public ContentPartFieldDefinition PartFieldDefinition { get; set; }
    }
}
