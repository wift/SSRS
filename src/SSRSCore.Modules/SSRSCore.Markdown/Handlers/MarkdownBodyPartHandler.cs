using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Html;
using SSRSCore.ContentManagement.Handlers;
using SSRSCore.ContentManagement.Metadata;
using SSRSCore.ContentManagement.Models;
using SSRSCore.Markdown.Models;
using SSRSCore.Markdown.Settings;

namespace SSRSCore.Markdown.Handlers
{
    public class MarkdownBodyPartHandler : ContentPartHandler<MarkdownBodyPart>
    {
        private readonly IContentDefinitionManager _contentDefinitionManager;

        public MarkdownBodyPartHandler(IContentDefinitionManager contentDefinitionManager)
        {
            _contentDefinitionManager = contentDefinitionManager;
        }

        public override Task GetContentItemAspectAsync(ContentItemAspectContext context, MarkdownBodyPart part)
        {
            return context.ForAsync<BodyAspect>(bodyAspect =>
            {
                var contentTypeDefinition = _contentDefinitionManager.GetTypeDefinition(part.ContentItem.ContentType);
                var contentTypePartDefinition = contentTypeDefinition.Parts.FirstOrDefault(p => p.PartDefinition.Name == nameof(MarkdownBodyPart));
                var settings = contentTypePartDefinition.GetSettings<MarkdownBodyPartSettings>();
                var html = Markdig.Markdown.ToHtml(part.Markdown ?? "");

                bodyAspect.Body = new HtmlString(html);
                return Task.CompletedTask;
            });
        }
    }
}
