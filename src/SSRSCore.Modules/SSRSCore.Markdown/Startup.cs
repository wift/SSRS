using Fluid;
using Microsoft.Extensions.DependencyInjection;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.Display.ContentDisplay;
using SSRSCore.ContentManagement.Handlers;
using SSRSCore.ContentTypes.Editors;
using SSRSCore.Data.Migration;
using SSRSCore.Indexing;
using SSRSCore.Liquid;
using SSRSCore.Markdown.Drivers;
using SSRSCore.Markdown.Fields;
using SSRSCore.Markdown.Filters;
using SSRSCore.Markdown.Handlers;
using SSRSCore.Markdown.Indexing;
using SSRSCore.Markdown.Models;
using SSRSCore.Markdown.Settings;
using SSRSCore.Markdown.ViewModels;
using SSRSCore.Modules;

namespace SSRSCore.Markdown
{
    public class Startup : StartupBase
    {
        static Startup()
        {
            TemplateContext.GlobalMemberAccessStrategy.Register<MarkdownBodyPartViewModel>();
            TemplateContext.GlobalMemberAccessStrategy.Register<MarkdownFieldViewModel>();
        }

        public override void ConfigureServices(IServiceCollection services)
        {
            // Markdown Part
            services.AddContentPart<MarkdownBodyPart>();
            services.AddScoped<IContentPartDisplayDriver, MarkdownBodyPartDisplay>();
            services.AddScoped<IContentTypePartDefinitionDisplayDriver, MarkdownBodyPartSettingsDisplayDriver>();
            services.AddScoped<IDataMigration, Migrations>();
            services.AddScoped<IContentPartIndexHandler, MarkdownBodyPartIndexHandler>();
            services.AddScoped<IContentPartHandler, MarkdownBodyPartHandler>();

            // Markdown Field
            services.AddContentField<MarkdownField>();
            services.AddScoped<IContentFieldDisplayDriver, MarkdownFieldDisplayDriver>();
            services.AddScoped<IContentPartFieldDefinitionDisplayDriver, MarkdownFieldSettingsDriver>();
            services.AddScoped<IContentFieldIndexHandler, MarkdownFieldIndexHandler>();

            services.AddLiquidFilter<Markdownify>("markdownify");
        }
    }
}
