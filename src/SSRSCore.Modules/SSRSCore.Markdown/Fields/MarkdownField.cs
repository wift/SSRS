﻿using SSRSCore.ContentManagement;

namespace SSRSCore.Markdown.Fields
{
    public class MarkdownField : ContentField
    {
        public string Markdown { get; set; }
    }
}
