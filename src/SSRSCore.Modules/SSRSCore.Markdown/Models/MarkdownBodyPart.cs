using SSRSCore.ContentManagement;

namespace SSRSCore.Markdown.Models
{
    public class MarkdownBodyPart : ContentPart
    {
        public string Markdown { get; set; }
    }
}
