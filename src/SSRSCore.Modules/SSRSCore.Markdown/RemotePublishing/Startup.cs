using SSRSCore.Modules;
using Microsoft.Extensions.DependencyInjection;
using SSRSCore.MetaWeblog;

namespace SSRSCore.Markdown.RemotePublishing
{

    [RequireFeatures("SSRSCore.RemotePublishing")]
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IMetaWeblogDriver, MarkdownBodyMetaWeblogDriver>();
        }
    }
}
