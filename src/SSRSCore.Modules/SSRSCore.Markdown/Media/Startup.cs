using SSRSCore.Modules;
using Microsoft.Extensions.DependencyInjection;
using SSRSCore.DisplayManagement.Descriptors;

namespace SSRSCore.Markdown.Media
{
    [RequireFeatures("SSRSCore.Media")]
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IShapeTableProvider, MediaShapes>();
        }
    }
}
