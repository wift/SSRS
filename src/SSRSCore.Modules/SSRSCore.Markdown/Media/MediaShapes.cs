using SSRSCore.DisplayManagement;
using SSRSCore.DisplayManagement.Descriptors;

namespace SSRSCore.Markdown.Media
{
    public class MediaShapes : IShapeTableProvider
    {
        public void Discover(ShapeTableBuilder builder)
        {
            builder.Describe("MarkdownBodyPart_Edit")
                .OnDisplaying(displaying =>
                {
                    IShape editor = displaying.Shape;

                    if (editor.Metadata.Type == "MarkdownBodyPart_Edit__Wysiwyg")
                    {
                        editor.Metadata.Wrappers.Add("Media_Wrapper__MarkdownBodyPart");
                    }
                });

            builder.Describe("MarkdownField_Edit")
                .OnDisplaying(displaying =>
                {
                    IShape editor = displaying.Shape;

                    if (editor.Metadata.Type == "MarkdownField_Edit__Wysiwyg")
                    {
                        editor.Metadata.Wrappers.Add("Media_Wrapper__MarkdownBodyPart");
                    }
                });
        }
    }
}
