using Microsoft.Extensions.DependencyInjection;
using SSRSCore.Apis;
using SSRSCore.Markdown.Fields;
using SSRSCore.Markdown.Models;
using SSRSCore.Modules;

namespace SSRSCore.Markdown.GraphQL
{
    [RequireFeatures("SSRSCore.Apis.GraphQL")]
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddObjectGraphType<MarkdownBodyPart, MarkdownBodyQueryObjectType>();
            services.AddObjectGraphType<MarkdownField, MarkdownFieldQueryObjectType>();
        }
    }
}
