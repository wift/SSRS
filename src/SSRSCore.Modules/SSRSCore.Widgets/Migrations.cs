using SSRSCore.ContentManagement.Metadata.Settings;
using SSRSCore.ContentManagement.Metadata;
using SSRSCore.Data.Migration;

namespace SSRSCore.Widgets
{
    public class Migrations : DataMigration
    {
        IContentDefinitionManager _contentDefinitionManager;

        public Migrations(IContentDefinitionManager contentDefinitionManager)
        {
            _contentDefinitionManager = contentDefinitionManager;
        }

        public int Create()
        {
            _contentDefinitionManager.AlterPartDefinition("WidgetsListPart", builder => builder
                .Attachable()
                .WithDescription("Provides a way to add widgets per content items.")
                );

            return 1;
        }
    }
}