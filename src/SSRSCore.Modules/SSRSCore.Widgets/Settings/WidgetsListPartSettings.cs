﻿using System;

namespace SSRSCore.Widgets.Settings
{
    public class WidgetsListPartSettings
    {
        public string[] Zones { get; set; } = Array.Empty<string>();
    }
}
