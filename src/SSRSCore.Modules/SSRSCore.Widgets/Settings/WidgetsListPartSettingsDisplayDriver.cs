using System;
using System.Threading.Tasks;
using SSRSCore.Widgets.Models;
using SSRSCore.ContentManagement.Metadata.Models;
using SSRSCore.ContentTypes.Editors;
using SSRSCore.DisplayManagement.ModelBinding;
using SSRSCore.DisplayManagement.Views;

namespace SSRSCore.Widgets.Settings
{
    public class WidgetsListPartSettingsDisplayDriver : ContentTypePartDefinitionDisplayDriver
    {
        public override IDisplayResult Edit(ContentTypePartDefinition contentTypePartDefinition, IUpdateModel updater)
        {
            if (!String.Equals(nameof(WidgetsListPart), contentTypePartDefinition.PartDefinition.Name, StringComparison.Ordinal))
            {
                return null;
            }

            return Initialize<WidgetsListPartSettingsViewModel>("WidgetsPartSettings_Edit", model =>
            {
                var settings = contentTypePartDefinition.GetSettings<WidgetsListPartSettings>();

                model.Zones = String.Join(", ", settings.Zones);
                model.WidgetsListPartSettings = settings;
            }).Location("Content");
        }

        public override async Task<IDisplayResult> UpdateAsync(ContentTypePartDefinition contentTypePartDefinition, UpdateTypePartEditorContext context)
        {
            if (!String.Equals(nameof(WidgetsListPart), contentTypePartDefinition.PartDefinition.Name, StringComparison.Ordinal))
            {
                return null;
            }

            var model = new WidgetsListPartSettingsViewModel();

            if (await context.Updater.TryUpdateModelAsync(model, Prefix, m => m.Zones))
            {
                context.Builder.WithSettings(new WidgetsListPartSettings { Zones = (model.Zones ?? "").Split(new[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries) });
            }

            return Edit(contentTypePartDefinition, context.Updater);
        }
    }
}