﻿using SSRSCore.ContentManagement;

namespace SSRSCore.Widgets.Models
{
    public class WidgetMetadata : ContentPart
    {
        public bool RenderTitle { get; set; }
        public string Position { get; set; }
    }
}
