﻿using System.Threading.Tasks;
using SSRSCore.Autoroute.Models;
using SSRSCore.Indexing;

namespace SSRSCore.Autoroute.Indexing
{
    public class AutoroutePartIndexHandler : ContentPartIndexHandler<AutoroutePart>
    {
        public override Task BuildIndexAsync(AutoroutePart part, BuildPartIndexContext context)
        {
            var options = context.Settings.ToOptions() 
                & ~DocumentIndexOptions.Sanitize
                & ~DocumentIndexOptions.Analyze
                ;

            foreach (var key in context.Keys)
            {
                context.DocumentIndex.Set(key, part.Path, options);
            }

            return Task.CompletedTask;
        }
    }
}
