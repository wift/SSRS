using System;
using System.Linq;
using Fluid;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using SSRSCore.Autoroute.Drivers;
using SSRSCore.Autoroute.Handlers;
using SSRSCore.Autoroute.Indexing;
using SSRSCore.Autoroute.Liquid;
using SSRSCore.Autoroute.Models;
using SSRSCore.Autoroute.Routing;
using SSRSCore.Autoroute.Services;
using SSRSCore.Autoroute.Settings;
using SSRSCore.Autoroute.ViewModels;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.Display.ContentDisplay;
using SSRSCore.ContentManagement.GraphQL.Options;
using SSRSCore.ContentManagement.Handlers;
using SSRSCore.ContentManagement.Records;
using SSRSCore.ContentManagement.Routing;
using SSRSCore.ContentTypes.Editors;
using SSRSCore.Data.Migration;
using SSRSCore.Indexing;
using SSRSCore.Liquid;
using SSRSCore.Modules;
using SSRSCore.Routing;
using SSRSCore.Security.Permissions;
using YesSql;
using YesSql.Indexes;

namespace SSRSCore.Autoroute
{
    public class Startup : StartupBase
    {
        public override int ConfigureOrder => -100;

        static Startup()
        {
            TemplateContext.GlobalMemberAccessStrategy.Register<AutoroutePartViewModel>();
        }

        public override void ConfigureServices(IServiceCollection services)
        {
            // Autoroute Part
            services.AddContentPart<AutoroutePart>();
            services.AddScoped<IContentPartDisplayDriver, AutoroutePartDisplay>();
            services.AddScoped<IPermissionProvider, Permissions>();
            services.AddScoped<IContentPartHandler, AutoroutePartHandler>();
            services.AddScoped<IContentTypePartDefinitionDisplayDriver, AutoroutePartSettingsDisplayDriver>();
            services.AddScoped<IContentPartIndexHandler, AutoroutePartIndexHandler>();

            services.AddSingleton<IIndexProvider, AutoroutePartIndexProvider>();
            services.AddScoped<IDataMigration, Migrations>();

            services.AddSingleton<IAutorouteEntries, AutorouteEntries>();
            services.AddScoped<IContentAliasProvider, AutorouteAliasProvider>();

            services.AddScoped<ILiquidTemplateEventHandler, ContentAutorouteLiquidTemplateEventHandler>();

            services.Configure<GraphQLContentOptions>(options =>
            {
                options.ConfigurePart<AutoroutePart>(partOptions =>
                {
                    partOptions.Collapse = true;
                });
            });

            services.AddSingleton<AutoRouteTransformer>();
            services.AddSingleton<IShellRouteValuesAddressScheme, AutoRouteValuesAddressScheme>();
        }

        public override void Configure(IApplicationBuilder app, IEndpointRouteBuilder routes, IServiceProvider serviceProvider)
        {
            var entries = serviceProvider.GetRequiredService<IAutorouteEntries>();
            var session = serviceProvider.GetRequiredService<ISession>();

            var autoroutes = session.QueryIndex<AutoroutePartIndex>(o => o.Published).ListAsync().GetAwaiter().GetResult();
            entries.AddEntries(autoroutes.Select(x => new AutorouteEntry { ContentItemId = x.ContentItemId, Path = x.Path }));

            // The 1st segment prevents the transformer to be executed for the home.
            routes.MapDynamicControllerRoute<AutoRouteTransformer>("/{any}/{**slug}");
        }
    }
}
