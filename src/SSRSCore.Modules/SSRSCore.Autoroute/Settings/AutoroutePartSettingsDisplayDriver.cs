using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Localization;
using SSRSCore.Autoroute.Models;
using SSRSCore.Autoroute.ViewModels;
using SSRSCore.ContentManagement.Metadata.Models;
using SSRSCore.ContentTypes.Editors;
using SSRSCore.DisplayManagement.ModelBinding;
using SSRSCore.DisplayManagement.Views;
using SSRSCore.Liquid;

namespace SSRSCore.Autoroute.Settings
{
    public class AutoroutePartSettingsDisplayDriver : ContentTypePartDefinitionDisplayDriver
    {
        private readonly ILiquidTemplateManager _templateManager;

        public AutoroutePartSettingsDisplayDriver(ILiquidTemplateManager templateManager, IStringLocalizer<AutoroutePartSettingsDisplayDriver> localizer)
        {
            _templateManager = templateManager;
            T = localizer;
        }

        public IStringLocalizer T { get; private set; }

        public override IDisplayResult Edit(ContentTypePartDefinition contentTypePartDefinition, IUpdateModel updater)
        {
            if (!String.Equals(nameof(AutoroutePart), contentTypePartDefinition.PartDefinition.Name, StringComparison.Ordinal))
            {
                return null;
            }

            return Initialize<AutoroutePartSettingsViewModel>("AutoroutePartSettings_Edit", model =>
            {
                var settings = contentTypePartDefinition.GetSettings<AutoroutePartSettings>();

                model.AllowCustomPath = settings.AllowCustomPath;
                model.AllowUpdatePath = settings.AllowUpdatePath;
                model.Pattern = settings.Pattern;
                model.ShowHomepageOption = settings.ShowHomepageOption;
                model.AutoroutePartSettings = settings;
            }).Location("Content");
        }

        public override async Task<IDisplayResult> UpdateAsync(ContentTypePartDefinition contentTypePartDefinition, UpdateTypePartEditorContext context)
        {
            if (!String.Equals(nameof(AutoroutePart), contentTypePartDefinition.PartDefinition.Name, StringComparison.Ordinal))
            {
                return null;
            }

            var model = new AutoroutePartSettingsViewModel();

            await context.Updater.TryUpdateModelAsync(model, Prefix, 
                m => m.Pattern,
                m => m.AllowCustomPath,
                m => m.AllowUpdatePath,
                m => m.ShowHomepageOption);

            if (!string.IsNullOrEmpty(model.Pattern) && !_templateManager.Validate(model.Pattern, out var errors))
            {
                context.Updater.ModelState.AddModelError(nameof(model.Pattern), T["Pattern doesn't contain a valid Liquid expression. Details: {0}", string.Join(" ", errors)]);
            } else {
                context.Builder.WithSettings(new AutoroutePartSettings
                {
                    Pattern = model.Pattern,
                    AllowCustomPath = model.AllowCustomPath,
                    AllowUpdatePath = model.AllowUpdatePath,
                    ShowHomepageOption = model.ShowHomepageOption
                });
            }

            return Edit(contentTypePartDefinition, context.Updater);
        }
    }
}