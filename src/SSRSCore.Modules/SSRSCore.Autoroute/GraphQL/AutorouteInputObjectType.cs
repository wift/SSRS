using GraphQL.Types;
using Microsoft.Extensions.Localization;
using SSRSCore.Apis.GraphQL.Queries;
using SSRSCore.Autoroute.Models;

namespace SSRSCore.Autoroute.GraphQL
{
    public class AutorouteInputObjectType : WhereInputObjectGraphType<AutoroutePart>
    {
        public AutorouteInputObjectType(IStringLocalizer<AutorouteInputObjectType> T)
        {
            Name = "AutoroutePartInput";
            Description = T["the custom URL part of the content item"];

            AddScalarFilterFields<StringGraphType>("path", T["the path of the content item to filter"]);
        }
    }
}
