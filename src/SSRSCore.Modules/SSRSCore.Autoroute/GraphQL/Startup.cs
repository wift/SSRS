using Microsoft.Extensions.DependencyInjection;
using SSRSCore.Apis;
using SSRSCore.Autoroute.Models;
using SSRSCore.ContentManagement.GraphQL;
using SSRSCore.ContentManagement.GraphQL.Queries;
using SSRSCore.Modules;

namespace SSRSCore.Autoroute.GraphQL
{
    [RequireFeatures("SSRSCore.Apis.GraphQL")]
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddInputObjectGraphType<AutoroutePart, AutorouteInputObjectType>();
            services.AddObjectGraphType<AutoroutePart, AutorouteQueryObjectType>();
            services.AddTransient<IIndexAliasProvider, AutoroutePartIndexAliasProvider>();
        }
    }
}
