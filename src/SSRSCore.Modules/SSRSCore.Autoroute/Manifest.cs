using SSRSCore.Modules.Manifest;

[assembly: Module(
    Name = "Autoroute",
    Author = "The Orchard Team",
    Website = "https://orchardproject.net",
    Version = "2.0.0",
    Dependencies = new [] { "SSRSCore.ContentTypes" },
    Category = "Navigation"
)]
