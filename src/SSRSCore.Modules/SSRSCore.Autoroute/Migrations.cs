using SSRSCore.Autoroute.Drivers;
using SSRSCore.Autoroute.Models;
using SSRSCore.ContentManagement.Metadata;
using SSRSCore.ContentManagement.Metadata.Settings;
using SSRSCore.ContentManagement.Records;
using SSRSCore.Data.Migration;

namespace SSRSCore.Autoroute
{
    public class Migrations : DataMigration
    {
        IContentDefinitionManager _contentDefinitionManager;

        public Migrations(IContentDefinitionManager contentDefinitionManager)
        {
            _contentDefinitionManager = contentDefinitionManager;
        }

        public int Create()
        {
            _contentDefinitionManager.AlterPartDefinition("AutoroutePart", builder => builder
                .Attachable()
                .WithDescription("Provides a custom url for your content item."));

            SchemaBuilder.CreateMapIndexTable(nameof(AutoroutePartIndex), table => table
                .Column<string>("ContentItemId", c => c.WithLength(26))
                .Column<string>("Path", col => col.WithLength(AutoroutePartDisplay.MaxPathLength))
                .Column<bool>("Published")
            );

            SchemaBuilder.AlterTable(nameof(AutoroutePartIndex), table => table
                .CreateIndex("IDX_AutoroutePartIndex_ContentItemId", "ContentItemId")
            );

            // Return 2 to shortcut the second migration on new content definition schemas.
            return 2;
        }

        // Migrate PartSettings. This only needs to run on old content definition schemas.
        // This code can be removed in a later version.
        public int UpdateFrom1()
        {
            _contentDefinitionManager.MigratePartSettings<AutoroutePart, AutoroutePartSettings>();

            return 2;
        }
    }
}