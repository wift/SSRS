using Microsoft.Extensions.DependencyInjection;
using SSRSCore.Deployment;
using SSRSCore.Modules;
using SSRSCore.Navigation;
using SSRSCore.Recipes.RecipeSteps;
using SSRSCore.Recipes.Services;

namespace SSRSCore.Recipes
{
    /// <summary>
    /// These services are registered on the tenant service collection
    /// </summary>
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddRecipes();

            services.AddScoped<INavigationProvider, AdminMenu>();

            services.AddRecipeExecutionStep<CommandStep>();
            services.AddRecipeExecutionStep<RecipesStep>();

            services.AddDeploymentTargetHandler<RecipeDeploymentTargetHandler>();
        }
    }
}
