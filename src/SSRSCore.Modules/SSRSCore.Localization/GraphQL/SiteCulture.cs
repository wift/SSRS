namespace SSRSCore.Localization.GraphQL
{
    public class SiteCulture
    {
        public string Culture { get; set; }
        public bool IsDefault { get; set; }
    }
}
