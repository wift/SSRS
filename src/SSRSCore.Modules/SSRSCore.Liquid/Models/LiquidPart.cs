using SSRSCore.ContentManagement;

namespace SSRSCore.Liquid.Models
{
    public class LiquidPart : ContentPart
    {
        public string Liquid { get; set; }
    }
}
