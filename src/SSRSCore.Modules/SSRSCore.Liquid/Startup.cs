using Fluid;
using Fluid.Values;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Linq;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.Display.ContentDisplay;
using SSRSCore.ContentManagement.Handlers;
using SSRSCore.Data.Migration;
using SSRSCore.DisplayManagement.Descriptors;
using SSRSCore.DisplayManagement.Views;
using SSRSCore.Indexing;
using SSRSCore.Liquid.Drivers;
using SSRSCore.Liquid.Filters;
using SSRSCore.Liquid.Handlers;
using SSRSCore.Liquid.Indexing;
using SSRSCore.Liquid.Models;
using SSRSCore.Liquid.Services;
using SSRSCore.Modules;

namespace SSRSCore.Liquid
{
    public class Startup : StartupBase
    {
        static Startup()
        {
            TemplateContext.GlobalMemberAccessStrategy.Register<ContentItem>();
            TemplateContext.GlobalMemberAccessStrategy.Register<ContentElement>();
            TemplateContext.GlobalMemberAccessStrategy.Register<ShapeViewModel<ContentItem>>();

            // When accessing a property of a JObject instance
            TemplateContext.GlobalMemberAccessStrategy.Register<JObject, object>((obj, name) => obj[name]);

            // Prevent JTokens from being converted to an ArrayValue as they implement IEnumerable
            FluidValue.SetTypeMapping<JObject>(o => new ObjectValue(o));
            FluidValue.SetTypeMapping<JValue>(o => FluidValue.Create(((JValue)o).Value));
            FluidValue.SetTypeMapping<System.DateTime>(o => new ObjectValue(o));
        }

        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<ISlugService, SlugService>();
            services.AddScoped<ILiquidTemplateManager, LiquidTemplateManager>();

            services.AddLiquidFilter<TimeZoneFilter>("local");
            services.AddLiquidFilter<SlugifyFilter>("slugify");
            services.AddLiquidFilter<ContainerFilter>("container");
            services.AddLiquidFilter<DisplayTextFilter>("display_text");
            services.AddLiquidFilter<DisplayUrlFilter>("display_url");
            services.AddLiquidFilter<ContentUrlFilter>("href");
            services.AddLiquidFilter<AbsoluteUrlFilter>("absolute_url");
            services.AddLiquidFilter<LiquidFilter>("liquid");
            services.AddLiquidFilter<JsonFilter>("json");
        }
    }

    [RequireFeatures("SSRSCore.Contents")]
    public class LiquidPartStartup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            // Liquid Part
            services.AddScoped<IContentPartDisplayDriver, LiquidPartDisplay>();
            services.AddScoped<IShapeTableProvider, LiquidShapes>();
            services.AddContentPart<LiquidPart>();
            services.AddScoped<IDataMigration, Migrations>();
            services.AddScoped<IContentPartIndexHandler, LiquidPartIndexHandler>();
            services.AddScoped<IContentPartHandler, LiquidPartHandler>();
        }
    }
}