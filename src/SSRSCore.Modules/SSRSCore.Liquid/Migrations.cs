using SSRSCore.ContentManagement.Metadata.Settings;
using SSRSCore.ContentManagement.Metadata;
using SSRSCore.Data.Migration;

namespace SSRSCore.Liquid
{
    public class Migrations : DataMigration
    {
        IContentDefinitionManager _contentDefinitionManager;

        public Migrations(IContentDefinitionManager contentDefinitionManager)
        {
            _contentDefinitionManager = contentDefinitionManager;
        }

        public int Create()
        {
            _contentDefinitionManager.AlterPartDefinition("LiquidPart", builder => builder
                .Attachable()
                .WithDescription("Provides a Liquid formatted body for your content item."));

            return 1;
        }
    }
}