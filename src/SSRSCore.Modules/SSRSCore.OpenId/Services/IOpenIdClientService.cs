using System.Collections.Immutable;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using SSRSCore.OpenId.Settings;

namespace SSRSCore.OpenId.Services
{
    public interface IOpenIdClientService
    {
        Task<OpenIdClientSettings> GetSettingsAsync();
        Task UpdateSettingsAsync(OpenIdClientSettings settings);
        Task<ImmutableArray<ValidationResult>> ValidateSettingsAsync(OpenIdClientSettings settings);
    }
}
