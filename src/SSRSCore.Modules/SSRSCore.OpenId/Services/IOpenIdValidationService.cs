using System.Collections.Immutable;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using SSRSCore.OpenId.Settings;

namespace SSRSCore.OpenId.Services
{
    public interface IOpenIdValidationService
    {
        Task<OpenIdValidationSettings> GetSettingsAsync();
        Task UpdateSettingsAsync(OpenIdValidationSettings settings);
        Task<ImmutableArray<ValidationResult>> ValidateSettingsAsync(OpenIdValidationSettings settings);
    }
}
