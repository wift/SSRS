﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SSRSCore.OpenId.ViewModels
{
    public class ErrorViewModel
    {
        public string Error { get; set; }
        
        public string ErrorDescription { get; set; }
    }
}
