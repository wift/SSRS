using SSRSCore.ContentManagement;

namespace SSRSCore.Taxonomies.Models
{
    // This part is added automatically to all terms
    public class TermPart : ContentPart
    {
        public string Handle { get; set; }
    }
}
