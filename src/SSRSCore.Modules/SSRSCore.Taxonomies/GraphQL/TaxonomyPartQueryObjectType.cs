using System.Collections.Generic;
using GraphQL.Types;
using SSRSCore.Apis.GraphQL;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.GraphQL.Queries.Types;
using SSRSCore.Taxonomies.Models;

namespace SSRSCore.Taxonomies.GraphQL
{
    public class TaxonomyPartQueryObjectType : ObjectGraphType<TaxonomyPart>
    {
        public TaxonomyPartQueryObjectType()
        {
            Name = "TaxonomyPart";

            Field(x => x.TermContentType);

            Field<ListGraphType<ContentItemInterface>, IEnumerable<ContentItem>>()
                .Name("contentItems")
                .Description("the content items")
                .PagingArguments()
                .Resolve(x => x.Page(x.Source.Terms));
        }
    }
}
