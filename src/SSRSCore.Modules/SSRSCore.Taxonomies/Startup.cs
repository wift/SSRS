using Fluid;
using Microsoft.Extensions.DependencyInjection;
using SSRSCore.Apis;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.Display.ContentDisplay;
using SSRSCore.ContentTypes.Editors;
using SSRSCore.Data;
using SSRSCore.Data.Migration;
using SSRSCore.Indexing;
using SSRSCore.Liquid;
using SSRSCore.Modules;
using SSRSCore.Security.Permissions;
using SSRSCore.Taxonomies.Drivers;
using SSRSCore.Taxonomies.Fields;
using SSRSCore.Taxonomies.GraphQL;
using SSRSCore.Taxonomies.Indexing;
using SSRSCore.Taxonomies.Liquid;
using SSRSCore.Taxonomies.Models;
using SSRSCore.Taxonomies.Settings;
using SSRSCore.Taxonomies.ViewModels;

namespace SSRSCore.Taxonomies
{
    public class Startup : StartupBase
    {
        static Startup()
        {
            // Registering both field types and shape types are necessary as they can 
            // be accessed from inner properties.

            TemplateContext.GlobalMemberAccessStrategy.Register<TaxonomyField>();
            TemplateContext.GlobalMemberAccessStrategy.Register<DisplayTaxonomyFieldViewModel>();
        }

        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IDataMigration, Migrations>();
            services.AddScoped<IPermissionProvider, Permissions>();

            // Taxonomy Part
            services.AddScoped<IContentPartDisplayDriver, TaxonomyPartDisplayDriver>();
            services.AddContentPart<TaxonomyPart>();

            // Taxonomy Field
            services.AddContentField<TaxonomyField>();
            services.AddScoped<IContentFieldDisplayDriver, TaxonomyFieldDisplayDriver>();
            services.AddScoped<IContentPartFieldDefinitionDisplayDriver, TaxonomyFieldSettingsDriver>();
            services.AddScoped<IContentFieldIndexHandler, TaxonomyFieldIndexHandler>();

            services.AddScoped<IScopedIndexProvider, TaxonomyIndexProvider>();
        }
    }

    [RequireFeatures("SSRSCore.Liquid")]
    public class LiquidStartup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddLiquidFilter<TaxonomyTermsFilter>("taxonomy_terms");
            services.AddLiquidFilter<InheritedTermsFilter>("inherited_terms");
        }
    }

    [RequireFeatures("SSRSCore.Apis.GraphQL")]
    public class GraphQLStartup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddObjectGraphType<TaxonomyPart, TaxonomyPartQueryObjectType>();
            services.AddObjectGraphType<TaxonomyField, TaxonomyFieldQueryObjectType>();
        }
    }
}
