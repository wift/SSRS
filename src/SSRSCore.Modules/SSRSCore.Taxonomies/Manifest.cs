using SSRSCore.Modules.Manifest;

[assembly: Module(
    Name = "Taxonomies",
    Author = "The Orchard Team",
    Website = "https://orchardproject.net",
    Version = "2.0.0",
    Description = "The taxonomies modules provides a way to categorize content items.",
    Dependencies = new [] { "SSRSCore.ContentTypes" },
    Category = "Content Management"
)]
