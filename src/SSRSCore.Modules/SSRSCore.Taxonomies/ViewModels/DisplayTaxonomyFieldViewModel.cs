using SSRSCore.Taxonomies.Fields;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.Metadata.Models;

namespace SSRSCore.Taxonomies.ViewModels
{
    public class DisplayTaxonomyFieldViewModel
    {
        public string TaxonomyContentItemId => Field.TaxonomyContentItemId;
        public string[] TermContentItemIds => Field.TermContentItemIds;
        public TaxonomyField Field { get; set; }
        public ContentPart Part { get; set; }
        public ContentPartFieldDefinition PartFieldDefinition { get; set; }
    }
}
