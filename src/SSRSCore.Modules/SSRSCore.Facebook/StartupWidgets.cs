using Microsoft.Extensions.DependencyInjection;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.Display.ContentDisplay;
using SSRSCore.ContentManagement.Handlers;
using SSRSCore.ContentTypes.Editors;
using SSRSCore.Data.Migration;
using SSRSCore.DisplayManagement.Descriptors;
using SSRSCore.Facebook.Widgets;
using SSRSCore.Facebook.Widgets.Drivers;
using SSRSCore.Facebook.Widgets.Handlers;
using SSRSCore.Facebook.Widgets.Models;
using SSRSCore.Facebook.Widgets.Services;
using SSRSCore.Facebook.Widgets.Settings;
using SSRSCore.Modules;

namespace SSRSCore.Facebook
{
    [Feature(FacebookConstants.Features.Widgets)]
    public class StartupWidgets : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IDataMigration, WidgetMigrations>();
            services.AddScoped<IShapeTableProvider, LiquidShapes>();

            services.AddScoped<IContentPartDisplayDriver, FacebookPluginPartDisplayDriver>();
            services.AddContentPart<FacebookPluginPart>();
            services.AddScoped<IContentPartHandler, FacebookPluginPartHandler>();
            services.AddScoped<IContentTypePartDefinitionDisplayDriver, FacebookPluginPartSettingsDisplayDriver>();
        }
    }

}
