using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using SSRSCore.DisplayManagement.Handlers;
using SSRSCore.Facebook.Drivers;
using SSRSCore.Facebook.Filters;
using SSRSCore.Facebook.Services;
using SSRSCore.Modules;
using SSRSCore.Navigation;
using SSRSCore.ResourceManagement;
using SSRSCore.Security.Permissions;
using SSRSCore.Settings;

namespace SSRSCore.Facebook
{
    public class Startup : StartupBase
    {
        public override void Configure(IApplicationBuilder builder, IEndpointRouteBuilder routes, IServiceProvider serviceProvider)
        {
            builder.UseMiddleware<ScriptsMiddleware>();
        }

        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IPermissionProvider, Permissions>();
            services.AddScoped<INavigationProvider, AdminMenu>();

            services.AddSingleton<IFacebookService, FacebookService>();
            services.AddScoped<IDisplayDriver<ISite>, FacebookSettingsDisplayDriver>();

            services.AddScoped<IResourceManifestProvider, ResourceManifest>();

            services.Configure<MvcOptions>((options) =>
            {
                options.Filters.Add(typeof(FBInitFilter));
            });

        }
    }
}
