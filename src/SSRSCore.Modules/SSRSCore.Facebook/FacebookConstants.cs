namespace SSRSCore.Facebook
{
    public static class FacebookConstants
    {
        public static class Features
        {
            public const string Widgets = "SSRSCore.Facebook.Widgets";
            public const string Login = "SSRSCore.Facebook.Login";
            public const string Core = "SSRSCore.Facebook";
        }
    }
}
