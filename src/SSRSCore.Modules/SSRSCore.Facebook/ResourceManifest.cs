using SSRSCore.ResourceManagement;

namespace SSRSCore.Facebook
{
    public class ResourceManifest : IResourceManifestProvider
    {
        public void BuildManifests(IResourceManifestBuilder builder)
        {
            var manifest = builder.Add();

            manifest
                .DefineScript("fb")
                .SetDependencies("fbsdk")
                .SetUrl("~/SSRSCore.Facebook/sdk/fb.js");

            manifest
                .DefineScript("fbsdk")
                .SetUrl("~/SSRSCore.Facebook/sdk/fbsdk.js");

        }
    }
}