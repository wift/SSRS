using Microsoft.AspNetCore.Mvc.ModelBinding;
using SSRSCore.Facebook.Widgets.Models;
using System.ComponentModel.DataAnnotations;
using SSRSCore.Facebook.Widgets.Settings;
using SSRSCore.ContentManagement;

namespace SSRSCore.Facebook.Widgets.ViewModels
{
    public class FacebookPluginPartViewModel
    {
        public string Liquid { get; set; }
        public string Html { get; set; }
        [BindNever]
        public FacebookPluginPartSettings Settings { get; set; }
        [BindNever]
        public FacebookPluginPart FacebookPluginPart { get; set; }
        [BindNever]
        public ContentItem ContentItem { get; set; }
    }
}
