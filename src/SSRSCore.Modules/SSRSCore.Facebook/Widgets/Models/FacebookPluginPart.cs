using System;
using System.Collections.Generic;
using System.Text;
using SSRSCore.ContentManagement;

namespace SSRSCore.Facebook.Widgets.Models
{
    public class FacebookPluginPart: ContentPart
    {
        public string Liquid { get; set; }
    }
}
