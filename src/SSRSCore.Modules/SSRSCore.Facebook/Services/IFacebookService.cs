using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using SSRSCore.Facebook.Settings;

namespace SSRSCore.Facebook.Services
{
    public interface IFacebookService
    {
        Task<FacebookSettings> GetSettingsAsync();
        Task UpdateSettingsAsync(FacebookSettings settings);
        Task<IEnumerable<ValidationResult>> ValidateSettingsAsync(FacebookSettings settings);
    }
}
