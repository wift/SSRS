using Microsoft.AspNetCore.Http;

namespace SSRSCore.Facebook.Login.Settings
{
    public class FacebookLoginSettings
    {
        public PathString CallbackPath { get; set; }
    }
}
