using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using SSRSCore.Facebook.Login.Settings;

namespace SSRSCore.Facebook.Login.Services
{
    public interface IFacebookLoginService
    {
        Task<FacebookLoginSettings> GetSettingsAsync();
        Task UpdateSettingsAsync(FacebookLoginSettings settings);
        Task<IEnumerable<ValidationResult>> ValidateSettingsAsync(FacebookLoginSettings settings);
    }
}
