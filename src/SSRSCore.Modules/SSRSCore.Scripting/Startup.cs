using SSRSCore.Modules;
using Microsoft.Extensions.DependencyInjection;
using SSRSCore.Scripting.JavaScript;
using SSRSCore.Scripting.Providers;

namespace SSRSCore.Scripting
{
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddScripting();
            services.AddJavaScriptEngine();
            services.AddSingleton<IGlobalMethodProvider, LogProvider>();
        }
    }
}
