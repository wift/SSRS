using SSRSCore.Deployment;

namespace SSRSCore.CustomSettings.Deployment
{
    public class CustomSettingsDeploymentStep : DeploymentStep
    {
        public CustomSettingsDeploymentStep()
        {
            Name = "CustomSettings";
        }

        public bool IncludeAll { get; set; } = true;

        public string[] SettingsTypeNames { get; set; }
    }
}