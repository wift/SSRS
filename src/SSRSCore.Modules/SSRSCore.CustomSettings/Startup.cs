using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.DependencyInjection;
using SSRSCore.CustomSettings.Deployment;
using SSRSCore.CustomSettings.Drivers;
using SSRSCore.CustomSettings.Recipes;
using SSRSCore.CustomSettings.Services;
using SSRSCore.Deployment;
using SSRSCore.DisplayManagement.Handlers;
using SSRSCore.Navigation;
using SSRSCore.Modules;
using SSRSCore.Recipes;
using SSRSCore.Security.Permissions;
using SSRSCore.Settings;

namespace SSRSCore.CustomSettings
{
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<INavigationProvider, AdminMenu>();
            services.AddScoped<IDisplayDriver<ISite>, CustomSettingsDisplayDriver>();
            services.AddScoped<CustomSettingsService>();

            // Permissions
            services.AddScoped<IPermissionProvider, Permissions>();
            services.AddScoped<IAuthorizationHandler, CustomSettingsAuthorizationHandler>();

            services.AddRecipeExecutionStep<CustomSettingsStep>();
        }
    }

    [RequireFeatures("SSRSCore.Deployment")]
    public class DeploymentStartup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<IDeploymentSource, CustomSettingsDeploymentSource>();
            services.AddSingleton<IDeploymentStepFactory>(new DeploymentStepFactory<CustomSettingsDeploymentStep>());
            services.AddScoped<IDisplayDriver<DeploymentStep>, CustomSettingsDeploymentStepDriver>();
        }
    }
}