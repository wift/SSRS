using SSRSCore.Modules;
using Microsoft.Extensions.DependencyInjection;
using SSRSCore.Deployment;
using SSRSCore.DisplayManagement;
using SSRSCore.DisplayManagement.Handlers;
using SSRSCore.Navigation;
using SSRSCore.Liquid;
using SSRSCore.Queries.Deployment;
using SSRSCore.Queries.Drivers;
using SSRSCore.Queries.Liquid;
using SSRSCore.Queries.Recipes;
using SSRSCore.Queries.Services;
using SSRSCore.Recipes;
using SSRSCore.Security.Permissions;
using SSRSCore.Scripting;

namespace SSRSCore.Queries
{
    /// <summary>
    /// These services are registered on the tenant service collection
    /// </summary>
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<INavigationProvider, AdminMenu>();
            services.AddScoped<IQueryManager, QueryManager>();
            services.AddScoped<IDisplayManager<Query>, DisplayManager<Query>>();

            services.AddScoped<IDisplayDriver<Query>, QueryDisplayDriver>();
            services.AddRecipeExecutionStep<QueryStep>();
            services.AddScoped<IPermissionProvider, Permissions>();


            services.AddTransient<IDeploymentSource, AllQueriesDeploymentSource>();
            services.AddSingleton<IDeploymentStepFactory>(new DeploymentStepFactory<AllQueriesDeploymentStep>());
            services.AddScoped<IDisplayDriver<DeploymentStep>, AllQueriesDeploymentStepDriver>();
            services.AddSingleton<IGlobalMethodProvider, QueryGlobalMethodProvider>();
        }
    }


    [RequireFeatures("SSRSCore.Liquid")]
    public class LiquidStartup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<ILiquidTemplateEventHandler, QueriesLiquidTemplateEventHandler>();

            services.AddLiquidFilter<QueryFilter>("query");
        }
    }
}
