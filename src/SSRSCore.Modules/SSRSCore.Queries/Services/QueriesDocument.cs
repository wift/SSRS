using System;
using System.Collections.Generic;

namespace SSRSCore.Queries.Services
{
    public class QueriesDocument
    {
        public Dictionary<string, Query> Queries { get; set; } = new Dictionary<string, Query>(StringComparer.OrdinalIgnoreCase);
    }
}
