using Microsoft.Extensions.DependencyInjection;
using SSRSCore.Apis.GraphQL;
using SSRSCore.Modules;
using SSRSCore.Queries.Sql.GraphQL.Queries;

namespace SSRSCore.Queries.Sql.GraphQL
{
    /// <summary>
    /// These services are registered on the tenant service collection
    /// </summary>
    [Feature("SSRSCore.Queries.Sql")]
    [RequireFeatures("SSRSCore.Apis.GraphQL")]
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<ISchemaBuilder, SqlQueryFieldTypeProvider>();
        }
    }
}
