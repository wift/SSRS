using System;

namespace SSRSCore.Queries.Sql
{
    public class SqlParserException : Exception
    {
        public SqlParserException(string message) : base(message)
        {

        }
    }
}
