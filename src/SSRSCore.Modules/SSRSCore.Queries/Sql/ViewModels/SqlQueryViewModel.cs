using System.ComponentModel.DataAnnotations;

namespace SSRSCore.Queries.Sql.ViewModels
{
    public class SqlQueryViewModel
    {
        public string Query { get; set; }

        public bool ReturnDocuments { get; set; }
    }
}
