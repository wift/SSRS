using System;
using Microsoft.AspNetCore.Builder;
using SSRSCore.Modules;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using SSRSCore.DisplayManagement.Handlers;
using SSRSCore.Navigation;
using SSRSCore.Queries.Sql.Drivers;
using SSRSCore.Security.Permissions;

namespace SSRSCore.Queries.Sql
{
    /// <summary>
    /// These services are registered on the tenant service collection
    /// </summary>
    [Feature("SSRSCore.Queries.Sql")]
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IPermissionProvider, Permissions>();
            services.AddScoped<IDisplayDriver<Query>, SqlQueryDisplayDriver>();
            services.AddScoped<IQuerySource, SqlQuerySource>();
            services.AddScoped<INavigationProvider, AdminMenu>();
        }
    }
}
