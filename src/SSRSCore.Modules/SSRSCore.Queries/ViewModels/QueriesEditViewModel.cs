using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace SSRSCore.Queries.ViewModels
{
    public class QueriesEditViewModel : QueriesCreateViewModel
    {
        public string Name { get; set; }
        public string Schema { get; set; }
    }
}
