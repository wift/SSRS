using SSRSCore.Modules.Manifest;

[assembly: Module(
    Name = "Queries",
    Author = "The Orchard Team",
    Website = "https://orchardproject.net",
    Version = "2.0.0"
)]

[assembly: Feature(
    Id = "SSRSCore.Queries",
    Name = "Queries",
    Description = "Provides querying capabilities.",
    Dependencies = new [] { "SSRSCore.Liquid" },
    Category = "Content Management"
)]

[assembly: Feature(
    Id = "SSRSCore.Queries.Sql",
    Name = "SQL Queries",
    Description = "Introduces a way to create custom Queries in pure SQL.",
    Dependencies = new [] { "SSRSCore.Queries" },
    Category = "Content Management"
)]
