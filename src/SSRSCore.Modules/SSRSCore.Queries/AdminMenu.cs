using Microsoft.Extensions.Localization;
using SSRSCore.Navigation;
using System;
using System.Threading.Tasks;

namespace SSRSCore.Queries
{
    public class AdminMenu : INavigationProvider
    {
        public AdminMenu(IStringLocalizer<AdminMenu> localizer)
        {
            T = localizer;
        }

        public IStringLocalizer T { get; set; }

        public Task BuildNavigationAsync(string name, NavigationBuilder builder)
        {
            if (!String.Equals(name, "admin", StringComparison.OrdinalIgnoreCase))
            {
                return Task.CompletedTask;
            }

            builder.Add(T["Configuration"], content => content
                .Add(T["Queries"], "1", contentItems => contentItems
                    .Action("Index", "Admin", new { area = "SSRSCore.Queries" })
                    .Permission(Permissions.ManageQueries)
                    .LocalNav())
                );

            return Task.CompletedTask;
        }
    }
}
