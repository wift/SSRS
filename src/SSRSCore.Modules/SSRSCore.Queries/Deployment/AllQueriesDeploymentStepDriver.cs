using SSRSCore.Deployment;
using SSRSCore.DisplayManagement.Handlers;
using SSRSCore.DisplayManagement.Views;

namespace SSRSCore.Queries.Deployment
{
    public class AllQueriesDeploymentStepDriver : DisplayDriver<DeploymentStep, AllQueriesDeploymentStep>
    {
        public override IDisplayResult Display(AllQueriesDeploymentStep step)
        {
            return
                Combine(
                    View("AllQueriesDeploymentStep_Summary", step).Location("Summary", "Content"),
                    View("AllQueriesDeploymentStep_Thumbnail", step).Location("Thumbnail", "Content")
                );
        }

        public override IDisplayResult Edit(AllQueriesDeploymentStep step)
        {
            return View("AllQueriesDeploymentStep_Edit", step).Location("Content");
        }
    }
}
