using System;
using Microsoft.AspNetCore.Builder;
using SSRSCore.Modules;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using SSRSCore.XmlRpc.Services;

namespace SSRSCore.XmlRpc
{
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IXmlRpcReader, XmlRpcReader>();
            services.AddScoped<IXmlRpcWriter, XmlRpcWriter>();
        }

        public override void Configure(IApplicationBuilder app, IEndpointRouteBuilder routes, IServiceProvider serviceProvider)
        {
            routes.MapAreaControllerRoute(
                name: "XmlRpc",
                areaName: "SSRSCore.XmlRpc",
                pattern: "xmlrpc",
                defaults: new { controller = "Home", action = "Index" }
            );
        }
    }

    [Feature("SSRSCore.RemotePublishing")]
    public class MetaWeblogStartup  : StartupBase
    {
        public override void Configure(IApplicationBuilder app, IEndpointRouteBuilder routes, IServiceProvider serviceProvider)
        {
            routes.MapAreaControllerRoute(
                name: "MetaWeblog",
                areaName: "SSRSCore.XmlRpc",
                pattern: "xmlrpc/metaweblog",
                defaults: new { controller = "MetaWeblog", action = "Manifest" }
            );
        }
    }
}
