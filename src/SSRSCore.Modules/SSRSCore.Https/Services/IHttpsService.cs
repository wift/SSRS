using System.Threading.Tasks;
using SSRSCore.Https.Settings;

namespace SSRSCore.Https.Services
{
    public interface IHttpsService
    {
        Task<HttpsSettings> GetSettingsAsync();
    }
}
