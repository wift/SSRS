using System.Collections.Generic;
using GraphQL.Types;
using Microsoft.Extensions.Localization;
using SSRSCore.Apis.GraphQL;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.GraphQL.Queries.Types;
using SSRSCore.Flows.Models;

namespace SSRSCore.Flows.GraphQL
{
    public class BagPartQueryObjectType : ObjectGraphType<BagPart>
    {
        public BagPartQueryObjectType(IStringLocalizer<BagPartQueryObjectType> T)
        {
            Name = "BagPart";
            Description = T["A BagPart allows to add content items directly within another content item"];

            Field<ListGraphType<ContentItemInterface>, IEnumerable<ContentItem>>()
                .Name("contentItems")
                .Description("the content items")
                .PagingArguments()
                .Resolve(x => x.Page(x.Source.ContentItems));
        }
    }
}
