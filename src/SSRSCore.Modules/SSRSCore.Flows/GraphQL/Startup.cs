using Microsoft.Extensions.DependencyInjection;
using SSRSCore.Apis;
using SSRSCore.ContentManagement.GraphQL.Queries.Types;
using SSRSCore.Flows.Models;
using SSRSCore.Modules;

namespace SSRSCore.Flows.GraphQL
{
    [RequireFeatures("SSRSCore.Apis.GraphQL")]
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddObjectGraphType<BagPart, BagPartQueryObjectType>();
            services.AddObjectGraphType<FlowPart, FlowPartQueryObjectType>();
            services.AddObjectGraphType<FlowMetadata, FlowMetadataQueryObjectType>();

            services.AddScoped<IContentTypeBuilder, FlowMetadataContentTypeBuilder>();
        }
    }
}
