using GraphQL.Types;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.GraphQL;
using SSRSCore.ContentManagement.GraphQL.Queries.Types;
using SSRSCore.ContentManagement.Metadata.Models;
using SSRSCore.ContentManagement.Metadata.Settings;
using SSRSCore.Flows.Models;

namespace SSRSCore.Flows.GraphQL
{
    public class FlowMetadataContentTypeBuilder : IContentTypeBuilder
    {
        public void Build(FieldType contentQuery, ContentTypeDefinition contentTypeDefinition, ContentItemType contentItemType)
        {
            var settings = contentTypeDefinition.GetSettings<ContentTypeSettings>();

            if (settings.Stereotype != "Widget") return;

            contentItemType.Field<FlowMetadataQueryObjectType>(
                "metadata",
                resolve: context => context.Source.As<FlowMetadata>()
            );
        }
    }
}
