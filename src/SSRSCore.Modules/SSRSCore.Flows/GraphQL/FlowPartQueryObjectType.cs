using GraphQL.Types;
using Microsoft.Extensions.Localization;
using SSRSCore.ContentManagement.GraphQL.Queries.Types;
using SSRSCore.Flows.Models;

namespace SSRSCore.Flows.GraphQL
{
    public class FlowPartQueryObjectType : ObjectGraphType<FlowPart>
    {
        public FlowPartQueryObjectType(IStringLocalizer<FlowPartQueryObjectType> T)
        {
            Name = "FlowPart";
            Description = T["A FlowPart allows to add content items directly within another content item"];

            Field<ListGraphType<ContentItemInterface>>(
                "widgets",
                "The widgets.",
                resolve: context => context.Source.Widgets);
        }
    }
}
