using GraphQL.Types;
using SSRSCore.Flows.Models;

namespace SSRSCore.Flows.GraphQL
{
    public class FlowMetadataQueryObjectType : ObjectGraphType<FlowMetadata>
    {
        public FlowMetadataQueryObjectType()
        {
            Name = "FlowMetadata";

            Field(x => x.Size, nullable: true);
            Field<FlowAlignmentEnum>("alignment");
        }
    }
}
