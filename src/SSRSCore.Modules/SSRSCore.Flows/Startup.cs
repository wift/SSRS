using Fluid;
using Microsoft.Extensions.DependencyInjection;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.Display.ContentDisplay;
using SSRSCore.ContentTypes.Editors;
using SSRSCore.Data.Migration;
using SSRSCore.Flows.Drivers;
using SSRSCore.Flows.Indexing;
using SSRSCore.Flows.Models;
using SSRSCore.Flows.Settings;
using SSRSCore.Flows.ViewModels;
using SSRSCore.Indexing;
using SSRSCore.Modules;

namespace SSRSCore.Flows
{
    public class Startup : StartupBase
    {
        static Startup()
        {
            TemplateContext.GlobalMemberAccessStrategy.Register<BagPartViewModel>();
            TemplateContext.GlobalMemberAccessStrategy.Register<FlowPartViewModel>();
            TemplateContext.GlobalMemberAccessStrategy.Register<FlowMetadata>();
            TemplateContext.GlobalMemberAccessStrategy.Register<FlowPart>();
        }

        public override void ConfigureServices(IServiceCollection services)
        {
            // Flow Part
            services.AddScoped<IContentPartDisplayDriver, FlowPartDisplay>();
            services.AddContentPart<FlowPart>();
            services.AddScoped<IContentDisplayDriver, FlowMetadataDisplay>();

            // Bag Part
            services.AddScoped<IContentPartDisplayDriver, BagPartDisplay>();
            services.AddContentPart<BagPart>();
            services.AddScoped<IContentTypePartDefinitionDisplayDriver, BagPartSettingsDisplayDriver>();
            services.AddScoped<IContentPartIndexHandler, BagPartIndexHandler>();

            services.AddContentPart<FlowMetadata>();

            services.AddScoped<IDataMigration, Migrations>();
        }
    }
}
