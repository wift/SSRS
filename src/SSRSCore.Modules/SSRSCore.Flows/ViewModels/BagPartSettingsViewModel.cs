using System;
using System.Collections.Specialized;
using SSRSCore.Flows.Models;

namespace SSRSCore.Flows.ViewModels
{
    public class BagPartSettingsViewModel
    {
        public BagPartSettings BagPartSettings { get; set; }
        public NameValueCollection ContentTypes { get; set; }
        public string DisplayType { get; set; }
        public string[] ContainedContentTypes { get; set; } = Array.Empty<string>();
    }
}
