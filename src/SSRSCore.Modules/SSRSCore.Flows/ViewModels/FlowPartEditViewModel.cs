using System;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using SSRSCore.DisplayManagement.ModelBinding;
using SSRSCore.Flows.Models;

namespace SSRSCore.Flows.ViewModels
{
    public class FlowPartEditViewModel
    {
        // Each element in a FlowPart is projected to the Prefixes and ContentTypes arrays
        // If for instance a FlowPart has three content items, there will be three elements in both
        // Prefixes and ContentTypes. Each value in Prefixes is a Guid that represents the unique
        // HtmlFieldPrefix value of its editor.

        public string[] Prefixes { get; set; } = Array.Empty<string>();
        public string[] ContentTypes { get; set; } = Array.Empty<string>();

        [BindNever]
        public FlowPart FlowPart { get; set; }

        [BindNever]
        public IUpdateModel Updater { get; set; }
    }
}
