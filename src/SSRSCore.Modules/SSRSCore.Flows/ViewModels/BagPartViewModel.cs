using System.Collections.Generic;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.Display.Models;
using SSRSCore.Flows.Models;

namespace SSRSCore.Flows.ViewModels
{
    public class BagPartViewModel
    {
        public BagPart BagPart { get; set; }
        public IEnumerable<ContentItem> ContentItems => BagPart.ContentItems;
        public BuildPartDisplayContext BuildPartDisplayContext { get; set; }
        public BagPartSettings Settings { get; set; }
        public string DisplayType => Settings?.DisplayType;
    }
}
