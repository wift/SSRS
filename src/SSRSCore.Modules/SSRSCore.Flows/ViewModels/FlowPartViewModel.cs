﻿using SSRSCore.ContentManagement.Display.Models;
using SSRSCore.Flows.Models;

namespace SSRSCore.Flows.ViewModels
{
    public class FlowPartViewModel
    {
        public FlowPart FlowPart { get; set; }
        public BuildPartDisplayContext BuildPartDisplayContext { get; set; }
    }
}
