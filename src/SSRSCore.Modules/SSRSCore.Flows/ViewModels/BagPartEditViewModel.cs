using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using SSRSCore.ContentManagement.Metadata.Models;
using SSRSCore.DisplayManagement.ModelBinding;
using SSRSCore.Flows.Models;

namespace SSRSCore.Flows.ViewModels
{
    public class BagPartEditViewModel
    {
        public string[] Prefixes { get; set; } = Array.Empty<string>();
        public string[] ContentTypes { get; set; } = Array.Empty<string>();

        [BindNever]
        public BagPart BagPart { get; set; }

        [BindNever]
        public IUpdateModel Updater { get; set; }

        [BindNever]
        public IEnumerable<ContentTypeDefinition> ContainedContentTypeDefinitions { get; set; }
    }
}
