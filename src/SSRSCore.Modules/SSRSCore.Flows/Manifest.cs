using SSRSCore.Modules.Manifest;

[assembly: Module(
    Name = "Flows",
    Author = "The Orchard Team",
    Website = "https://orchardproject.net",
    Version = "2.0.0"
)]

[assembly: Feature(
    Id = "SSRSCore.Flows",
    Name = "Flows",
    Description = "Provides a content part allowing users to edit their content based on Widgets.",
    Dependencies = new [] { "SSRSCore.Widgets" },
    Category = "Content"
)]
