# Alias (`SSRSCore.Alias`)

这个模块允许您为内容项指定友好的标识符。
还可以导入和导出别名，这意味着在运行菜谱或部署内容时将持久化别名(而内容项id不是)。

## Alias Part

将此部分附加到内容类型以指定内容项的别名。

## Liquid

启用别名后，您可以通过别名检索内容在您的Liquid视图和模板:
```liquid
{% assign my_content = Content["alias:footer-widget"] %}
```

或者

```liquid
{% assign my_content = Content.Alias["footer-widget"] %}
```

