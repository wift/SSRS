using SSRSCore.ContentManagement.Metadata.Settings;
using SSRSCore.ContentManagement.Metadata;
using SSRSCore.Data.Migration;
using SSRSCore.Alias.Indexes;
using SSRSCore.Alias.Models;
using SSRSCore.Alias.Drivers;

namespace SSRSCore.Alias
{
    public class Migrations : DataMigration
    {
        IContentDefinitionManager _contentDefinitionManager;

        public Migrations(IContentDefinitionManager contentDefinitionManager)
        {
            _contentDefinitionManager = contentDefinitionManager;
        }

        public int Create()
        {
            _contentDefinitionManager.AlterPartDefinition(nameof(AliasPart), builder => builder
                .Attachable()
                .WithDescription("Provides a way to define custom aliases for content items."));

            // NOTE: The Alias Length has been upgraded from 64 characters to 1024.
            // For existing SQL databases update the AliasPartIndex tables Alias column length manually. 
            SchemaBuilder.CreateMapIndexTable(nameof(AliasPartIndex), table => table
                .Column<string>("Alias", col => col.WithLength(AliasPartDisplayDriver.MaxAliasLength))
                .Column<string>("ContentItemId", c => c.WithLength(26))
            );

            SchemaBuilder.AlterTable(nameof(AliasPartIndex), table => table
                .CreateIndex("IDX_AliasPartIndex_Alias", "Alias")
            );

            return 1;
        }
    }
}
