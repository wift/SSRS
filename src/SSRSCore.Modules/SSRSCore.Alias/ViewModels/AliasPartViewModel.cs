﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using SSRSCore.Alias.Models;
using SSRSCore.Alias.Settings;

namespace SSRSCore.Alias.ViewModels
{
    public class AliasPartViewModel
    {
        public string Alias { get; set; }

        [BindNever]
        public AliasPart AliasPart { get; set; }

        [BindNever]
        public AliasPartSettings Settings { get; set; }
    }
}
