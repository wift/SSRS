using GraphQL.Types;
using Microsoft.Extensions.Localization;
using SSRSCore.Alias.Models;

namespace SSRSCore.Alias.GraphQL
{
    public class AliasQueryObjectType : ObjectGraphType<AliasPart>
    {
        public AliasQueryObjectType(IStringLocalizer<AliasQueryObjectType> T)
        {
            Name = "AliasPart";
            Description = T["Alternative path for the content item"];

            Field("alias", x => x.Alias, true);
        }
    }
}
