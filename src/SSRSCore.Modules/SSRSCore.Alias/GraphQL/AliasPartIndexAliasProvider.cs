using System.Collections.Generic;
using SSRSCore.Alias.Indexes;
using SSRSCore.ContentManagement.GraphQL.Queries;

namespace SSRSCore.Alias.GraphQL
{
    public class AliasPartIndexAliasProvider : IIndexAliasProvider
    {
        private static readonly IndexAlias[] _aliases = new[]
        {
            new IndexAlias
            {
                Alias = "aliasPart",
                Index = "AliasPartIndex",
                With = q => q.With<AliasPartIndex>()
            }
        };

        public IEnumerable<IndexAlias> GetAliases()
        {
            return _aliases;
        }
    }
}
