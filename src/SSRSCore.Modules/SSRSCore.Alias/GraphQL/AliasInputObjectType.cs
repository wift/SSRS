using GraphQL.Types;
using Microsoft.Extensions.Localization;
using SSRSCore.Alias.Models;
using SSRSCore.Apis.GraphQL.Queries;

namespace SSRSCore.Alias.GraphQL
{
    public class AliasInputObjectType : WhereInputObjectGraphType<AliasPart>
    {
        public AliasInputObjectType(IStringLocalizer<AliasInputObjectType> T)
        {
            Name = "AliasPartInput";
            Description = T["the alias part of the content item"];

            AddScalarFilterFields<StringGraphType>("alias", T["the alias of the content item"]);
        }
    }
}
