using Microsoft.Extensions.DependencyInjection;
using SSRSCore.Alias.Models;
using SSRSCore.Apis;
using SSRSCore.ContentManagement.GraphQL.Queries;
using SSRSCore.Modules;

namespace SSRSCore.Alias.GraphQL
{
    [RequireFeatures("SSRSCore.Apis.GraphQL")]
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddObjectGraphType<AliasPart, AliasQueryObjectType>();
            services.AddInputObjectGraphType<AliasPart, AliasInputObjectType>();
            services.AddTransient<IIndexAliasProvider, AliasPartIndexAliasProvider>();
        }
    }
}
