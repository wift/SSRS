﻿using SSRSCore.ContentManagement;

namespace SSRSCore.Alias.Models
{
    public class AliasPart : ContentPart
    {
        public string Alias { get; set; }
    }
}
