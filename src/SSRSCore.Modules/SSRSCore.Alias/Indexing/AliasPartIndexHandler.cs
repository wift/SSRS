﻿using System.Threading.Tasks;
using SSRSCore.Alias.Models;
using SSRSCore.Indexing;

namespace SSRSCore.Alias.Indexing
{
    public class AliasPartIndexHandler : ContentPartIndexHandler<AliasPart>
    {
        public override Task BuildIndexAsync(AliasPart part, BuildPartIndexContext context)
        {
            var options = DocumentIndexOptions.Store;

            foreach (var key in context.Keys)
            {
                context.DocumentIndex.Set(key, part.Alias, options);
            }

            return Task.CompletedTask;
        }
    }
}
