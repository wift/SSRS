using Fluid;
using Microsoft.Extensions.DependencyInjection;
using SSRSCore.Alias.Drivers;
using SSRSCore.Alias.Handlers;
using SSRSCore.Alias.Indexes;
using SSRSCore.Alias.Indexing;
using SSRSCore.Alias.Liquid;
using SSRSCore.Alias.Models;
using SSRSCore.Alias.Services;
using SSRSCore.Alias.Settings;
using SSRSCore.Alias.ViewModels;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.Display.ContentDisplay;
using SSRSCore.ContentManagement.Handlers;
using SSRSCore.ContentTypes.Editors;
using SSRSCore.Data.Migration;
using SSRSCore.Indexing;
using SSRSCore.Liquid;
using SSRSCore.Modules;
using YesSql.Indexes;

namespace SSRSCore.Alias
{
    public class Startup : StartupBase
    {
        static Startup()
        {
            TemplateContext.GlobalMemberAccessStrategy.Register<AliasPartViewModel>();
        }

        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IIndexProvider, AliasPartIndexProvider>();
            services.AddScoped<IDataMigration, Migrations>();
            services.AddScoped<IContentAliasProvider, AliasPartContentAliasProvider>();

            // Identity Part
            services.AddContentPart<AliasPart>();
            services.AddScoped<IContentPartDisplayDriver, AliasPartDisplayDriver>();
            services.AddScoped<IContentPartHandler, AliasPartHandler>();
            services.AddScoped<IContentPartIndexHandler, AliasPartIndexHandler>();
            services.AddScoped<IContentTypePartDefinitionDisplayDriver, AliasPartSettingsDisplayDriver>();

            services.AddScoped<ILiquidTemplateEventHandler, ContentAliasLiquidTemplateEventHandler>();
        }
    }
}
