﻿using SSRSCore.Modules;
using Microsoft.Extensions.DependencyInjection;
using SSRSCore.DisplayManagement.Descriptors;
using SSRSCore.Navigation;

namespace SSRSCore.Navigation
{
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddNavigation();

            services.AddScoped<IShapeTableProvider, NavigationShapes>();
            services.AddScoped<IShapeTableProvider, PagerShapesTableProvider>();
            services.AddShapeAttributes<PagerShapes>();
        }
    }
}
