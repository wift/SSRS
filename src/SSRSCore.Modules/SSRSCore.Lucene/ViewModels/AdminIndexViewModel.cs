﻿using System.Collections.Generic;

namespace SSRSCore.Lucene.ViewModels
{
    public class AdminIndexViewModel
    {
        public IEnumerable<IndexViewModel> Indexes { get; set; }
    }
}
