using SSRSCore.Modules.Manifest;

[assembly: Module(
    Name = "Lucene",
    Author = "The Orchard Team",
    Website = "https://orchardproject.net",
    Version = "2.0.0"
)]

[assembly: Feature(
    Id = "SSRSCore.Lucene",
    Name = "Lucene",
    Description = "Creates Lucene indexes to support search scenarios, introduces a preconfigured container-enabled content type.",
    Dependencies = new[]
    {
        "SSRSCore.Indexing",
        "SSRSCore.Liquid"
    },
    Category = "Content Management"
)]

[assembly: Feature(
    Id = "SSRSCore.Lucene.Worker",
    Name = "Lucene Worker",
    Description = "Provides a background task to keep local indices in sync with other instances.",
    Dependencies = new[] { "SSRSCore.Lucene" },
    Category = "Content Management"
)]

[assembly: Feature(
    Id = "SSRSCore.Lucene.ContentPicker",
    Name = "Lucene Content Picker",
    Description = "Provides a Lucene content picker field editor.",
    Dependencies = new[] { "SSRSCore.Lucene", "SSRSCore.ContentFields" },
    Category = "Content Management"
)]
