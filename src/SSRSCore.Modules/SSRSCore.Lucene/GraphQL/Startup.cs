using Microsoft.Extensions.DependencyInjection;
using SSRSCore.Apis.GraphQL;
using SSRSCore.Modules;
using SSRSCore.Queries.Lucene.GraphQL.Queries;

namespace SSRSCore.Lucene.GraphQL
{
    /// <summary>
    /// These services are registered on the tenant service collection
    /// </summary>
    [RequireFeatures("SSRSCore.Apis.GraphQL")]
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<ISchemaBuilder, LuceneQueryFieldTypeProvider>();
        }
    }
}
