﻿using SSRSCore.Indexing;

namespace SSRSCore.Lucene.Settings
{
    public class ContentIndexSettingsViewModel
    {
        public ContentIndexSettings ContentIndexSettings { get; set; }
    }
}
