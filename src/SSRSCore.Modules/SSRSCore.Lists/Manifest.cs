using SSRSCore.Modules.Manifest;

[assembly: Module(
    Name = "Lists",
    Author = "The Orchard Team",
    Website = "https://orchardproject.net",
    Version = "2.0.0"
)]

[assembly: Feature(
    Id = "SSRSCore.Lists",
    Name = "Lists",
    Description = "Introduces a preconfigured container-enabled content type.",
    Dependencies = new [] { "SSRSCore.ContentTypes" },
    Category = "Content Management"
)]
