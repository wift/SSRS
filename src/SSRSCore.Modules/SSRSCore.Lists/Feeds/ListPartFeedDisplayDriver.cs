using System.Threading.Tasks;
using SSRSCore.ContentManagement.Display.ContentDisplay;
using SSRSCore.ContentManagement.Display.Models;
using SSRSCore.DisplayManagement.ModelBinding;
using SSRSCore.DisplayManagement.Views;
using SSRSCore.Lists.Models;

namespace SSRSCore.Lists.Feeds
{
    public class ListPartFeedDisplayDriver : ContentPartDisplayDriver<ListPart>
    {
        public override IDisplayResult Display(ListPart listPart, BuildPartDisplayContext context)
        {
            return Dynamic("ListPartFeed", shape =>
            {
                shape.ContentItem = listPart.ContentItem;
            })
            .Location("Detail", "Content");
        }

        public override IDisplayResult Edit(ListPart part)
        {
            return Initialize<ListFeedEditViewModel>("ListPartFeed_Edit", m =>
            {
                m.FeedProxyUrl = part.ContentItem.Content.ListPart.FeedProxyUrl;
                m.FeedItemsCount = part.ContentItem.Content.ListPart.FeedItemsCount ?? 20;
                m.ContentItem = part.ContentItem;
            });
        }

        public override async Task<IDisplayResult> UpdateAsync(ListPart part, IUpdateModel updater)
        {
            var model = new ListFeedEditViewModel();
            model.ContentItem = part.ContentItem;

            await updater.TryUpdateModelAsync(model, Prefix, t => t.FeedProxyUrl, t => t.FeedItemsCount);

            part.ContentItem.Content.ListPart.FeedProxyUrl= model.FeedProxyUrl;
            part.ContentItem.Content.ListPart.FeedItemsCount = model.FeedItemsCount;

            return Edit(part);
        }
    }
}
