using System.Threading.Tasks;
using SSRSCore.ContentManagement;
using SSRSCore.Indexing;
using SSRSCore.Lists.Models;

namespace SSRSCore.Lists.Indexes
{
    public class ContainedPartContentIndexHandler : IContentItemIndexHandler
    {
        public const string ParentKey = "Content.ContentItem.ListContentItemId";

        public Task BuildIndexAsync(BuildIndexContext context)
        {
            var parent = context.ContentItem.As<ContainedPart>();

            if (parent == null)
            {
                return Task.CompletedTask;
            }

            context.DocumentIndex.Set(
                ParentKey,
                parent.ListContentItemId,
                DocumentIndexOptions.Store);

            return Task.CompletedTask;
        }
    }
}
