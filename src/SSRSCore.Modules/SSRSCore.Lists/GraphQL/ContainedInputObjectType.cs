using GraphQL.Types;
using Microsoft.Extensions.Localization;
using SSRSCore.Apis.GraphQL.Queries;
using SSRSCore.Lists.Models;

namespace SSRSCore.Lists.GraphQL
{
    public class ContainedInputObjectType : WhereInputObjectGraphType<ContainedPart>
    {
        public ContainedInputObjectType(IStringLocalizer<ContainedPart> T)
        {
            Name = "ContainedPartInput";
            Description = T["the list part of the content item"];

            AddScalarFilterFields<IdGraphType>("listContentItemId", T["the content item id of the parent list of the content item to filter"]);
        }
    }
}
