using GraphQL.Types;
using Microsoft.Extensions.Localization;
using SSRSCore.Lists.Models;

namespace SSRSCore.Lists.GraphQL
{
    public class ContainedQueryObjectType : ObjectGraphType<ContainedPart>
    {
        public ContainedQueryObjectType(IStringLocalizer<ContainedQueryObjectType> T)
        {
            Name = "ContainedPart";
            Description = T["Represents a link to the parent content item, and the order that content item is represented."];

            Field(x => x.ListContentItemId);
            Field(x => x.Order);
        }
    }
}
