using System.Collections.Generic;
using SSRSCore.ContentManagement.GraphQL.Queries;
using SSRSCore.Lists.Indexes;

namespace SSRSCore.Lists.GraphQL
{
    public class ContainedPartIndexAliasProvider : IIndexAliasProvider
    {
        private static readonly IndexAlias[] _aliases = new []
        {
            new IndexAlias
            {
                Alias = "containedPart",
                Index = nameof(ContainedPartIndex),
                With = q => q.With<ContainedPartIndex>()
            }
        };

        public IEnumerable<IndexAlias> GetAliases()
        {
            return _aliases;
        }
    }
}
