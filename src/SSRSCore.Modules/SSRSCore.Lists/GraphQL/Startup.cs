using Microsoft.Extensions.DependencyInjection;
using SSRSCore.Apis;
using SSRSCore.ContentManagement.GraphQL.Queries;
using SSRSCore.Lists.Models;
using SSRSCore.Modules;

namespace SSRSCore.Lists.GraphQL
{
    [RequireFeatures("SSRSCore.Apis.GraphQL")]
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddInputObjectGraphType<ContainedPart, ContainedInputObjectType>();
            services.AddObjectGraphType<ContainedPart, ContainedQueryObjectType>();
            services.AddObjectGraphType<ListPart, ListQueryObjectType>();
            services.AddTransient<IIndexAliasProvider, ContainedPartIndexAliasProvider>();
        }
    }
}
