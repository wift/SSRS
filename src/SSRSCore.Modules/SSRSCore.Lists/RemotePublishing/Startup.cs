using System;
using Microsoft.AspNetCore.Builder;
using SSRSCore.Modules;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using SSRSCore.ContentManagement.Display.ContentDisplay;
using SSRSCore.XmlRpc;

namespace SSRSCore.Lists.RemotePublishing
{

    [RequireFeatures("SSRSCore.RemotePublishing")]
    public class RemotePublishingStartup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IXmlRpcHandler, MetaWeblogHandler>();
            services.AddScoped<IContentPartDisplayDriver, ListMetaWeblogDriver>();
        }

        public override void Configure(IApplicationBuilder app, IEndpointRouteBuilder routes, IServiceProvider serviceProvider)
        {
            routes.MapAreaControllerRoute(
                name: "RSD",
                areaName: "SSRSCore.Lists",
                pattern: "xmlrpc/metaweblog/{contentItemId}/rsd",
                defaults: new { controller = "RemotePublishing", action = "Rsd" }
            );
        }
    }
}
