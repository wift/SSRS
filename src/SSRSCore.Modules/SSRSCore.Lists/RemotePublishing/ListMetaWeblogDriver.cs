using System.Threading.Tasks;
using SSRSCore.Modules;
using SSRSCore.ContentManagement.Display.ContentDisplay;
using SSRSCore.ContentManagement.Display.Models;
using SSRSCore.DisplayManagement.Views;
using SSRSCore.Lists.Models;

namespace SSRSCore.Lists.RemotePublishing
{
    [RequireFeatures("SSRSCore.RemotePublishing")]
    public class ListMetaWeblogDriver : ContentPartDisplayDriver<ListPart>
    {
        public override IDisplayResult Display(ListPart listPart, BuildPartDisplayContext context)
        {
            return Dynamic("ListPart_RemotePublishing", shape =>
            {
                shape.ContentItem = listPart.ContentItem;
            }).Location("Content");
        }
    }
}