using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Routing;
using SSRSCore.ContentLocalization.Handlers;
using SSRSCore.ContentLocalization.Models;
using SSRSCore.ContentLocalization.Records;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.Handlers;
using SSRSCore.ContentManagement.Records;
using SSRSCore.Lists.Indexes;
using SSRSCore.Lists.Models;
using YesSql;
using YesSql.Services;

namespace SSRSCore.Lists.Drivers
{
    public class ListPartLocalizationHandler : ContentLocalizationPartHandlerBase<ListPart>
    {

        private readonly ISession _session;
        public ListPartLocalizationHandler(ISession session)
        {
            _session = session;
        }

        /// <summary>
        /// Select Contained ContentItems that are already in the target culture
        /// but attached to the original list and reassign their ListContenItemId.
        /// </summary>
        public override async Task LocalizedAsync(LocalizationContentContext context, ListPart part)
        {
            var containedList = await _session.Query<ContentItem, ContainedPartIndex>(
                x => x.ListContentItemId == context.Original.ContentItemId).ListAsync();

            if (!containedList.Any())
            {
                return;
            }

            foreach(var item in containedList)
            {
                var localizationPart  = item.As<LocalizationPart>();
                if(localizationPart.Culture == context.Culture)
                {
                    var cp = item.As<ContainedPart>();
                    cp.ListContentItemId = context.ContentItem.ContentItemId;
                    cp.Apply();
                    _session.Save(item);
                }
               
            }
        }
    }
}
