using System.Threading.Tasks;
using Microsoft.AspNetCore.Routing;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.Handlers;
using SSRSCore.Lists.Models;

namespace SSRSCore.Lists.Drivers
{
    public class ListPartHandler : ContentPartHandler<ListPart>
    {
        public override Task GetContentItemAspectAsync(ContentItemAspectContext context, ListPart part)
        {
            return context.ForAsync<ContentItemMetadata>(contentItemMetadata =>
            {
                contentItemMetadata.AdminRouteValues = new RouteValueDictionary
                {
                    {"Area", "SSRSCore.Contents"},
                    {"Controller", "Admin"},
                    {"Action", "Display"},
                    {"ContentItemId", context.ContentItem.ContentItemId}
                };

                return Task.CompletedTask;
            });
        }
    }
}
