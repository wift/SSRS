using System;
using Fluid;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using SSRSCore.AdminMenu.Services;
using SSRSCore.ContentLocalization.Handlers;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.Display.ContentDisplay;
using SSRSCore.ContentManagement.Handlers;
using SSRSCore.Contents.Services;
using SSRSCore.ContentTypes.Editors;
using SSRSCore.Data.Migration;
using SSRSCore.DisplayManagement.Handlers;
using SSRSCore.Feeds;
using SSRSCore.Indexing;
using SSRSCore.Lists.AdminNodes;
using SSRSCore.Lists.Drivers;
using SSRSCore.Lists.Feeds;
using SSRSCore.Lists.Indexes;
using SSRSCore.Lists.Models;
using SSRSCore.Lists.Services;
using SSRSCore.Lists.Settings;
using SSRSCore.Lists.ViewModels;
using SSRSCore.Modules;
using SSRSCore.Navigation;
using YesSql.Indexes;

namespace SSRSCore.Lists
{
    public class Startup : StartupBase
    {
        static Startup()
        {
            TemplateContext.GlobalMemberAccessStrategy.Register<ListPartViewModel>();
        }

        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IIndexProvider, ContainedPartIndexProvider>();
            services.AddScoped<IContentDisplayDriver, ContainedPartDisplayDriver>();
            services.AddContentPart<ContainedPart>();
            services.AddTransient<IContentAdminFilter, ListPartContentAdminFilter>();

            // List Part
            services.AddScoped<IContentPartDisplayDriver, ListPartDisplayDriver>();
            services.AddContentPart<ListPart>();
            services.AddScoped<IContentPartHandler, ListPartHandler>();
            services.AddScoped<IContentTypePartDefinitionDisplayDriver, ListPartSettingsDisplayDriver>();
            services.AddScoped<IDataMigration, Migrations>();
            services.AddScoped<IContentItemIndexHandler, ContainedPartContentIndexHandler>();

            // Feeds
            // TODO: Create feature
            services.AddScoped<IFeedQueryProvider, ListFeedQuery>();
            services.AddScoped<IContentPartDisplayDriver, ListPartFeedDisplayDriver>();
            services.AddScoped<IContentPartHandler, ListPartFeedHandler>();
        }

        public override void Configure(IApplicationBuilder app, IEndpointRouteBuilder routes, IServiceProvider serviceProvider)
        {
            routes.MapAreaControllerRoute(
                name: "ListFeed",
                areaName: "SSRSCore.Feeds",
                pattern: "Contents/Lists/{contentItemId}/rss",
                defaults: new { controller = "Feed", action = "Index", format = "rss"}
            );
        }
    }


    [RequireFeatures("SSRSCore.AdminMenu")]
    public class AdminMenuStartup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IAdminNodeProviderFactory>(new AdminNodeProviderFactory<ListsAdminNode>());
            services.AddScoped<IAdminNodeNavigationBuilder, ListsAdminNodeNavigationBuilder>();
            services.AddScoped<IDisplayDriver<MenuItem>, ListsAdminNodeDriver>();
        }
    }
    [RequireFeatures("SSRSCore.ContentLocalization")]
    public class ContentLocalizationStartup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IContentLocalizationPartHandler, ContainedPartLocalizationHandler>();
            services.AddScoped<IContentLocalizationPartHandler, ListPartLocalizationHandler>();
            services.AddScoped<IContentPartHandler, ContainedPartHandler>();
        }
    }

}
