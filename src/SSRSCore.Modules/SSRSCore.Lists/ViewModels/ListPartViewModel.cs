using System.Collections.Generic;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.Display.Models;
using SSRSCore.ContentManagement.Metadata.Models;
using SSRSCore.Lists.Models;

namespace SSRSCore.Lists.ViewModels
{
    public class ListPartViewModel
    {
        public ListPart ListPart { get; set; }
        public IEnumerable<ContentItem> ContentItems { get; set; }
        public IEnumerable<ContentTypeDefinition> ContainedContentTypeDefinitions { get; set; }
        public BuildPartDisplayContext Context { get; set; }
        public dynamic Pager { get; set; }
    }
}
