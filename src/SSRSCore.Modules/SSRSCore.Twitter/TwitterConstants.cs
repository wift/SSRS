namespace SSRSCore.Twitter
{
    public static class TwitterConstants
    {
        public static class Features
        {
            public const string Twitter = "SSRSCore.Twitter";
            public const string Signin = "SSRSCore.Twitter.Signin";
        }
    }
}
