using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using SSRSCore.Twitter.Settings;

namespace SSRSCore.Twitter.Services
{
    public interface ITwitterSettingsService
    {
        Task<TwitterSettings> GetSettingsAsync();
        Task UpdateSettingsAsync(TwitterSettings settings);
        IEnumerable<ValidationResult> ValidateSettings(TwitterSettings settings);
    }
}
