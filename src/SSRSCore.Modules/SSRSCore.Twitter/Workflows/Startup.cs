using Microsoft.Extensions.DependencyInjection;
using SSRSCore.Twitter.Workflows.Activities;
using SSRSCore.Twitter.Workflows.Drivers;
using SSRSCore.Modules;
using SSRSCore.Workflows.Helpers;

namespace SSRSCore.Twitter.Workflows
{
    [RequireFeatures("SSRSCore.Workflows")]
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddActivity<UpdateTwitterStatusTask, UpdateTwitterStatusTaskDisplay>();
        }
    }
}
