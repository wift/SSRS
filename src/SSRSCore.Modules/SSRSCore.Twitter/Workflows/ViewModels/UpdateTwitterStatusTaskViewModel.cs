using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SSRSCore.Twitter.Workflows.ViewModels
{
    public class UpdateTwitterStatusTaskViewModel
    {
        [Required]
        public string StatusTemplate { get; set; }
    }
}
