using SSRSCore.Twitter.Workflows.Activities;
using SSRSCore.Twitter.Workflows.ViewModels;
using SSRSCore.Workflows.Display;
using SSRSCore.Workflows.Models;

namespace SSRSCore.Twitter.Workflows.Drivers
{
    public class UpdateTwitterStatusTaskDisplay : ActivityDisplayDriver<UpdateTwitterStatusTask, UpdateTwitterStatusTaskViewModel>
    {
        protected override void EditActivity(UpdateTwitterStatusTask activity, UpdateTwitterStatusTaskViewModel model)
        {
            model.StatusTemplate= activity.StatusTemplate.Expression;
        }

        protected override void UpdateActivity(UpdateTwitterStatusTaskViewModel model, UpdateTwitterStatusTask activity)
        {
            activity.StatusTemplate = new WorkflowExpression<string>(model.StatusTemplate);
        }
    }
}
