using Microsoft.AspNetCore.Http;

namespace SSRSCore.Twitter.Signin.Settings
{
    public class TwitterSigninSettings
    {
        public PathString CallbackPath { get; set; }
    }
}
