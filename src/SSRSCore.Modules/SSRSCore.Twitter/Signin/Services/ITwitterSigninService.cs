using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using SSRSCore.Twitter.Signin.Settings;

namespace SSRSCore.Twitter.Signin.Services
{
    public interface ITwitterSigninService
    {
        Task<TwitterSigninSettings> GetSettingsAsync();
        Task UpdateSettingsAsync(TwitterSigninSettings settings);
    }
}
