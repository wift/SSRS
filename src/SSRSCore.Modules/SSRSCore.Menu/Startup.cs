using Microsoft.Extensions.DependencyInjection;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.Display.ContentDisplay;
using SSRSCore.ContentManagement.Handlers;
using SSRSCore.Data.Migration;
using SSRSCore.DisplayManagement.Descriptors;
using SSRSCore.Menu.Drivers;
using SSRSCore.Menu.Handlers;
using SSRSCore.Menu.Models;
using SSRSCore.Menu.TagHelpers;
using SSRSCore.Modules;
using SSRSCore.Security.Permissions;

namespace SSRSCore.Menu
{
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IDataMigration, Migrations>();
            services.AddScoped<IShapeTableProvider, MenuShapes>();
            services.AddScoped<IPermissionProvider, Permissions>();

            // MenuPart
            services.AddScoped<IContentHandler, MenuContentHandler>();
            services.AddScoped<IContentPartDisplayDriver, MenuPartDisplayDriver>();
            services.AddContentPart<MenuPart>();
            services.AddContentPart<MenuItemsListPart>();

            // LinkMenuItemPart
            services.AddScoped<IContentPartDisplayDriver, LinkMenuItemPartDisplayDriver>();
            services.AddContentPart<LinkMenuItemPart>();

            services.AddTagHelpers<MenuTagHelper>();
        }
    }
}
