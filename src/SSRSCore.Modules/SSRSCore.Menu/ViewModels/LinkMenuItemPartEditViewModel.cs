﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using SSRSCore.Menu.Models;

namespace SSRSCore.Menu.ViewModels
{
    public class LinkMenuItemPartEditViewModel
    {
        public string Name { get; set; }

        public string Url { get; set; }

        [BindNever]
        public LinkMenuItemPart MenuItemPart { get; set; }
    }
}
