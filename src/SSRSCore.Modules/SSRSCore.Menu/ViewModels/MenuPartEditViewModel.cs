﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using SSRSCore.Menu.Models;

namespace SSRSCore.Menu.ViewModels
{
    public class MenuPartEditViewModel
    {
        public string Hierarchy { get; set; }

        [BindNever]
        public MenuPart MenuPart { get; set; }
    }
}
