using System.Threading.Tasks;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.Display.ContentDisplay;
using SSRSCore.ContentManagement.Display.Models;
using SSRSCore.DisplayManagement.ModelBinding;
using SSRSCore.DisplayManagement.Views;
using SSRSCore.Menu.Models;
using SSRSCore.Menu.ViewModels;

namespace SSRSCore.Menu.Drivers
{
    public class LinkMenuItemPartDisplayDriver : ContentPartDisplayDriver<LinkMenuItemPart>
    {
        private readonly IContentManager _contentManager;

        public LinkMenuItemPartDisplayDriver(
            IContentManager contentManager
            )
        {
            _contentManager = contentManager;
        }

        public override IDisplayResult Display(LinkMenuItemPart part, BuildPartDisplayContext context)
        {
            return Combine(
                Dynamic("LinkMenuItemPart_Admin", shape =>
                {
                    shape.MenuItemPart = part;
                })
                .Location("Admin", "Content:10"),
                Dynamic("LinkMenuItemPart_Thumbnail", shape =>
                {
                    shape.MenuItemPart = part;
                })
                .Location("Thumbnail", "Content:10")
            );
        }

        public override IDisplayResult Edit(LinkMenuItemPart part)
        {
            return Initialize<LinkMenuItemPartEditViewModel>("LinkMenuItemPart_Edit", model =>
            {
                model.Name = part.Name;
                model.Url = part.Url;
                model.MenuItemPart = part;
            });
        }

        public override async Task<IDisplayResult> UpdateAsync(LinkMenuItemPart part, IUpdateModel updater)
        {
            await updater.TryUpdateModelAsync(part, Prefix, x => x.Name, x => x.Url);

            return Edit(part);
        }
    }
}