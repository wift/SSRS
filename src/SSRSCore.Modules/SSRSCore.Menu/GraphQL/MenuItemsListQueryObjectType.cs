﻿using GraphQL.Types;
using SSRSCore.Menu.Models;

namespace SSRSCore.Menu.GraphQL
{
    public class MenuItemsListQueryObjectType : ObjectGraphType<MenuItemsListPart>
    {
        public MenuItemsListQueryObjectType()
        {
            Name = "MenuItemsListPart";

            Field<ListGraphType<MenuItemInterface>>(
                "menuItems", 
                "The menu items.", 
                resolve: context => context.Source.MenuItems);
        }
    }
}