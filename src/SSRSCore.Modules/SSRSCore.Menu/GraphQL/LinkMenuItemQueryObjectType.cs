﻿using GraphQL.Types;
using SSRSCore.Menu.Models;

namespace SSRSCore.Menu.GraphQL
{
    public class LinkMenuItemQueryObjectType : ObjectGraphType<LinkMenuItemPart>
    {
        public LinkMenuItemQueryObjectType()
        {
            Name = "LinkMenuItemPart";

            Field(x => x.Name, nullable: true);
            Field(x => x.Url, nullable: true);
        }
    }
}