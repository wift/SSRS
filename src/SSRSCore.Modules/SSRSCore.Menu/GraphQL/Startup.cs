using Microsoft.Extensions.DependencyInjection;
using SSRSCore.Apis;
using SSRSCore.ContentManagement.GraphQL.Queries.Types;
using SSRSCore.Menu.Models;
using SSRSCore.Modules;

namespace SSRSCore.Menu.GraphQL
{
    [RequireFeatures("SSRSCore.Apis.GraphQL")]
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddObjectGraphType<MenuItemsListPart, MenuItemsListQueryObjectType>();
            services.AddObjectGraphType<LinkMenuItemPart, LinkMenuItemQueryObjectType>();
            services.AddScoped<IContentTypeBuilder, MenuItemContentTypeBuilder>();
        }
    }
}