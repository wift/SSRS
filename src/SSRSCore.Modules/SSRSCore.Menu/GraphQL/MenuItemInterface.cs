using GraphQL.Types;
using SSRSCore.ContentManagement;
using SSRSCore.Menu.Models;

namespace SSRSCore.Menu.GraphQL
{
    public class MenuItemInterface : InterfaceGraphType<ContentItem>
    {
        public MenuItemInterface()
        {
            Name = "MenuItem";

            Field(typeof(MenuItemsListQueryObjectType), "menuItemsList", resolve: context => context.Source.As<MenuItemsListPart>());
        }
    }
}