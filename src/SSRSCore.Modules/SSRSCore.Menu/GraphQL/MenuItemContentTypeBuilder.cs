using GraphQL.Types;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.GraphQL;
using SSRSCore.ContentManagement.GraphQL.Queries.Types;
using SSRSCore.ContentManagement.Metadata.Models;
using SSRSCore.ContentManagement.Metadata.Settings;
using SSRSCore.Menu.Models;

namespace SSRSCore.Menu.GraphQL
{
    public class MenuItemContentTypeBuilder : IContentTypeBuilder
    {
        public void Build(FieldType contentQuery, ContentTypeDefinition contentTypeDefinition, ContentItemType contentItemType)
        {
            var settings = contentTypeDefinition.GetSettings<ContentTypeSettings>();

            if (settings.Stereotype != "MenuItem") return;

            contentItemType.Field<MenuItemsListQueryObjectType>(
                nameof(MenuItemsListPart).ToFieldName(),
                resolve: context => context.Source.As<MenuItemsListPart>()
            );

            contentItemType.Interface<MenuItemInterface>();
        }
    }
}