using System.Threading.Tasks;
using SSRSCore.Data.Migration;
using SSRSCore.Recipes.Services;

namespace SSRSCore.Menu
{
    public class Migrations : DataMigration
    {
        private readonly IRecipeMigrator _recipeMigrator;

        public Migrations(IRecipeMigrator recipeMigrator)
        {
            _recipeMigrator = recipeMigrator;
        }

        public async Task<int> CreateAsync()
        {
            await _recipeMigrator.ExecuteAsync("menu.recipe.json", this);

            return 1;
        }
    }
}
