using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SSRSCore.Security.Permissions;

namespace SSRSCore.Menu
{
    public class Permissions : IPermissionProvider
    {
        public static readonly Permission ManageMenu = new Permission("ManageMenu", "Manage menus");

        public Task<IEnumerable<Permission>> GetPermissionsAsync()
        {
            return Task.FromResult(new[] { ManageMenu }.AsEnumerable());
        }

        public IEnumerable<PermissionStereotype> GetDefaultStereotypes()
        {
            return new[]
            {
                new PermissionStereotype
                {
                    Name = "Administrator",
                    Permissions = new[] { ManageMenu }
                }
            };
        }
    }
}
