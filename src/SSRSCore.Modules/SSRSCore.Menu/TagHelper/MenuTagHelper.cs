using Microsoft.AspNetCore.Razor.TagHelpers;
using SSRSCore.DisplayManagement;
using SSRSCore.DisplayManagement.TagHelpers;

namespace SSRSCore.Menu.TagHelpers
{
    [HtmlTargetElement("menu")]
    public class MenuTagHelper : BaseShapeTagHelper
    {
        public MenuTagHelper(IShapeFactory shapeFactory, IDisplayHelper displayHelper) :
            base(shapeFactory, displayHelper)
        {
            Type = "Menu";
        }
    }
}