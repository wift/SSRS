using SSRSCore.Modules.Manifest;

[assembly: Module(
    Name = "Menu",
    Author = "The Orchard Team",
    Website = "https://orchardproject.net",
    Version = "2.0.0",
    Description = "The Menu module provides menu management features.",
    Dependencies = new []
    {
        "SSRSCore.Contents",
        "SSRSCore.Title",
        "SSRSCore.Alias"
    },
    Category = "Navigation"
)]
