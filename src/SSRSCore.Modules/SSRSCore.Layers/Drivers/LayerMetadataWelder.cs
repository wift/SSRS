using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Localization;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.Display.ContentDisplay;
using SSRSCore.DisplayManagement.Handlers;
using SSRSCore.DisplayManagement.Views;
using SSRSCore.Layers.Models;
using SSRSCore.Layers.Services;
using SSRSCore.Layers.ViewModels;

namespace SSRSCore.Layers.Drivers
{
    public class LayerMetadataWelder : ContentDisplayDriver
    {
		private readonly ILayerService _layerService;
        private readonly IStringLocalizer<LayerMetadataWelder> S;

        public LayerMetadataWelder(ILayerService layerService, IStringLocalizer<LayerMetadataWelder> stringLocalizer)
		{
			_layerService = layerService;
            S = stringLocalizer;
        }

        protected override void BuildPrefix(ContentItem model, string htmlFieldPrefix)
        {
            Prefix = "LayerMetadata";
        }

        public override async Task<IDisplayResult> EditAsync(ContentItem model, BuildEditorContext context)
		{
			var layerMetadata = model.As<LayerMetadata>();

			if (layerMetadata == null)
			{
				layerMetadata = new LayerMetadata();

				// Are we loading an editor that requires layer metadata?
				if (await context.Updater.TryUpdateModelAsync(layerMetadata, Prefix, m => m.Zone, m => m.Position)
					&& !String.IsNullOrEmpty(layerMetadata.Zone))
				{
					model.Weld(layerMetadata);
				}
				else
				{
					return null;
				}
			}

			return Initialize<LayerMetadataEditViewModel>("LayerMetadata_Edit", async shape =>
			{
                shape.Title = model.DisplayText;
				shape.LayerMetadata = layerMetadata;
				shape.Layers = (await _layerService.GetLayersAsync()).Layers;
			})
			.Location("Content:before");
		}

        public override async Task<IDisplayResult> UpdateAsync(ContentItem model, UpdateEditorContext context)
        {
            var viewModel = new LayerMetadataEditViewModel();

            await context.Updater.TryUpdateModelAsync(viewModel, Prefix);

            if (viewModel.LayerMetadata == null)
            {
                return null;
            }

            if (String.IsNullOrEmpty(viewModel.LayerMetadata.Zone))
            {
                context.Updater.ModelState.AddModelError("LayerMetadata.Zone", S["Zone is missing"]);
            }

            if (context.Updater.ModelState.IsValid)
            {
                model.Apply(viewModel.LayerMetadata);
            }

            model.DisplayText = viewModel.Title;

			return await EditAsync(model, context);
        }
    }
}
