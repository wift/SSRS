using System;
using Microsoft.AspNetCore.Builder;
using SSRSCore.Modules;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.Display.ContentDisplay;
using SSRSCore.ContentManagement.Handlers;
using SSRSCore.Data.Migration;
using SSRSCore.Deployment;
using SSRSCore.DisplayManagement.Handlers;
using SSRSCore.Navigation;
using SSRSCore.Layers.Deployment;
using SSRSCore.Layers.Drivers;
using SSRSCore.Layers.Handlers;
using SSRSCore.Layers.Indexes;
using SSRSCore.Layers.Models;
using SSRSCore.Layers.Recipes;
using SSRSCore.Layers.Services;
using SSRSCore.Recipes;
using SSRSCore.Scripting;
using SSRSCore.Security.Permissions;
using SSRSCore.Settings;
using YesSql.Indexes;

namespace SSRSCore.Layers
{
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.Configure<MvcOptions>((options) =>
            {
                options.Filters.Add(typeof(LayerFilter));
            });

            services.AddScoped<IDisplayDriver<ISite>, LayerSiteSettingsDisplayDriver>();
            services.AddContentPart<LayerMetadata>();
            services.AddScoped<IContentDisplayDriver, LayerMetadataWelder>();
            services.AddScoped<INavigationProvider, AdminMenu>();
            services.AddScoped<ILayerService, LayerService>();
            services.AddScoped<IContentHandler, LayerMetadataHandler>();
            services.AddSingleton<IIndexProvider, LayerMetadataIndexProvider>();
            services.AddScoped<IDataMigration, Migrations>();
            services.AddScoped<IPermissionProvider, Permissions>();
            services.AddRecipeExecutionStep<LayerStep>();

            services.AddTransient<IDeploymentSource, AllLayersDeploymentSource>();
            services.AddSingleton<IDeploymentStepFactory>(new DeploymentStepFactory<AllLayersDeploymentStep>());
            services.AddScoped<IDisplayDriver<DeploymentStep>, AllLayersDeploymentStepDriver>();
        }

        public override void Configure(IApplicationBuilder app, IEndpointRouteBuilder routes, IServiceProvider serviceProvider)
        {
            var scriptingManager = serviceProvider.GetRequiredService<IScriptingManager>();
            scriptingManager.GlobalMethodProviders.Add(new DefaultLayersMethodProvider());
        }
    }
}
