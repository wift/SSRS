﻿using SSRSCore.Layers.Models;

namespace SSRSCore.Layers.ViewModels
{
    public class LayerEditViewModel
    {
		public string Name { get; set; }
		public string Rule { get; set; }
		public string Description { get; set; }
	}
}
