using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SSRSCore.Layers.Models;

namespace SSRSCore.Layers.ViewModels
{
    public class LayerMetadataEditViewModel
    {
        public string Title { get; set; }
        public LayerMetadata LayerMetadata { get; set; }
		public List<Layer> Layers { get; set; }
    }
}
