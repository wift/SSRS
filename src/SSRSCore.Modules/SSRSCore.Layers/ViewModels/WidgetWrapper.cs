using SSRSCore.ContentManagement;
using SSRSCore.DisplayManagement;
using SSRSCore.DisplayManagement.Views;

namespace SSRSCore.Layers.ViewModels
{
    public class WidgetWrapper : ShapeViewModel
    {
        public WidgetWrapper() : base("Widget_Wrapper")
        {
        }

        public ContentItem Widget { get; set; }
		public IShape Content { get; set; }
    }
}
