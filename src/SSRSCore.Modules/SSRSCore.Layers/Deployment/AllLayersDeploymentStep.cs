using SSRSCore.Deployment;

namespace SSRSCore.Layers.Deployment
{
    /// <summary>
    /// Adds layers to a <see cref="DeploymentPlanResult"/>. 
    /// </summary>
    public class AllLayersDeploymentStep : DeploymentStep
    {
        public AllLayersDeploymentStep()
        {
            Name = "AllLayers";
        }
    }
}
