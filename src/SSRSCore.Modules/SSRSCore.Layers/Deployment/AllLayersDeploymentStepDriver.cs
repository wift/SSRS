using SSRSCore.Deployment;
using SSRSCore.DisplayManagement.Handlers;
using SSRSCore.DisplayManagement.Views;

namespace SSRSCore.Layers.Deployment
{
    public class AllLayersDeploymentStepDriver : DisplayDriver<DeploymentStep, AllLayersDeploymentStep>
    {
        public override IDisplayResult Display(AllLayersDeploymentStep step)
        {
            return
                Combine(
                    View("AllLayersDeploymentStep_Summary", step).Location("Summary", "Content"),
                    View("AllLayersDeploymentStep_Thumbnail", step).Location("Thumbnail", "Content")
                );
        }

        public override IDisplayResult Edit(AllLayersDeploymentStep step)
        {
            return View("AllLayersDeploymentStep_Edit", step).Location("Content");
        }
    }
}
