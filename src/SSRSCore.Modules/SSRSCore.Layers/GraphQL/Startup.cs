using Microsoft.Extensions.DependencyInjection;
using SSRSCore.Apis.GraphQL;
using SSRSCore.Modules;

namespace SSRSCore.Layers.GraphQL
{
    [RequireFeatures("SSRSCore.Apis.GraphQL")]
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<ISchemaBuilder, SiteLayersQuery>();
            services.AddTransient<LayerQueryObjectType>();
            services.AddTransient<LayerWidgetQueryObjectType>();
        }
    }
}
