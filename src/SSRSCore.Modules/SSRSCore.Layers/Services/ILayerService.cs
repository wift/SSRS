using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.Records;
using SSRSCore.Layers.Models;

namespace SSRSCore.Layers.Services
{
    public interface ILayerService
    {
		Task<LayersDocument> GetLayersAsync();
		Task<IEnumerable<ContentItem>> GetLayerWidgetsAsync(Expression<Func<ContentItemIndex, bool>> predicate);
        Task<IEnumerable<LayerMetadata>> GetLayerWidgetsMetadataAsync(Expression<Func<ContentItemIndex, bool>> predicate);
		Task UpdateAsync(LayersDocument layers);
	}
}
