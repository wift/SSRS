using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Localization;
using SSRSCore.Title.Models;
using SSRSCore.Title.ViewModels;
using SSRSCore.ContentManagement.Metadata.Models;
using SSRSCore.ContentTypes.Editors;
using SSRSCore.DisplayManagement.ModelBinding;
using SSRSCore.DisplayManagement.Views;
using SSRSCore.Liquid;

namespace SSRSCore.Title.Settings
{
    public class TitlePartSettingsDisplayDriver : ContentTypePartDefinitionDisplayDriver
    {
        private readonly ILiquidTemplateManager _templateManager;

        public TitlePartSettingsDisplayDriver(ILiquidTemplateManager templateManager, IStringLocalizer<TitlePartSettingsDisplayDriver> localizer)
        {
            _templateManager = templateManager;
            T = localizer;
        }

        public IStringLocalizer T { get; private set; }

        public override IDisplayResult Edit(ContentTypePartDefinition contentTypePartDefinition, IUpdateModel updater)
        {
            if (!String.Equals(nameof(TitlePart), contentTypePartDefinition.PartDefinition.Name, StringComparison.Ordinal))
            {
                return null;
            }

            return Initialize<TitlePartSettingsViewModel>("TitlePartSettings_Edit", model =>
            {
                var settings = contentTypePartDefinition.GetSettings<TitlePartSettings>();

                model.Options = settings.Options;
                model.Pattern = settings.Pattern;
                model.TitlePartSettings = settings;
            }).Location("Content");
        }

        public override async Task<IDisplayResult> UpdateAsync(ContentTypePartDefinition contentTypePartDefinition, UpdateTypePartEditorContext context)
        {
            if (!String.Equals(nameof(TitlePart), contentTypePartDefinition.PartDefinition.Name, StringComparison.Ordinal))
            {
                return null;
            }

            var model = new TitlePartSettingsViewModel();

            await context.Updater.TryUpdateModelAsync(model, Prefix, 
                m => m.Pattern,
                m => m.Options);

            if (!string.IsNullOrEmpty(model.Pattern) && !_templateManager.Validate(model.Pattern, out var errors))
            {
                context.Updater.ModelState.AddModelError(nameof(model.Pattern), T["Pattern doesn't contain a valid Liquid expression. Details: {0}", string.Join(" ", errors)]);
            }
            else
            {
                context.Builder.WithSettings(new TitlePartSettings {Pattern = model.Pattern, Options = model.Options,});
            }

            return Edit(contentTypePartDefinition, context.Updater);
        }
    }
}