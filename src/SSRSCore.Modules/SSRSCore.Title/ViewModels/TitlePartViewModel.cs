using Microsoft.AspNetCore.Mvc.ModelBinding;
using SSRSCore.Title.Models;

namespace SSRSCore.Title.ViewModels
{
    public class TitlePartViewModel
    {
        public string Title { get; set; }

        [BindNever]
        public TitlePart TitlePart { get; set; }
        [BindNever]
        public TitlePartSettings Settings { get; set; }
    }
}
