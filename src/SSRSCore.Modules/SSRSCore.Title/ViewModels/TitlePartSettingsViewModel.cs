using Microsoft.AspNetCore.Mvc.ModelBinding;
using SSRSCore.Title.Models;

namespace SSRSCore.Title.ViewModels
{
    public class TitlePartSettingsViewModel
    {
        public TitlePartOptions Options { get; set; }
        public string Pattern { get; set; }

        [BindNever]
        public TitlePartSettings TitlePartSettings { get; set; }
    }
}
