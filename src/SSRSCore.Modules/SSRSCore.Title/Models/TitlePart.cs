using SSRSCore.ContentManagement;

namespace SSRSCore.Title.Models
{
    public class TitlePart : ContentPart
    {
        public string Title { get; set; }
    }
}
