﻿using System.Threading.Tasks;
using SSRSCore.Indexing;
using SSRSCore.Title.Models;

namespace SSRSCore.Title.Indexing
{
    public class TitlePartIndexHandler : ContentPartIndexHandler<TitlePart>
    {
        public override Task BuildIndexAsync(TitlePart part, BuildPartIndexContext context)
        {
            var options = context.Settings.ToOptions() 
                | DocumentIndexOptions.Analyze
                ;

            foreach (var key in context.Keys)
            {
                context.DocumentIndex.Set(key, part.Title, options);
            }

            return Task.CompletedTask;
        }
    }
}
