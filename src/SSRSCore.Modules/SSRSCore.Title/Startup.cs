using Fluid;
using Microsoft.Extensions.DependencyInjection;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.Display.ContentDisplay;
using SSRSCore.ContentManagement.Handlers;
using SSRSCore.ContentTypes.Editors;
using SSRSCore.Data.Migration;
using SSRSCore.Indexing;
using SSRSCore.Modules;
using SSRSCore.Title.Drivers;
using SSRSCore.Title.Handlers;
using SSRSCore.Title.Indexing;
using SSRSCore.Title.Models;
using SSRSCore.Title.Settings;
using SSRSCore.Title.ViewModels;

namespace SSRSCore.Title
{
    public class Startup : StartupBase
    {
        static Startup()
        {
            TemplateContext.GlobalMemberAccessStrategy.Register<TitlePartViewModel>();
            TemplateContext.GlobalMemberAccessStrategy.Register<TitlePartSettingsViewModel>();
        }

        public override void ConfigureServices(IServiceCollection services)
        {
            // Title Part
            services.AddScoped<IContentPartDisplayDriver, TitlePartDisplay>();
            services.AddContentPart<TitlePart>();
            services.AddScoped<IContentPartIndexHandler, TitlePartIndexHandler>();
            services.AddScoped<IContentPartHandler, TitlePartHandler>();
            services.AddScoped<IContentTypePartDefinitionDisplayDriver, TitlePartSettingsDisplayDriver>();

            services.AddScoped<IDataMigration, Migrations>();
        }
    }
}
