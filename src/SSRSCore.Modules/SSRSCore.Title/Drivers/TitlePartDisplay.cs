using System;
using System.Linq;
using System.Threading.Tasks;
using SSRSCore.ContentManagement.Display.ContentDisplay;
using SSRSCore.ContentManagement.Metadata;
using SSRSCore.DisplayManagement.ModelBinding;
using SSRSCore.DisplayManagement.Views;
using SSRSCore.Title.Models;
using SSRSCore.Title.ViewModels;

namespace SSRSCore.Title.Drivers
{
    public class TitlePartDisplay : ContentPartDisplayDriver<TitlePart>
    {
        private readonly IContentDefinitionManager _contentDefinitionManager;
        public TitlePartDisplay(IContentDefinitionManager contentDefinitionManager)
        {
            _contentDefinitionManager = contentDefinitionManager;
        }
        public override IDisplayResult Display(TitlePart titlePart)
        {
            return Initialize<TitlePartViewModel>("TitlePart", model =>
            {
                model.Title = titlePart.ContentItem.DisplayText;
                model.TitlePart = titlePart;
            })
            .Location("Detail", "Header:5")
            .Location("Summary", "Header:5");
        }

        public override IDisplayResult Edit(TitlePart titlePart)
        {
            return Initialize<TitlePartViewModel>("TitlePart_Edit", model =>
            {
                model.Title = titlePart.ContentItem.DisplayText;
                model.TitlePart = titlePart;
                model.Settings = GetSettings(titlePart);
            });
        }

        public override async Task<IDisplayResult> UpdateAsync(TitlePart model, IUpdateModel updater)
        {
            await updater.TryUpdateModelAsync(model, Prefix, t => t.Title);

            model.ContentItem.DisplayText = model.Title;

            return Edit(model);
        }


        private TitlePartSettings GetSettings(TitlePart titlePart)
        {
            var contentTypeDefinition = _contentDefinitionManager.GetTypeDefinition(titlePart.ContentItem.ContentType);
            var contentTypePartDefinition = contentTypeDefinition.Parts.FirstOrDefault(x => string.Equals(x.PartDefinition.Name, nameof(TitlePart), StringComparison.Ordinal));
            return contentTypePartDefinition?.GetSettings<TitlePartSettings>();
        }

    }
}
