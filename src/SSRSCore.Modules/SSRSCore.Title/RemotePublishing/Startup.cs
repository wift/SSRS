using SSRSCore.Modules;
using Microsoft.Extensions.DependencyInjection;
using SSRSCore.MetaWeblog;

namespace SSRSCore.Title.RemotePublishing
{

    [RequireFeatures("SSRSCore.RemotePublishing")]
    public class RemotePublishingStartup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IMetaWeblogDriver, TitleMetaWeblogDriver>();
        }
    }
}
