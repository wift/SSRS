using System.Text.Encodings.Web;
using SSRSCore.ContentManagement;
using SSRSCore.XmlRpc;
using SSRSCore.XmlRpc.Models;
using SSRSCore.MetaWeblog;
using SSRSCore.Title.Models;

namespace SSRSCore.Title.RemotePublishing
{
    public class TitleMetaWeblogDriver : MetaWeblogDriver
    {
        private readonly HtmlEncoder _encoder;

        public TitleMetaWeblogDriver(HtmlEncoder encoder)
        {
            _encoder = encoder;
        }

        public override void BuildPost(XRpcStruct rpcStruct, XmlRpcContext context, ContentItem contentItem)
        {
            var titlePart = contentItem.As<TitlePart>();
            if (titlePart == null)
            {
                return;
            }

            rpcStruct.Set("title", _encoder.Encode(titlePart.Title));
        }

        public override void EditPost(XRpcStruct rpcStruct, ContentItem contentItem)
        {
            contentItem.DisplayText = rpcStruct.Optional<string>("title");
        }
    }
}
