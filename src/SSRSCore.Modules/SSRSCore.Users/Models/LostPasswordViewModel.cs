using SSRSCore.DisplayManagement.Views;
using SSRSCore.Users.Models;

namespace SSRSCore.Users.ViewModels
{
    public class LostPasswordViewModel : ShapeViewModel
    {
        public LostPasswordViewModel()
        {
            Metadata.Type = "TemplateUserLostPassword";
        }

        public User User { get; set; }
        public string LostPasswordUrl { get; set; }
    }
} 