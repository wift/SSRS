namespace SSRSCore.Users.Models
{
    public class ResetPasswordSettings
    {
        public bool AllowResetPassword { get; set; }
        public bool UseSiteTheme { get; set; }
    }
}
