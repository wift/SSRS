using System.ComponentModel.DataAnnotations;

namespace SSRSCore.Users.ViewModels
{
    public class ForgotPasswordViewModel
    {
        [Required]
        public string UserIdentifier { get; set; }
    }
} 