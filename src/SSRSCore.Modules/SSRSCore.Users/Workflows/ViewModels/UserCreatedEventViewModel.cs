using System.Collections.Generic;
using SSRSCore.Users.Workflows.Activities;

namespace SSRSCore.Users.Workflows.ViewModels
{
    public class UserCreatedEventViewModel : UserEventViewModel<UserCreatedEvent>
    {
        public UserCreatedEventViewModel()
        {
        }

        public UserCreatedEventViewModel(UserCreatedEvent activity)
        {
            Activity = activity;
        }
    }
}
