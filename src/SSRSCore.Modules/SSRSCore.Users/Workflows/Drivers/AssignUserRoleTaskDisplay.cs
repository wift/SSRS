using SSRSCore.Users.Workflows.Activities;
using SSRSCore.Users.Workflows.ViewModels;
using SSRSCore.Workflows.Display;
using SSRSCore.Workflows.Models;

namespace SSRSCore.Users.Workflows.Drivers
{
    public class AssignUserRoleTaskDisplay : ActivityDisplayDriver<AssignUserRoleTask, AssignUserRoleTaskViewModel>
    {
        protected override void EditActivity(AssignUserRoleTask activity, AssignUserRoleTaskViewModel model)
        {
            model.UserName = activity.UserName.Expression;
            model.RoleName = activity.RoleName.Expression;
        }

        protected override void UpdateActivity(AssignUserRoleTaskViewModel model, AssignUserRoleTask activity)
        {
            activity.UserName = new WorkflowExpression<string>(model.UserName);
            activity.RoleName = new WorkflowExpression<string>(model.RoleName);
        }
    }
}
