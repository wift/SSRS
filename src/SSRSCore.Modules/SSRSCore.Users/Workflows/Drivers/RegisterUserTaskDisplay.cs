using SSRSCore.Users.Workflows.Activities;
using SSRSCore.Users.Workflows.ViewModels;
using SSRSCore.Workflows.Display;
using SSRSCore.Workflows.Models;

namespace SSRSCore.Users.Workflows.Drivers
{
    public class RegisterUserTaskDisplay : ActivityDisplayDriver<RegisterUserTask, RegisterUserTaskViewModel>
    {
        protected override void EditActivity(RegisterUserTask activity, RegisterUserTaskViewModel model)
        {
            model.SendConfirmationEmail = activity.SendConfirmationEmail;
            model.ConfirmationEmailTemplate = activity.ConfirmationEmailTemplate.Expression;
        }

        protected override void UpdateActivity(RegisterUserTaskViewModel model, RegisterUserTask activity)
        {
            activity.SendConfirmationEmail = model.SendConfirmationEmail;
            activity.ConfirmationEmailTemplate = new WorkflowExpression<string>(model.ConfirmationEmailTemplate);
        }
    }
}
