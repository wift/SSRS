using System.Threading.Tasks;
using SSRSCore.DisplayManagement.ModelBinding;
using SSRSCore.DisplayManagement.Views;
using SSRSCore.Users.Services;
using SSRSCore.Users.Workflows.Activities;
using SSRSCore.Users.Workflows.ViewModels;
using SSRSCore.Workflows.Display;

namespace SSRSCore.Users.Workflows.Drivers
{
    public class UserCreatedEventDisplay : ActivityDisplayDriver<UserCreatedEvent, UserCreatedEventViewModel>
    {
        public UserCreatedEventDisplay(IUserService userService)
        {
            UserService = userService;
        }

        protected IUserService UserService { get; }

        protected override void EditActivity(UserCreatedEvent source, UserCreatedEventViewModel target)
        {
        }

        public override IDisplayResult Display(UserCreatedEvent activity)
        {
            return Combine(
                Shape($"{nameof(UserCreatedEvent)}_Fields_Thumbnail", new UserCreatedEventViewModel(activity)).Location("Thumbnail", "Content"),
                Factory($"{nameof(UserCreatedEvent)}_Fields_Design", ctx =>
                {
                    var shape = new UserCreatedEventViewModel();
                    shape.Activity = activity;

                    return shape;

                }).Location("Design", "Content")
            );
        }
    }
}
