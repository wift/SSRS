using Microsoft.Extensions.DependencyInjection;
using SSRSCore.Users.Workflows.Activities;
using SSRSCore.Users.Workflows.Drivers;
using SSRSCore.Modules;
using SSRSCore.Workflows.Helpers;
using SSRSCore.Users.Handlers;
using SSRSCore.Users.Workflows.Handlers;

namespace SSRSCore.Users.Workflows
{
    [RequireFeatures("SSRSCore.Workflows")]
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddActivity<RegisterUserTask, RegisterUserTaskDisplay>();
            services.AddActivity<UserCreatedEvent, UserCreatedEventDisplay>();
            services.AddScoped<IUserCreatedEventHandler, UserCreatedHandler>();
            services.AddActivity<AssignUserRoleTask, AssignUserRoleTaskDisplay>();
        }
    }
}
