using System.Collections.Generic;
using Microsoft.Extensions.Localization;
using SSRSCore.Users.Services;
using SSRSCore.Workflows.Services;

namespace SSRSCore.Users.Workflows.Activities
{
    public class UserCreatedEvent : UserEvent
    {
        public UserCreatedEvent(IUserService userService, IWorkflowScriptEvaluator scriptEvaluator, IStringLocalizer<UserCreatedEvent> localizer) : base(userService, scriptEvaluator, localizer)
        {
        }

        public override string Name => nameof(UserCreatedEvent);
        public override LocalizedString DisplayText => T["User Created Event"];
    }
}