using System.Threading.Tasks;
using SSRSCore.Users.Handlers;
using SSRSCore.Users.Models;
using SSRSCore.Users.Workflows.Activities;
using SSRSCore.Workflows.Services;

namespace SSRSCore.Users.Workflows.Handlers
{
    public class UserCreatedHandler : IUserCreatedEventHandler
    {
        private readonly IWorkflowManager _workflowManager;

        public UserCreatedHandler(IWorkflowManager workflowManager)
        {
            _workflowManager = workflowManager;
        }

        public Task CreatedAsync(CreateUserContext context)
        {
            return TriggerWorkflowEventAsync(nameof(UserCreatedEvent), (User)context.User);
        }

        private Task TriggerWorkflowEventAsync(string name, User user)
        {
            return _workflowManager.TriggerEventAsync(name,
                input: new { User = user },
                correlationId: user.Id.ToString()
            );
        }
    }
}
