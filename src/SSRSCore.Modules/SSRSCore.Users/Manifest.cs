using SSRSCore.Modules.Manifest;

[assembly: Module(
    Name = "Users",
    Author = "The Orchard Team",
    Website = "https://orchardproject.net",
    Version = "2.0.0"
)]

[assembly: Feature(
    Id = "SSRSCore.Users",
    Name = "Users",
    Description = "The users module enables authentication UI and user management.",
    Category = "Security"
)]

[assembly: Feature(
    Id = "SSRSCore.Users.Registration",
    Name = "Users Registration",
    Description = "The registration feature allows external users to sign up to the site and ask to confirm their email.",
    Dependencies = new[] { "SSRSCore.Users", "SSRSCore.Email" },
    Category = "Security"
)]

[assembly: Feature(
    Id = "SSRSCore.Users.ResetPassword",
    Name = "Users Reset Password",
    Description = "The reset password feature allows users to reset their password.",
    Dependencies = new[] { "SSRSCore.Users", "SSRSCore.Email" },
    Category = "Security"
)]

[assembly: Feature(
    Id = "SSRSCore.Users.TimeZone",
    Name = "User Time Zone",
    Description = "Provides a way to set the time zone per user.",
    Category = "Settings"
)]