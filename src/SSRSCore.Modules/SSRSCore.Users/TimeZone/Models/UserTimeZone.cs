namespace SSRSCore.Users.TimeZone.Models
{
    public class UserTimeZone
    {
        public string TimeZoneId { get; set; }
    }
}
