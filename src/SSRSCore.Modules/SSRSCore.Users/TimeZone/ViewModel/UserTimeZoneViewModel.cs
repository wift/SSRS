namespace SSRSCore.Users.TimeZone.ViewModels
{
    public class UserTimeZoneViewModel
    {
        public string TimeZoneId { get; set; }
    }
}
