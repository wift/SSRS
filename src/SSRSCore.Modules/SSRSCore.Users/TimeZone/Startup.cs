using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using SSRSCore.DisplayManagement.Handlers;
using SSRSCore.Modules;
using SSRSCore.Users.Models;
using SSRSCore.Users.TimeZone.Drivers;
using SSRSCore.Users.TimeZone.Services;

namespace SSRSCore.Users.TimeZone
{
    [Feature("SSRSCore.Users.TimeZone")]
    public class Startup : StartupBase
    {
        public override void Configure(IApplicationBuilder builder, IEndpointRouteBuilder routes, IServiceProvider serviceProvider) { }

        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<ITimeZoneSelector, UserTimeZoneSelector>();
            services.AddScoped<UserTimeZoneService>();

            services.AddScoped<IDisplayDriver<User>, UserTimeZoneDisplayDriver>();
        }
    }
}