using System.Threading.Tasks;
using SSRSCore.DisplayManagement.Entities;
using SSRSCore.DisplayManagement.Handlers;
using SSRSCore.DisplayManagement.Views;
using SSRSCore.Modules;
using SSRSCore.Settings;
using SSRSCore.Users.Models;

namespace SSRSCore.Users.Drivers
{
    [Feature("SSRSCore.Users.ResetPassword")]
    public class ResetPasswordSettingsDisplayDriver : SectionDisplayDriver<ISite, ResetPasswordSettings>
    {
        public const string GroupId = "ResetPasswordSettings";

        public override IDisplayResult Edit(ResetPasswordSettings section)
        {
            return Initialize<ResetPasswordSettings>("ResetPasswordSettings_Edit", model => {
                model.AllowResetPassword = section.AllowResetPassword;
                model.UseSiteTheme = section.UseSiteTheme;
            }).Location("Content:5").OnGroup(GroupId);
        }

        public override async Task<IDisplayResult> UpdateAsync(ResetPasswordSettings section, BuildEditorContext context)
        {
            if (context.GroupId == GroupId)
            {
                await context.Updater.TryUpdateModelAsync(section, Prefix);
            }
            return await EditAsync(section, context);
        }
    }
}
