using System.Threading.Tasks;
using SSRSCore.DisplayManagement.Entities;
using SSRSCore.DisplayManagement.Handlers;
using SSRSCore.DisplayManagement.Views;
using SSRSCore.Modules;
using SSRSCore.Settings;
using SSRSCore.Users.Models;

namespace SSRSCore.Users.Drivers
{
    [Feature("SSRSCore.Users.Registration")]
    public class RegistrationSettingsDisplayDriver : SectionDisplayDriver<ISite, RegistrationSettings>
    {
        public const string GroupId = "RegistrationSettings";

        public override IDisplayResult Edit(RegistrationSettings section)
        {
            return Initialize<RegistrationSettings>("RegistrationSettings_Edit", model => {
                model.UsersCanRegister = section.UsersCanRegister;
                model.UsersMustValidateEmail = section.UsersMustValidateEmail;
                model.UseSiteTheme = section.UseSiteTheme;
            }).Location("Content:5").OnGroup(GroupId);
        }

        public override async Task<IDisplayResult> UpdateAsync(RegistrationSettings section, BuildEditorContext context)
        {
            if (context.GroupId == GroupId)
            {
                await context.Updater.TryUpdateModelAsync(section, Prefix);
            }
            return await EditAsync(section, context);
        }
    }
}
