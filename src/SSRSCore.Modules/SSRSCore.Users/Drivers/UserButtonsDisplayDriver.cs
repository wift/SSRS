using System.Threading.Tasks;
using SSRSCore.DisplayManagement.Handlers;
using SSRSCore.DisplayManagement.Views;
using SSRSCore.Users.Models;

namespace SSRSCore.Users.Drivers
{
    public class UserButtonsDisplayDriver : DisplayDriver<User>
    {
        public override IDisplayResult Edit(User user)
        {
            return Dynamic("UserSaveButtons_Edit").Location("Actions");
        }

        public override Task<IDisplayResult> UpdateAsync(User user, UpdateEditorContext context)
        {
            return Task.FromResult(Edit(user));
        }
    }
}
