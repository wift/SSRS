using SSRSCore.Modules.Manifest;

[assembly: Module(
    Name = "Contents",
    Author = "The Orchard Team",
    Website = "https://orchardproject.net",
    Version = "2.0.0"
)]

[assembly: Feature(
    Id = "SSRSCore.Contents",
    Name = "Contents",
    Description = "The contents module enables the edition and rendering of content items.",
    Dependencies = new[]
    {
        "SSRSCore.Settings",
        "SSRSCore.Liquid"
    },
    Category = "Content Management"
)]

[assembly:Feature(
    Id = "SSRSCore.Contents.FileContentDefinition",
    Name = "File Content Definition",
    Description = "Stores Content Definition in a local file.",
    Dependencies = new[] { "SSRSCore.Contents" },
    Category = "Content Management"
)]
