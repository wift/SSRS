using SSRSCore.ContentManagement.Metadata.Settings;
using SSRSCore.ContentManagement.Metadata;
using SSRSCore.Data.Migration;

namespace SSRSCore.Contents
{
    public class Migrations : DataMigration
    {
        IContentDefinitionManager _contentDefinitionManager;

        public Migrations(IContentDefinitionManager contentDefinitionManager)
        {
            _contentDefinitionManager = contentDefinitionManager;
        }

        public int Create()
        {
            _contentDefinitionManager.AlterPartDefinition("CommonPart", builder => builder
                .Attachable()
                .WithDescription("Provides an editor for the common properties of a content item."));

            return 1;
        }
    }
}