using SSRSCore.Deployment;

namespace SSRSCore.Contents.Deployment
{
    /// <summary>
    /// Adds all content items to a <see cref="DeploymentPlanResult"/>. 
    /// </summary>
    public class AllContentDeploymentStep : DeploymentStep
    {
        public AllContentDeploymentStep()
        {
            Name = "AllContent";
        }
    }
}
