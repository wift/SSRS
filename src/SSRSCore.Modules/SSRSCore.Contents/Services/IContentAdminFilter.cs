﻿using SSRSCore.ContentManagement;
using SSRSCore.Contents.ViewModels;
using SSRSCore.DisplayManagement.ModelBinding;
using SSRSCore.Navigation;
using System.Threading.Tasks;
using YesSql;

namespace SSRSCore.Contents.Services
{
    public interface IContentAdminFilter
    {
        Task FilterAsync(IQuery<ContentItem> query, ListContentsViewModel model, PagerParameters pagerParameters, IUpdateModel updateModel);
    }
}
