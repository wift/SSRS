﻿using System;

namespace SSRSCore.Contents.ViewModels
{
    public class OwnerEditorViewModel
    {
        public string Owner { get; set; }
    }
}
