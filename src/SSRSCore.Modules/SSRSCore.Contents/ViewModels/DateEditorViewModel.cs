using System;

namespace SSRSCore.Contents.ViewModels
{
    public class DateEditorViewModel
    {
        public DateTime? LocalDateTime { get; set; }
    }
}
