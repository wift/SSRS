using System;

namespace SSRSCore.Contents.ViewModels
{
    public class IndexingEditorViewModel
    {
        public bool IsIndexed { get; set; }
    }
}
