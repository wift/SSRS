﻿using System.Collections.Generic;
using SSRSCore.ContentManagement.Metadata.Models;

namespace SSRSCore.Contents.ViewModels
{
    public class ListContentTypesViewModel
    {
        public IEnumerable<ContentTypeDefinition> Types { get; set; }
    }
}