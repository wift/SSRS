using Microsoft.Extensions.DependencyInjection;
using SSRSCore.ContentManagement.GraphQL;
using SSRSCore.Modules;

namespace SSRSCore.Contents.GraphQL
{
    [RequireFeatures("SSRSCore.Apis.GraphQL")]
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddContentGraphQL();
        }
    }
}
