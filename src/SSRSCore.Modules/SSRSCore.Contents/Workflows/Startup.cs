using Microsoft.Extensions.DependencyInjection;
using SSRSCore.ContentManagement.Handlers;
using SSRSCore.Contents.Workflows.Activities;
using SSRSCore.Contents.Workflows.Drivers;
using SSRSCore.Contents.Workflows.Handlers;
using SSRSCore.Modules;
using SSRSCore.Workflows.Helpers;
using SSRSCore.Workflows.Services;

namespace SSRSCore.Contents.Workflows
{
    [RequireFeatures("SSRSCore.Workflows")]
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddActivity<ContentCreatedEvent, ContentCreatedEventDisplay>();
            services.AddActivity<ContentDeletedEvent, ContentDeletedEventDisplay>();
            services.AddActivity<ContentPublishedEvent, ContentPublishedEventDisplay>();
            services.AddActivity<ContentUnpublishedEvent, ContentUnpublishedEventDisplay>();
            services.AddActivity<ContentUpdatedEvent, ContentUpdatedEventDisplay>();
            services.AddActivity<ContentVersionedEvent, ContentVersionedEventDisplay>();
            services.AddActivity<DeleteContentTask, DeleteContentTaskDisplay>();
            services.AddActivity<PublishContentTask, PublishContentTaskDisplay>();
            services.AddActivity<UnpublishContentTask, UnpublishContentTaskDisplay>();
            services.AddActivity<CreateContentTask, CreateContentTaskDisplay>();

            services.AddScoped<IContentHandler, ContentsHandler>();
            services.AddScoped<IWorkflowValueSerializer, ContentItemSerializer>();
        }
    }
}
