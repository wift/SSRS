using SSRSCore.Contents.Workflows.Activities;

namespace SSRSCore.Contents.Workflows.ViewModels
{
    public class ContentUpdatedEventViewModel : ContentEventViewModel<ContentUpdatedEvent>
    {
    }
}
