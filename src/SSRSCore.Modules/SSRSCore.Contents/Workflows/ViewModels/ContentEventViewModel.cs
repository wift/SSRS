using System.Collections.Generic;
using SSRSCore.ContentManagement.Metadata.Models;
using SSRSCore.Contents.Workflows.Activities;
using SSRSCore.Workflows.ViewModels;

namespace SSRSCore.Contents.Workflows.ViewModels
{
    public class ContentEventViewModel<T> : ActivityViewModel<T> where T : ContentEvent
    {
        public ContentEventViewModel()
        {
        }

        public ContentEventViewModel(T activity)
        {
            Activity = activity;
        }

        public IList<ContentTypeDefinition> ContentTypeFilter { get; set; }
        public IList<string> SelectedContentTypeNames { get; set; } = new List<string>();
    }

}
