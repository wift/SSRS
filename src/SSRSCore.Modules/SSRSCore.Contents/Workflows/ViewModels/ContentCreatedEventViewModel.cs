using System.Collections.Generic;
using SSRSCore.Contents.Workflows.Activities;

namespace SSRSCore.Contents.Workflows.ViewModels
{
    public class ContentCreatedEventViewModel : ContentEventViewModel<ContentCreatedEvent>
    {
    }
}
