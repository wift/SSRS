using SSRSCore.Contents.Workflows.Activities;
using SSRSCore.Workflows.ViewModels;

namespace SSRSCore.Contents.Workflows.ViewModels
{
    public class ContentTaskViewModel<T> : ActivityViewModel<T> where T : ContentTask
    {
        public ContentTaskViewModel()
        {

        }

        public ContentTaskViewModel(T activity)
        {
            Activity = activity;
        }
    }
}
