using SSRSCore.ContentManagement.Metadata;
using SSRSCore.Contents.Workflows.Activities;
using SSRSCore.Contents.Workflows.ViewModels;

namespace SSRSCore.Contents.Workflows.Drivers
{
    public class ContentUnpublishedEventDisplay : ContentEventDisplayDriver<ContentUnpublishedEvent, ContentUnpublishedEventViewModel>
    {
        public ContentUnpublishedEventDisplay(IContentDefinitionManager contentDefinitionManager) : base(contentDefinitionManager)
        {
        }
    }
}
