using SSRSCore.ContentManagement;
using SSRSCore.Contents.Workflows.Activities;
using SSRSCore.Contents.Workflows.ViewModels;
using SSRSCore.Workflows.Models;

namespace SSRSCore.Contents.Workflows.Drivers
{
    public class PublishContentTaskDisplay : ContentTaskDisplayDriver<PublishContentTask, PublishContentTaskViewModel>
    {

        protected override void EditActivity(PublishContentTask activity, PublishContentTaskViewModel model)
        {
            model.Expression = activity.Content.Expression;
        }

        protected override void UpdateActivity(PublishContentTaskViewModel model, PublishContentTask activity)
        {
            activity.Content = new WorkflowExpression<IContent>(model.Expression);
        }
    }
}
