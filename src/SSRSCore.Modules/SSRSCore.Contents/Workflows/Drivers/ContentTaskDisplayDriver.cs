using SSRSCore.Contents.Workflows.Activities;
using SSRSCore.Contents.Workflows.ViewModels;
using SSRSCore.DisplayManagement.Views;
using SSRSCore.Workflows.Display;

namespace SSRSCore.Contents.Workflows.Drivers
{
    public abstract class ContentTaskDisplayDriver<TActivity, TViewModel> : ActivityDisplayDriver<TActivity, TViewModel> where TActivity : ContentTask where TViewModel : ContentTaskViewModel<TActivity>, new()
    {
        public override IDisplayResult Display(TActivity activity)
        {
            return Combine(
                Shape($"{typeof(TActivity).Name}_Fields_Thumbnail", new ContentTaskViewModel<TActivity>(activity)).Location("Thumbnail", "Content"),
                Shape($"{typeof(TActivity).Name}_Fields_Design", new ContentTaskViewModel<TActivity>(activity)).Location("Design", "Content")
            );
        }
    }
}
