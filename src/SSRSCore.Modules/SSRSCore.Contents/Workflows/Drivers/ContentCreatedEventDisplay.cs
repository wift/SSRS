using SSRSCore.ContentManagement.Metadata;
using SSRSCore.Contents.Workflows.Activities;
using SSRSCore.Contents.Workflows.ViewModels;

namespace SSRSCore.Contents.Workflows.Drivers
{
    public class ContentCreatedEventDisplay : ContentEventDisplayDriver<ContentCreatedEvent, ContentCreatedEventViewModel>
    {
        public ContentCreatedEventDisplay(IContentDefinitionManager contentDefinitionManager) : base(contentDefinitionManager)
        {
        }
    }
}
