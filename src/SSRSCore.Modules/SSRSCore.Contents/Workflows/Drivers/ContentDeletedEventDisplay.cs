using SSRSCore.ContentManagement.Metadata;
using SSRSCore.Contents.Workflows.Activities;
using SSRSCore.Contents.Workflows.ViewModels;

namespace SSRSCore.Contents.Workflows.Drivers
{
    public class ContentDeletedEventDisplay : ContentEventDisplayDriver<ContentDeletedEvent, ContentDeletedEventViewModel>
    {
        public ContentDeletedEventDisplay(IContentDefinitionManager contentDefinitionManager) : base(contentDefinitionManager)
        {
        }
    }
}
