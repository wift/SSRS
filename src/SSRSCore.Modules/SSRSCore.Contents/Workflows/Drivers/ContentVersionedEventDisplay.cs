using SSRSCore.ContentManagement.Metadata;
using SSRSCore.Contents.Workflows.Activities;
using SSRSCore.Contents.Workflows.ViewModels;

namespace SSRSCore.Contents.Workflows.Drivers
{
    public class ContentVersionedEventDisplay : ContentEventDisplayDriver<ContentVersionedEvent, ContentVersionedEventViewModel>
    {
        public ContentVersionedEventDisplay(IContentDefinitionManager contentDefinitionManager) : base(contentDefinitionManager)
        {

        }
    }
}
