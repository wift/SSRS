using SSRSCore.ContentManagement;
using SSRSCore.Contents.Workflows.Activities;
using SSRSCore.Contents.Workflows.ViewModels;
using SSRSCore.Workflows.Models;

namespace SSRSCore.Contents.Workflows.Drivers
{
    public class UnpublishContentTaskDisplay : ContentTaskDisplayDriver<UnpublishContentTask, UnpublishContentTaskViewModel>
    {

        protected override void EditActivity(UnpublishContentTask activity, UnpublishContentTaskViewModel model)
        {
            model.Expression = activity.Content.Expression;
        }

        protected override void UpdateActivity(UnpublishContentTaskViewModel model, UnpublishContentTask activity)
        {
            activity.Content = new WorkflowExpression<IContent>(model.Expression);
        }
    }
}
