using SSRSCore.ContentManagement.Metadata;
using SSRSCore.Contents.Workflows.Activities;
using SSRSCore.Contents.Workflows.ViewModels;

namespace SSRSCore.Contents.Workflows.Drivers
{
    public class ContentUpdatedEventDisplay : ContentEventDisplayDriver<ContentUpdatedEvent, ContentUpdatedEventViewModel>
    {
        public ContentUpdatedEventDisplay(IContentDefinitionManager contentDefinitionManager) : base(contentDefinitionManager)
        {
        }
    }
}
