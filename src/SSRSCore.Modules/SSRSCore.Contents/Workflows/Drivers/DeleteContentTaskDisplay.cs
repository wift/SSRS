using SSRSCore.ContentManagement;
using SSRSCore.Contents.Workflows.Activities;
using SSRSCore.Contents.Workflows.ViewModels;
using SSRSCore.Workflows.Models;

namespace SSRSCore.Contents.Workflows.Drivers
{
    public class DeleteContentTaskDisplay : ContentTaskDisplayDriver<DeleteContentTask, DeleteContentTaskViewModel>
    {
        protected override void EditActivity(DeleteContentTask activity, DeleteContentTaskViewModel model)
        {
            model.Expression = activity.Content.Expression;
        }

        protected override void UpdateActivity(DeleteContentTaskViewModel model, DeleteContentTask activity)
        {
            activity.Content = new WorkflowExpression<IContent>(model.Expression);
        }
    }
}
