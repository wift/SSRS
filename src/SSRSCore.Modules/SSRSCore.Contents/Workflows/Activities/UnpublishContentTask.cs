using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Localization;
using SSRSCore.ContentManagement;
using SSRSCore.Workflows.Abstractions.Models;
using SSRSCore.Workflows.Activities;
using SSRSCore.Workflows.Models;
using SSRSCore.Workflows.Services;

namespace SSRSCore.Contents.Workflows.Activities
{
    public class UnpublishContentTask : ContentTask
    {
        public UnpublishContentTask(IContentManager contentManager, IWorkflowScriptEvaluator scriptEvaluator, IStringLocalizer<UnpublishContentTask> localizer) : base(contentManager, scriptEvaluator, localizer)
        {
        }

        public override string Name => nameof(UnpublishContentTask);
        public override LocalizedString DisplayText => T["Unpublish Content Task"];
        public override LocalizedString Category => T["Content"];

        public override IEnumerable<Outcome> GetPossibleOutcomes(WorkflowExecutionContext workflowContext, ActivityContext activityContext)
        {
            return Outcomes(T["Unpublished"]);
        }

        public override async Task<ActivityExecutionResult> ExecuteAsync(WorkflowExecutionContext workflowContext, ActivityContext activityContext)
        {
            var content = await GetContentAsync(workflowContext);
            await ContentManager.UnpublishAsync(content.ContentItem);
            return Outcomes("Unpublished");
        }
    }
}
