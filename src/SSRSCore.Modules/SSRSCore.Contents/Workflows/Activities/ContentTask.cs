using Microsoft.Extensions.Localization;
using SSRSCore.ContentManagement;
using SSRSCore.Workflows.Activities;
using SSRSCore.Workflows.Services;

namespace SSRSCore.Contents.Workflows.Activities
{
    public abstract class ContentTask : ContentActivity, ITask
    {
        protected ContentTask(IContentManager contentManager, IWorkflowScriptEvaluator scriptEvaluator, IStringLocalizer localizer) 
            : base(contentManager, scriptEvaluator, localizer)
        {
        }
    }
}