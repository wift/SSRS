using Microsoft.Extensions.Localization;
using SSRSCore.ContentManagement;
using SSRSCore.Workflows.Services;

namespace SSRSCore.Contents.Workflows.Activities
{
    public class ContentUnpublishedEvent : ContentEvent
    {
        public ContentUnpublishedEvent(IContentManager contentManager, IWorkflowScriptEvaluator scriptEvaluator, IStringLocalizer<ContentUnpublishedEvent> localizer) : base(contentManager, scriptEvaluator, localizer)
        {
        }

        public override string Name => nameof(ContentUnpublishedEvent);
        public override LocalizedString DisplayText => T["Content Unpublished Event"];
    }
}