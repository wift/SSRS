using Microsoft.Extensions.Localization;
using SSRSCore.ContentManagement;
using SSRSCore.Workflows.Services;

namespace SSRSCore.Contents.Workflows.Activities
{
    public class ContentPublishedEvent : ContentEvent
    {
        public ContentPublishedEvent(IContentManager contentManager, IWorkflowScriptEvaluator scriptEvaluator, IStringLocalizer<ContentCreatedEvent> localizer) : base(contentManager, scriptEvaluator, localizer)
        {
        }

        public override string Name => nameof(ContentPublishedEvent);
        public override LocalizedString DisplayText => T["Content Published Event"];
    }
}