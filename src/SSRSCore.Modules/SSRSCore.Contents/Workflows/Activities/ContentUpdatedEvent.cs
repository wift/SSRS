using Microsoft.Extensions.Localization;
using SSRSCore.ContentManagement;
using SSRSCore.Workflows.Services;

namespace SSRSCore.Contents.Workflows.Activities
{
    public class ContentUpdatedEvent : ContentEvent
    {
        public ContentUpdatedEvent(IContentManager contentManager, IWorkflowScriptEvaluator scriptEvaluator, IStringLocalizer<ContentCreatedEvent> localizer) : base(contentManager, scriptEvaluator, localizer)
        {
        }

        public override string Name => nameof(ContentUpdatedEvent);
        public override LocalizedString DisplayText => T["Content Updated Event"];
    }
}