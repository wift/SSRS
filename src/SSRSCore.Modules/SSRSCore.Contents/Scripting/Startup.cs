using Microsoft.Extensions.DependencyInjection;
using SSRSCore.Modules;
using SSRSCore.Scripting;

namespace SSRSCore.Contents.Scripting
{
    [RequireFeatures("SSRSCore.Scripting")]
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IGlobalMethodProvider, ContentMethodsProvider>();
            services.AddSingleton<IGlobalMethodProvider, UrlMethodsProvider>();
        }
    }
}
