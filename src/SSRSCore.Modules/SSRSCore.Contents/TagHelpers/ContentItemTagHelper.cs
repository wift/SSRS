using Microsoft.AspNetCore.Razor.TagHelpers;
using SSRSCore.DisplayManagement;
using SSRSCore.DisplayManagement.TagHelpers;

namespace SSRSCore.Contents.TagHelpers
{
    [HtmlTargetElement("contentitem")]
    public class ContentItemTagHelper : BaseShapeTagHelper
    {
        public ContentItemTagHelper(IShapeFactory shapeFactory, IDisplayHelper displayHelper)
            : base(shapeFactory, displayHelper)
        {
            Type = "ContentItem";
        }
    }
}
