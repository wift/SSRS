using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using SSRSCore.AdminMenu.Services;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.Display;
using SSRSCore.ContentManagement.Display.ContentDisplay;
using SSRSCore.ContentManagement.Handlers;
using SSRSCore.ContentManagement.Routing;
using SSRSCore.Contents.AdminNodes;
using SSRSCore.Contents.Deployment;
using SSRSCore.Contents.Drivers;
using SSRSCore.Contents.Feeds.Builders;
using SSRSCore.Contents.Handlers;
using SSRSCore.Contents.Indexing;
using SSRSCore.Contents.Liquid;
using SSRSCore.Contents.Models;
using SSRSCore.Contents.Recipes;
using SSRSCore.Contents.Security;
using SSRSCore.Contents.Services;
using SSRSCore.Contents.Settings;
using SSRSCore.Contents.TagHelpers;
using SSRSCore.ContentTypes.Editors;
using SSRSCore.Data.Migration;
using SSRSCore.Deployment;
using SSRSCore.DisplayManagement.Descriptors;
using SSRSCore.DisplayManagement.Handlers;
using SSRSCore.DisplayManagement.Liquid.Tags;
using SSRSCore.Entities;
using SSRSCore.Feeds;
using SSRSCore.Indexing;
using SSRSCore.Liquid;
using SSRSCore.Lists.Settings;
using SSRSCore.Modules;
using SSRSCore.Navigation;
using SSRSCore.Recipes;
using SSRSCore.Security.Permissions;

namespace SSRSCore.Contents
{
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddContentManagement();
            services.AddContentManagementDisplay();
            services.AddScoped<IPermissionProvider, Permissions>();
            services.AddScoped<IPermissionProvider, ContentTypePermissions>();
            services.AddScoped<IAuthorizationHandler, ContentTypeAuthorizationHandler>();
            services.AddScoped<IShapeTableProvider, Shapes>();
            services.AddScoped<INavigationProvider, AdminMenu>();
            services.AddScoped<IContentDisplayDriver, ContentsDriver>();
            services.AddScoped<IContentHandler, ContentsHandler>();
            services.AddRecipeExecutionStep<ContentStep>();

            services.AddScoped<IContentItemIndexHandler, FullTextContentIndexHandler>();
            services.AddScoped<IContentItemIndexHandler, AspectsContentIndexHandler>();
            services.AddScoped<IContentItemIndexHandler, DefaultContentIndexHandler>();
            services.AddScoped<IContentAliasProvider, ContentItemIdAliasProvider>();
            services.AddScoped<IContentItemIndexHandler, ContentItemIndexCoordinator>();

            services.AddIdGeneration();
            services.AddScoped<IDataMigration, Migrations>();

            // Common Part
            services.AddContentPart<CommonPart>();

            services.AddScoped<IContentTypePartDefinitionDisplayDriver, CommonPartSettingsDisplayDriver>();
            services.AddScoped<IContentPartDisplayDriver, DateEditorDriver>();
            services.AddScoped<IContentPartDisplayDriver, OwnerEditorDriver>();

            // FullTextAspect
            services.AddScoped<IContentTypeDefinitionDisplayDriver, FullTextAspectSettingsDisplayDriver>();
            services.AddScoped<IContentHandler, FullTextAspectSettingsHandler>();

            // Feeds
            // TODO: Move to feature
            services.AddScoped<IFeedItemBuilder, CommonFeedItemBuilder>();

            services.AddTagHelpers<ContentLinkTagHelper>();
            services.AddTagHelpers<ContentItemTagHelper>();
            services.Configure<AutorouteOptions>(options =>
            {
                if (options.GlobalRouteValues.Count == 0)
                {
                    options.GlobalRouteValues = new RouteValueDictionary
                    {
                        {"Area", "SSRSCore.Contents"},
                        {"Controller", "Item"},
                        {"Action", "Display"}
                    };

                    options.ContentItemIdKey = "contentItemId";
                }
            });
        }

        public override void Configure(IApplicationBuilder builder, IEndpointRouteBuilder routes, IServiceProvider serviceProvider)
        {
            routes.MapAreaControllerRoute(
                name: "DisplayContentItem",
                areaName: "SSRSCore.Contents",
                pattern: "Contents/ContentItems/{contentItemId}",
                defaults: new { controller = "Item", action = "Display" }
            );

            routes.MapAreaControllerRoute(
                name: "PreviewContentItem",
                areaName: "SSRSCore.Contents",
                pattern: "Contents/ContentItems/{contentItemId}/Preview",
                defaults: new { controller = "Item", action = "Preview" }
            );

            routes.MapAreaControllerRoute(
                name: "PreviewContentItemVersion",
                areaName: "SSRSCore.Contents",
                pattern: "Contents/ContentItems/{contentItemId}/Version/{version}/Preview",
                defaults: new { controller = "Item", action = "Preview" }
            );

            // Admin
            routes.MapAreaControllerRoute(
                name: "EditContentItem",
                areaName: "SSRSCore.Contents",
                pattern: "Admin/Contents/ContentItems/{contentItemId}/Edit",
                defaults: new { area = "SSRSCore.Contents", controller = "Admin", action = "Edit" }
            );

            routes.MapAreaControllerRoute(
                name: "CreateContentItem",
                areaName: "SSRSCore.Contents",
                pattern: "Admin/Contents/ContentTypes/{id}/Create",
                defaults: new { controller = "Admin", action = "Create" }
            );

            routes.MapAreaControllerRoute(
                name: "AdminContentItem",
                areaName: "SSRSCore.Contents",
                pattern: "Admin/Contents/ContentItems/{contentItemId}/Display",
                defaults: new { controller = "Admin", action = "Display" }
            );

            routes.MapAreaControllerRoute(
                name: "ListContentItems",
                areaName: "SSRSCore.Contents",
                pattern: "Admin/Contents/ContentItems/{contentTypeId?}",
                defaults: new {controller = "Admin", action = "List" }
            );
        }
    }

    [RequireFeatures("SSRSCore.Liquid")]
    public class LiquidStartup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<ILiquidTemplateEventHandler, ContentLiquidTemplateEventHandler>();

            services.AddLiquidFilter<BuildDisplayFilter>("shape_build_display");
            services.AddLiquidFilter<ContentItemFilter>("content_item_id");
        }
    }

    [RequireFeatures("SSRSCore.Deployment")]
    public class DeploymentStartup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<IDeploymentSource, AllContentDeploymentSource>();
            services.AddSingleton<IDeploymentStepFactory>(new DeploymentStepFactory<AllContentDeploymentStep>());
            services.AddScoped<IDisplayDriver<DeploymentStep>, AllContentDeploymentStepDriver>();

            services.AddTransient<IDeploymentSource, ContentDeploymentSource>();
            services.AddSingleton<IDeploymentStepFactory>(new DeploymentStepFactory<ContentDeploymentStep>());
            services.AddScoped<IDisplayDriver<DeploymentStep>, ContentDeploymentStepDriver>();
        }
    }


    [RequireFeatures("SSRSCore.AdminMenu")]
    public class AdminMenuStartup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IAdminNodeProviderFactory>(new AdminNodeProviderFactory<ContentTypesAdminNode>());
            services.AddScoped<IAdminNodeNavigationBuilder, ContentTypesAdminNodeNavigationBuilder>();
            services.AddScoped<IDisplayDriver<MenuItem>, ContentTypesAdminNodeDriver>();
        }
    }

    [Feature("SSRSCore.Contents.FileContentDefinition")]
    public class FileContentDefinitionStartup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddFileContentDefinitionStore();
        }
    }
}
