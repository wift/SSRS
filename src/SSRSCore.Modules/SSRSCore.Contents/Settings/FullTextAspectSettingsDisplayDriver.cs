using System.Threading.Tasks;
using Microsoft.Extensions.Localization;
using SSRSCore.ContentManagement.Metadata.Models;
using SSRSCore.Contents.Models;
using SSRSCore.Contents.ViewModels;
using SSRSCore.ContentTypes.Editors;
using SSRSCore.DisplayManagement.Views;
using SSRSCore.Liquid;

namespace SSRSCore.Contents.Settings
{
    public class FullTextAspectSettingsDisplayDriver : ContentTypeDefinitionDisplayDriver
    {

        private readonly ILiquidTemplateManager _templateManager;

        public FullTextAspectSettingsDisplayDriver(
            ILiquidTemplateManager templateManager,
            IStringLocalizer<FullTextAspectSettingsDisplayDriver> localizer)
        {
            _templateManager = templateManager;
            T = localizer;
        }

        public IStringLocalizer T { get; private set; }

        public override IDisplayResult Edit(ContentTypeDefinition contentTypeDefinition)
        {
            return Initialize<FullTextAspectSettingsViewModel>("FullTextAspectSettings_Edit", model =>
            {
                var settings = contentTypeDefinition.GetSettings<FullTextAspectSettings>();

                model.IncludeFullTextTemplate = settings.IncludeFullTextTemplate;
                model.FullTextTemplate = settings.FullTextTemplate;
                model.IncludeDisplayText = settings.IncludeDisplayText;
                model.IncludeBodyAspect = settings.IncludeBodyAspect;
            }).Location("Content:6");
        }

        public override async Task<IDisplayResult> UpdateAsync(ContentTypeDefinition contentTypeDefinition, UpdateTypeEditorContext context)
        {
            var model = new FullTextAspectSettingsViewModel();

            await context.Updater.TryUpdateModelAsync(model, Prefix, 
                m => m.IncludeFullTextTemplate,
                m => m.FullTextTemplate,
                m => m.IncludeDisplayText,
                m => m.IncludeBodyAspect);

            if (!string.IsNullOrEmpty(model.FullTextTemplate) && !_templateManager.Validate(model.FullTextTemplate, out var errors))
            {
                context.Updater.ModelState.AddModelError(nameof(model.FullTextTemplate), T["Full-text doesn't contain a valid Liquid expression. Details: {0}", string.Join(" ", errors)]);
            } 
            else 
            {
                context.Builder.WithSettings(new FullTextAspectSettings
                {
                    IncludeFullTextTemplate = model.IncludeFullTextTemplate,
                    FullTextTemplate = model.FullTextTemplate,
                    IncludeDisplayText = model.IncludeDisplayText,
                    IncludeBodyAspect = model.IncludeBodyAspect
                });
            }

            return Edit(contentTypeDefinition, context.Updater);
        }
    }
}
