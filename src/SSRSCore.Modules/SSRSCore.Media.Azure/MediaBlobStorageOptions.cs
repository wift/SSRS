using System;
using SSRSCore.FileStorage.AzureBlob;

namespace SSRSCore.Media.Azure
{
    public class MediaBlobStorageOptions : BlobStorageOptions
    {
        [Obsolete("PublicHostName is obsolete. Use MediaOptions.CdnBaseUrl instead.")]
        public string PublicHostName { get; set; }
    }
}
