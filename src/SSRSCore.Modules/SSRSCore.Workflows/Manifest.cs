using SSRSCore.Modules.Manifest;

[assembly: Module(
    Name = "Workflows",
    Author = "The Orchard Team",
    Website = "https://orchardproject.net",
    Version = "2.0.0",
    Description = "The Workflows module provides tools and APIs to create custom workflows",
    Category = "Workflows"
)]

[assembly: Feature(
    Id = "SSRSCore.Workflows",
    Name = "Workflows",
    Description = "The Workflows module provides tools and APIs to create custom workflows",
    Dependencies = new[] { "SSRSCore.Liquid", "SSRSCore.Scripting" },
    Category = "Workflows"
)]

[assembly: Feature(
    Id = "SSRSCore.Workflows.Http",
    Name = "HTTP Workflows Activities",
    Description = "Provides HTTP-related services and activities.",
    Dependencies = new[] { "SSRSCore.Workflows" },
    Category = "Workflows"
)]

[assembly: Feature(
    Id = "SSRSCore.Workflows.Timers",
    Name = "Timer Workflow Activities",
    Description = "Provides timer-based services and activities.",
    Dependencies = new[] { "SSRSCore.Workflows" },
    Category = "Workflows"
)]