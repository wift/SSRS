using System;
using System.Collections.Generic;
using System.Text;
using SSRSCore.Deployment;

namespace SSRSCore.Workflows.Deployment
{
    public class AllWorkflowTypeDeploymentStep : DeploymentStep
    {
        public AllWorkflowTypeDeploymentStep()
        {
            Name = "AllWorkflowType";
        }
    }
}
