using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using SSRSCore.BackgroundTasks;
using SSRSCore.Workflows.Services;

namespace SSRSCore.Workflows.Timers
{
    [BackgroundTask(Schedule = "* * * * *", Description = "Trigger workflow timer events.")]
    public class TimerBackgroundTask : IBackgroundTask
    {
        public Task DoWorkAsync(IServiceProvider serviceProvider, CancellationToken cancellationToken)
        {
            var workflowManager = serviceProvider.GetRequiredService<IWorkflowManager>();
            return workflowManager.TriggerEventAsync(TimerEvent.EventName);
        }
    }
}
