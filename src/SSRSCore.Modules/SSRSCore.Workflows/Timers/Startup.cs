using Microsoft.Extensions.DependencyInjection;
using SSRSCore.BackgroundTasks;
using SSRSCore.Modules;
using SSRSCore.Workflows.Helpers;

namespace SSRSCore.Workflows.Timers
{
    [Feature("SSRSCore.Workflows.Timers")]
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddActivity<TimerEvent, TimerEventDisplay>();
            services.AddSingleton<IBackgroundTask, TimerBackgroundTask>();
        }
    }
}
