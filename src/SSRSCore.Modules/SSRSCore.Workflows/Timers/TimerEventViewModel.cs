using System.ComponentModel.DataAnnotations;

namespace SSRSCore.Workflows.Timers
{
    public class TimerEventViewModel
    {
        [Required]
        public string CronExpression { get; set; }
    }
}
