using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using SSRSCore.Liquid;
using SSRSCore.Modules;
using SSRSCore.Scripting;
using SSRSCore.Workflows.Helpers;
using SSRSCore.Workflows.Http.Activities;
using SSRSCore.Workflows.Http.Drivers;
using SSRSCore.Workflows.Http.Filters;
using SSRSCore.Workflows.Http.Handlers;
using SSRSCore.Workflows.Http.Liquid;
using SSRSCore.Workflows.Http.Scripting;
using SSRSCore.Workflows.Http.Services;
using SSRSCore.Workflows.Http.WorkflowContextProviders;
using SSRSCore.Workflows.Services;

namespace SSRSCore.Workflows.Http
{
    [Feature("SSRSCore.Workflows.Http")]
    [RequireFeatures("SSRSCore.Workflows")]
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.Configure<MvcOptions>((options) =>
            {
                options.Filters.Add(typeof(WorkflowActionFilter));
            });

            services.AddScoped<IWorkflowTypeEventHandler, WorkflowTypeRoutesHandler>();
            services.AddScoped<IWorkflowHandler, WorkflowRoutesHandler>();

            services.AddSingleton<IWorkflowTypeRouteEntries, WorkflowTypeRouteEntries>();
            services.AddSingleton<IWorkflowInstanceRouteEntries, WorkflowRouteEntries>();
            services.AddSingleton<IGlobalMethodProvider, HttpMethodsProvider>();
            services.AddScoped<IWorkflowExecutionContextHandler, SignalWorkflowExecutionContextHandler>();

            services.AddActivity<HttpRequestEvent, HttpRequestEventDisplay>();
            services.AddActivity<HttpRequestFilterEvent, HttpRequestFilterEventDisplay>();
            services.AddActivity<HttpRedirectTask, HttpRedirectTaskDisplay>();
            services.AddActivity<HttpRequestTask, HttpRequestTaskDisplay>();
            services.AddActivity<HttpResponseTask, HttpResponseTaskDisplay>();
            services.AddActivity<SignalEvent, SignalEventDisplay>();

            services.AddSingleton<IGlobalMethodProvider, TokenMethodProvider>();

            services.AddScoped<ILiquidTemplateEventHandler, SignalLiquidTemplateHandler>();
            services.AddLiquidFilter<SignalUrlFilter>("signal_url");

            services.AddTransient<IModularTenantEvents, HttpRequestRouteActivator>();
        }

        public override void Configure(IApplicationBuilder app, IEndpointRouteBuilder routes, IServiceProvider serviceProvider)
        {
            routes.MapAreaControllerRoute(
                name: "HttpWorkflow",
                areaName: "SSRSCore.Workflows",
                pattern: "workflows/{action}",
                defaults: new { controller = "HttpWorkflow" }
            );

            routes.MapAreaControllerRoute(
                name: "InvokeWorkflow",
                areaName: "SSRSCore.Workflows",
                pattern: "workflows/invoke/{token}",
                defaults: new { controller = "HttpWorkflow", action = "Invoke" }
            );
        }
    }
}
