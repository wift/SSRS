using System.Collections.Generic;
using Microsoft.AspNetCore.Routing;
using SSRSCore.Workflows.Http.Models;

namespace SSRSCore.Workflows.Http.Services
{
    internal interface IWorkflowRouteEntries
    {
        IEnumerable<WorkflowRoutesEntry> GetWorkflowRouteEntries(string httpMethod, RouteValueDictionary routeValues);
        void AddEntries(IEnumerable<WorkflowRoutesEntry> entries);
        void RemoveEntries(string workflowId);
    }
}
