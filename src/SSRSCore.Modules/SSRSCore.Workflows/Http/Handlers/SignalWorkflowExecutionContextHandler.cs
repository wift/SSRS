using System.Threading.Tasks;
using SSRSCore.Workflows.Http.Scripting;
using SSRSCore.Workflows.Models;
using SSRSCore.Workflows.Services;

namespace SSRSCore.Workflows.Http.WorkflowContextProviders
{
    public class SignalWorkflowExecutionContextHandler : WorkflowExecutionContextHandlerBase
    {
        private readonly ISecurityTokenService _signalService;

        public SignalWorkflowExecutionContextHandler(ISecurityTokenService signalService)
        {
            _signalService = signalService;
        }

        public override Task EvaluatingScriptAsync(WorkflowExecutionScriptContext context)
        {
            context.ScopedMethodProviders.Add(new SignalMethodProvider(context.WorkflowContext, _signalService));
            return Task.CompletedTask;
        }
    }
}
