using System.ComponentModel.DataAnnotations;

namespace SSRSCore.Workflows.Http.ViewModels
{
    public class HttpRedirectTaskViewModel
    {
        [Required]
        public string Location { get; set; }
        public bool Permanent { get; set; }
    }
}
