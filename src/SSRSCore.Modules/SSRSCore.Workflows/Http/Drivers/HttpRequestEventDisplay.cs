using SSRSCore.Workflows.Display;
using SSRSCore.Workflows.Http.Activities;
using SSRSCore.Workflows.Http.ViewModels;

namespace SSRSCore.Workflows.Http.Drivers
{
    public class HttpRequestEventDisplay : ActivityDisplayDriver<HttpRequestEvent, HttpRequestEventViewModel>
    {
        protected override void EditActivity(HttpRequestEvent activity, HttpRequestEventViewModel model)
        {
            model.HttpMethod = activity.HttpMethod;
            model.Url = activity.Url;
            model.ValidateAntiforgeryToken = activity.ValidateAntiforgeryToken;
            model.TokenLifeSpan = activity.TokenLifeSpan;
        }

        protected override void UpdateActivity(HttpRequestEventViewModel model, HttpRequestEvent activity)
        {
            activity.HttpMethod = model.HttpMethod?.Trim();
            activity.Url = model.Url?.Trim();
            activity.ValidateAntiforgeryToken = model.ValidateAntiforgeryToken;
            activity.TokenLifeSpan = model.TokenLifeSpan;
        }
    }
}
