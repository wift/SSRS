using SSRSCore.Workflows.Display;
using SSRSCore.Workflows.Http.Activities;
using SSRSCore.Workflows.Http.ViewModels;
using SSRSCore.Workflows.Models;

namespace SSRSCore.Workflows.Http.Drivers
{
    public class HttpRedirectTaskDisplay : ActivityDisplayDriver<HttpRedirectTask, HttpRedirectTaskViewModel>
    {
        protected override void EditActivity(HttpRedirectTask activity, HttpRedirectTaskViewModel model)
        {
            model.Location = activity.Location.Expression;
            model.Permanent = activity.Permanent;
        }

        protected override void UpdateActivity(HttpRedirectTaskViewModel model, HttpRedirectTask activity)
        {
            activity.Location = new WorkflowExpression<string>(model.Location?.Trim());
            activity.Permanent = model.Permanent;
        }
    }
}
