using SSRSCore.Workflows.Display;
using SSRSCore.Workflows.Http.Activities;
using SSRSCore.Workflows.Http.ViewModels;
using SSRSCore.Workflows.Models;

namespace SSRSCore.Workflows.Http.Drivers
{
    public class HttpResponseTaskDisplay : ActivityDisplayDriver<HttpResponseTask, HttpResponseTaskViewModel>
    {
        protected override void EditActivity(HttpResponseTask activity, HttpResponseTaskViewModel model)
        {
            model.HttpStatusCode = activity.HttpStatusCode;
            model.Content = activity.Content.Expression;
            model.ContentType = activity.ContentType.Expression;
            model.Headers = activity.Headers.Expression;
        }

        protected override void UpdateActivity(HttpResponseTaskViewModel model, HttpResponseTask activity)
        {
            activity.HttpStatusCode = model.HttpStatusCode;
            activity.Content = new WorkflowExpression<string>(model.Content);
            activity.ContentType = new WorkflowExpression<string>(model.ContentType?.Trim());
            activity.Headers = new WorkflowExpression<string>(model.Headers?.Trim());
        }
    }
}
