using System.ComponentModel.DataAnnotations;

namespace SSRSCore.Workflows.ViewModels
{
    public class SignalEventViewModel
    {
        [Required]
        public string SignalNameExpression { get; set; }
    }
}
