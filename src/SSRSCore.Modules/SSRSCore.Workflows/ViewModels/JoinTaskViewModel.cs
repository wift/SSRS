using SSRSCore.Workflows.Activities;

namespace SSRSCore.Workflows.ViewModels
{
    public class JoinTaskViewModel
    {
        public JoinTask.JoinMode Mode { get; set; }
    }
}
