using System.ComponentModel.DataAnnotations;
using SSRSCore.DisplayManagement.Notify;

namespace SSRSCore.Workflows.ViewModels
{
    public class NotifyTaskViewModel
    {
        public NotifyType NotificationType { get; set; }

        [Required]
        public string Message { get; set; }
    }
}
