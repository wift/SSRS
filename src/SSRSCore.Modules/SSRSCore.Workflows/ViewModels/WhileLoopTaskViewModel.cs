using System.ComponentModel.DataAnnotations;

namespace SSRSCore.Workflows.ViewModels
{
    public class WhileLoopTaskViewModel
    {
        [Required]
        public string ConditionExpression { get; set; }
    }
}
