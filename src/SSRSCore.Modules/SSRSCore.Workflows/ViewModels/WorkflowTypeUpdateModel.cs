namespace SSRSCore.Workflows.ViewModels
{
    public class WorkflowTypeUpdateModel
    {
        public int Id { get; set; }
        public string State { get; set; }
    }
}