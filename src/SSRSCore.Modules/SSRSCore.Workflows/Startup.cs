using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using SSRSCore.Data.Migration;
using SSRSCore.Deployment;
using SSRSCore.DisplayManagement.Handlers;
using SSRSCore.Entities;
using SSRSCore.Modules;
using SSRSCore.Navigation;
using SSRSCore.Recipes;
using SSRSCore.Security.Permissions;
using SSRSCore.Workflows.Activities;
using SSRSCore.Workflows.Deployment;
using SSRSCore.Workflows.Drivers;
using SSRSCore.Workflows.Evaluators;
using SSRSCore.Workflows.Expressions;
using SSRSCore.Workflows.Helpers;
using SSRSCore.Workflows.Indexes;
using SSRSCore.Workflows.Recipes;
using SSRSCore.Workflows.Services;
using SSRSCore.Workflows.WorkflowContextProviders;
using YesSql.Indexes;

namespace SSRSCore.Workflows
{
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddIdGeneration();
            services.AddSingleton<IWorkflowTypeIdGenerator, WorkflowTypeIdGenerator>();
            services.AddSingleton<IWorkflowIdGenerator, WorkflowIdGenerator>();
            services.AddSingleton<IActivityIdGenerator, ActivityIdGenerator>();

            services.AddScoped(typeof(Resolver<>));
            services.AddSingleton<ISecurityTokenService, SecurityTokenService>();
            services.AddScoped<IActivityLibrary, ActivityLibrary>();
            services.AddScoped<IWorkflowTypeStore, WorkflowTypeStore>();
            services.AddScoped<IWorkflowStore, WorkflowStore>();
            services.AddScoped<IWorkflowManager, WorkflowManager>();
            services.AddScoped<IActivityDisplayManager, ActivityDisplayManager>();
            services.AddScoped<IDataMigration, Migrations>();
            services.AddScoped<INavigationProvider, AdminMenu>();
            services.AddScoped<IPermissionProvider, Permissions>();
            services.AddScoped<IDisplayDriver<IActivity>, MissingActivityDisplay>();
            services.AddSingleton<IIndexProvider, WorkflowTypeIndexProvider>();
            services.AddSingleton<IIndexProvider, WorkflowIndexProvider>();
            services.AddScoped<IWorkflowExecutionContextHandler, DefaultWorkflowExecutionContextHandler>();
            services.AddScoped<IWorkflowExpressionEvaluator, LiquidWorkflowExpressionEvaluator>();
            services.AddScoped<IWorkflowScriptEvaluator, JavaScriptWorkflowScriptEvaluator>();

            services.AddActivity<Activity, ActivityMetadataDisplay>();
            services.AddActivity<NotifyTask, NotifyTaskDisplay>();
            services.AddActivity<SetPropertyTask, SetVariableTaskDisplay>();
            services.AddActivity<SetOutputTask, SetOutputTaskDisplay>();
            services.AddActivity<CorrelateTask, CorrelateTaskDisplay>();
            services.AddActivity<ForkTask, ForkTaskDisplay>();
            services.AddActivity<JoinTask, JoinTaskDisplay>();
            services.AddActivity<ForLoopTask, ForLoopTaskDisplay>();
            services.AddActivity<ForEachTask, ForEachTaskDisplay>();
            services.AddActivity<WhileLoopTask, WhileLoopTaskDisplay>();
            services.AddActivity<IfElseTask, IfElseTaskDisplay>();
            services.AddActivity<ScriptTask, ScriptTaskDisplay>();
            services.AddActivity<LogTask, LogTaskDisplay>();

            services.AddActivity<CommitTransactionTask, CommitTransactionTaskDisplay>();

            services.AddRecipeExecutionStep<WorkflowTypeStep>();
        }

        public override void Configure(IApplicationBuilder app, IEndpointRouteBuilder routes, IServiceProvider serviceProvider)
        {
            routes.MapAreaControllerRoute(
                name: "AddActivity",
                areaName: "SSRSCore.Workflows",
                pattern: "Admin/Workflows/Types/{workflowTypeId}/Activity/{activityName}/Add",
                defaults: new { controller = "Activity", action = "Create" }
            );

            routes.MapAreaControllerRoute(
                name: "EditActivity",
                areaName: "SSRSCore.Workflows",
                pattern: "Admin/Workflows/Types/{workflowTypeId}/Activity/{activityId}/Edit",
                defaults: new { controller = "Activity", action = "Edit" }
            );

            routes.MapAreaControllerRoute(
                name: "Workflows",
                areaName: "SSRSCore.Workflows",
                pattern: "Admin/Workflows/Types/{workflowTypeId}/Instances/{action}",
                defaults: new { controller = "Workflow", action = "Index" }
            );

            routes.MapAreaControllerRoute(
                name: "WorkflowTypes",
                areaName: "SSRSCore.Workflows",
                pattern: "Admin/Workflows/Types/{action}/{id?}",
                defaults: new { controller = "WorkflowType", action = "Index" }
            );
        }
    }

    [RequireFeatures("SSRSCore.Deployment")]
    public class DeploymentStartup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<IDeploymentSource, AllWorkflowTypeDeploymentSource>();
            services.AddSingleton<IDeploymentStepFactory>(new DeploymentStepFactory<AllWorkflowTypeDeploymentStep>());
            services.AddScoped<IDisplayDriver<DeploymentStep>, AllWorkflowTypeDeploymentStepDriver>();
        }
    }
}
