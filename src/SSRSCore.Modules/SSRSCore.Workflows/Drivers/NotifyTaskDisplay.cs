using SSRSCore.Workflows.Activities;
using SSRSCore.Workflows.Display;
using SSRSCore.Workflows.Models;
using SSRSCore.Workflows.ViewModels;

namespace SSRSCore.Workflows.Drivers
{
    public class NotifyTaskDisplay : ActivityDisplayDriver<NotifyTask, NotifyTaskViewModel>
    {
        protected override void EditActivity(NotifyTask activity, NotifyTaskViewModel model)
        {
            model.NotificationType = activity.NotificationType;
            model.Message = activity.Message.Expression;
        }

        protected override void UpdateActivity(NotifyTaskViewModel model, NotifyTask activity)
        {
            activity.NotificationType = model.NotificationType;
            activity.Message = new WorkflowExpression<string>(model.Message);
        }
    }
}
