using SSRSCore.Workflows.Activities;
using SSRSCore.Workflows.Display;
using SSRSCore.Workflows.Models;
using SSRSCore.Workflows.ViewModels;

namespace SSRSCore.Workflows.Drivers
{
    public class SetVariableTaskDisplay : ActivityDisplayDriver<SetPropertyTask, SetPropertyTaskViewModel>
    {
        protected override void EditActivity(SetPropertyTask source, SetPropertyTaskViewModel model)
        {
            model.PropertyName = source.PropertyName;
            model.Value = source.Value.Expression;
        }

        protected override void UpdateActivity(SetPropertyTaskViewModel model, SetPropertyTask activity)
        {
            activity.PropertyName = model.PropertyName.Trim();
            activity.Value = new WorkflowExpression<object>(model.Value);
        }
    }
}
