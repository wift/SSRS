using SSRSCore.Workflows.Activities;
using SSRSCore.Workflows.Display;
using SSRSCore.Workflows.Models;
using SSRSCore.Workflows.ViewModels;

namespace SSRSCore.Workflows.Drivers
{
    public class WhileLoopTaskDisplay : ActivityDisplayDriver<WhileLoopTask, WhileLoopTaskViewModel>
    {
        protected override void EditActivity(WhileLoopTask source, WhileLoopTaskViewModel model)
        {
            model.ConditionExpression = source.Condition.Expression;
        }

        protected override void UpdateActivity(WhileLoopTaskViewModel model, WhileLoopTask activity)
        {
            activity.Condition = new WorkflowExpression<bool>(model.ConditionExpression.Trim());
        }
    }
}
