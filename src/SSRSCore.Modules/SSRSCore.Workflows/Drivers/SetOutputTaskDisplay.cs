using SSRSCore.Workflows.Activities;
using SSRSCore.Workflows.Display;
using SSRSCore.Workflows.Models;
using SSRSCore.Workflows.ViewModels;

namespace SSRSCore.Workflows.Drivers
{
    public class SetOutputTaskDisplay : ActivityDisplayDriver<SetOutputTask, SetOutputTaskViewModel>
    {
        protected override void EditActivity(SetOutputTask source, SetOutputTaskViewModel model)
        {
            model.OutputName = source.OutputName;
            model.Value = source.Value.Expression;
        }

        protected override void UpdateActivity(SetOutputTaskViewModel model, SetOutputTask activity)
        {
            activity.OutputName = model.OutputName.Trim();
            activity.Value = new WorkflowExpression<object>(model.Value);
        }
    }
}
