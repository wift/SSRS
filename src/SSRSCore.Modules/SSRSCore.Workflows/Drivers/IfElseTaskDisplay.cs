using SSRSCore.Workflows.Display;
using SSRSCore.Workflows.Activities;
using SSRSCore.Workflows.Models;
using SSRSCore.Workflows.ViewModels;

namespace SSRSCore.Workflows.Drivers
{
    public class IfElseTaskDisplay : ActivityDisplayDriver<IfElseTask, IfElseTaskViewModel>
    {
        protected override void EditActivity(IfElseTask activity, IfElseTaskViewModel model)
        {
            model.ConditionExpression = activity.Condition.Expression;
        }

        protected override void UpdateActivity(IfElseTaskViewModel model, IfElseTask activity)
        {
            activity.Condition = new WorkflowExpression<bool>(model.ConditionExpression);
        }
    }
}
