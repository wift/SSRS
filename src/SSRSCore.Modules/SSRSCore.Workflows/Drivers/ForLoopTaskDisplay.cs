using SSRSCore.Workflows.Display;
using SSRSCore.Workflows.Activities;
using SSRSCore.Workflows.Models;
using SSRSCore.Workflows.ViewModels;

namespace SSRSCore.Workflows.Drivers
{
    public class ForLoopTaskDisplay : ActivityDisplayDriver<ForLoopTask, ForLoopTaskViewModel>
    {
        protected override void EditActivity(ForLoopTask activity, ForLoopTaskViewModel model)
        {
            model.FromExpression = activity.From.Expression;
            model.ToExpression = activity.To.Expression;
            model.LoopVariableName = activity.LoopVariableName;
            model.StepExpression = activity.Step.Expression;
        }

        protected override void UpdateActivity(ForLoopTaskViewModel model, ForLoopTask activity)
        {
            activity.From = new WorkflowExpression<double>(model.FromExpression);
            activity.To = new WorkflowExpression<double>(model.ToExpression);
            activity.Step = new WorkflowExpression<double>(model.StepExpression);
            activity.LoopVariableName = model.LoopVariableName;
        }
    }
}
