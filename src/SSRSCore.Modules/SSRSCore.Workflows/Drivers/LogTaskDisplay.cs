using SSRSCore.Workflows.Activities;
using SSRSCore.Workflows.Display;
using SSRSCore.Workflows.Models;
using SSRSCore.Workflows.ViewModels;

namespace SSRSCore.Workflows.Drivers
{
    public class LogTaskDisplay : ActivityDisplayDriver<LogTask, LogTaskViewModel>
    {
        protected override void EditActivity(LogTask activity, LogTaskViewModel model)
        {
            model.LogLevel = activity.LogLevel;
            model.Text = activity.Text.Expression;
        }

        protected override void UpdateActivity(LogTaskViewModel model, LogTask activity)
        {
            activity.LogLevel = model.LogLevel;
            activity.Text = new WorkflowExpression<string>(model.Text);
        }
    }
}
