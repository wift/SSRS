using SSRSCore.Workflows.Display;
using SSRSCore.Workflows.Activities;
using SSRSCore.Workflows.Models;
using SSRSCore.Workflows.ViewModels;
using System.Collections.Generic;

namespace SSRSCore.Workflows.Drivers
{
    public class ForEachTaskDisplay : ActivityDisplayDriver<ForEachTask, ForEachTaskViewModel>
    {
        protected override void EditActivity(ForEachTask activity, ForEachTaskViewModel model)
        {
            model.EnumerableExpression = activity.Enumerable.Expression;
            model.LoopVariableName = activity.LoopVariableName;
        }

        protected override void UpdateActivity(ForEachTaskViewModel model, ForEachTask activity)
        {
            activity.LoopVariableName = model.LoopVariableName;
            activity.Enumerable = new WorkflowExpression<IEnumerable<object>>(model.EnumerableExpression);
        }
    }
}
