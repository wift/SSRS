using SSRSCore.Workflows.Display;
using SSRSCore.DisplayManagement.Views;
using SSRSCore.Workflows.Activities;

namespace SSRSCore.Workflows.Drivers
{
    public class MissingActivityDisplay : ActivityDisplayDriver<MissingActivity>
    {
        public override IDisplayResult Display(MissingActivity activity)
        {
            return View($"MissingActivity_Fields_Design", activity).Location("Design", "Content");
        }
    }
}
