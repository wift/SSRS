using SSRSCore.Workflows.Activities;
using SSRSCore.Workflows.Display;
using SSRSCore.Workflows.Models;
using SSRSCore.Workflows.ViewModels;

namespace SSRSCore.Workflows.Drivers
{
    public class CorrelateTaskDisplay : ActivityDisplayDriver<CorrelateTask, CorrelateTaskViewModel>
    {
        protected override void EditActivity(CorrelateTask activity, CorrelateTaskViewModel model)
        {
            model.Value = activity.Value.Expression;
        }

        protected override void UpdateActivity(CorrelateTaskViewModel model, CorrelateTask activity)
        {
            activity.Value = new WorkflowExpression<string>(model.Value);
        }
    }
}
