using SSRSCore.Workflows.Activities;
using SSRSCore.Workflows.Display;
using SSRSCore.Workflows.ViewModels;

namespace SSRSCore.Workflows.Drivers
{
    public class JoinTaskDisplay : ActivityDisplayDriver<JoinTask, JoinTaskViewModel>
    {
        protected override void EditActivity(JoinTask activity, JoinTaskViewModel model)
        {
            model.Mode = activity.Mode;
        }

        protected override void UpdateActivity(JoinTaskViewModel model, JoinTask activity)
        {
            activity.Mode = model.Mode;
        }
    }
}
