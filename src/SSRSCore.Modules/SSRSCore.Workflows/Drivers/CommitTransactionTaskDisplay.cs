using SSRSCore.Workflows.Activities;
using SSRSCore.Workflows.Display;
using SSRSCore.Workflows.ViewModels;

namespace SSRSCore.Workflows.Drivers
{
    public class CommitTransactionTaskDisplay : ActivityDisplayDriver<CommitTransactionTask, CommitTransactionTaskViewModel>
    {
        protected override void EditActivity(CommitTransactionTask activity, CommitTransactionTaskViewModel model)
        {
        }

        protected override void UpdateActivity(CommitTransactionTaskViewModel model, CommitTransactionTask activity)
        {
        }
    }
}
