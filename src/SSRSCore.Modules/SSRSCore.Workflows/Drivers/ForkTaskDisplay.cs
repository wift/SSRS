using System;
using System.Linq;
using SSRSCore.Workflows.Activities;
using SSRSCore.Workflows.Display;
using SSRSCore.Workflows.ViewModels;

namespace SSRSCore.Workflows.Drivers
{
    public class ForkTaskDisplay : ActivityDisplayDriver<ForkTask, ForkTaskViewModel>
    {
        protected override void EditActivity(ForkTask activity, ForkTaskViewModel model)
        {
            model.Forks = string.Join(", ", activity.Forks);
        }

        protected override void UpdateActivity(ForkTaskViewModel model, ForkTask activity)
        {
            activity.Forks = model.Forks.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim()).ToList();
        }
    }
}
