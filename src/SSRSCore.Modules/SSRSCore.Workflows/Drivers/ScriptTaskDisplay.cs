using System;
using System.Linq;
using SSRSCore.Workflows.Activities;
using SSRSCore.Workflows.Display;
using SSRSCore.Workflows.Models;
using SSRSCore.Workflows.ViewModels;

namespace SSRSCore.Workflows.Drivers
{
    public class ScriptTaskDisplay : ActivityDisplayDriver<ScriptTask, ScriptTaskViewModel>
    {
        protected override void EditActivity(ScriptTask source, ScriptTaskViewModel model)
        {
            model.AvailableOutcomes = string.Join(", ", source.AvailableOutcomes);
            model.Script = source.Script.Expression;
        }

        protected override void UpdateActivity(ScriptTaskViewModel model, ScriptTask activity)
        {
            activity.AvailableOutcomes = model.AvailableOutcomes.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim()).ToList();
            activity.Script = new WorkflowExpression<object>(model.Script);
        }
    }
}
