using System.Threading.Tasks;
using SSRSCore.DisplayManagement.Entities;
using SSRSCore.DisplayManagement.Handlers;
using SSRSCore.DisplayManagement.ModelBinding;
using SSRSCore.DisplayManagement.Views;
using SSRSCore.Workflows.Activities;
using SSRSCore.Workflows.Models;
using SSRSCore.Workflows.ViewModels;

namespace SSRSCore.Workflows.Drivers
{
    public class ActivityMetadataDisplay : SectionDisplayDriver<IActivity, ActivityMetadata>
    {
        public override IDisplayResult Edit(ActivityMetadata section, BuildEditorContext context)
        {
            return Initialize<ActivityMetadataEditViewModel>("ActivityMetadata_Edit", viewModel =>
            {
                viewModel.Title = section.Title;
            }).Location("Content:before");
        }

        public override async Task<IDisplayResult> UpdateAsync(ActivityMetadata section, IUpdateModel updater, BuildEditorContext context)
        {
            var viewModel = new ActivityMetadataEditViewModel();

            if (await context.Updater.TryUpdateModelAsync(viewModel, Prefix))
            {
                section.Title = viewModel.Title?.Trim();
            }

            return await EditAsync(section, context);
        }
    }
}
