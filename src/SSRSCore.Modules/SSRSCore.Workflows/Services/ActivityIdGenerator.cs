using SSRSCore.Entities;
using SSRSCore.Workflows.Models;

namespace SSRSCore.Workflows.Services
{
    public class ActivityIdGenerator : IActivityIdGenerator
    {
        private readonly IIdGenerator _idGenerator;

        public ActivityIdGenerator(IIdGenerator idGenerator)
        {
            _idGenerator = idGenerator;
        }

        public string GenerateUniqueId(ActivityRecord activityRecord)
        {
            return _idGenerator.GenerateUniqueId();
        }
    }
}
