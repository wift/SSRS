using SSRSCore.Entities;
using SSRSCore.Workflows.Models;

namespace SSRSCore.Workflows.Services
{
    public class WorkflowTypeIdGenerator : IWorkflowTypeIdGenerator
    {
        private readonly IIdGenerator _idGenerator;

        public WorkflowTypeIdGenerator(IIdGenerator idGenerator)
        {
            _idGenerator = idGenerator;
        }

        public string GenerateUniqueId(WorkflowType workflowType)
        {
            return _idGenerator.GenerateUniqueId();
        }
    }
}
