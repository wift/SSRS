using SSRSCore.Entities;
using SSRSCore.Workflows.Models;

namespace SSRSCore.Workflows.Services
{
    public class WorkflowIdGenerator : IWorkflowIdGenerator
    {
        private readonly IIdGenerator _idGenerator;

        public WorkflowIdGenerator(IIdGenerator idGenerator)
        {
            _idGenerator = idGenerator;
        }

        public string GenerateUniqueId(Workflow workflow)
        {
            return _idGenerator.GenerateUniqueId();
        }
    }
}
