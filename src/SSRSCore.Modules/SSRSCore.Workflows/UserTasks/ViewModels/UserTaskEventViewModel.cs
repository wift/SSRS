using System.Collections.Generic;

namespace SSRSCore.Workflows.UserTasks.ViewModels
{
    public class UserTaskEventViewModel
    {
        public string Actions { get; set; }
        public IList<string> Roles { get; set; } = new List<string>();
    }
}
