using System.Collections.Generic;

namespace SSRSCore.Workflows.UserTasks.ViewModels
{
    public class UserTaskEventContentViewModel
    {
        public IList<string> Actions { get; set; }
    }
}
