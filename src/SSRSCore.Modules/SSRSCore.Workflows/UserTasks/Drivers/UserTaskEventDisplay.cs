using System;
using System.Linq;
using SSRSCore.Workflows.Display;
using SSRSCore.Workflows.UserTasks.Activities;
using SSRSCore.Workflows.UserTasks.ViewModels;

namespace SSRSCore.Workflows.UserTasks.Drivers
{
    public class UserTaskEventDisplay : ActivityDisplayDriver<UserTaskEvent, UserTaskEventViewModel>
    {
        protected override void EditActivity(UserTaskEvent activity, UserTaskEventViewModel model)
        {
            model.Actions = string.Join(", ", activity.Actions);
            model.Roles = activity.Roles;
        }

        protected override void UpdateActivity(UserTaskEventViewModel model, UserTaskEvent activity)
        {
            activity.Actions = model.Actions.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim()).ToList();
            activity.Roles = model.Roles;
        }
    }
}
