using Microsoft.Extensions.DependencyInjection;
using SSRSCore.ContentManagement.Display.ContentDisplay;
using SSRSCore.Modules;
using SSRSCore.Workflows.Helpers;
using SSRSCore.Workflows.UserTasks.Activities;
using SSRSCore.Workflows.UserTasks.Drivers;

namespace SSRSCore.Workflows.UserTasks
{
    [RequireFeatures("SSRSCore.Workflows", "SSRSCore.Contents", "SSRSCore.Roles")]
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IContentDisplayDriver, UserTaskEventContentDriver>();
            services.AddActivity<UserTaskEvent, UserTaskEventDisplay>();
        }
    }
}
