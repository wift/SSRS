using System.Threading.Tasks;
using SSRSCore.Workflows.Models;
using SSRSCore.Workflows.Scripting;
using SSRSCore.Workflows.Services;

namespace SSRSCore.Workflows.WorkflowContextProviders
{
    public class DefaultWorkflowExecutionContextHandler : WorkflowExecutionContextHandlerBase
    {
        public override Task EvaluatingScriptAsync(WorkflowExecutionScriptContext context)
        {
            context.ScopedMethodProviders.Add(new WorkflowMethodsProvider(context.WorkflowContext));
            return Task.CompletedTask;
        }
    }
}
