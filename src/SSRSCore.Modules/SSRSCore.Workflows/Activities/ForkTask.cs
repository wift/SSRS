using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Localization;
using SSRSCore.Workflows.Abstractions.Models;
using SSRSCore.Workflows.Models;

namespace SSRSCore.Workflows.Activities
{
    public class ForkTask : TaskActivity
    {
        public ForkTask(IStringLocalizer<ForkTask> localizer)
        {
            T = localizer;
        }

        private IStringLocalizer T { get; }

        public override string Name => nameof(ForkTask);
        public override LocalizedString DisplayText => T["Fork Task"];
        public override LocalizedString Category => T["Control Flow"];

        public IList<string> Forks
        {
            get => GetProperty(() => new List<string>());
            set => SetProperty(value);
        }

        public override IEnumerable<Outcome> GetPossibleOutcomes(WorkflowExecutionContext workflowContext, ActivityContext activityContext)
        {
            return Forks.Select(x => Outcome(T[x]));
        }

        public override ActivityExecutionResult Execute(WorkflowExecutionContext workflowContext, ActivityContext activityContext)
        {
            return Outcomes(Forks);
        }
    }
}