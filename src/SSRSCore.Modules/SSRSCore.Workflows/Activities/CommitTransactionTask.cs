using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Localization;
using SSRSCore.Environment.Shell.Scope;
using SSRSCore.Workflows.Abstractions.Models;
using SSRSCore.Workflows.Models;
using YesSql;

namespace SSRSCore.Workflows.Activities
{
    public class CommitTransactionTask : TaskActivity
    {
        public CommitTransactionTask(IStringLocalizer<CommitTransactionTask> localizer)
        {
            T = localizer;
        }

        private IStringLocalizer T { get; }
        public override string Name => nameof(CommitTransactionTask);
        public override LocalizedString DisplayText => T["Commit Transaction Task"];
        public override LocalizedString Category => T["Primitives"];

        public override IEnumerable<Outcome> GetPossibleOutcomes(WorkflowExecutionContext workflowContext, ActivityContext activityContext)
        {
            return Outcomes(T["Done"]);
        }

        public override async Task<ActivityExecutionResult> ExecuteAsync(WorkflowExecutionContext workflowContext, ActivityContext activityContext)
        {
            await ShellScope.Services.GetRequiredService<ISession>().CommitAsync();
            return Outcomes("Done");
        }
    }
}
