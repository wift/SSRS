namespace SSRSCore.Google
{
    public static class GoogleConstants
    {
        public static class Features
        {
            public const string GoogleAuthentication = "SSRSCore.Google.GoogleAuthentication";
            public const string GoogleAnalytics = "SSRSCore.Google.Analytics";
        }
    }
}
