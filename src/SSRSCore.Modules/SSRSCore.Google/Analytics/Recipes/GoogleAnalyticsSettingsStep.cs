using System;
using System.Threading.Tasks;
using SSRSCore.Entities;
using SSRSCore.Google.Analytics.Settings;
using SSRSCore.Google.Analytics.ViewModels;
using SSRSCore.Recipes.Models;
using SSRSCore.Recipes.Services;
using SSRSCore.Settings;


namespace SSRSCore.Google.Analytics.Recipes
{
    /// <summary>
    /// This recipe step sets Google Analytics settings.
    /// </summary>
    public class GoogleAnalyticsSettingsStep : IRecipeStepHandler
    {
        readonly ISiteService _siteService;
        public GoogleAnalyticsSettingsStep(ISiteService siteService)
        {
            _siteService = siteService;
        }

        public async Task ExecuteAsync(RecipeExecutionContext context)
        {
            if (!string.Equals(context.Name, nameof(GoogleAnalyticsSettings), StringComparison.OrdinalIgnoreCase))
            {
                return;
            }
            var model = context.Step.ToObject<GoogleAnalyticsSettingsViewModel>();
            var container = await _siteService.GetSiteSettingsAsync();
            container.Alter<GoogleAnalyticsSettings>(nameof(GoogleAnalyticsSettings), aspect =>
            {
                aspect.TrackingID = model.TrackingID;
            });
        }
    }
}