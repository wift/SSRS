using Microsoft.AspNetCore.Http;

namespace SSRSCore.Google.Analytics.Settings
{
    public class GoogleAnalyticsSettings
    {
        public string TrackingID { get; set; }
    }
}
