using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using SSRSCore.DisplayManagement.Entities;
using SSRSCore.DisplayManagement.Handlers;
using SSRSCore.DisplayManagement.Views;
using SSRSCore.Environment.Shell;
using SSRSCore.Google.Analytics.Settings;
using SSRSCore.Google.Analytics.ViewModels;
using SSRSCore.Settings;

namespace SSRSCore.Google.Analytics.Drivers
{
    public class GoogleAnalyticsSettingsDisplayDriver : SectionDisplayDriver<ISite, GoogleAnalyticsSettings>
    {
        private readonly IAuthorizationService _authorizationService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IShellHost _shellHost;
        private readonly ShellSettings _shellSettings;

        public GoogleAnalyticsSettingsDisplayDriver(
            IAuthorizationService authorizationService,
            IHttpContextAccessor httpContextAccessor,
            IShellHost shellHost,
            ShellSettings shellSettings)
        {
            _authorizationService = authorizationService;
            _httpContextAccessor = httpContextAccessor;
            _shellHost = shellHost;
            _shellSettings = shellSettings;
        }

        public override async Task<IDisplayResult> EditAsync(GoogleAnalyticsSettings settings, BuildEditorContext context)
        {
            var user = _httpContextAccessor.HttpContext?.User;
            if (user == null || !await _authorizationService.AuthorizeAsync(user, Permissions.ManageGoogleAnalytics))
            {
                return null;
            }

            return Initialize<GoogleAnalyticsSettingsViewModel>("GoogleAnalyticsSettings_Edit", model =>
            {
                model.TrackingID = settings.TrackingID;
            }).Location("Content:5").OnGroup(GoogleConstants.Features.GoogleAnalytics);
        }

        public override async Task<IDisplayResult> UpdateAsync(GoogleAnalyticsSettings settings, BuildEditorContext context)
        {
            if (context.GroupId == GoogleConstants.Features.GoogleAnalytics)
            {
                var user = _httpContextAccessor.HttpContext?.User;
                if (user == null || !await _authorizationService.AuthorizeAsync(user, Permissions.ManageGoogleAnalytics))
                {
                    return null;
                }

                var model = new GoogleAnalyticsSettingsViewModel();
                await context.Updater.TryUpdateModelAsync(model, Prefix);

                if (context.Updater.ModelState.IsValid)
                {
                    settings.TrackingID = model.TrackingID;
                }
            }
            return await EditAsync(settings, context);
        }
    }
}