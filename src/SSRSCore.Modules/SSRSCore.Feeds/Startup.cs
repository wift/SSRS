﻿using SSRSCore.Modules;
using Microsoft.Extensions.DependencyInjection;
using SSRSCore.Feeds;

namespace SSRSCore.Scripting
{
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddFeeds();
        }
    }
}
