using System.Threading.Tasks;
using SSRSCore.Entities;
using SSRSCore.ReverseProxy.Settings;
using SSRSCore.Settings;

namespace SSRSCore.ReverseProxy.Services
{
    public class ReverseProxyService
    {
        private readonly ISiteService _siteService;

        public ReverseProxyService(ISiteService siteService)
        {
            _siteService = siteService;
        }

        public async Task<ReverseProxySettings> GetSettingsAsync()
        {
            var siteSettings = await _siteService.GetSiteSettingsAsync();
            return siteSettings.As<ReverseProxySettings>();
        }
    }
}
