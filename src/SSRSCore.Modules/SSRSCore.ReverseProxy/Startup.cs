using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Options;
using SSRSCore.DisplayManagement.Handlers;
using SSRSCore.Modules;
using SSRSCore.Navigation;
using SSRSCore.ReverseProxy.Drivers;
using SSRSCore.ReverseProxy.Services;
using SSRSCore.Settings;

namespace SSRSCore.ReverseProxy
{
    public class Startup : StartupBase
    {
        // we need this to start before other security related initialization logic
        public override int Order => -1;

        public override void Configure(IApplicationBuilder app, IEndpointRouteBuilder routes, IServiceProvider serviceProvider)
        {
            app.UseForwardedHeaders();
        }

        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<INavigationProvider, AdminMenu>();
            services.AddScoped<IDisplayDriver<ISite>, ReverseProxySettingsDisplayDriver>();
            services.AddSingleton<ReverseProxyService>();

            services.TryAddEnumerable(ServiceDescriptor
                .Transient<IConfigureOptions<ForwardedHeadersOptions>, ForwardedHeadersOptionsConfiguration>());
        }
    }
}
