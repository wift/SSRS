using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using SSRSCore.Data.Migration;
using SSRSCore.Deployment.Core;
using SSRSCore.Deployment.Deployment;
using SSRSCore.Deployment.Indexes;
using SSRSCore.Deployment.Recipes;
using SSRSCore.Deployment.Steps;
using SSRSCore.DisplayManagement;
using SSRSCore.DisplayManagement.Handlers;
using SSRSCore.Navigation;
using SSRSCore.Modules;
using SSRSCore.Recipes;
using SSRSCore.Security.Permissions;
using YesSql.Indexes;

namespace SSRSCore.Deployment
{
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddDeploymentServices();

            services.AddScoped<INavigationProvider, AdminMenu>();
            services.AddScoped<IPermissionProvider, Permissions>();

            services.AddScoped<IDisplayManager<DeploymentStep>, DisplayManager<DeploymentStep>>();
            services.AddSingleton<IDeploymentTargetProvider, FileDownloadDeploymentTargetProvider>();

            // Custom File deployment step
            services.AddTransient<IDeploymentSource, CustomFileDeploymentSource>();
            services.AddSingleton<IDeploymentStepFactory>(new DeploymentStepFactory<CustomFileDeploymentStep>());
            services.AddScoped<IDisplayDriver<DeploymentStep>, CustomFileDeploymentStepDriver>();

            services.AddSingleton<IIndexProvider, DeploymentPlanIndexProvider>();
            services.AddTransient<IDataMigration, Migrations>();

            services.AddScoped<DeploymentPlanService>();

            services.AddRecipeExecutionStep<DeploymentPlansRecipeStep>();

            services.AddTransient<IDeploymentSource, DeploymentPlanDeploymentSource>();
            services.AddSingleton<IDeploymentStepFactory>(new DeploymentStepFactory<DeploymentPlanDeploymentStep>());
            services.AddScoped<IDisplayDriver<DeploymentStep>, DeploymentPlanDeploymentStepDriver>();
        }

        public override void Configure(IApplicationBuilder app, IEndpointRouteBuilder routes, IServiceProvider serviceProvider)
        {
            routes.MapAreaControllerRoute(
                name: "DeleteStep",
                areaName: "SSRSCore.Deployment",
                pattern: "Deployment/DeploymentPlan/{id}/Step/{stepId}/Delete",
                defaults: new { controller = "Step", action = "Delete" }
            );

            routes.MapAreaControllerRoute(
                name: "ExecutePlan",
                areaName: "SSRSCore.Deployment",
                pattern: "Deployment/DeploymentPlan/{id}/Type/{type}/Execute",
                defaults: new { controller = "DeploymentPlan", action = "Execute" }
            );
        }
    }
}