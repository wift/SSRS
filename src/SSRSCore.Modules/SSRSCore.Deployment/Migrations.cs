﻿using SSRSCore.Data.Migration;
using SSRSCore.Deployment.Indexes;

namespace SSRSCore.Deployment
{
    public class Migrations : DataMigration
    {
        public int Create()
        {
            SchemaBuilder.CreateMapIndexTable(nameof(DeploymentPlanIndex), table => table
                .Column<string>("Name")
            );

            return 1;
        }
    }
}