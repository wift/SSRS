﻿using System.ComponentModel.DataAnnotations;

namespace SSRSCore.Deployment.ViewModels
{
    public class EditDeploymentPlanViewModel
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
    }
}
