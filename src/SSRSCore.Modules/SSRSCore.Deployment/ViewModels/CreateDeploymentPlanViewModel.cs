﻿using System.ComponentModel.DataAnnotations;

namespace SSRSCore.Deployment.ViewModels
{
    public class CreateDeploymentPlanViewModel
    {
        [Required]
        public string Name { get; set; }
    }
}
