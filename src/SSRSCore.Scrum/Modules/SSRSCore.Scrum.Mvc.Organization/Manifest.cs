using SSRSCore.Modules.Manifest;

[assembly: Module(
    Name = "SSRSCore.Scrum.Mvc.Organization",
    Version = "1.0.0",
    Description = "敏捷开发框架基础模块-组织架构模块"
)]
