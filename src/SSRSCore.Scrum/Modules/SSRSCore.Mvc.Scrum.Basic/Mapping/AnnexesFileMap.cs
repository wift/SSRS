﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SSRSCore.Scrum.Mvc.Basic.Services.Basic;

namespace SSRSCore.Scrum.Mvc.Basic
{
    /// <summary>
    /// 日 期：2017.03.08
    /// 附件管理
    /// </summary>
    public class AnnexesFileMap : IEntityTypeConfiguration<AnnexesFileEntity>
    {
        public void Configure(EntityTypeBuilder<AnnexesFileEntity> builder)
        {
            builder.HasKey(p => p.F_Id);
        }
    }
}
