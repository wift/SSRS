﻿
using Microsoft.EntityFrameworkCore;
using SSRSCore.Scrum.Mvc.Basic.Services.Basic;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace SSRS.PlugIns.Mapping.System.Database
{
    /// <summary>
    /// 数据库建表草稿类
    /// </summary>
    public class DbDraftMap : IEntityTypeConfiguration<DbDraftEntity>
    {
        public void Configure(EntityTypeBuilder<DbDraftEntity> builder)
        {
            builder.HasKey(p => p.F_Id);
        }
    }
}
