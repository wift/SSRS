﻿
using Microsoft.EntityFrameworkCore;
using SSRSCore.Scrum.Mvc.Basic.Services.Basic;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace SSRS.PlugIns.Mapping.System.Database
{
    /// <summary>
    /// 常用字段类
    /// </summary>
    public class DbFieldMap : IEntityTypeConfiguration<DbFieldEntity>
    {
        public void Configure(EntityTypeBuilder<DbFieldEntity> builder)
        {
            builder.HasKey(p => p.F_Id);
        }
    }
}
