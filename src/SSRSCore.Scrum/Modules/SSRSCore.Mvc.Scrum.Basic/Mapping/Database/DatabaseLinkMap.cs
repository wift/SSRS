﻿

using Microsoft.EntityFrameworkCore;
using SSRSCore.Scrum.Mvc.Basic.Services.Basic;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SSRS.PlugIns.Mapping
{
    /// <summary>
    /// 数据库连接
    /// </summary>
    public class DataBaseLinkMap : IEntityTypeConfiguration<DatabaseLinkEntity>
    {
        public void Configure(EntityTypeBuilder<DatabaseLinkEntity> builder)
        {
            builder.HasKey(p => p.F_DatabaseLinkId);
        }
    }
}
