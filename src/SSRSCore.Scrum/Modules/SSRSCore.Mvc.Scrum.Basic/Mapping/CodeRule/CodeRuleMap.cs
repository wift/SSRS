﻿

using SSRSCore.Scrum.Mvc.Basic.Services.Basic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SSRS.Application.Mapping
{
    /// <summary>
    /// 编号规则
    /// </summary>
    public class CodeRuleMap : IEntityTypeConfiguration<CodeRuleEntity>
    {
        public void Configure(EntityTypeBuilder<CodeRuleEntity> builder)
        {
            builder.HasKey(p => p.F_RuleId);
        }
    }
}
