﻿

using Microsoft.EntityFrameworkCore;
using SSRSCore.Scrum.Mvc.Basic.Services.Basic;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SSRS.PlugIns.Mapping
{
    /// <summary>
    
   
  

    /// 数据字典分类
    /// </summary>
    public class DataItemMap : IEntityTypeConfiguration<DataItemEntity>
    {
        public void Configure(EntityTypeBuilder<DataItemEntity> builder)
        {
            builder.HasKey(p => p.F_ItemId);
        }
    }
}
