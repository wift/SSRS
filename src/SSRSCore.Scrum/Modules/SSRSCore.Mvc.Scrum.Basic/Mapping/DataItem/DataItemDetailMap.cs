﻿

using Microsoft.EntityFrameworkCore;
using SSRSCore.Scrum.Mvc.Basic.Services.Basic;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SSRS.PlugIns.Mapping
{
    /// <summary>
    /// 数据字典详细
    /// </summary>
    public class DataItemDetailMap : IEntityTypeConfiguration<DataItemDetailEntity>
    {
        public void Configure(EntityTypeBuilder<DataItemDetailEntity> builder)
        {
            builder.HasKey(p => p.F_ItemDetailId);
        }
    }
}
