﻿
using SSRSCore.Scrum.Mvc.Basic.Services.Basic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SSRS.PlugIns.Mapping.System
{
    public class LogoImgMap : IEntityTypeConfiguration<LogoImgEntity>
    {
        public void Configure(EntityTypeBuilder<LogoImgEntity> builder)
        {
            builder.HasKey(p => p.F_Code);
        }
    }
}
