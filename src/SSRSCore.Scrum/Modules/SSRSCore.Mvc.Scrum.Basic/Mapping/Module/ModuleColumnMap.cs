﻿using SSRSCore.Scrum.Mvc.Basic.Services.Basic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace SSRS.PlugIns.Mapping
{
    /// <summary>
    /// 系统功能模块视图列
    /// </summary>
    public class ModuleColumnMap : IEntityTypeConfiguration<ModuleColumnEntity>
    {
        public void Configure(EntityTypeBuilder<ModuleColumnEntity> builder)
        {
            builder.HasKey(p => p.F_ModuleColumnId);
        }
    }
}
