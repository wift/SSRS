﻿

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SSRSCore.Scrum.Mvc.Basic.Services.Basic;

namespace SSRS.PlugIns.Mapping
{
    /// <summary>
    /// 系统功能模块按钮
    /// </summary>
    public class ModuleButtonMap : IEntityTypeConfiguration<ModuleButtonEntity>
    {
        public void Configure(EntityTypeBuilder<ModuleButtonEntity> builder)
        {
            builder.HasKey(p => p.F_ModuleButtonId);
        }
    }
}
