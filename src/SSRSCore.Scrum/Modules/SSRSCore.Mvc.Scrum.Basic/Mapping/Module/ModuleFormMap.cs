﻿

using SSRSCore.Scrum.Mvc.Basic.Services.Basic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace SSRS.PlugIns.Mapping.System.Module
{
    public class ModuleFormMap : IEntityTypeConfiguration<ModuleFormEntity>
    {
        public void Configure(EntityTypeBuilder<ModuleFormEntity> builder)
        {
            builder.HasKey(p => p.F_ModuleFormId);
        }
    }
}
