﻿using SSRSCore.Scrum.Mvc.Basic.Services.Basic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace SSRS.PlugIns.Mapping
{
    /// <summary>
 
 
    /// 系统功能模块
    /// </summary>
    public class ModuleMap : IEntityTypeConfiguration<ModuleEntity>
    {
        public void Configure(EntityTypeBuilder<ModuleEntity> builder)
        {
            builder.HasKey(p => p.F_ModuleId);
        }
    }
}
