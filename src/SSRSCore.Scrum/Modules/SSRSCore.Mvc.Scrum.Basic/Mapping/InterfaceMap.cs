﻿using SSRSCore.Scrum.Mvc.Basic.Services.Basic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace SSRS.PlugIns.Mapping
{
    /// <summary>



    /// 日 期：2017.04.01
    /// 接口管理
    /// </summary>
    public class InterfaceMap : IEntityTypeConfiguration<InterfaceEntity>
    {
        public void Configure(EntityTypeBuilder<InterfaceEntity> builder)
        {
            builder.HasKey(p => p.F_Id);
        }
    }
}