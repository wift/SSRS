using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SSRSCore.Modules;
using SSRSCore.Scrum.Toolkit;

namespace SSRSCore.Mvc.HelloWorld
{
    public class Startup : StartupBase
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public override void ConfigureServices(IServiceCollection services)
        {
            base.ConfigureServices(services);
            services.AddSession();

            services.MyAddHttpContextAccessor();
        }

        public override void Configure(IApplicationBuilder builder, IEndpointRouteBuilder routes, IServiceProvider serviceProvider)
        {
            if (string.IsNullOrEmpty(_configuration["Sample"]))
            {
                throw new Exception(":(");
            }
            
            routes.MapAreaControllerRoute
            (
                name: "Basic", 
                areaName: "SSRSCore.Scrum.Mvc.Basic",
                pattern: "",
                defaults: new { controller = "Home", action = "Index" }
            );
        }
    }
}
