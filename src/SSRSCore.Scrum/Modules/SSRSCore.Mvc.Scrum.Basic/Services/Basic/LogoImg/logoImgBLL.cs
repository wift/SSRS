﻿
using System;
using System.Collections.Generic;
using SSRSCore.Scrum.Toolkit;
using SSRSCore.Scrum.Toolkit.Excepes;

namespace SSRSCore.Scrum.Mvc.Basic.Services.Basic
{

    /// <summary> 
    /// 版 本 SSRSCore 
    /// Copyright (c) 云简 3615526996 
    /// 创 建：超级管理员 
    /// 日 期：2018-07-30 14:53 
    /// 描 述：logo设置 
    /// </summary> 

    public class LogoImgBLL: LogoImgIBLL
    {
        private LogoImgService logoImgService = new LogoImgService();

        #region 获取数据 
        /// <summary> 
        /// 获取列表数据 
        /// <summary> 
        /// <returns></returns> 
        public IEnumerable<LogoImgEntity> GetList(string queryJson)
        {
            try
            {
                return logoImgService.GetList(queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionExtend)
                {
                    throw;
                }
                else
                {
                    throw ExceptionExtend.ThrowBusinessException(ex);
                }
            }
        }
        /// <summary> 
        /// 获取实体数据 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        public LogoImgEntity GetEntity(string keyValue)
        {
            try
            {
                return logoImgService.GetEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionExtend)
                {
                    throw;
                }
                else
                {
                    throw ExceptionExtend.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取logo图片
        /// </summary>
        /// <param name="code">编码</param>
        public void GetImg(string code)
        {
            LogoImgEntity entity = GetEntity(code);
            string fileImg = "";
            string fileHeadImg = Config.GetValue("fileLogoImg");
            if (entity != null)
            {
                fileImg = string.Format("{0}/{1}{2}", fileHeadImg, entity.F_Code, entity.F_FileName);

            }
            else
            {
                fileImg = string.Format("{0}/{1}.png", fileHeadImg, code);
            }
            FileDownHelper.DownLoadnew(fileImg);
        }
        #endregion

        #region 提交数据 

        /// <summary> 
        /// 删除实体数据 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        public void DeleteEntity(string keyValue)
        {
            try
            {
                logoImgService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionExtend)
                {
                    throw;
                }
                else
                {
                    throw ExceptionExtend.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary> 
        /// 保存实体数据（新增、修改） 
        /// <param name="keyValue">主键</param> 
        /// <summary> 
        /// <returns></returns> 
        public void SaveEntity(LogoImgEntity entity)
        {
            try
            {
                logoImgService.SaveEntity(entity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionExtend)
                {
                    throw;
                }
                else
                {
                    throw ExceptionExtend.ThrowBusinessException(ex);
                }
            }
        }

        #endregion
    }
}
