﻿using System.ComponentModel.DataAnnotations.Schema;

namespace SSRSCore.Scrum.Mvc.Basic.Services.Basic
{
    /// <summary>
    /// 版 本 SSRSCore
    /// Copyright (c) 云简 3615526996
    /// 创建人：云简 3615526996
    /// 日 期：2018.07.30
    /// 描 述：系统logo设置
    /// </summary>
    [Table("SSRS_Base_Logo")]
    public class LogoImgEntity
    {
        #region 实体成员 
        /// <summary> 
        /// 编码 
        /// </summary> 
        /// <returns></returns> 
        [Column("F_CODE")]
        public string F_Code { get; set; }
        /// <summary> 
        /// 文件名字 
        /// </summary> 
        /// <returns></returns> 
        [Column("F_FILENAME")]
        public string F_FileName { get; set; }
        #endregion

        #region 扩展操作 
        /// <summary> 
        /// 新增调用 
        /// </summary> 
        public void Create()
        {
        }
        /// <summary> 
        /// 编辑调用 
        /// </summary> 
        /// <param name="keyValue"></param> 
        public void Modify(string keyValue)
        {
            this.F_Code = keyValue;
        }
        #endregion
    }
}
