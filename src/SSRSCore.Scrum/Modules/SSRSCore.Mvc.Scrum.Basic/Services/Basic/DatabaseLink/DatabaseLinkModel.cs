﻿namespace SSRSCore.Scrum.Mvc.Basic.Services.Basic
{
    /// <summary>
    /// 版 本 SSRSCore
    /// Copyright (c) 云简 3615526996
    /// 创建人：云简 3615526996
    /// 日 期：2018.03.27
    /// 描 述：数据库连接数据模型
    /// </summary>
    public class DatabaseLinkModel
    {
        /// <summary>
        /// 别名
        /// </summary>
        public string alias { get; set; }
        /// <summary>
        /// 名字
        /// </summary>
        public string name { get; set; }
    }
}
