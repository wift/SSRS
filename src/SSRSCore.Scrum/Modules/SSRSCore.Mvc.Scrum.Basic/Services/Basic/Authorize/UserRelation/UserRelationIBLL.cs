﻿using System.Collections.Generic;
using SSRSCore.Scrum.Toolkit;
namespace SSRSCore.Scrum.Mvc.Basic
{
    /// <summary>
    /// 版 本 SSRSCore
    /// Copyright (c) 云简 3615526996
    /// 创建人：云简 3615526996
    /// 日 期：2017.04.17
    /// 描 述：用户关联对象
    /// </summary>
    public interface UserRelationIBLL
    {
        #region 获取数据
        /// <summary>
        /// 获取对象主键列表信息
        /// </summary>
        /// <param name="userId">用户主键</param>
        /// <param name="category">分类:1-角色2-岗位</param>
        /// <returns></returns>
        List<UserRelationEntity> GetObjectIdList(string userId, int category);
        /// <summary>
        /// 获取对象主键列表信息
        /// </summary>
        /// <param name="userId">用户主键</param>
        /// <param name="category">分类:1-角色2-岗位</param>
        /// <returns></returns>
        string GetObjectIds(string userId, int category);
        /// <summary>
        /// 获取用户主键列表信息
        /// </summary>
        /// <param name="objectId">用户主键</param>
        /// <returns></returns>
        IEnumerable<UserRelationEntity> GetUserIdList(string objectId);
        /// <summary>
        /// 获取用户主键列表信息
        /// </summary>
        /// <param name="objectIdList">关联或角色主键集合</param>
        /// <returns></returns>
        IEnumerable<UserRelationEntity> GetUserIdList(List<string> objectIdList);
        #endregion

        #region 提交数据
        /// <summary>
        /// 保存用户对应对象数据
        /// </summary>
        /// <param name="objectId">对应对象主键</param>
        /// <param name="category">分类:1-角色2-岗位</param>
        /// <param name="userIds">对用户主键列表</param>
        void SaveEntityList(string objectId, int category, string userIds);
        #endregion
    }
}
