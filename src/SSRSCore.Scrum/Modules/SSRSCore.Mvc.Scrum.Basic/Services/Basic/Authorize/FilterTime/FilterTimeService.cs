﻿

using System;
using System.Collections.Generic;
using System.Linq;
using SSRSCore.Scrum.DataBase;
using SSRSCore.Scrum.Toolkit.Excepes;

namespace SSRSCore.Scrum.Mvc.Basic
{
    /// <summary>
    /// 版 本 SSRSCore
    /// Copyright (c) 云简 3615526996
    /// 创建人：云简 3615526996
    /// 日 期：2017.04.17
    /// 描 述：时段过滤
    /// </summary>
    public class FilterTimeService : RepositoryFactory
    {
        #region 获取数据
        /// <summary>
        /// 过滤时段实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        public FilterTimeEntity GetEntity(string keyValue)
        {
            try
            {
                return this.BaseRepository().FindEntity<FilterTimeEntity>(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionExtend)
                {
                    throw;
                }
                else
                {
                    throw ExceptionExtend.ThrowServiceException(ex);
                }
            }
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除过滤时段
        /// </summary>
        /// <param name="keyValue">主键</param>
        public void DeleteEntiy(string keyValue)
        {
            try
            {
                this.BaseRepository().Delete(new FilterTimeEntity { F_FilterTimeId = keyValue });
            }
            catch (Exception ex)
            {
                if (ex is ExceptionExtend)
                {
                    throw;
                }
                else
                {
                    throw ExceptionExtend.ThrowServiceException(ex);
                }
            }
        }
        /// <summary>
        /// 保存过滤时段表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="filterTimeEntity">过滤时段实体</param>
        /// <returns></returns>
        public void SaveForm(string keyValue, FilterTimeEntity filterTimeEntity)
        {
            try
            {
                if (!string.IsNullOrEmpty(keyValue))
                {
                    filterTimeEntity.Modify();
                    this.BaseRepository().UpdateEx(filterTimeEntity);
                }
                else
                {
                    filterTimeEntity.Create();
                    this.BaseRepository().Insert(filterTimeEntity);
                }
            }
            catch (Exception ex)
            {
                if (ex is ExceptionExtend)
                {
                    throw;
                }
                else
                {
                    throw ExceptionExtend.ThrowServiceException(ex);
                }
            }
        }
        #endregion
    }
}
