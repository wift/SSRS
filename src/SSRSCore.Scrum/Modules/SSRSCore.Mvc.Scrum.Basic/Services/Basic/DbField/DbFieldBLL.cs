﻿
using System;
using System.Collections.Generic;
using SSRSCore.Scrum.Toolkit;
using SSRSCore.Scrum.Toolkit.Excepes;


namespace SSRSCore.Scrum.Mvc.Basic.Services.Basic
{
    /// <summary>
    /// 版 本 SSRSCore
    /// Copyright (c) 云简 3615526996
    /// 创 建：超级管理员
    /// 日 期：2017-12-19 12:03
    /// 描 述：常用字段类
    /// </summary>
    public class DbFieldBLL : DbFieldIBLL
    {
        private DbFieldService commonFieldService = new DbFieldService();

        #region 获取数据
        /// <summary>
        /// 获取列表数据
        /// <summary>
        /// <returns></returns>
        public IEnumerable<DbFieldEntity> GetList(string queryJson)
        {
            try
            {
                return commonFieldService.GetList(queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionExtend)
                {
                    throw;
                }
                else
                {
                    throw ExceptionExtend.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取列表分页数据
        /// <param name="pagination">分页参数</param>
        /// <summary>
        /// <returns></returns>
        public IEnumerable<DbFieldEntity> GetPageList(Pagination pagination, string queryJson)
        {
            try
            {
                return commonFieldService.GetPageList(pagination, queryJson);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionExtend)
                {
                    throw;
                }
                else
                {
                    throw ExceptionExtend.ThrowBusinessException(ex);
                }
            }
        }

        /// <summary>
        /// 获取实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public DbFieldEntity GetEntity(string keyValue)
        {
            try
            {
                return commonFieldService.GetEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionExtend)
                {
                    throw;
                }
                else
                {
                    throw ExceptionExtend.ThrowBusinessException(ex);
                }
            }
        }

        #endregion

        #region 提交数据

        /// <summary>
        /// 删除实体数据
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void DeleteEntity(string keyValue)
        {
            try
            {
                commonFieldService.DeleteEntity(keyValue);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionExtend)
                {
                    throw;
                }
                else
                {
                    throw ExceptionExtend.ThrowBusinessException(ex);
                }
            }
        }


        /// <summary>
        /// 保存实体数据（新增、修改）
        /// <param name="keyValue">主键</param>
        /// <summary>
        /// <returns></returns>
        public void SaveEntity(string keyValue, DbFieldEntity entity)
        {
            try
            {
                commonFieldService.SaveEntity(keyValue, entity);
            }
            catch (Exception ex)
            {
                if (ex is ExceptionExtend)
                {
                    throw;
                }
                else
                {
                    throw ExceptionExtend.ThrowBusinessException(ex);
                }
            }
        }

        #endregion
    }
}
