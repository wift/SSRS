﻿

namespace SSRSCore.Scrum.Mvc.Redis
{
    /// <summary>
    /// 定义缓存工厂类
    /// </summary>
    public class CacheFactory
    {
        public static ICache CaChe()
        {
            return new CacheByRedis();
        }
    }
}
