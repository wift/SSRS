﻿
using Microsoft.Practices.Unity.Configuration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using Unity;
using Unity.Resolution;
namespace SSRSCore.Scrum.DataBase.Ioc
{
    public class UnityIocToolkit
    {
        private readonly IUnityContainer container;
        private static  UnityIocToolkit instance = null;

        private UnityIocToolkit(string containerName)
        {
            UnityConfigurationSection section = (UnityConfigurationSection)ConfigurationManager.GetSection("unity");
            if (containerName == "IOCcontainer")
            {
                container = new UnityContainer();
                section.Configure(container, containerName);
            }
            else if (section.Containers.Count > 1)
            {
                container = new UnityContainer();
                section.Configure(container, containerName);
            }
        }
        public T GetService<T>(string name, params ParameterOverride[] parameter)
        {
            return container.Resolve<T>(name, parameter);
        }
        public static UnityIocToolkit Instance
        {
            get
            {
                if (instance==null)
                {
                    instance = new UnityIocToolkit("IOCcontainer");
                }
                return instance;
            }
        }

    }
}
