﻿using Microsoft.EntityFrameworkCore;
using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace SSRSCore.Scrum.DataBase.SqlServer
{


    /// <summary>
    /// 数据访问(SqlServer) 上下文
    /// </summary>
    public class DatabaseContext : DbContext
    {
        private string ConnString { get; set; }

        #region 徐志强 构造函数
        /// <summary>
        /// 初始化一个 使用指定数据连接名称或连接串 的数据访问上下文类 的新实例
        /// </summary>
        /// <param name="connString">连接字串</param>
        public DatabaseContext(string connString)

        {
            ConnString = connString;





        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(ConnString, i =>
            {
                i.UseRowNumberForPaging();
            });

            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {





            //string assembleFileName = Assembly.GetExecutingAssembly().CodeBase.Replace("SSRS.DataBase.SqlServer.dll", "SSRS.PlugIns.Mapping.DLL").Replace("file:///", "");
            //Assembly asm = Assembly.LoadFile(assembleFileName);
            //var typesToRegister = asm.GetTypes().Where(p => !String.IsNullOrEmpty(p.Namespace) && p.BaseType != null);

            //foreach (var type in typesToRegister)
            //{
            //    try
            //    {
            //        dynamic configurationInstance = Activator.CreateInstance(type);


            //        modelBuilder.ApplyConfiguration(configurationInstance);//应用设置
            //    }
            //    catch
            //    {

            //    }

            //}
            //所有的插件映射

            try
            {
                var myType = typeof(DatabaseContext);
                string currentDirectory = Path.GetDirectoryName(myType.Assembly.Location);

                string[] alliocdll = Directory.GetFiles(currentDirectory, "SSRS.PlugIns.*.dll");

                if (alliocdll.Length > 0)
                {
                    Assembly[] icoAssemblies = new Assembly[alliocdll.Length];
                    for (int i = 0; i < icoAssemblies.Length; i++)
                    {
                        FileInfo fi = new FileInfo(alliocdll[i]);


                        icoAssemblies[i] = Assembly.Load(fi.Name.Substring(0, fi.Name.LastIndexOf('.')));

                        try
                        {

                            //IEntityTypeConfiguration

                            var typesToRegister1 = icoAssemblies[i].GetTypes().Where(p => !String.IsNullOrEmpty(p.Namespace) && p.BaseType != null && p.Name.EndsWith("Map"));

                            foreach (var type in typesToRegister1)
                            {
                                try
                                {
                                    dynamic configurationInstance = Activator.CreateInstance(type);


                                    modelBuilder.ApplyConfiguration(configurationInstance);//应用设置
                                }
                                catch
                                {

                                }

                            }
                        }
                        catch
                        {

                        }


                    }



                }
            }
            catch
            {

            }

            base.OnModelCreating(modelBuilder);


        }

        #endregion


    }
}