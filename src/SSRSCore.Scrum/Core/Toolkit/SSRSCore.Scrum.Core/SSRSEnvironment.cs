﻿using System.IO;
using Autofac;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using SSRS.Autofac;

namespace SSRS.NetCore.Common
{
    public   class SSRSEnvironment
    {
        public static IHostingEnvironment HostingEnvironment => SrAutoContainer.AutoContainer.Resolve<IHostingEnvironment>();
        public static IConfiguration Configuration => SrAutoContainer.AutoContainer.Resolve<IConfiguration>();

        public static string MapPath(string relativePath)
        {
            if (relativePath.StartsWith("~/"))
            {
                //~/Resource/DocumentFile/System/20180702/3f80e9d4-00e1-4672-983e-099f09c9a923.docx

                return Path.Combine(HostingEnvironment.WebRootPath, relativePath.Substring(2));
            }else
            if (relativePath=="~")
            {
                //~/Resource/DocumentFile/System/20180702/3f80e9d4-00e1-4672-983e-099f09c9a923.docx

                return HostingEnvironment.WebRootPath;
            }
            else
            if (relativePath.StartsWith("/")|| relativePath.StartsWith("\\"))
            {
                //~/Resource/DocumentFile/System/20180702/3f80e9d4-00e1-4672-983e-099f09c9a923.docx

                  return Path.Combine(HostingEnvironment.WebRootPath, relativePath.Substring(1)); ;
            }
            else if(! (relativePath.StartsWith("/") || relativePath.StartsWith("~/")|| relativePath.StartsWith("\\")) )
            {
                return Path.Combine(HostingEnvironment.WebRootPath, relativePath);
            }
            else
            {
                return Path.Combine(HostingEnvironment.WebRootPath, relativePath);
            }
        }
    }
}
