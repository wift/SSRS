﻿
namespace SSRSCore.Scrum.Toolkit
{
    /// <summary>
    /// 版 本  SSRS-ADMS V1.0.0 极限敏捷开发框架
    /// Copyright (c) 2018-2019 上善若水
    /// 创建人：上善若水
    /// 日期：2019年8月5日
    /// 模板数据模型
    /// </summary>
    public class TemplateDataModel
    {
        /// <summary>
        /// 行号
        /// </summary>
        public int row { get; set; }
        /// <summary>
        /// 列号
        /// </summary>
        public int cell { get; set; }
        /// <summary>
        /// 数据值
        /// </summary>
        public string value { get; set; }
    }
}
