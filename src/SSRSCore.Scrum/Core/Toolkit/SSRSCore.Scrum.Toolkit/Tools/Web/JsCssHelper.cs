﻿ 
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using SSRS.NetCore.Common;
using Yahoo.Yui.Compressor;

namespace SSRSCore.Scrum.Toolkit
{
    /// <summary>
    /// 版 本  SSRS-ADMS V1.0.0 极限敏捷开发框架
    /// Copyright (c) 2018-2019 上善若水
    /// 创建人：上善若水
    /// 日 期：2019年8月5日
    /// js,css,文件压缩和下载
    /// </summary>
    public  class JsCssHelper
    {
        private static JavaScriptCompressor javaScriptCompressor = new JavaScriptCompressor();
        private static CssCompressor cssCompressor = new CssCompressor();

        #region Js 文件操作
        /// <summary>
        /// 读取js文件内容并压缩
        /// </summary>
        /// <param name="filePathlist"></param>
        /// <returns></returns>
        public static string ReadJSFile(string[] filePathlist)
        {
            StringBuilder jsStr = new StringBuilder();
            try
            {

                string rootPath = SSRSEnvironment.HostingEnvironment.WebRootPath;


                foreach (var filePath in filePathlist)
                {
                     
                    string path = rootPath+ filePath ;
                    if (DirFileHelper.IsExistFile(path))
                    {
                        string content = File.ReadAllText(path, Encoding.UTF8);
                        if (Config.GetValue("JsCompressor") == "true")
                        {
                            content = javaScriptCompressor.Compress(content);
                        }
                        jsStr.Append(content);
                    }
                }
                return jsStr.ToString();
            }
            catch (Exception)
            {
                return "";
            }
        }
        #endregion

        #region Css 文件操作
        /// <summary>
        /// 读取css 文件内容并压缩
        /// </summary>
        /// <param name="filePathlist"></param>
        /// <returns></returns>
        public static string ReadCssFile(string[] filePathlist)
        {
            StringBuilder cssStr = new StringBuilder();
            try
            {
               
                string rootPath = SSRSEnvironment.HostingEnvironment.WebRootPath;
                foreach (var filePath in filePathlist)
                {
                    string path = rootPath + filePath;
                    if (DirFileHelper.IsExistFile(path))
                    {
                        string content = File.ReadAllText(path, Encoding.UTF8);
                        content = cssCompressor.Compress(content);
                        cssStr.Append(content);
                    }
                }
                return cssStr.ToString();
            }
            catch (Exception)
            {
                return cssStr.ToString();
            }
        }
        #endregion

        #region 读取文件
        /// <summary>
        /// 读取对应文件
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static string Read(string filePath)
        {
            StringBuilder str = new StringBuilder();
            try
            {
                string rootPath = SSRSEnvironment.HostingEnvironment.WebRootPath;
                string path = rootPath + filePath;
                if (DirFileHelper.IsExistFile(path))
                {
                    string content = File.ReadAllText(path, Encoding.UTF8);
                    str.Append(content);
                }
                return str.ToString();
            }
            catch (Exception)
            {
                return "";
            }
        }
        /// <summary>
        /// 读取js文件
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static string ReadJS(string filePath)
        {
            StringBuilder str = new StringBuilder();
            try
            {
                string rootPath = SSRSEnvironment.HostingEnvironment.WebRootPath;
                string path = rootPath + filePath;
                if (DirFileHelper.IsExistFile(path))
                {
                    string content = File.ReadAllText(path, Encoding.UTF8);
                    if (Config.GetValue("JsCompressor") == "true")
                    {
                        content = javaScriptCompressor.Compress(content);
                    }
                    str.Append(content);
                }
                return str.ToString();
            }
            catch (Exception)
            {
                return "";
            }
        }
        /// <summary>
        /// 读取css文件
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static string ReadCss(string filePath)
        {
            StringBuilder str = new StringBuilder();
            try
            {
                string rootPath = SSRSEnvironment.HostingEnvironment.WebRootPath;
                string path = rootPath + filePath;
                if (DirFileHelper.IsExistFile(path))
                {
                    string content = File.ReadAllText(path, Encoding.UTF8);
                    content = cssCompressor.Compress(content);
                    str.Append(content);
                }
                return str.ToString();
            }
            catch (Exception)
            {
                return "";
            }
        }
        #endregion


        #region Js 文件操作
        /// <summary>
        /// 读取js文件内容并压缩
        /// </summary>
        /// <param name="filePathlist"></param>
        /// <returns></returns>
        public static string ReadImplantJSFile(string[] filePathlist)
        {
            StringBuilder jsStr = new StringBuilder();
            try
            {

               
                var myassembly = Assembly.Load("SSRS.Core.Application.Web");
                var allfiles = myassembly.GetManifestResourceNames();
                
                foreach (var filePath in filePathlist)
                {
                    string location = "SSRS.Core.Application.Web" + filePath.Replace('/', '.');
                    string content=Str.Empty;
                    if (allfiles.Contains(location))
                    {
                        var rs = myassembly.GetManifestResourceStream(location);
                        if (rs!=null)
                        {
                            var ms = new MemoryStream();
                            rs?.CopyTo(ms);
                             content = System.Text.Encoding.UTF8.GetString(ms.ToArray());
                            if (Config.GetValue("JsCompressor") == "true")
                            {
                                content = javaScriptCompressor.Compress(content);
                            }
                        }
                       
                        jsStr.Append(content);
                    }

                }
                return jsStr.ToString();
            }
            catch (Exception)
            {
                return "";
            }
        }
        #endregion

        #region Css 文件操作
        /// <summary>
        /// 读取css 文件内容并压缩
        /// </summary>
        /// <param name="filePathlist"></param>
        /// <returns></returns>
        public static string ReadImplantCssFile(string[] filePathlist)
        {
            StringBuilder cssStr = new StringBuilder();
            try
            {

                var myassembly = Assembly.Load("SSRS.Core.Application.Web");
                var allfiles = myassembly.GetManifestResourceNames();

                foreach (var filePath in filePathlist)
                {
                    string location = "SSRS.Core.Application.Web" + filePath.Replace('/', '.');
                    string content = Str.Empty;
                    if (allfiles.Contains(location))
                    {
                        var rs = myassembly.GetManifestResourceStream(location);
                        if (rs != null)
                        {
                            var ms = new MemoryStream();
                            rs?.CopyTo(ms);
                            content = System.Text.Encoding.UTF8.GetString(ms.ToArray());
                            //if (Config.GetValue("CssCompressor") == "true")
                            {
                                content = cssCompressor.Compress(content);
                            }
                        }

                        cssStr.Append(content);
                    }

                }
               
                return cssStr.ToString();
            }
            catch (Exception)
            {
                return cssStr.ToString();
            }
        }
        #endregion
    }
}
