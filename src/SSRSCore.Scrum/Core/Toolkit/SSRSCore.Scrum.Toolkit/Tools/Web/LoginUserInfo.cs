﻿using SSRSCore.Scrum.Mvc.Redis;
using System.Web;


namespace SSRSCore.Scrum.Toolkit
{
    /// <summary>
    /// 版 本  SSRS-ADMS V1.0.0 极限敏捷开发框架
    /// Copyright (c) 2018-2019 上善若水
    /// 创建人：上善若水
    /// 日 期：2017.03.08
    /// 当前上下文执行用户信息获取
    /// </summary>
    public static class LoginUserInfo
    {
        private static ICache redisCache = CacheFactory.CaChe();
        /// <summary>
        /// 获取当前上下文执行用户信息
        /// </summary>
        /// <returns></returns>
        public static UserInfo Get()
        {
            return (UserInfo)HttpContext.Current.Items["LoginUserInfo"];
        }
    }
}
