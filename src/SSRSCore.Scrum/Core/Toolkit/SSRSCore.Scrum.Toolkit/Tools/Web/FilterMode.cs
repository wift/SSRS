﻿namespace SSRSCore.Scrum.Toolkit
{
    /// <summary>
    /// 版 本  SSRS-ADMS V1.0.0 极限敏捷开发框架
    /// Copyright (c) 2018-2019 上善若水
    /// 创建人：上善若水
    /// 日 期：2019年8月5日
    /// mvc过滤模式
    /// </summary>
    public enum FilterMode
    {
        /// <summary>执行</summary>
        Enforce,
        /// <summary>忽略</summary>
        Ignore
    }
}
