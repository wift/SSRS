﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Http;

namespace SSRS.Core
{
    public static class AjaxRequest
    {

        public static bool IsAjaxRequest(this HttpRequest request)
        {
            var requestedWith = request.Headers["X-Requested-With"];
            string method = request.Method;

            if (method.ToUpper() == "POST" || method.ToUpper() == "GET")
            {
                if (requestedWith == "XMLHttpRequest")
                {
                    return true;
                }
            }

            return false;
        }
    }
}
