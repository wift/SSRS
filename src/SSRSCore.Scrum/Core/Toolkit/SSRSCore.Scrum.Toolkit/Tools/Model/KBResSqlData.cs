﻿namespace SSRSCore.Scrum.Toolkit.Model
{
    public class KBResSqlData
    {
        public string id { get; set; }
        public string type { get; set; }

        public string dataType { get; set; }
        public string modelType { get; set; }
        public string dbId { get; set; }
        public string sql { get; set; }
        public string url { get; set; }
        public object data { get; set; }



    }
}
