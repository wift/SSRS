﻿namespace SSRSCore.Scrum.Toolkit
{
    /// <summary>
    /// 版 本  SSRS-ADMS V1.0.0 极限敏捷开发框架
    /// Copyright (c) 2018-2019 上善若水
    /// 创建人：上善若水
    /// 日 期：2017.07.10
    /// 表格属性模型
    /// </summary>
    public class jfGridModel
    {
        public string name { get; set; }
        public string headerName { get; set; }
        public string field { get; set; }
        public string label { get; set; }
        public string width { get; set; }
        public string align { get; set; }
        public string height { get; set; }
        public string hidden { get; set; }
    }
}
