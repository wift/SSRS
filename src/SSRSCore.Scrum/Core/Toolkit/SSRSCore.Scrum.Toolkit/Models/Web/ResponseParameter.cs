﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSRSCore.Scrum.Toolkit
{
    public class ResponseParameter
    {
        /// <summary>
        /// 接口响应码
        /// </summary>
        public ResponseCode Code { get; set; }
        /// <summary>
        /// 接口响应消息
        /// </summary>
        public string Info { get; set; }
        /// <summary>
        /// 接口响应数据
        /// </summary>
        public object Data { get; set; }
    }

    public enum ResponseCode
    {
        /// <summary>
        /// 成功
        /// </summary>
        Success = 200,
        /// <summary>
        /// 失败
        /// </summary>
        Fail = 400,
        /// <summary>
        /// 异常
        /// </summary>
        Exception = 500,
        /// <summary>
        /// 没有登录信息
        /// </summary>
        NoLogin = 410
    }
}
