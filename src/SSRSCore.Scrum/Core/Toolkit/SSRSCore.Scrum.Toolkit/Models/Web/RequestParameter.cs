﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSRSCore.Scrum.Toolkit.Models.Web
{
    /// <summary>
    /// api请求数据结构
    /// </summary>
    /// <typeparam name="T">数据结构</typeparam>
    public class RequestParameter<T> where T : class
    {
        /// <summary>
        /// 接口票据
        /// </summary>
        public string Token { get; set; }
        /// <summary>
        /// 登录设备标识
        /// </summary>
        public string LoginMark { get; set; }
        /// <summary>
        /// 接口数据
        /// </summary>
        public T Data { get; set; }
    }
    /// <summary>
    /// 请求数据结构
    /// </summary>
    public class RequestParameter
    {
        /// <summary>
        /// 接口票据
        /// </summary>
        public string Token { get; set; }
        /// <summary>
        /// 登录设备标识
        /// </summary>
        public string LoginMark { get; set; }
    }
}
