﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSRSCore.Scrum.Toolkit.Models.Web
{
    public enum ResponseCode
    {
        /// <summary>
        /// 成功
        /// </summary>
        Success = 200,
        /// <summary>
        /// 失败
        /// </summary>
        Fail = 400,
        /// <summary>
        /// 异常
        /// </summary>
        Exception = 500,
        /// <summary>
        /// 没有登录信息
        /// </summary>
        NoLogin = 410
    }
}
