﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSRSCore.Scrum.Toolkit.Models.Database
{
    public class FieldValueParam
    {
        /// <summary>
        /// 字段名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 数据值
        /// </summary>
        public object Value { get; set; }
        /// <summary>
        /// 数据类型
        /// </summary>
        public int Type { get; set; }
    }
}
