﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSRSCore.Scrum.Toolkit.Models.Database
{
    /// <summary>
    /// 数据权限的筛查
    /// </summary>
    public class DbAuthDataWhere
    {
        /// <summary>
        /// sql语句
        /// </summary>
        public string Sql { get; set; }
        /// <summary>
        /// 查询参数
        /// </summary>
        public List<FieldValueParam> dbParameters { get; set; }
    }
}
