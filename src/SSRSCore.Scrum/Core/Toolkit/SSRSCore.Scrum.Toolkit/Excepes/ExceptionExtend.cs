﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSRSCore.Scrum.Toolkit.Excepes
{
    [Serializable]
    public class ExceptionExtend : Exception
    {
        /// <summary>
        ///使用异常消息与一个内部异常实例化一个 类的新实例
        /// </summary>
        /// <param name="message">异常消息</param>
        /// <param name="inner">用于封装在DalException内部的异常实例</param>
        public ExceptionExtend(string message, Exception inner)
            : base(message, inner) { }

        /// <summary>
        ///向调用层抛出业务逻辑访问层异常
        /// </summary>
        /// <param name="msg"> 自定义异常消息 </param>
        /// <param name="e"> 实际引发异常的异常实例 </param>
        public static ExceptionExtend ThrowBusinessException(Exception e, string msg = "")
        {
            return new ExceptionExtend("业务逻辑层异常，详情请查看日志信息。", e);
        }
        /// <summary>
        /// 向调用层抛出数据服务访问层异常
        /// </summary>
        /// <param name="e"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static ExceptionExtend ThrowServiceException(Exception e, string msg = "")
        {
            return new ExceptionExtend("数据服务层异常，详情请查看日志信息。", e);
        }
        /// <summary>
        ///向调用层抛出数据访问层异常
        /// </summary>
        /// <param name="msg"> 自定义异常消息 </param>
        /// <param name="e"> 实际引发异常的异常实例 </param>
        public static ExceptionExtend ThrowDataAccessException(Exception e, string msg = "")
        {
            if (!string.IsNullOrEmpty(msg))
            {
                return new ExceptionExtend(msg, e);
            }
            else
            {
                return new ExceptionExtend("数据访问层异常，详情请查看日志信息。", e);
            }
        }
        /// <summary>
        ///     向调用层抛出组件异常
        /// </summary>
        /// <param name="msg"> 自定义异常消息 </param>
        /// <param name="e"> 实际引发异常的异常实例 </param>
        public static ExceptionExtend ThrowComponentException(Exception e, string msg = "")
        {
            return new ExceptionExtend("组件异常，详情请查看日志信息。", e);
        }
    }
}
