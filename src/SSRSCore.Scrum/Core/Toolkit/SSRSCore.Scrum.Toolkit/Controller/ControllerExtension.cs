﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SSRSCore.Scrum.Toolkit;
 


public static partial class SSRSExtensions
{
    /// <summary>
    /// 向前端反馈Json格式的结果集
    /// </summary>
    /// <param name="controller">控制器对象</param>
    /// <param name="data">要返回的数据对象</param>
    /// <returns>前端Json结果集</returns>
    public static ActionResult ToJsonResult(this Controller controller, object data)
    {
        return controller.Content(data.ToJson());
    }

    /// <summary>
    /// 返回成功结果集
    /// </summary>
    /// <param name="controller"></param>
    /// <param name="info"></param>
    /// <returns></returns>
    public static ActionResult Success(this Controller controller, string info)
    {
        return controller.Content(new ResParameter { code = ResponseCode.success, info = info, data = new object { } }.ToJson());
    }
    /// <summary>
    /// 返回字符串到前端
    /// </summary>
    /// <param name="controller"></param>
    /// <param name="data"></param>
    /// <returns></returns>
    public static ActionResult SuccessString(this Controller controller, string data)
    {
        return controller.Content(new ResParameter { code = ResponseCode.success, info = "响应成功", data = data }.ToJson());
 
    }

    public static ActionResult Success(this Controller controller, object data)
    {
        return controller.Content(new ResParameter { code = ResponseCode.success, info = "响应成功", data = data }.ToJson());

    }


    public static ActionResult Success(this Controller controller,string info, object data)
    {
        return controller.Content(new ResParameter { code = ResponseCode.success, info = info, data = data }.ToJson());

    }

    public static ActionResult Fail(this Controller controller, string info)
    {
        return controller.Content(new ResParameter { code = ResponseCode.fail, info = info }.ToJson());
    }


    public static ActionResult Fail(this Controller controller, string info, object data)
    {
        return controller.Content(new ResParameter { code = ResponseCode.fail, info = info }.ToJson());
    }
}
