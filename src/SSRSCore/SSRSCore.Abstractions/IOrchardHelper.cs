using Microsoft.AspNetCore.Http;

namespace SSRSCore
{
    public interface IOrchardHelper
    {
        HttpContext HttpContext { get; }
    }
}
