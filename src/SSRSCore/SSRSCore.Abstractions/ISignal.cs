using Microsoft.Extensions.Primitives;

namespace SSRSCore.Environment.Cache
{
    public interface ISignal
    {
        IChangeToken GetToken(string key);

        void SignalToken(string key);
    }
}
