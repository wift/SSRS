namespace SSRSCore.Localization
{
    public enum CalendarName
    {
        Hebrew,
        Hijri,
        Gregorian,
        Julian,
        Persian,
        UmAlQura,
        Unknown
    }
}