using System.Threading.Tasks;

namespace SSRSCore.Localization
{
    public interface ICalendarSelector
    {
        Task<CalendarSelectorResult> GetCalendarAsync();
    }
}
