using System;
using System.Threading.Tasks;

namespace SSRSCore.Localization
{
    public class CalendarSelectorResult
    {
        public int Priority { get; set; }
        public Func<Task<CalendarName>> CalendarName { get; set; }
    }
}
