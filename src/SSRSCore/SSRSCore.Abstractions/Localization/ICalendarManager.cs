using System.Threading.Tasks;

namespace SSRSCore.Localization
{
    public interface ICalendarManager
    {
        Task<CalendarName> GetCurrentCalendar();
    }
}