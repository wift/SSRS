﻿using SSRSCore.Environment.Extensions.Features;

namespace SSRSCore.Environment.Extensions
{
    public interface IExtensionPriorityStrategy
    {
        int GetPriority(IFeatureInfo feature);
    }
}
