﻿using SSRSCore.Environment.Extensions.Features;

namespace SSRSCore.Environment.Extensions
{
    public interface IExtensionDependencyStrategy
    {
        bool HasDependency(IFeatureInfo observer, IFeatureInfo subject);
    }
}
