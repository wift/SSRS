using Microsoft.Extensions.Configuration;

namespace SSRSCore.Environment.Shell.Configuration
{
    public interface IShellConfiguration : IConfiguration
    {
    }
}
