﻿using SSRSCore.Environment.Shell.Builders.Models;
using System;

namespace SSRSCore.Environment.Shell.Builders
{
    public interface IShellContainerFactory
    {
        IServiceProvider CreateContainer(ShellSettings settings, ShellBlueprint blueprint);
    }
}