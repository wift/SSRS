using Microsoft.AspNetCore.Http;

namespace SSRSCore.Modules.FileProviders
{
    public interface IVirtualPathBaseProvider
    {
        PathString VirtualPathBase { get; }
    }
}
