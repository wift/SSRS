namespace SSRSCore.Modules
{
    /// <summary>
    /// Represents a time zone.
    /// </summary>
    public interface ITimeZone
    {
        string TimeZoneId { get; set; }
    }
}
