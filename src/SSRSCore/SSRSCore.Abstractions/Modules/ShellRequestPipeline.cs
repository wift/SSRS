using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using SSRSCore.Environment.Shell.Builders;

namespace SSRSCore.Modules
{
    public class ShellRequestPipeline : IShellPipeline
    {
        public RequestDelegate Next { get; set; }
        public Task Invoke(object context) => Next(context as HttpContext);
    }
}
