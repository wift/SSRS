﻿using System.Collections.Generic;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.Metadata.Models;

namespace SSRSCore.Indexing
{
    public class BuildPartIndexContext : BuildIndexContext
    {
        public BuildPartIndexContext(
            DocumentIndex documentIndex, 
            ContentItem contentItem,
            IList<string> keys, 
            ContentTypePartDefinition typePartDefinition, 
            ContentIndexSettings settings)
            :base(documentIndex, contentItem, keys)
        {
            ContentTypePartDefinition = typePartDefinition;
            Settings = settings;
        }

        public ContentTypePartDefinition ContentTypePartDefinition { get; }
        public ContentIndexSettings Settings { get; }
    }
}
