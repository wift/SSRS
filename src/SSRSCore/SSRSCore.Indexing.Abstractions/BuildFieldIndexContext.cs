﻿using System.Collections.Generic;
using SSRSCore.ContentManagement;
using SSRSCore.ContentManagement.Metadata.Models;

namespace SSRSCore.Indexing
{
    public class BuildFieldIndexContext : BuildPartIndexContext
    {
        public BuildFieldIndexContext(
            DocumentIndex documentIndex, 
            ContentItem contentItem, 
            IList<string> keys, 
            ContentPart contentPart, 
            ContentTypePartDefinition typePartDefinition, 
            ContentPartFieldDefinition partFieldDefinition, 
            ContentIndexSettings settings)
            :base(documentIndex, contentItem, keys, typePartDefinition, settings)
        {
            ContentPartFieldDefinition = partFieldDefinition;
            ContentPart = contentPart;
        }

        public ContentPart ContentPart { get; }
        public ContentPartFieldDefinition ContentPartFieldDefinition { get; }
    }
}
