﻿namespace SSRSCore.ContentManagement
{
    public interface IContentItemIdGenerator
    {
        string GenerateUniqueId(ContentItem contentItem);
    }
}
