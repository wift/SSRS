﻿using Microsoft.AspNetCore.Html;

namespace SSRSCore.ContentManagement.Models
{
    public class BodyAspect
    {
        public IHtmlContent Body { get; set; }
    }
}
