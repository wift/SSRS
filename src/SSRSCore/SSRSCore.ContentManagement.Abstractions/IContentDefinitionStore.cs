using System.Threading.Tasks;
using SSRSCore.ContentManagement.Metadata.Records;

namespace SSRSCore.ContentManagement
{
    public interface IContentDefinitionStore
    {
        Task<ContentDefinitionRecord> LoadContentDefinitionAsync();

        Task SaveContentDefinitionAsync(ContentDefinitionRecord contentDefinitionRecord);
    }
}
