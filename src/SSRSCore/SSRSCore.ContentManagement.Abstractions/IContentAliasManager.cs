﻿using System.Threading.Tasks;

namespace SSRSCore.ContentManagement
{
    public interface IContentAliasManager
    {
        Task<string> GetContentItemIdAsync(string alias);
    }
}
