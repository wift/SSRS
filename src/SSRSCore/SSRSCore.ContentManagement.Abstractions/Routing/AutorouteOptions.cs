using Microsoft.AspNetCore.Routing;

namespace SSRSCore.ContentManagement.Routing
{
    public class AutorouteOptions
    {
        public RouteValueDictionary GlobalRouteValues { get; set; } = new RouteValueDictionary();
        public string ContentItemIdKey { get; set; } = "";
    }
}
