namespace SSRSCore.ContentManagement.Routing
{
    public struct AutorouteEntry
    {
        public string ContentItemId;
        public string Path;
    }
}
