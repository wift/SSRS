﻿using System.Threading.Tasks;

namespace SSRSCore.ContentManagement
{
    public interface IContentAliasProvider
    {
        int Order { get; }
        Task<string> GetContentItemIdAsync(string alias);
    }
}
