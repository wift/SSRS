namespace SSRSCore.ContentManagement
{
    public interface IContent
    {
        ContentItem ContentItem { get; }
    }
}