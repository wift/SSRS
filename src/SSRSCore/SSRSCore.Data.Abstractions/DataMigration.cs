using YesSql.Sql;

namespace SSRSCore.Data.Migration
{
    public abstract class DataMigration : IDataMigration
    {
        public ISchemaBuilder SchemaBuilder { get; set; }
    }
}
