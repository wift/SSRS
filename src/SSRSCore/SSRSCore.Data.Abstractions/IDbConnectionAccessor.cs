using System.Data.Common;

namespace SSRSCore.Data
{
    public interface IDbConnectionAccessor
    {
        DbConnection CreateConnection();
    }
}
