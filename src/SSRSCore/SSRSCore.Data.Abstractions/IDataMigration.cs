using YesSql.Sql;

namespace SSRSCore.Data.Migration
{
    public interface IDataMigration
    {
        ISchemaBuilder SchemaBuilder { get; set; }
    }
}