using Microsoft.AspNetCore.Mvc.ModelBinding;
using SSRSCore.DisplayManagement.Views;
using SSRSCore.Workflows.Activities;

namespace SSRSCore.Workflows.ViewModels
{
    public class ActivityViewModel<TActivity> : ShapeViewModel where TActivity : IActivity
    {
        public ActivityViewModel()
        {
        }

        public ActivityViewModel(TActivity activity)
        {
            Activity = activity;
        }

        [BindNever]
        public TActivity Activity { get; set; }
    }
}
