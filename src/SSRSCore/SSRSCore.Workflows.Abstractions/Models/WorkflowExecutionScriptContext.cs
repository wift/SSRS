using System.Collections.Generic;
using SSRSCore.Scripting;

namespace SSRSCore.Workflows.Models
{
    public class WorkflowExecutionScriptContext : WorkflowExecutionHandlerContextBase
    {
        public WorkflowExecutionScriptContext(WorkflowExecutionContext workflowContext) : base(workflowContext)
        {
        }

        public IList<IGlobalMethodProvider> ScopedMethodProviders { get; } = new List<IGlobalMethodProvider>();
    }
}
