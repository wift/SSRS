namespace SSRSCore.Workflows.Models
{
    public class WorkflowTypeCreatedContext : WorkflowTypeContext
    {
        public WorkflowTypeCreatedContext(WorkflowType workflowType) : base(workflowType)
        {
        }
    }
}
