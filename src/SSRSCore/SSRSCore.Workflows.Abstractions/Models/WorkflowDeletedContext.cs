namespace SSRSCore.Workflows.Models
{
    public class WorkflowDeletedContext : WorkflowContext
    {
        public WorkflowDeletedContext(Workflow workflow) : base(workflow)
        {
        }
    }
}
