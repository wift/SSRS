namespace SSRSCore.Workflows.Models
{
    public class WorkflowUpdatedContext : WorkflowContext
    {
        public WorkflowUpdatedContext(Workflow workflow) : base(workflow)
        {
        }
    }
}
