namespace SSRSCore.Workflows.Models
{
    public class WorkflowTypeUpdatedContext : WorkflowTypeContext
    {
        public WorkflowTypeUpdatedContext(WorkflowType workflowType) : base(workflowType)
        {
        }
    }
}
