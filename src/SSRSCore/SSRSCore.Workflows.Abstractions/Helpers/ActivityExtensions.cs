using System;
using Microsoft.AspNetCore.Mvc.Localization;
using SSRSCore.Entities;
using SSRSCore.Workflows.Activities;
using SSRSCore.Workflows.Models;

namespace SSRSCore.Workflows.Helpers
{
    public static class ActivityExtensions
    {
        public static bool IsEvent(this IActivity activity)
        {
            return activity is IEvent;
        }

        public static LocalizedHtmlString GetTitleOrDefault(this IActivity activity, Func<LocalizedHtmlString> defaultTitle)
        {
            var title = activity.As<ActivityMetadata>().Title;
            return !string.IsNullOrEmpty(title) ? new LocalizedHtmlString(title, title) : defaultTitle();
        }
    }
}
