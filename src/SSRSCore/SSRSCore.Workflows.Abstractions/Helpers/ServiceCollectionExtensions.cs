using Microsoft.Extensions.DependencyInjection;
using SSRSCore.DisplayManagement.Handlers;
using SSRSCore.Workflows.Activities;
using SSRSCore.Workflows.Options;

namespace SSRSCore.Workflows.Helpers
{
    public static class ServiceCollectionExtensions
    {
        public static void AddActivity<TActivity, TDriver>(this IServiceCollection services) where TActivity : class, IActivity where TDriver : class, IDisplayDriver<IActivity>
        {
            services.Configure<WorkflowOptions>(options => options.RegisterActivity<TActivity, TDriver>());
        }
    }
}
