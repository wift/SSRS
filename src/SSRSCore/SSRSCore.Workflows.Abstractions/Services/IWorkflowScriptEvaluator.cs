using System.Threading.Tasks;
using SSRSCore.Scripting;
using SSRSCore.Workflows.Models;

namespace SSRSCore.Workflows.Services
{
    public interface IWorkflowScriptEvaluator
    {
        Task<T> EvaluateAsync<T>(WorkflowExpression<T> expression, WorkflowExecutionContext workflowContext, params IGlobalMethodProvider[] scopedMethodProviders);
    }
}
