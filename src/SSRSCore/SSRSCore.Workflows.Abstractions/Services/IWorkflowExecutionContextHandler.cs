using System.Threading.Tasks;
using SSRSCore.Workflows.Models;

namespace SSRSCore.Workflows.Services
{
    public interface IWorkflowExecutionContextHandler
    {
        Task EvaluatingExpressionAsync(WorkflowExecutionExpressionContext context);
        Task EvaluatingScriptAsync(WorkflowExecutionScriptContext context);
        Task DehydrateValueAsync(SerializeWorkflowValueContext context);
        Task RehydrateValueAsync(SerializeWorkflowValueContext context);
    }
}
