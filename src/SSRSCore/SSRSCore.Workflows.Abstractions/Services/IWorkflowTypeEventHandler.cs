using System.Threading.Tasks;
using SSRSCore.Workflows.Models;

namespace SSRSCore.Workflows.Services
{
    public interface IWorkflowTypeEventHandler
    {
        Task CreatedAsync(WorkflowTypeCreatedContext context);
        Task UpdatedAsync(WorkflowTypeUpdatedContext context);
        Task DeletedAsync(WorkflowTypeDeletedContext context);
    }
}
