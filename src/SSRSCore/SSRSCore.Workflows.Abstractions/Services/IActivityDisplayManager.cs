using System.Threading.Tasks;
using SSRSCore.DisplayManagement;
using SSRSCore.Workflows.Activities;
using SSRSCore.Workflows.Models;

namespace SSRSCore.Workflows.Services
{
    public interface IActivityDisplayManager : IDisplayManager<IActivity>
    {
    }
}
