using System.Threading.Tasks;
using SSRSCore.Workflows.Models;

namespace SSRSCore.Workflows.Services
{
    public interface IWorkflowHandler
    {
        Task CreatedAsync(WorkflowCreatedContext context);
        Task UpdatedAsync(WorkflowUpdatedContext context);
        Task DeletedAsync(WorkflowDeletedContext context);
    }
}
