using SSRSCore.Workflows.Models;

namespace SSRSCore.Workflows.Services
{
    public interface IActivityIdGenerator
    {
        string GenerateUniqueId(ActivityRecord activityRecord);
    }
}
