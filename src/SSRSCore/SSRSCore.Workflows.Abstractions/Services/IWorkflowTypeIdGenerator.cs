using SSRSCore.Workflows.Models;

namespace SSRSCore.Workflows.Services
{
    public interface IWorkflowTypeIdGenerator
    {
        string GenerateUniqueId(WorkflowType workflowType);
    }
}
