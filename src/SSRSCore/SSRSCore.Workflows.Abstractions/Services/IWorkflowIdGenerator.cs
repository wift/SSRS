using SSRSCore.Workflows.Models;

namespace SSRSCore.Workflows.Services
{
    public interface IWorkflowIdGenerator
    {
        string GenerateUniqueId(Workflow workflow);
    }
}
