using System.Threading.Tasks;
using SSRSCore.Workflows.Models;

namespace SSRSCore.Workflows.Services
{
    public interface IWorkflowExpressionEvaluator
    {
        Task<T> EvaluateAsync<T>(WorkflowExpression<T> expression, WorkflowExecutionContext workflowContext);
    }
}
