﻿using Microsoft.AspNetCore.Mvc.Razor;

namespace SSRSCore.Mvc.LocationExpander
{
    public interface IViewLocationExpanderProvider : IViewLocationExpander
    {
        int Priority { get; }
    }
}
