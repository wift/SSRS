using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.Options;
using SSRSCore.Mvc.LocationExpander;

namespace SSRSCore.Mvc
{
    public class ModularRazorViewEngineOptionsSetup : IConfigureOptions<RazorViewEngineOptions>
    {
        public ModularRazorViewEngineOptionsSetup()
        {
        }

        public void Configure(RazorViewEngineOptions options)
        {
            options.ViewLocationExpanders.Add(new CompositeViewLocationExpanderProvider());
        }
    }
}
