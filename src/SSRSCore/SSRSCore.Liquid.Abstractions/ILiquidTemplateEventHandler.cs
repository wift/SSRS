using System.Threading.Tasks;
using Fluid;

namespace SSRSCore.Liquid
{
    public interface ILiquidTemplateEventHandler
    {
        Task RenderingAsync(TemplateContext context);
    }
}
