using System.Threading.Tasks;
using SSRSCore.Feeds.Models;

namespace SSRSCore.Feeds
{
    public interface IFeedQueryProvider
    {
        Task<FeedQueryMatch> MatchAsync(FeedContext context);
    }

    public class FeedQueryMatch
    {
        public int Priority { get; set; }
        public IFeedQuery FeedQuery { get; set; }
    }
}
