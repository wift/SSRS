using System.Threading.Tasks;
using SSRSCore.Feeds.Models;

namespace SSRSCore.Feeds
{
    public interface IFeedQuery
    {
        Task ExecuteAsync(FeedContext context);
    }
}
