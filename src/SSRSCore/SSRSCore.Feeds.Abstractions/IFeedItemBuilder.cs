using System.Threading.Tasks;
using SSRSCore.Feeds.Models;

namespace SSRSCore.Feeds
{
    public interface IFeedItemBuilder
    {
        Task PopulateAsync(FeedContext context);
    }
}
