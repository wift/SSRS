using System.Threading;
using System.Threading.Tasks;
using OpenIddict.Abstractions;

namespace SSRSCore.OpenId.Abstractions.Stores
{
    public interface IOpenIdScopeStore<TScope> : IOpenIddictScopeStore<TScope> where TScope : class
    {
        Task<TScope> FindByPhysicalIdAsync(string identifier, CancellationToken cancellationToken);
        ValueTask<string> GetPhysicalIdAsync(TScope scope, CancellationToken cancellationToken);
    }
}