namespace SSRSCore.OpenId
{
    public static class OpenIdConstants
    {
        public static class Claims
        {
            public const string EntityType = "oc:entyp";
        }

        public static class EntityTypes
        {
            public const string Application = "application";
            public const string User = "user";
        }

        public static class Features
        {
            public const string Client = "SSRSCore.OpenId.Client";
            public const string Core = "SSRSCore.OpenId";
            public const string Management = "SSRSCore.OpenId.Management";
            public const string Server = "SSRSCore.OpenId.Server";
            public const string Validation = "SSRSCore.OpenId.Validation";
        }

        public static class Prefixes
        {
            public const string Tenant = "oct:";
        }

        public static class Properties
        {
            public const string Roles = "Roles";
        }

        public static class Schemes
        {
            public const string Userinfo = "Userinfo";
        }
    }
}
