using System.Threading.Tasks;
using SSRSCore.Environment.Cache;

namespace SSRSCore.DynamicCache
{
    public interface IDynamicCacheService
    {
        Task<string> GetCachedValueAsync(CacheContext context);
        Task SetCachedValueAsync(CacheContext context, string value);
    }
}