using Microsoft.Extensions.DependencyInjection;
using SSRSCore.Setup.Services;

namespace SSRSCore.Setup
{
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// Adds tenant level services.
        /// </summary>
        public static IServiceCollection AddSetup(this IServiceCollection services)
        {
            services.AddScoped<ISetupService, SetupService>();

            return services;
        }
    }
}