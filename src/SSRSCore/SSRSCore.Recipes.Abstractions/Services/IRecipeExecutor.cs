using System.Threading;
using System.Threading.Tasks;
using SSRSCore.Recipes.Models;

namespace SSRSCore.Recipes.Services
{
    public interface IRecipeExecutor
    {
        Task<string> ExecuteAsync(string executionId, RecipeDescriptor recipeDescriptor, object environment, CancellationToken cancellationToken);
    }
}