using System.Threading.Tasks;
using SSRSCore.Data.Migration;

namespace SSRSCore.Recipes.Services
{
    public interface IRecipeMigrator
    {
        Task<string> ExecuteAsync(string recipeFileName, IDataMigration migration);
    }
}
