using System.Threading.Tasks;
using Microsoft.Extensions.FileProviders;
using SSRSCore.Recipes.Models;

namespace SSRSCore.Recipes.Services
{
    public interface IRecipeReader
    {
        Task<RecipeDescriptor> GetRecipeDescriptor(string recipeBasePath, IFileInfo recipeFileInfo, IFileProvider fileProvider);
    }
}
