using System.Collections.Generic;
using System.Threading.Tasks;
using SSRSCore.Recipes.Models;

namespace SSRSCore.Recipes.Services
{
    public interface IRecipeHarvester
    {
        /// <summary>
        /// Returns a collection of all recipes.
        /// </summary>
        Task<IEnumerable<RecipeDescriptor>> HarvestRecipesAsync();

    }
}