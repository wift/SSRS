using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using SSRSCore.Environment.Shell;
using SSRSCore.Environment.Shell.Configuration;
using SSRSCore.Environment.Shell.Descriptor;
using SSRSCore.Environment.Shell.Descriptor.Models;
using SSRSCore.Environment.Shell.Descriptor.Settings;
using SSRSCore.Modules;

namespace Microsoft.Extensions.DependencyInjection
{
    public static partial class SSRSCoreBuilderExtensions
    {
        /// <summary>
        /// Registers at the host level a set of features which are always enabled for any tenant.
        /// </summary>
        public static SSRSCoreBuilder AddGlobalFeatures(this SSRSCoreBuilder builder, params string[] featureIds)
        {
            foreach (var featureId in featureIds)
            {
                builder.ApplicationServices.AddTransient(sp => new ShellFeature(featureId, alwaysEnabled: true));
            }

            return builder;
        }

        /// <summary>
        /// Registers at the tenant level a set of features which are always enabled.
        /// </summary>
        public static SSRSCoreBuilder AddTenantFeatures(this SSRSCoreBuilder builder, params string[] featureIds)
        {
            builder.ConfigureServices(services =>
            {
                foreach (var featureId in featureIds)
                {
                    services.AddTransient(sp => new ShellFeature(featureId, alwaysEnabled: true));
                }
            });

            return builder;
        }

        /// <summary>
        /// Registers a default tenant with a set of features that are used to setup and configure the actual tenants.
        /// For instance you can use this to add a custom Setup module.
        /// </summary>
        public static SSRSCoreBuilder AddSetupFeatures(this SSRSCoreBuilder builder, params string[] featureIds)
        {
            foreach (var featureId in featureIds)
            {
                builder.ApplicationServices.AddTransient(sp => new ShellFeature(featureId));
            }

            return builder;
        }

        /// <summary>
        /// Registers tenants defined in configuration.
        /// </summary>
        public static SSRSCoreBuilder WithTenants(this SSRSCoreBuilder builder)
        {
            var services = builder.ApplicationServices;

            services.AddSingleton<IShellsSettingsSources, ShellsSettingsSources>();
            services.AddSingleton<IShellsConfigurationSources, ShellsConfigurationSources>();
            services.AddSingleton<IShellConfigurationSources, ShellConfigurationSources>();
            services.AddTransient<IConfigureOptions<ShellOptions>, ShellOptionsSetup>();
            services.AddSingleton<IShellSettingsManager, ShellSettingsManager>();

            return builder.ConfigureServices(s =>
            {
                s.AddScoped<IShellDescriptorManager, ConfiguredFeaturesShellDescriptorManager>();
            });
        }

        /// <summary>
        /// Registers a single tenant with the specified set of features.
        /// </summary>
        public static SSRSCoreBuilder WithFeatures(this SSRSCoreBuilder builder, params string[] featureIds)
        {
            foreach (var featureId in featureIds)
            {
                builder.ApplicationServices.AddTransient(sp => new ShellFeature(featureId));
            }

            builder.ApplicationServices.AddSetFeaturesDescriptor();

            return builder;
        }

        /// <summary>
        /// Registers and configures a background hosted service to manage tenant background tasks.
        /// </summary>
        public static SSRSCoreBuilder AddBackgroundService(this SSRSCoreBuilder builder)
        {
            builder.ApplicationServices.AddSingleton<IHostedService, ModularBackgroundService>();

            return builder;
        }
    }
}
