namespace SSRSCore.Email
{
    public enum SmtpDeliveryMethod
    {
        Network,
        SpecifiedPickupDirectory
    }
}