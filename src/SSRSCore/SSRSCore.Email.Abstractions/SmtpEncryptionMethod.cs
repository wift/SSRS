using System;
using System.Collections.Generic;
using System.Text;

namespace SSRSCore.Email
{
    public enum SmtpEncryptionMethod
    {
        None = 0,
        SSLTLS = 1,
        STARTTLS = 2
    }
}
