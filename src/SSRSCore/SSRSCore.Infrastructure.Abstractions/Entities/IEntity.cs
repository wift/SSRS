using Newtonsoft.Json.Linq;

namespace SSRSCore.Entities
{
    public interface IEntity
    {
        JObject Properties { get; }
    }
}
