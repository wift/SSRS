namespace SSRSCore.Entities
{
    public interface IIdGenerator
    {
        string GenerateUniqueId();
    }
}
