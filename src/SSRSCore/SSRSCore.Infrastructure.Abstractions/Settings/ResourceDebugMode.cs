﻿namespace SSRSCore.Settings
{
    public enum ResourceDebugMode
    {
        FromConfiguration,
        Enabled,
        Disabled
    }
}
