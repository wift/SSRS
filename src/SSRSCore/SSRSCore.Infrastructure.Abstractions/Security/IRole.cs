namespace SSRSCore.Security
{
    public interface IRole
    {
        string RoleName { get; }
    }
}