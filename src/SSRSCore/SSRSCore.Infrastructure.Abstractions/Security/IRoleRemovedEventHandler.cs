using System.Threading.Tasks;

namespace SSRSCore.Security
{
    public interface IRoleRemovedEventHandler
    {
        Task RoleRemovedAsync(string roleName);
    }
}