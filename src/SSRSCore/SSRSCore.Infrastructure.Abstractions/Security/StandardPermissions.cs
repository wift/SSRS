using SSRSCore.Security.Permissions;

namespace SSRSCore.Security
{
    public class StandardPermissions
    {
        public static readonly Permission SiteOwner = new Permission("SiteOwner", "Site Owners Permission", isSecurityCritical: true);
    }
}
