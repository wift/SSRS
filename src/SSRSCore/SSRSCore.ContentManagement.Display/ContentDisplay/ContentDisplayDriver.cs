using SSRSCore.DisplayManagement.Handlers;

namespace SSRSCore.ContentManagement.Display.ContentDisplay
{
    public abstract class ContentDisplayDriver : DisplayDriver<ContentItem, BuildDisplayContext, BuildEditorContext, UpdateEditorContext>, IContentDisplayDriver
    {
        public override bool CanHandleModel(ContentItem model)
        {
            return true;
        }
    }
}
