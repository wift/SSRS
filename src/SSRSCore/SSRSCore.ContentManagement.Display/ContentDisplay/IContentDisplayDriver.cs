﻿using SSRSCore.DisplayManagement.Handlers;

namespace SSRSCore.ContentManagement.Display.ContentDisplay
{
    public interface IContentDisplayDriver : IDisplayDriver<ContentItem, BuildDisplayContext, BuildEditorContext, UpdateEditorContext>
    {
    }
}
