﻿using SSRSCore.ContentManagement.Metadata.Models;
using SSRSCore.DisplayManagement.Handlers;

namespace SSRSCore.ContentManagement.Display.Models
{
    public class UpdateFieldEditorContext : BuildFieldEditorContext
    {
        public UpdateFieldEditorContext(ContentPart contentPart, ContentTypePartDefinition typePartDefinition, ContentPartFieldDefinition partFieldDefinition, UpdateEditorContext context)
            : base(contentPart, typePartDefinition, partFieldDefinition, context)
        {
        }
    }
}
