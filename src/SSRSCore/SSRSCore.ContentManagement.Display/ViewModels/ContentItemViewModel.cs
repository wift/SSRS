using SSRSCore.DisplayManagement.Views;

namespace SSRSCore.ContentManagement.Display.ViewModels
{
    public class ContentItemViewModel : ShapeViewModel
    {
        public ContentItemViewModel()
        {
        }

        public ContentItemViewModel(ContentItem contentItem)
        {
            ContentItem = contentItem;
        }

        public ContentItem ContentItem { get; set; }
    }
}
