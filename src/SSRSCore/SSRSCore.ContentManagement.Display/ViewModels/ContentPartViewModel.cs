using SSRSCore.DisplayManagement.Views;

namespace SSRSCore.ContentManagement.Display.ViewModels
{
    public class ContentPartViewModel : ShapeViewModel
    {
        public ContentPartViewModel()
        {
        }

        public ContentPartViewModel(ContentPart contentPart)
        {
            ContentPart = contentPart;
        }

        public ContentPart ContentPart { get; set; }
    }
}
