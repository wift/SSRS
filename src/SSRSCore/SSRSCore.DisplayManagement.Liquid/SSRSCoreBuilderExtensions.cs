using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.AspNetCore.Mvc.Razor.Compilation;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Options;
using SSRSCore.DisplayManagement.Descriptors.ShapeTemplateStrategy;
using SSRSCore.DisplayManagement.Liquid;
using SSRSCore.DisplayManagement.Liquid.Filters;
using SSRSCore.DisplayManagement.Liquid.Internal;
using SSRSCore.DisplayManagement.Liquid.TagHelpers;
using SSRSCore.DisplayManagement.Razor;
using SSRSCore.Liquid;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class SSRSCoreBuilderExtensions
    {
        /// <summary>
        /// Adds tenant level services for managing liquid view template files.
        /// </summary>
        public static SSRSCoreBuilder AddLiquidViews(this SSRSCoreBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                services.TryAddEnumerable(
                ServiceDescriptor.Transient<IConfigureOptions<LiquidViewOptions>,
                LiquidViewOptionsSetup>());

                services.TryAddEnumerable(
                    ServiceDescriptor.Transient<IConfigureOptions<ShapeTemplateOptions>,
                    LiquidShapeTemplateOptionsSetup>());

                services.TryAddSingleton<ILiquidViewFileProviderAccessor, LiquidViewFileProviderAccessor>();
                services.AddSingleton<IApplicationFeatureProvider<ViewsFeature>, LiquidViewsFeatureProvider>();
                services.AddScoped<IRazorViewExtensionProvider, LiquidViewExtensionProvider>();
                services.AddSingleton<LiquidTagHelperFactory>();

                services.AddScoped<ILiquidTemplateEventHandler, RequestLiquidTemplateEventHandler>();
                services.AddScoped<ILiquidTemplateEventHandler, CultureLiquidTemplateEventHandler>();

                services.AddLiquidFilter<AppendVersionFilter>("append_version");
                services.AddLiquidFilter<ResourceUrlFilter>("resource_url");
            });

            return builder;
        }
    }
}