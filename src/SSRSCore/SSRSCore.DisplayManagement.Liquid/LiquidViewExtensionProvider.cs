using SSRSCore.DisplayManagement.Razor;

namespace SSRSCore.DisplayManagement.Liquid
{
    public class LiquidViewExtensionProvider : IRazorViewExtensionProvider
    {
        public string ViewExtension => LiquidViewTemplate.ViewExtension;
    }
}
