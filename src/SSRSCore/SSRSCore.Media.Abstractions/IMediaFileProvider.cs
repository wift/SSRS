using SSRSCore.Modules.FileProviders;

namespace SSRSCore.Media
{
    public interface IMediaFileProvider : IStaticFileProvider, IVirtualPathBaseProvider
    {
    }
}
