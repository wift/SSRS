using System.IO;

namespace SSRSCore.Media
{
    public interface IMediaFactorySelector
    {
        MediaFactorySelectorResult GetMediaFactory(Stream stream, string fileName, string mimeType, string contentType);
    }
}