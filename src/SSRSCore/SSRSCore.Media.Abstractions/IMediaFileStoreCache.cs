using System.Threading.Tasks;
using SSRSCore.FileStorage;

namespace SSRSCore.Media
{
    /// <summary>
    /// Cache a media file store.
    /// </summary>
    public interface IMediaFileStoreCache : IFileStoreCache
    {
        Task<bool> PurgeAsync();
    }
}
