using System.Threading.Tasks;

namespace SSRSCore.Users.Handlers
{
    public interface IUserCreatedEventHandler
    {
        Task CreatedAsync(CreateUserContext context);
    }
}