﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace SSRSCore.Environment.Commands
{
    public interface ICommandManager
    {
        Task ExecuteAsync(CommandParameters parameters);
        IEnumerable<CommandDescriptor> GetCommandDescriptors();
    }
}