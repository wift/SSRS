﻿using System;

namespace SSRSCore.Environment.Commands
{
    [AttributeUsage(AttributeTargets.Property)]
    public class OrchardSwitchAttribute : Attribute
    {
    }
}