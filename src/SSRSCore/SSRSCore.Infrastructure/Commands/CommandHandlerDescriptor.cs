﻿using System.Collections.Generic;

namespace SSRSCore.Environment.Commands
{
    public class CommandHandlerDescriptor
    {
        public IEnumerable<CommandDescriptor> Commands { get; set; }
    }
}