﻿using System.Threading.Tasks;

namespace SSRSCore.Environment.Commands
{
    public interface ICommandHandler
    {
        Task ExecuteAsync(CommandContext context);
    }
}