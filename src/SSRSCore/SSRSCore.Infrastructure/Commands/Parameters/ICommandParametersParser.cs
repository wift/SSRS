﻿using System.Collections.Generic;

namespace SSRSCore.Environment.Commands.Parameters
{
    public interface ICommandParametersParser
    {
        CommandParameters Parse(IEnumerable<string> args);
    }
}