using SSRSCore.Environment.Commands;
using SSRSCore.Environment.Commands.Builtin;
using SSRSCore.Environment.Commands.Parameters;

namespace Microsoft.Extensions.DependencyInjection
{
    public static partial class SSRSCoreBuilderExtensions
    {
        /// <summary>
        /// Adds host level services to provide CLI commands.
        /// </summary>
        public static SSRSCoreBuilder AddCommands(this SSRSCoreBuilder builder)
        {
            var services = builder.ApplicationServices;

            services.AddScoped<ICommandManager, DefaultCommandManager>();
            services.AddScoped<ICommandHandler, HelpCommand>();

            services.AddScoped<ICommandParametersParser, CommandParametersParser>();
            services.AddScoped<ICommandParser, CommandParser>();

            return builder;
        }
    }
}