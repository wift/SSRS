using Microsoft.Extensions.Options;
using SSRSCore.Environment.Shell;
using SSRSCore.Environment.Shell.Configuration;
using SSRSCore.Environment.Shell.Data.Descriptors;
using SSRSCore.Environment.Shell.Descriptor;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ShellSSRSCoreBuilderExtensions
    {
        /// <summary>
        /// Adds services at the host level to load site settings from the file system
        /// and tenant level services to store states and descriptors in the database.
        /// </summary>
        public static SSRSCoreBuilder AddDataStorage(this SSRSCoreBuilder builder)
        {
            builder.AddSitesFolder()
                .ConfigureServices(services =>
                {
                    services.AddScoped<IShellDescriptorManager, ShellDescriptorManager>();
                    services.AddScoped<IShellStateManager, ShellStateManager>();
                    services.AddScoped<IShellFeaturesManager, ShellFeaturesManager>();
                    services.AddScoped<IShellDescriptorFeaturesManager, ShellDescriptorFeaturesManager>();
                });

            return builder;
        }

        /// <summary>
        /// Host services to load site settings from the file system
        /// </summary>
        public static SSRSCoreBuilder AddSitesFolder(this SSRSCoreBuilder builder)
        {
            var services = builder.ApplicationServices;

            services.AddSingleton<IShellsSettingsSources, ShellsSettingsSources>();
            services.AddSingleton<IShellsConfigurationSources, ShellsConfigurationSources>();
            services.AddSingleton<IShellConfigurationSources, ShellConfigurationSources>();
            services.AddTransient<IConfigureOptions<ShellOptions>, ShellOptionsSetup>();
            services.AddSingleton<IShellSettingsManager, ShellSettingsManager>();

            return builder;
        }
    }
}