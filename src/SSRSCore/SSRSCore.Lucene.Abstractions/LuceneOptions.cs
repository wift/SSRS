using System.Collections.Generic;

namespace SSRSCore.Lucene
{
    public class LuceneOptions
    {
        public IList<ILuceneAnalyzer> Analyzers { get; } = new List<ILuceneAnalyzer>();
    }
}