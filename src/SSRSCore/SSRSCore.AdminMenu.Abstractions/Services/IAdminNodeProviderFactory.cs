using System;
using System.Collections.Generic;
using System.Text;
using SSRSCore.AdminMenu.Models;
using SSRSCore.Navigation;

namespace SSRSCore.AdminMenu.Services
{
    public interface IAdminNodeProviderFactory
    {
        string Name { get; }
        AdminNode Create();
    }

    public class AdminNodeProviderFactory<TAdminNode> : IAdminNodeProviderFactory where TAdminNode : AdminNode, new()
    {
        private static readonly string TypeName = typeof(TAdminNode).Name;

        public string Name => TypeName;

        public AdminNode Create()
        {
            return new TAdminNode();
        }
    }
}
