﻿using System;
using SSRSCore.ContentManagement;
using SSRSCore.XmlRpc;
using SSRSCore.XmlRpc.Models;

namespace SSRSCore.MetaWeblog
{
    public interface IMetaWeblogDriver
    {
        void SetCapabilities(Action<string, string> setCapability);
        void BuildPost(XRpcStruct rpcStruct, XmlRpcContext context, ContentItem contentItem);
        void EditPost(XRpcStruct rpcStruct, ContentItem contentItem);
    }
}
