﻿using System;
using SSRSCore.ContentManagement;
using SSRSCore.XmlRpc;
using SSRSCore.XmlRpc.Models;

namespace SSRSCore.MetaWeblog
{
    public abstract class MetaWeblogDriver : IMetaWeblogDriver
    {
        public virtual void BuildPost(XRpcStruct rpcStruct, XmlRpcContext context, ContentItem contentItem)
        {
        }

        public virtual void EditPost(XRpcStruct rpcStruct, ContentItem contentItem)
        {
        }

        public virtual void SetCapabilities(Action<string, string> setCapability)
        {
        }
    }
}
