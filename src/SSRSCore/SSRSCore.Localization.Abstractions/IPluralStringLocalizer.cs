using Microsoft.Extensions.Localization;

namespace SSRSCore.Localization
{
    public interface IPluralStringLocalizer : IStringLocalizer
    {
        (LocalizedString, object[]) GetTranslation(string name, params object[] arguments);
    }
}
