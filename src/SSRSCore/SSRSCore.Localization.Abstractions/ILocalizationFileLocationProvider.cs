using System.Collections.Generic;
using Microsoft.Extensions.FileProviders;

namespace SSRSCore.Localization
{
    public interface ILocalizationFileLocationProvider
    {
        IEnumerable<IFileInfo> GetLocations(string cultureName);
    }
}
