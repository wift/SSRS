using System.Globalization;

namespace SSRSCore.Localization
{
    public interface ILocalizationManager
    {
        CultureDictionary GetDictionary(CultureInfo culture);
    }
}