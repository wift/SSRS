﻿namespace SSRSCore.DisplayManagement.ModelBinding
{
    public interface IUpdateModelAccessor
    {
        IUpdateModel ModelUpdater { get; set; }
    }
}
