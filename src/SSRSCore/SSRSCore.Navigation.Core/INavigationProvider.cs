using System.Threading.Tasks;

namespace SSRSCore.Navigation

{
    public interface INavigationProvider
    {
        Task BuildNavigationAsync(string name, NavigationBuilder builder);
    }
}
