using System.Collections.Generic;
using System.Threading.Tasks;
using SSRSCore.Recipes.Models;

namespace SSRSCore.Setup.Services
{
    public interface ISetupService
    {
        Task<IEnumerable<RecipeDescriptor>> GetSetupRecipesAsync();
        Task<string> SetupAsync(SetupContext context);
    }
}