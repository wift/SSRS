namespace SSRSCore.ContentLocalization.Models
{
    public class LocalizationEntry
    {
        public string ContentItemId;
        public string LocalizationSet;
        public string Culture;
    }
}
