using System;
using System.Collections.Generic;
using System.Text;

namespace SSRSCore.ContentLocalization
{
    public interface ILocalizable
    {
        string LocalizationSet { get; }
        string Culture { get; }
    }
}
