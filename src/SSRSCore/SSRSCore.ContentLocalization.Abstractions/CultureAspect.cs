using System.Globalization;

namespace SSRSCore.ContentLocalization
{
    /// <summary>
    /// Represents a culture metadata for the content item.
    /// </summary>
    public class CultureAspect
    {
        /// <summary>
        /// Gets or sets the culture.
        /// </summary>
        public CultureInfo Culture { get; set; } = CultureInfo.CurrentUICulture;
    }
}