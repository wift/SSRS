using System.Threading.Tasks;
using SSRSCore.ContentManagement;

namespace SSRSCore.ContentLocalization.Handlers
{
    public interface IContentLocalizationPartHandler
    {
        Task LocalizingAsync(LocalizationContentContext context, ContentPart part);
        Task LocalizedAsync(LocalizationContentContext context, ContentPart part);
    }
}
