using System.Threading.Tasks;

namespace SSRSCore.ContentLocalization.Handlers
{
    public interface IContentLocalizationHandler
    {
        Task LocalizingAsync(LocalizationContentContext context);
        Task LocalizedAsync(LocalizationContentContext context);
    }
}
