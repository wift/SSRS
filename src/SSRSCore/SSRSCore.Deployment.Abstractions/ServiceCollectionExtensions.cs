using Microsoft.Extensions.DependencyInjection;

namespace SSRSCore.Deployment
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddDeploymentTargetHandler<TImplementation>(
            this IServiceCollection serviceCollection)
            where TImplementation : class, IDeploymentTargetHandler
        {
            serviceCollection.AddScoped<IDeploymentTargetHandler, TImplementation>();

            return serviceCollection;
        }
    }
}
