﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace SSRSCore.Deployment
{
    public interface IDeploymentTargetProvider
    {
        Task<IEnumerable<DeploymentTarget>> GetDeploymentTargetsAsync();
    }
}
