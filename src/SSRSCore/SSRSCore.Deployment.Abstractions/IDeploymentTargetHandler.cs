using System.Threading.Tasks;
using Microsoft.Extensions.FileProviders;

namespace SSRSCore.Deployment
{
    public interface IDeploymentTargetHandler
    {
        Task ImportFromFileAsync(IFileProvider fileProvider);
    }
}
