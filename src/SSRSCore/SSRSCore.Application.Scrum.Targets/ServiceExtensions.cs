using System;
using Microsoft.AspNetCore.Builder;
using SSRSCore.ResourceManagement.TagHelpers;
using SSRSCore.Scrum.Toolkit;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceExtensions
    {
        /// <summary>
        /// Adds Orchard CMS services to the application. 
        /// </summary>
        public static IServiceCollection AddSSRSCoreScrum(this IServiceCollection services)
        {
            return AddSSRSCoreScrum(services, null);
        }
      
        /// <summary>
        /// Adds Orchard CMS services to the application and let the app change the
        /// default tenant behavior and set of features through a configure action.
        /// </summary>
        public static IServiceCollection AddSSRSCoreScrum(this IServiceCollection services, Action<SSRSCoreBuilder> configure)
        {
            var builder = services.AddSSRSCore()



                .AddMvc();

            services.AddDistributedMemoryCache();
            services.AddSession();
            services.MyAddHttpContextAccessor();





            // SSRSCoreBuilder is not available in SSRSCore.ResourceManagement as it has to
            // remain independent from SSRSCore.
            builder.ConfigureServices(s =>
            {
                s.AddResourceManagement();

                s.AddTagHelpers<LinkTagHelper>();
                s.AddTagHelpers<MetaTagHelper>();
                s.AddTagHelpers<ResourcesTagHelper>();
                s.AddTagHelpers<ScriptTagHelper>();
                s.AddTagHelpers<StyleTagHelper>();
            });

            configure?.Invoke(builder);

            return services;
        }


        public static IApplicationBuilder AddSSRSCoreScrumApp(this IApplicationBuilder app)
        {
            app.UseSession(new SessionOptions() { });
            app.UseStaticHttpContext();
            app.UseStaticFiles();
            app.UseSSRSCore();
            return app;
        }
    }
}
