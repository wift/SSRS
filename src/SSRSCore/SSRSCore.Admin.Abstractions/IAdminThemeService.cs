﻿using System.Threading.Tasks;
using SSRSCore.Environment.Extensions;

namespace SSRSCore.Admin
{
    public interface IAdminThemeService
    {
        Task<IExtensionInfo> GetAdminThemeAsync();
        Task SetAdminThemeAsync(string themeName);
        Task<string> GetAdminThemeNameAsync();
    }
}
