﻿using SSRSCore.Environment.Commands;

namespace SSRSCore.Hosting
{
    public interface IOrchardParametersParser
    {
        OrchardParameters Parse(CommandParameters parameters);
    }
}