using Microsoft.Extensions.DependencyInjection;
using SSRSCore.Feeds.Rss;

namespace SSRSCore.Feeds
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddFeeds(this IServiceCollection services)
        {
            services.AddSingleton<IFeedBuilderProvider, RssFeedBuilderProvider>();

            return services;
        }
    }
}