﻿using System.Threading.Tasks;
using System.Xml.Linq;

namespace SSRSCore.XmlRpc
{
    public interface IXmlRpcHandler
    {
        void SetCapabilities(XElement element);
        Task ProcessAsync(XmlRpcContext context);
    }
}