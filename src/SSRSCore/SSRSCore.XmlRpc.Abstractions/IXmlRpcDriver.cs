﻿namespace SSRSCore.XmlRpc
{
    public interface IXmlRpcDriver
    {
        void Process(object item);
    }
}