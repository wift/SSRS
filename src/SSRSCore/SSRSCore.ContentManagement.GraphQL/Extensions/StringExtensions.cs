using System;
using GraphQL;
using SSRSCore.ContentManagement.Utilities;

namespace SSRSCore.ContentManagement.GraphQL
{
    public static class StringExtensions
    {
        public static string ToFieldName(this string name)
        {
            return name.TrimEnd("Part").ToCamelCase();
        }
    }
}
