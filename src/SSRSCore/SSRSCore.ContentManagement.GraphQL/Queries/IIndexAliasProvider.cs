using System.Collections.Generic;

namespace SSRSCore.ContentManagement.GraphQL.Queries
{
    public interface IIndexAliasProvider
    {
        IEnumerable<IndexAlias> GetAliases();
    }
}