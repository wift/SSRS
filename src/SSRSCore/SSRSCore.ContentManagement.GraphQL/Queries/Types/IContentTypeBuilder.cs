using System.Threading.Tasks;
using GraphQL.Types;
using SSRSCore.ContentManagement.Metadata.Models;

namespace SSRSCore.ContentManagement.GraphQL.Queries.Types
{
    public interface IContentTypeBuilder
    {
        void Build(FieldType contentQuery, ContentTypeDefinition contentTypeDefinition, ContentItemType contentItemType);
    }
}