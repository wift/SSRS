using GraphQL.Types;
using SSRSCore.ContentManagement.Metadata.Models;

namespace SSRSCore.ContentManagement.GraphQL.Queries.Types
{
    public interface IContentFieldProvider
    {
        FieldType GetField(ContentPartFieldDefinition field);
    }
}