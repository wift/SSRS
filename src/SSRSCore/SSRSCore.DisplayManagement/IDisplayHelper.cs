using System.Threading.Tasks;
using Microsoft.AspNetCore.Html;

namespace SSRSCore.DisplayManagement
{
    public interface IDisplayHelper
    {
        Task<IHtmlContent> ShapeExecuteAsync(object shape);
    }
}