namespace SSRSCore.DisplayManagement.Razor
{
    public interface IRazorViewExtensionProvider
    {
        string ViewExtension { get; }
    }
}
