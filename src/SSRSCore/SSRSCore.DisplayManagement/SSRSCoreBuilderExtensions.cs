using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.AspNetCore.Mvc.Razor.Compilation;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Options;
using SSRSCore.DisplayManagement;
using SSRSCore.DisplayManagement.Descriptors;
using SSRSCore.DisplayManagement.Descriptors.ShapeAttributeStrategy;
using SSRSCore.DisplayManagement.Descriptors.ShapePlacementStrategy;
using SSRSCore.DisplayManagement.Descriptors.ShapeTemplateStrategy;
using SSRSCore.DisplayManagement.Events;
using SSRSCore.DisplayManagement.Extensions;
using SSRSCore.DisplayManagement.Implementation;
using SSRSCore.DisplayManagement.Layout;
using SSRSCore.DisplayManagement.LocationExpander;
using SSRSCore.DisplayManagement.ModelBinding;
using SSRSCore.DisplayManagement.Notify;
using SSRSCore.DisplayManagement.Razor;
using SSRSCore.DisplayManagement.Shapes;
using SSRSCore.DisplayManagement.TagHelpers;
using SSRSCore.DisplayManagement.Theming;
using SSRSCore.DisplayManagement.Title;
using SSRSCore.DisplayManagement.Zones;
using SSRSCore.Environment.Extensions;
using SSRSCore.Environment.Extensions.Features;
using SSRSCore.Mvc.LocationExpander;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class SSRSCoreBuilderExtensions
    {
        /// <summary>
        /// Adds host and tenant level services for managing themes.
        /// </summary>
        public static SSRSCoreBuilder AddTheming(this SSRSCoreBuilder builder)
        {
            builder.AddThemingHost()
                .ConfigureServices(services =>
                {
                    services.Configure<MvcOptions>((options) =>
                    {
                        options.Filters.Add(typeof(ModelBinderAccessorFilter));
                        options.Filters.Add(typeof(NotifyFilter));
                        options.Filters.Add(typeof(RazorViewResultFilter));
                    });

                    // Used as a service when we create a fake 'ActionContext'.
                    services.AddScoped<IAsyncViewResultFilter, RazorViewResultFilter>();

                    services.AddScoped<IUpdateModelAccessor, LocalModelBinderAccessor>();
                    services.AddScoped<ViewContextAccessor>();

                    services.AddScoped<IShapeTemplateViewEngine, RazorShapeTemplateViewEngine>();
                    services.AddSingleton<IApplicationFeatureProvider<ViewsFeature>, ThemingViewsFeatureProvider>();
                    services.AddScoped<IViewLocationExpanderProvider, ThemeViewLocationExpanderProvider>();

                    services.AddScoped<IShapeTemplateHarvester, BasicShapeTemplateHarvester>();
                    services.AddTransient<IShapeTableManager, DefaultShapeTableManager>();

                    services.AddScoped<IShapeTableProvider, ShapeAttributeBindingStrategy>();
                    services.AddScoped<IShapeTableProvider, ShapePlacementParsingStrategy>();
                    services.AddScoped<IShapeTableProvider, ShapeTemplateBindingStrategy>();

                    services.AddScoped<IPlacementNodeFilterProvider, PathPlacementNodeFilterProvider>();

                    services.TryAddEnumerable(
                        ServiceDescriptor.Transient<IConfigureOptions<ShapeTemplateOptions>, ShapeTemplateOptionsSetup>());
                    services.TryAddSingleton<IShapeTemplateFileProviderAccessor, ShapeTemplateFileProviderAccessor>();

                    services.AddShapeAttributes<CoreShapes>();
                    services.AddScoped<IShapeTableProvider, CoreShapesTableProvider>();
                    services.AddShapeAttributes<ZoneShapes>();
                    services.AddScoped<IShapeTableProvider, LayoutShapes>();

                    services.AddScoped<IHtmlDisplay, DefaultHtmlDisplay>();
                    services.AddScoped<ILayoutAccessor, LayoutAccessor>();
                    services.AddScoped<IThemeManager, ThemeManager>();
                    services.AddScoped<IPageTitleBuilder, PageTitleBuilder>();

                    services.AddScoped<IShapeFactory, DefaultShapeFactory>();
                    services.AddScoped<IDisplayHelper, DisplayHelper>();

                    services.AddScoped<INotifier, Notifier>();

                    services.AddShapeAttributes<DateTimeShapes>();
                    services.AddShapeAttributes<PageTitleShapes>();

                    services.AddTagHelpers<AddAlternateTagHelper>();
                    services.AddTagHelpers<AddClassTagHelper>();
                    services.AddTagHelpers<AddWrapperTagHelper>();
                    services.AddTagHelpers<BaseShapeTagHelper>();
                    services.AddTagHelpers<ClearAlternatesTagHelper>();
                    services.AddTagHelpers<ClearClassesTagHelper>();
                    services.AddTagHelpers<ClearWrappersTagHelper>();
                    services.AddTagHelpers<InputIsDisabledTagHelper>();
                    services.AddTagHelpers<RemoveAlternateTagHelper>();
                    services.AddTagHelpers<RemoveClassTagHelper>();
                    services.AddTagHelpers<RemoveWrapperTagHelper>();
                    services.AddTagHelpers<ShapeMetadataTagHelper>();
                    services.AddTagHelpers<ShapeTagHelper>();
                    services.AddTagHelpers<ValidationMessageTagHelper>();
                    services.AddTagHelpers<ZoneTagHelper>();
                });

            return builder;
        }

        /// <summary>
        /// Adds host level services for managing themes.
        /// </summary>
        public static SSRSCoreBuilder AddThemingHost(this SSRSCoreBuilder builder)
        {
            var services = builder.ApplicationServices;

            services.AddSingleton<IExtensionDependencyStrategy, ThemeExtensionDependencyStrategy>();
            services.AddSingleton<IFeatureBuilderEvents, ThemeFeatureBuilderEvents>();

            return builder;
        }
    }
}
