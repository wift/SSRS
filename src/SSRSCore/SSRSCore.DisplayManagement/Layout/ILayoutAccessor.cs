using System.Threading.Tasks;

namespace SSRSCore.DisplayManagement.Layout
{
    public interface ILayoutAccessor
    {
        Task<IShape> GetLayoutAsync();
    }
}
