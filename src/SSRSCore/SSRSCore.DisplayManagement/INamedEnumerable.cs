using System.Collections.Generic;

namespace SSRSCore.DisplayManagement
{
    public interface INamedEnumerable<T> : IEnumerable<T>
    {
        IList<T> Positional { get; }
        IDictionary<string, T> Named { get; }
    }
}