﻿namespace SSRSCore.DisplayManagement
{
    public interface IPositioned
    {
        string Position { get; set; }
    }
}