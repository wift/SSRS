﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Html;
using SSRSCore.DisplayManagement.Implementation;

namespace SSRSCore.DisplayManagement.Descriptors.ShapeTemplateStrategy
{
    public interface IShapeTemplateViewEngine
    {
        IEnumerable<string> TemplateFileExtensions { get; }
        Task<IHtmlContent> RenderAsync(string relativePath, DisplayContext displayContext);
    }
}