using Microsoft.AspNetCore.Mvc.Rendering;

namespace SSRSCore.DisplayManagement
{
    public class ViewContextAccessor
    {
        public ViewContext ViewContext { get; set; }
    }
}
