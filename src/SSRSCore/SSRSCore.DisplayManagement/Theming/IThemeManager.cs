﻿using SSRSCore.Environment.Extensions;
using System.Threading.Tasks;

namespace SSRSCore.DisplayManagement.Theming
{
    public interface IThemeManager
    {
        Task<IExtensionInfo> GetThemeAsync();
    }
}
