using System.Threading.Tasks;
using SSRSCore.DisplayManagement.Handlers;

namespace SSRSCore.DisplayManagement.Views
{
    public interface IDisplayResult
    {
        Task ApplyAsync(BuildDisplayContext context);
        Task ApplyAsync(BuildEditorContext context);
    }
}
