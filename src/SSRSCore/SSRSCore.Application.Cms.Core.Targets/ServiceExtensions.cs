using System;
using SSRSCore.ResourceManagement.TagHelpers;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceExtensions
    {
        /// <summary>
        /// Adds Orchard CMS services to the application. 
        /// </summary>
        public static IServiceCollection AddOrchardCms(this IServiceCollection services)
        {
            return AddOrchardCms(services, null);
        }

        /// <summary>
        /// Adds Orchard CMS services to the application and let the app change the
        /// default tenant behavior and set of features through a configure action.
        /// </summary>
        public static IServiceCollection AddOrchardCms(this IServiceCollection services, Action<SSRSCoreBuilder> configure)
        {
            var builder = services.AddSSRSCore()

                .AddCommands()

                .AddMvc()
                .AddSetupFeatures("SSRSCore.Setup")

                .AddDataAccess()
                .AddDataStorage()
                .AddBackgroundService()

                .AddTheming()
                .AddLiquidViews()
                .AddCaching();

            // SSRSCoreBuilder is not available in SSRSCore.ResourceManagement as it has to
            // remain independent from SSRSCore.
            builder.ConfigureServices(s =>
            {
                s.AddResourceManagement();

                s.AddTagHelpers<LinkTagHelper>();
                s.AddTagHelpers<MetaTagHelper>();
                s.AddTagHelpers<ResourcesTagHelper>();
                s.AddTagHelpers<ScriptTagHelper>();
                s.AddTagHelpers<StyleTagHelper>();
            });

            configure?.Invoke(builder);

            return services;
        }
    }
}
