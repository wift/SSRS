using System.Collections.Generic;

namespace SSRSCore.Apis.GraphQL.Queries
{
    public interface INamedQueryProvider
    {
        IDictionary<string, string> Resolve();
    }
}