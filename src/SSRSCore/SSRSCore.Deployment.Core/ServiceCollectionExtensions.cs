﻿using Microsoft.Extensions.DependencyInjection;
using SSRSCore.Deployment.Core.Services;
using SSRSCore.Deployment.Services;

namespace SSRSCore.Deployment.Core
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddDeploymentServices(this IServiceCollection services)
        {
            services.AddScoped<IDeploymentManager, DeploymentManager>();

            return services;
        }
    }
}
