﻿using SSRSCore.ContentManagement.Metadata.Models;

namespace SSRSCore.ContentTypes.Events
{
    public class ContentPartContext
    {
        public ContentPartDefinition ContentPartDefinition { get; set; }
    }
}