﻿using SSRSCore.ContentManagement.Metadata.Models;

namespace SSRSCore.ContentTypes.Events
{
    public class ContentTypeContext
    {
        public ContentTypeDefinition ContentTypeDefinition { get; set; }
    }
}