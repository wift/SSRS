﻿namespace SSRSCore.ContentTypes.Events
{
    public class ContentTypePartContext
    {
        public string ContentTypeName { get; set; }
        public string ContentPartName { get; set; }
    }
}