﻿using SSRSCore.ContentManagement.Metadata.Models;

namespace SSRSCore.ContentTypes.Events
{
    public class ContentPartFieldContext
    {
        public string ContentPartName { get; set; }
        public string ContentFieldName { get; set; }
    }
}