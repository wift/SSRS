﻿namespace SSRSCore.ContentTypes.Events
{
    public class ContentPartImportingContext : ContentPartContext
    {
        public string ContentPartName { get; set; }
    }
}