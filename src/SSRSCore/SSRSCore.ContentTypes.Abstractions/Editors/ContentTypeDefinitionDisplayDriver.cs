using System;
using SSRSCore.ContentManagement.Metadata.Models;
using SSRSCore.DisplayManagement.Handlers;

namespace SSRSCore.ContentTypes.Editors
{
    public abstract class ContentTypeDefinitionDisplayDriver : DisplayDriver<ContentTypeDefinition, BuildDisplayContext, BuildEditorContext, UpdateTypeEditorContext>, IContentTypeDefinitionDisplayDriver
    {
        public override bool CanHandleModel(ContentTypeDefinition model)
        {
            return true;
        }
    }
}