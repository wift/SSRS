using SSRSCore.ContentManagement.Metadata.Builders;
using SSRSCore.DisplayManagement;
using SSRSCore.DisplayManagement.ModelBinding;

namespace SSRSCore.ContentTypes.Editors
{
    public class UpdateTypeEditorContext : UpdateContentDefinitionEditorContext<ContentTypeDefinitionBuilder>
    {
        public UpdateTypeEditorContext(
                ContentTypeDefinitionBuilder builder,
                IShape model,
                string groupId,
                bool isNew,
                IShapeFactory shapeFactory,
                IShape layout,
                IUpdateModel updater)
            : base(builder, model, groupId, isNew, shapeFactory, layout, updater)
        {
        }
    }
}
