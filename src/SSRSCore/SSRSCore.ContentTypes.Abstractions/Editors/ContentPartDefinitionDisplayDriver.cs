using SSRSCore.ContentManagement.Metadata.Models;
using SSRSCore.DisplayManagement.Handlers;

namespace SSRSCore.ContentTypes.Editors
{
    public abstract class ContentPartDefinitionDisplayDriver : DisplayDriver<ContentPartDefinition, BuildDisplayContext, BuildEditorContext, UpdatePartEditorContext>, IContentPartDefinitionDisplayDriver
    {
        public override bool CanHandleModel(ContentPartDefinition model)
        {
            return true;
        }
    }
}