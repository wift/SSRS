using SSRSCore.ContentManagement.Metadata.Builders;
using SSRSCore.DisplayManagement;
using SSRSCore.DisplayManagement.ModelBinding;

namespace SSRSCore.ContentTypes.Editors
{
    public class UpdatePartFieldEditorContext : UpdateContentDefinitionEditorContext<ContentPartFieldDefinitionBuilder>
    {
        public UpdatePartFieldEditorContext(
                ContentPartFieldDefinitionBuilder builder,
                IShape model,
                string groupId,
                bool isNew,
                IShapeFactory shapeFactory,
                IShape layout,
                IUpdateModel updater)
            : base(builder, model, groupId, isNew, shapeFactory, layout, updater)
        {
        }
    }
}
