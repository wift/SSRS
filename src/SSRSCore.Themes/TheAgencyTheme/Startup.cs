using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using SSRSCore.Modules;
using SSRSCore.ResourceManagement;

namespace SSRSCore.Themes.TheAgencyTheme
{
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IResourceManifestProvider, ResourceManifest>();
        }
    }
}
