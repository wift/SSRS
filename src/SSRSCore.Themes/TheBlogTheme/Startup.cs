using Microsoft.Extensions.DependencyInjection;
using SSRSCore.Modules;
using SSRSCore.ResourceManagement;

namespace SSRSCore.Themes.TheBlogTheme
{
    public class Startup : StartupBase
    {
        public override void ConfigureServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IResourceManifestProvider, ResourceManifest>();
        }
    }
}
