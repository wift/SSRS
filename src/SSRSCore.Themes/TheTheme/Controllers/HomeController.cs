using Microsoft.AspNetCore.Mvc;

namespace SSRSCore.TheTheme.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
